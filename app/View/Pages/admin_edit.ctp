<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/pages/order">Re-order Pages</a></li>
			<li><a href="/admin/pages/add">Add Page</a></li>
			
		</ul>
		<h1>Editing Page: <?php echo $this->request->data['Page']['page_title']; ?></h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<div id="tabs">

			<ul>
				<li class="tab"><a href="#one">Content</a></li>
				<li class="tab"><a href="#two">Meta Data</a></li>
				<li class="tab"><a href="#three">Options</a></li>
				<li class="tab"><a href="#extra">Extra</a></li>
				<li class="tab"><a href="#adverts">Adverts</a></li>
				<li class="tab"><a href="#related">Related Pages</a></li>
				<li class="tab"><a href="#resources">Resources</a></li>
				<li class="tab"><a href="#four">Preview</a></li>
			</ul>

			<?php echo $this->Form->create('Page',array('class'=>'cmxform'));?>
			<?php echo $this->Form->input('id'); ?>

			<div id="one" class="tabArea">
				<fieldset>
					<ol>
					<?php
					echo $this->Form->input('page_title',array('label'=>'Page Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false,'onchange'=>"updateUrlField('PagePageUrl','PagePageTitle')"));
					echo $this->Form->input('page_url',array('label'=>'Page URL<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('parent_id',array('label'=>'Parent Page','type'=>'select','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
					echo $this->Form->input('template',array('label'=>'Choose template','type'=>'select','options'=>$templates,'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
					echo $this->Form->input('h1',array('label'=>'Top level heading<br>(leave blank for parent value)','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					?>
					</ol>
				</fieldset>
				<fieldset>
					<ol id="contentBlocks">
					<?php
					if(!empty($this->data['ContentBlock'])):
						$i = 0;
						foreach($this->data['ContentBlock'] as $contentBlock):
							$buttons = ($i == 0 ? '<img src="/assets/admin/icon_plus.png" class="plusButton addContentBlock">' : '<img src="/assets/admin/icon_minus.png" class="minusButton removeContentBlock" rel="'.$contentBlock['id'].'"> <img src="/assets/admin/icon_plus.png" class="plusButton addContentBlock" rel="'.$i.'">');
							echo $this->Form->input('ContentBlock.'.$i.'.id',array('type'=>'hidden'));
							echo $this->Ckeditor->input('ContentBlock.'.$i.'.content',array('type'=>'textarea','label'=> '','before'=>'<li id="content'.$contentBlock['id'].'">','after'=> $buttons.'</li>','div'=>false));
						$i++; endforeach;
					else:
						echo $this->Ckeditor->input('ContentBlock.0.content',array('type'=>'textarea','label'=> '','before'=>'<li>','after'=>'<img src="/assets/admin/icon_plus.png" class="plusButton addContentBlock"></li>','div'=>false));
						echo $this->Form->input('ContentBlock.0.published',array('type'=>'hidden','value'=>1));
					endif;
						?>
					</ol>
				</fieldset>
				<fieldset>
					<ol>
					<?php
					echo $this->Form->input('show_on_home',array('type'=>'checkbox','label'=> '&nbsp;Show on homepage?','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					echo $this->Form->input('hide_from_menu',array('type'=>'checkbox','label'=> '&nbsp;Hide from menu?','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					echo $this->Form->input('published',array('type'=>'checkbox','label'=> '&nbsp;Publish?','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					?>
					</ol>
				</fieldset>
			</div>

			<div id="two" class="tabArea">
				<fieldset>
					<legend>Meta Data</legend>
					<ol>
						<?php
						//echo $this->Form->input('menu_title',array('label'=>'Menu Title','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('meta_title',array('type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('meta_desc',array('label'=>'Meta Description','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('meta_keywords',array('label'=>'Meta Keywords','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						//echo $this->Form->input('meta_robots',array('type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						//echo $this->Form->input('meta_head',array('label'=>'Meta Head','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
			</div>

			<div id="three" class="tabArea">
				<fieldset>
					<legend>Choose Where the Page Should Appear</legend>
					<ol>
						<?php
						echo $this->Form->input('Page.menu_id',array('label'=>'Select menu','type'=>'select','options'=>$menus,'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Make Direct link</legend>
					<p>If you enter a URL in the box below, this will go directly to this page and not display any content entered previously (internal or external).</p>
					<ol>
						<?php
						echo $this->Form->input('direct_url',array('type'=>'text','label'=> 'Direct Link','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<ol>
						<?php
						echo $this->Form->input('is_homepage',array('type'=>'checkbox','label'=> '&nbsp;Set as homepage?','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
						//echo $this->Form->input('show_subs',array('type'=>'checkbox','label'=> '&nbsp;Display a list of sub-pages?','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Associated News Category (to display posts relevant to current section of website)</legend>
					<ol>
						<?php
						echo $this->Form->input('Page.post_category_id',array('label'=>'Select category (blank defaults to parent page)','type'=>'select','options'=>$postCategories,'class'=>'selectbox','empty'=>true,'before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Main Image (if any)</legend>
					<ol>
						<?php
						echo $this->Ckeditor->browseField('page_image',array('id'=>'PagePageImage','type'=>'text','class'=>'textbox image','label'=> 'Browse for an image','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Ckeditor->browseField('page_image_large',array('id'=>'PagePageImageLarge','type'=>'text','class'=>'textbox image','label'=> 'Browse for a large image','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('page_image_alt',array('label'=>'Brief desc (for search engines)','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
//						echo $this->Form->input('page_image_link',array('type'=>'text','label'=> 'Links to (optional)','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
//						echo $this->Form->input('page_image_link_text',array('type'=>'text','label'=> 'Link text (optional)','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Page Summary</legend>
					<ol>
						<?php
						echo $this->Ckeditor->browseField('summary_image',array('id'=>'PagePageSummaryImage','type'=>'text','class'=>'textbox image','label'=> 'Browse for an image','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Ckeditor->browseField('summary_image_large',array('id'=>'PagePageSummaryImageLarge','type'=>'text','class'=>'textbox image','label'=> 'Browse for a large image','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('short_desc',array('label'=>'Summary Description','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('link_text',array('type'=>'text','label'=> 'Link Text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
						//echo $this->Form->input('no_excerpt',array('type'=>'checkbox','label'=> '&nbsp;No auto excerpt text','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
			</div>

			<div id="extra" class="tabArea">
				<fieldset>
					<legend>Side bar links</legend>
					<p>Use the options below to change the titles which appear above the useful links and resources boxes on the right hand side of a page.</p>
					<ol>
						<?php
						echo $this->Form->input('related_pages_title',array('type'=>'text','label'=> 'Title for useful links','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('resources_title',array('type'=>'text','label'=> 'Title for resources','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Information Sheet</legend>
					<ol>
						<?php
						echo $this->Ckeditor->browseField('attached_file',array('id'=>'PageAttachedFile','type'=>'text','class'=>'textbox','label'=> 'Attach an info sheet','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Extra Images</legend>
					<ol>
						<?php
						echo $this->Ckeditor->browseField('page_image2',array('id'=>'PagePageImage2','type'=>'text','class'=>'textbox image','label'=> 'Browse for an image 2','before'=>'<li>','after'=>'</li>','div'=>false));
	//					echo $this->Ckeditor->browseField('page_image_large2',array('id'=>'PagePageImageLarge2','type'=>'text','class'=>'textbox image','label'=> 'Browse for a large image 2','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('page_image_alt2',array('type'=>'text','class'=>'textbox','label'=> 'Image 2 brief description','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Ckeditor->browseField('page_image3',array('id'=>'PagePageImage3','type'=>'text','class'=>'textbox image','label'=> 'Browse for an image 3','before'=>'<li>','after'=>'</li>','div'=>false));
	//					echo $this->Ckeditor->browseField('page_image_large3',array('id'=>'PagePageImageLarge3','type'=>'text','class'=>'textbox image','label'=> 'Browse for a large image 3','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('page_image_alt3',array('type'=>'text','class'=>'textbox','label'=> 'Image 3 brief description','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Ckeditor->browseField('page_image4',array('id'=>'PagePageImage4','type'=>'text','class'=>'textbox image','label'=> 'Browse for an image 4','before'=>'<li>','after'=>'</li>','div'=>false));
	//					echo $this->Ckeditor->browseField('page_image_large4',array('id'=>'PagePageImageLarge4','type'=>'text','class'=>'textbox image','label'=> 'Browse for a large image 4','before'=>'<li>','after'=>'</li>','div'=>false));
						echo $this->Form->input('page_image_alt4',array('type'=>'text','class'=>'textbox','label'=> 'Image 4 brief description','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
				<fieldset>
					<legend>Video</legend>
					<ol>
						<?php
						echo $this->Form->input('video',array('label'=>'Video Embed Code','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
						?>
					</ol>
				</fieldset>
			</div>

			<div id="adverts" class="tabArea">
				<fieldset>
					<legend>Associated Adverts</legend>
					<ol>
						<?php echo $this->Form->input('Advert.Advert',array('label'=>'','type'=>'select','options'=>$adverts,'multiple'=>'checkbox','class'=>'tickbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false,'showEmpty'=>false)); ?>
					</ol>
				</fieldset>
			</div>

			<div id="related" class="tabArea">
				<fieldset>
					<legend>Related pages</legend>
					<ol>
						<?php
						echo $this->Form->input('RelatedPage.RelatedPage',array('label'=>'Choose pages','type'=>'select','multiple'=>'checkbox','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'class'=>'tickbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
						?>
					</ol>
				</fieldset>
			</div>

			<div id="resources" class="tabArea">
			<fieldset>
				<legend>Resources</legend>
					<ol>
						<?php
						echo $this->Form->input('Resource.Resource',array('label'=>'Choose resource','type'=>'select','multiple'=>'checkbox','options'=>$resources,'class'=>'tickbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
						?>
					</ol>
				</fieldset>
			</div>

			<div id="four" class="tabArea">
				<iframe src="/admin/pages/preview/<?php echo $this->Html->value('id'); ?>" width="100%" height="500" id="previewPanel"></iframe>
			</div>

		</div>
		<?php echo $this->Form->button('save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton')); ?>

		<?php echo $this->Form->button('save & close', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>

		<?php echo $this->Form->end(null);?>
	</div>
</div>