
<section class="section-medium bg-dark alt">
    <div class="container">
        <div class="col12">
            <ul class="breadcrumbs inline">
                <li><?php echo $this->Pages->breadcrumbs($page) ?></li>
            </ul>
            <h2><?php echo $parentLandingPage['Page']['h1']; ?></h2>
        </div>
    </div>
</section>

<section class="section-medium">
    <div class="container">
        <div class="col3">
            <?php echo $this->element('page_sidenav'); ?>
        </div>
        <div class="col9">
            <?php 
			if(isset($page['ContentBlock'])):
				foreach($page['ContentBlock'] as $block): ?>
				<?php echo $this->Pages->content($block['content'],$page); ?>
			<?php 
				endforeach; 
			endif;
			?>
        </div>
    </div>
	<?php echo $this->element('page_footer'); ?>
    
</section>
