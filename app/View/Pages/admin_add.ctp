<div id="page-title">
	<div class="container clearfix">
		
		<h1>New Page</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		    

		<?php echo $this->Form->create('Page',array('class'=>'cmxform'));?>
		
		<fieldset>
			<ol>
			<?php 
			echo $this->Form->input('page_title',array('label'=>'Page Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false,'onchange'=>"updateUrlField('PagePageUrl','PagePageTitle')"));	
			echo $this->Form->input('page_url',array('label'=>'Page URL<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Form->input('parent_id',array('label'=>'Parent Page','type'=>'select','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
			
			?>	
			</ol>
		</fieldset>
		
		<?php echo $this->Form->button('continue', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton')); ?>
		
		<?php //echo $this->Form->button('save & close', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		
		<?php echo $this->Form->end(null);?>
		
	</div>
</div>