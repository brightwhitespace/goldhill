<div class="content group">
	<?php if ($userIsEmployee): ?>
		<div class="col6">
			<a href="/purchase_orders/add/" class="button button-primary"><i class="fa fa-plus-circle"></i> Add PO</a>
			<a href="/purchase_orders/" class="button button-primary"><i class="fa fa-search"></i> View My POs</a>
		</div>
	<?php endif; ?>

	<?php if (!empty($currentUser['Employee']['id'])): ?>
		<div class="col6">
			<a href="/tools/" class="button button-primary"><i class="fa fa-search"></i> View My Tools</a>
		</div>
	<?php endif; ?>
</div>