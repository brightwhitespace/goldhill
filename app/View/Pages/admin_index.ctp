
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/pages/order">Re-order Pages</a></li>
			<li><a href="/admin/pages/add">Add Page</a></li>
			<li><a href="/admin/pages/list">Show Page List</a></li>
		</ul>
		<h1>Pages</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
			<?php
				echo $this->Form->input('menu_id', array('label'=>'Menu ', 'type'=>'select','options'=>$menus, 'div'=>false,'empty'=>'-- Show All --'));
			?>
			<?php echo $this->Form->button('Go', array('name'=>'search','type'=>'submit','value'=>'search','class'=>'filterButton')); ?>
			
		<?php echo $this->Form->end(); ?>
		
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th>Name</th>
				<th>Published</th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
			<?php
			//echo $this->Tree->table($pages,'Page','page_title');
			
			?>
		</table>
		
		<?php echo $this->Tree->expandable($pages,'Page','page_title'); ?>
	</div>
</div>