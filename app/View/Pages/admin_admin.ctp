
<div class="content-group group">
	<div class="col12">
		<div class="content group">
			<?php if ($projectStatusGroupName == 'current'): ?>
				<div class="col4">
					<?php if ($userIsAdmin || $userIsOfficeAdmin): ?>
						<a href="/admin/pages/admin/holding" class="button button-primary">View Holding Projects</a>
					<?php endif; ?>
					&nbsp;
				</div>
				<div class="col8">
					<a href="/admin/clients/add/" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Client</a>
					<a href="/admin/suppliers/add/" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Supplier</a>
					<a href="/admin/projects/add/" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Project</a>
					<a href="/admin/purchase_orders/add/" class="button button-primary"><i class="fa fa-plus-circle"></i> Add PO</a>
				</div>
			<?php else: ?>
				<div class="col4">
					<a href="/admin/pages/admin/current" class="button button-primary">Back To Current projects</a>
				</div>
				<div class="col8">
					<?php if ($userIsAdmin || $userIsOfficeAdmin): ?>
						<a href="/admin/invoices/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Invoice</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="content-group group">
	<div class="col12">
		<div class="content">
			<h3><?php echo ucwords($projectStatusGroupName) ?> Projects</h3>
			<table class="cols-align-top">
				<tr>
					<th>Start date</th>
					<th width="20%">Name</th>
					<th>Description</th>
					<th>Files?</th>
					<th>Tools?</th>
					<th>Client</th>
					<?php if($userIsAdmin): ?>
						<th>Hours</th>
						<th>Value</th>
						<th>Profit</th>
					<?php endif; ?>
					<th style="width: 100px">Status</th>
					<th width="150" class="actions"></th>
				</tr>
				<?php
					$i = 0;
					foreach ($projects as $project):
						$class = null;
						if ($project['Project']['profit'] <= $settings_for_layout['profit_warning']) {
							$class = ' class="alert"';
						} else if ($project['Project']['profit'] <= $settings_for_layout['profit_warning_amber']) {
							$class = ' class="warning"';
						}
						// get most recent invoice for project (although should only ever be one)
						$invoice = !empty($project['Invoice']) ? end($project['Invoice']) : null;
					?>
					<tr<?php echo $class;?>>
						<td><?php echo $this->Time->format('d/m/Y',$project['Project']['start_date']); ?>&nbsp;</td>
						<td><?php echo $this->Html->link($project['Project']['name'], array('controller' => 'projects', 'action' => 'view', $project['Project']['id'])); ?>&nbsp;</td>
						<td><?php echo $project['Project']['description']  . ($project['Project']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($project['Project']['notes']) . '</div>' : ''); ?></td>
						<td><?php $files = glob(APP . "tmp/project/".$project['Project']['id']."/*"); echo !empty($files) ? '<i class="fa fa-check uploaded-files-tick"></i> ' . count($files) : '' ?>&nbsp;</td>
						<td><?php echo !empty($project['Tool']) ? '<i class="fa fa-wrench uploaded-files-tick"></i> ' . count($project['Tool']) : ''; ?>&nbsp;</td>
						<td><?php echo $project['Client']['name']; ?>&nbsp;</td>
						<?php if ($userIsAdmin): ?>
							<td><?php echo $project['Project']['man_hours_allowed'] ? sprintf('<span class="%s">%s</span>', $project['Project']['man_hours_left'] < 0 ? 'alert' : '', $project['Project']['man_hours_left']) : ''; ?>&nbsp;</td>
							<td><?php echo $this->Number->currency($project['Project']['value'],'GBP'); ?>&nbsp;</td>
							<td><?php echo isset($project['Project']['profit']) ? $project['Project']['profit'].'%' : ''; ?></td>
						<?php endif; ?>

						<?php if ($projectStatusGroupName != 'holding'): ?>
							<td><span class="<?php echo $project['ProjectStatus']['name']; ?>"><?php echo $project['ProjectStatus']['name']; ?></span></td>
						<?php else: ?>
							<td><?php echo $this->element('approve_invoice_from_holding', array('invoice'=>$invoice, 'userIsAdmin'=>$userIsAdmin)) ?></td>
						<?php endif; ?>

						<td class="actions">
							<?php if ($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller' => 'projects', 'action' => 'edit', $project['Project']['id']),array('escape'=>false)); ?>
							<?php endif; ?>

							<?php if ($userIsAdmin || $userIsOfficeAdmin): ?>
								<?php if ($project['Project']['project_status_id'] == Project::STATUS_HOLDING): // is holding so can be moved back to "current" ?>
									<?php echo $this->Html->link('<i class="fa fa-undo fa-2x" title="Move back to Current"></i>', array('controller' => 'projects', 'action' => 'holding', $project['Project']['id'], 0), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this project back to Current?')); ?>
								<?php else: // must be pending/live/complete so can be moved to holding - BUT only if project has project items ?>
									<?php echo $this->element('move_to_holding_icon_link', array('project'=>$project)) ?>
								<?php endif; ?>
							<?php endif; ?>

							<?php if ($invoice && ($userIsAdmin|| $userIsOfficeAdmin)): ?>
								<?php echo $this->element('invoice_icon', array('invoice'=>$invoice)) ?>
								<a href="/admin/invoices/generateInvoice/<?php echo $invoice['id']; ?>/invoice_to_print" target="_blank" title="View &amp; print PDF (opens in new tab)" <?php echo $invoice['invoice_printed'] ? 'style="color: #1d1"' : '' ?> onclick="$(this).css('color', '#1d1')">
									<span style="margin-top: -8px" class="fa-stack"><i class="fa fa-file-o fa-stack-2x"></i><i class="fa fa-print fa-lg fa-stack-1x"></i></span>
								</a>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if( !$projects ) : ?>
					<tr>
						<td colspan="8">No projects found</td>
					</tr>
				<?php endif; ?>
			</table>
		</div>
	</div>

	<div class="col12">
		<div class="content">
			<h3><?php echo ucwords($projectStatusGroupName) ?> Day Works</h3>
			<table class="cols-align-top">
				<tr>
					<th>Start date</th>
					<th width="20%">Name</th>
					<th>Description</th>
					<th>Files?</th>
					<th>Tools?</th>
					<th>Client</th>
					<?php if($userIsAdmin): ?>
						<th>Value</th>
						<th>Profit</th>
					<?php endif; ?>
					<th style="width: 100px">Status</th>
					<th width="150" class="actions"></th>
				</tr>
				<?php
					$i = 0;
					foreach ($projectsDayWorks as $project):
						$class = null;
						if ($project['Project']['profit'] <= $settings_for_layout['profit_warning']) {
							$class = ' class="alert"';
						} else if ($project['Project']['profit'] <= $settings_for_layout['profit_warning_amber']) {
							$class = ' class="warning"';
						}
						// get most recent invoice for project (although should only ever be one)
						$invoice = !empty($project['Invoice']) ? end($project['Invoice']) : null;
					?>
					<tr<?php echo $class;?>>
						<td><?php echo $this->Time->format('d/m/Y',$project['Project']['start_date']); ?>&nbsp;</td>
						<td><?php echo $this->Html->link($project['Project']['name'], array('controller' => 'projects', 'action' => 'view', $project['Project']['id'])); ?>&nbsp;</td>
						<td><?php echo $project['Project']['description']  . ($project['Project']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($project['Project']['notes']) . '</div>' : ''); ?></td>
						<td><?php $files = glob(APP . "tmp/project/".$project['Project']['id']."/*"); echo !empty($files) ? '<i class="fa fa-check  uploaded-files-tick"></i> ' . count($files) : '' ?>&nbsp;</td>
						<td><?php echo !empty($project['Tool']) ? '<i class="fa fa-wrench uploaded-files-tick"></i> ' . count($project['Tool']) : ''; ?>&nbsp;</td>
						<td><?php echo $project['Client']['name']; ?>&nbsp;</td>
						<?php if($userIsAdmin): ?>
							<td><?php echo $this->Number->currency($project['Project']['value'],'GBP'); ?>&nbsp;</td>
							<td><?php echo isset($project['Project']['profit']) ? $project['Project']['profit'].'%' : ''; ?></td>
						<?php endif; ?>

						<?php if ($projectStatusGroupName != 'holding'): ?>
							<td><span class="<?php echo $project['ProjectStatus']['name']; ?>"><?php echo $project['ProjectStatus']['name']; ?></span></td>
						<?php else: ?>
							<td><?php echo $this->element('approve_invoice_from_holding', array('invoice'=>$invoice, 'userIsAdmin'=>$userIsAdmin)) ?></td>
						<?php endif; ?>

						<td class="actions">
							<?php if ($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller' => 'projects', 'action' => 'edit', $project['Project']['id']),array('escape'=>false)); ?>
							<?php endif; ?>

							<?php if ($userIsAdmin || $userIsOfficeAdmin): ?>
								<?php if ($project['Project']['project_status_id'] == Project::STATUS_HOLDING): // is holding so can be moved back to "current" ?>
									<?php echo $this->Html->link('<i class="fa fa-undo fa-2x" title="Move back to Current"></i>', array('controller' => 'projects', 'action' => 'holding', $project['Project']['id'], 0), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this project back to Current?')); ?>
								<?php else: // must be pending/live/complete so can be moved to holding - BUT only if project has project items ?>
									<?php echo $this->element('move_to_holding_icon_link', array('project'=>$project)) ?>
								<?php endif; ?>
							<?php endif; ?>

							<?php if ($invoice && ($userIsAdmin|| $userIsOfficeAdmin)): ?>
								<?php echo $this->element('invoice_icon', array('invoice'=>$invoice)) ?>
								<a href="/admin/invoices/generateInvoice/<?php echo $invoice['id']; ?>/invoice_to_print" target="_blank" title="View &amp; print PDF (opens in new tab)" <?php echo $invoice['invoice_printed'] ? 'style="color: #1d1"' : '' ?> onclick="$(this).css('color', '#1d1')">
									<span style="margin-top: -8px" class="fa-stack"><i class="fa fa-file-o fa-stack-2x"></i><i class="fa fa-print fa-lg fa-stack-1x"></i></span>
								</a>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$projectsDayWorks): ?>
					<tr>
						<td colspan="9">No projects found</td>
					</tr>
				<?php endif; ?>
			</table>
		</div>
	</div>

	<?php if ($projectStatusGroupName == 'current'): ?>
		<div class="col12">
			<div id="po-list" class="content">
				<h3>Hire Alerts</h3>
				<?php if (!empty($purchaseOrdersHire)): ?>
					<table>
						<tr>
							<th>PO</th>
							<th>Project</th>
							<th>Item</th>
							<th>Due</th>
							<th>Off Hire?</th>
							<th width="150" class="actions"></th>
						</tr>
						<?php
							$i = 0;
							foreach ($purchaseOrdersHire as $purchase_order):
								$class = null;
								// do not show warning/alert background if PO marked as "Off hire"
								if (!$purchase_order['PurchaseOrder']['off_hire']):
									if(strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW + 3 days')):
										$class = ' class="warning"';
									endif;

									if(strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW')):
										$class = ' class="alert"';
									endif;
								endif;
							?>
							<tr<?php echo $class;?>>

								<td><?php echo $this->Html->link($purchase_order['PurchaseOrder']['ref'], array('controller' => 'purchase_orders', 'action' => 'edit', $purchase_order['PurchaseOrder']['id'])); ?>&nbsp;</td>
								<td><?php echo $purchase_order['Project']['name']; ?>&nbsp;</td>
								<td><?php echo $purchase_order['PurchaseOrder']['desc']; ?>&nbsp;</td>
								<td><?php echo $this->Time->format('d/m/Y',$purchase_order['PurchaseOrder']['end_date']); ?>&nbsp;</td>
								<td><?php echo $purchase_order['PurchaseOrder']['off_hire'] ? '<i class="fa fa-check uploaded-files-tick"></i>' : ''; ?>&nbsp;</td>

								<td class="actions">
									<?php if ($userIsAdmin|| $userIsOfficeAdmin): ?>
										<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'purchase_orders','action' => 'edit', $purchase_order['PurchaseOrder']['id']),array('escape'=>false)); ?>
										<?php echo $this->Html->link('<i class="fa fa-mail-forward fa-2x"></i>', array('controller'=>'purchase_orders','action' => 'set_returned', $purchase_order['PurchaseOrder']['id'], 1),
											array('title'=>'Mark hire item as returned','escape'=>false), __('Are you sure you want to mark this hire item as returned?')); ?>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>

						<?php if( !$purchaseOrdersHire ) : ?>
							<tr>
								<td colspan="6">No records found</td>
							</tr>
						<?php endif; ?>
					</table>
				<?php else: ?>
					<p><em>No alerts to show</em></p>
				<?php endif; ?>
				<a href="/admin/purchase_orders/index/type_id:1/returned:0" class="button button-primary">See All</a>
			</div>
		</div>
	<?php endif; ?>
</div>