
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/pages/order">Re-order Pages</a></li>
			<li><a href="/admin/pages/add">Add Page</a></li>
			
		</ul>
		<h1>Pages</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		
		
		
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th>Name</th>
				<th>Published</th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
			<?php foreach($pages as $page): ?>
			<tr>
				<td><?php echo $this->Html->link($page['Page']['page_title'],array('action'=>'edit',$page['Page']['id'])); ?></td>
				<td><?php echo $page['Page']['published'] == 1 ? 'yes' : 'no'; ?></td>
				<td class="actions"><?php echo $this->Html->link('EDIT',array('action'=>'edit',$page['Page']['id'])); ?><?php echo $this->Html->link('DELETE', array('action'=>'delete', $page['Page']['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?'), $page['Page']['id']),false); ?></th>
			</tr>
			<?php endforeach; ?>
		</table>
		
	</div>
</div>