<div id="page-title">
	<div class="container clearfix">
		
		<h1>Re-order Pages</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<p>Drag and drop the pages to change the order</p>
		<div id="ajax-status">order saved</div>
		
		<?php echo $this->Tree->sortable($pages,'Page','page_title'); ?>
		
		<?php echo $this->Form->button('save order', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton saveOrder','id'=>'saveOrder_pages')); ?>
		
		<?php echo $this->Html->link('Back',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		
	</div>
</div>
