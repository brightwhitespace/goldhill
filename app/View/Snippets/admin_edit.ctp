<div id="page-title">
	<div class="container clearfix">
		
		<h1>Edit Snippet</h1>
	</div>
</div>

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<?php echo $this->Form->create('Snippet',array('class'=>'cmxform','novalidate'=>'novalidate'));?>
			<fieldset>
			<?php
				echo $this->Form->input('id');
				echo $this->Form->input('name',array('label'=>'Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
				echo $this->Ckeditor->input('snippet',array('label'=>'','type'=>'textarea','class'=>'CKEDITOR','before'=>'<li>','after'=>'</li>','div'=>false));
				echo $this->Form->input('page_id',array('label'=>'Page','type'=>'select','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
			?>
			</fieldset>
			<?php echo $this->Form->button('Save changes', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton')); ?>
			
			<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		<?php echo $this->Form->end();?>
	</div>
</div>