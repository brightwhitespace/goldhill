
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/snippets/add">Add Snippet</a></li>
		</ul>
		<h1>Snippets</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
	
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
			<?php
				echo $this->Form->input('term', array('label'=>'Search ', 'type'=>'text', 'div'=>false,'class'=>'textbox'));
			?>
			<?php echo $this->Form->button('Go', array('name'=>'search','type'=>'submit','value'=>'search','class'=>'filterButton')); ?>
		<?php echo $this->Form->end(); ?>
		
		<table>
			<tr>
				<th><?php echo $this->Paginator->sort('id');?></th>
				<th><?php echo $this->Paginator->sort('name');?></th>
				<th><?php echo $this->Paginator->sort('page_id');?></th>
				<th><?php echo $this->Paginator->sort('created');?></th>
				<th><?php echo $this->Paginator->sort('modified');?></th>
				<th width="150" class="actions"></th>
			</tr>
			<?php
				$i = 0;
				foreach ($snippets as $snippet):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
				?>
				<tr<?php echo $class;?>>
					<td><?php echo $snippet['Snippet']['id']; ?>&nbsp;</td>
					<td><?php echo $snippet['Snippet']['name']; ?>&nbsp;</td>
					<td><?php echo $this->Html->link($snippet['Page']['page_title'], array('controller' => 'pages', 'action' => 'edit', $snippet['Page']['id'])); ?>&nbsp;</td>
					<td><?php echo $this->Time->niceShort($snippet['Snippet']['created']); ?>&nbsp;</td>
					<td><?php echo $this->Time->timeAgoInWords($snippet['Snippet']['modified']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link('EDIT', array('action' => 'edit', $snippet['Snippet']['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('DELETE', array('action' => 'delete', $snippet['Snippet']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $snippet['Snippet']['name'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
			
			<?php if( !$snippets ) : ?>
				<tr>
					<td colspan="6">No records found</td>
				</tr>
			<?php endif; ?>
		</table>
		<div class="pagination clearfix">
			<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
			<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
		</div>
	</div>
</div>
	


