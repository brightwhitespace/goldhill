
<htmlpageheader name="defaultheader">
    <div class="header" style="width: 90%; margin: 0 auto; text-align: right">
        <img src="../../app/webroot/assets/images/logo.png" alt=""/>
    </div>
</htmlpageheader>

<htmlpagefooter name="defaultfooter">
    <div class="footer" style="width: 90%; margin: 0 auto">
        <table width="100%" border="0" cellpadding="2" cellspacing="0">
            <tbody>
            <tr>
                <td width="70px"><img style="width: 70px; border: 0" src="../../app/webroot/assets/images/safe_contractor.png" alt=""/></td>
                <td style="text-align: center">
                    <table width="100%" border="0" cellpadding="1" cellspacing="0">
                        <tbody>
                        <tr>
                            <td align="center">
                                <p style="font-size: 14px; font-weight: bold"><?php echo $invoice['Invoice']['footer_main_text'] ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <p style="font-size: 11px; line-height: 14px">
                                    <?php echo nl2br(str_replace('  ', '&nbsp;&nbsp;', h($invoice['Invoice']['footer_sub_text']))) ?>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="78px"><img style="width: 78px; border: 0" src="../../app/webroot/assets/images/bicsc.png" alt=""/></td>
            </tr>
            </tbody>
        </table>
    </div>
</htmlpagefooter>

<sethtmlpageheader name="defaultheader" page="ALL" value="1" show-this-page="1" />

<sethtmlpagefooter name="defaultfooter" page="ALL" value="1" show-this-page="1" />

<div class="content" style="width: 90%; margin: 20px auto 0">
    <table width="100%" border="0" cellpadding="5">
        <tbody>
        <tr>
            <td>
                <p style="font-size: 13px; font-weight: bold">
                    <?php echo $invoice['Client']['name']; ?><br>
                    <?php echo $this->App->formatAddress($invoice['Client']) ?>
                </p>
            </td>
        </tr>
        <tr>
            <td width="20%" valign="top">
                <p style="font-size: 13px; font-weight: bold">
                    <br><?php echo $this->Time->format('jS F Y',$invoice['Invoice']['invoice_date']); ?>
                </p>
            </td>
            <td valign="top" style="text-align: right">
                <?php if (!empty($invoice['Project']['purchase_order_no'])): ?>
                    <p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
                        Purchase Order No: <?php echo $invoice['Project']['purchase_order_no']; ?>
                    </p>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td style="width: 175px" valign="top">
                <p style="font-size: 13px; font-weight: bold">
                    Invoice No: <?php echo $invoice['Invoice']['invoice_num']; ?>
                </p>
            </td>
            <td valign="top">
                <p style="font-size: 13px; font-weight: bold">
                    Ref: <?php echo $invoice['Invoice']['ref']; ?>
                </p>
            </td>
        </tr>
        </tbody>
    </table>

    <table border="0" cellpadding="5" style="width: 90%; margin-top: 20px">
        <thead>
            <tr>
                <th width="180px" style="text-align: left; border-bottom: 1px solid #000;"><p style="font-size: 13px;">Date</p></th>
                <th colspan="2" style="text-align: left; border-bottom: 1px solid #000;"><p style="font-size: 13px;">Description</p></th>
                <th style="text-align: left; border-bottom: 1px solid #000;" align="right"><p style="font-size: 13px;">Cost (ex VAT)</p></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($invoice['InvoiceItem'] as $invoiceItem): ?>
                <tr>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="vertical-align: top"><p style="font-size: 13px;"><?php echo $this->Time->format('jS F Y',$invoiceItem['date']); ?></p></td>
                    <td style="vertical-align: top" colspan="2"><p style="font-size: 13px;"><?php echo $invoiceItem['description']; ?></p></td>
                    <td style="vertical-align: top; text-align: right"><p style="font-size: 13px;"><?php echo $invoiceItem['cost'] ? '&pound;' . $invoiceItem['cost'] : ''; ?></p></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="50%"></td>
                <td width="15%"><p style="font-size: 13px;">NETT</p></td>
                <td width="15%" align="right" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
                    <p style="font-size: 13px; font-weight: bold;">&pound;<?php echo number_format($invoice['Invoice']['total'], 2) ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="4"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="45%"></td>
                <td width="15%"><p style="font-size: 13px;">VAT @ 20%</p></td>
                <td width="15%" align="right">
                    <p style="font-size: 13px; font-weight: bold">&pound;<?php echo number_format($invoice['Invoice']['total'] / 100 * 20, 2) ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="4"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="45%"><p style="font-size: 13px; font-weight: bold;"><?php echo $invoice['Invoice']['gross_status_text'] ?></p></td>
                <td width="15%"><p style="font-size: 13px;">GROSS</p></td>
                <td width="20%" align="right" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
                    <p style="font-size: 13px; font-weight: bold;">&pound;<?php echo number_format($invoice['Invoice']['total'] + ($invoice['Invoice']['total'] / 100 * 20), 2); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="4"></td>
            </tr>
            <?php if (!empty($invoice['Invoice']['banking_details'])): ?>
                <tr>
                    <td colspan="4">
                        <p style="font-size: 12px">
                            <strong>Bank Details</strong><br>
                            <?php echo nl2br($invoice['Invoice']['banking_details']) ?>
                        </p>
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>