<div class="content">
	<div class="group">
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col4">
			<h1>Invoices</h1>
		</div>
		<div class="col2 right">
			<?php echo $this->Form->input('invoice_status_id', array('label'=>false, 'type'=>'select', 'options'=>Invoice::getStatuses(), 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by status...')); ?>
		</div>
		<div class="col3 right">
			&nbsp;
		</div>
		<div class="col3">
			<?php
				echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
				echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if(!empty($this->request->data['Search'])): ?>
			<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
		
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('invoice_num');?></th>
			<th><?php echo $this->Paginator->sort('invoice_date');?></th>
			<th style="width: 16%"><?php echo $this->Paginator->sort('client_id');?></th>
			<th style="width: 16%"><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('ref', 'Reference');?></th>
			<th><?php echo $this->Paginator->sort('total', 'Total (ex VAT)');?></th>
			<?php if ($userIsAdmin): ?>
				<th>Profit</th>
			<?php endif; ?>
			<th><?php echo $this->Paginator->sort('invoice_status_id');?></th>
			<th><?php echo $this->Paginator->sort('email_sent', 'Date Sent');?></th>
			<th style="width: 120px" class="actions"></th>
		</tr>
		<?php foreach ($invoices as $invoice): ?>
			<tr>
				<td><?php echo $this->Html->link($invoice['Invoice']['invoice_num'], array('controller' => 'invoices', 'action' => 'view', $invoice['Invoice']['id'])); ?></td>
				<td><?php echo $invoice['Invoice']['invoice_date']; ?></td>
				<td><?php echo $invoice['Client']['name']; ?></td>
				<td><?php echo $invoice['Project']['name'] ? $this->Html->link($invoice['Project']['name'], array('controller'=>'projects', 'action'=>'view', $invoice['Project']['id'])) : ''; ?></td>

				<td><?php echo $invoice['Invoice']['ref']; ?></td>
				<td><?php echo $this->Number->currency($invoice['Invoice']['total'],'GBP'); ?></td>
				<?php if ($userIsAdmin): ?>
					<td><?php echo !empty($invoice['Project']['id']) ? $invoice['Project']['profit'].'%' : ''; ?></td>
				<?php endif; ?>
				<td style="white-space: nowrap">
					<i class="fa <?php echo $invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_DRAFT ? 'fa-question fa-lg' : ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_APPROVED ? 'fa-check fa-lg' : 'fa-envelope-o') ?>"></i>
					<?php echo $invoice['InvoiceStatus']['name']; ?>
				</td>
				<td><?php echo $invoice['Invoice']['email_sent'] ? date('j M Y', strtotime($invoice['Invoice']['email_sent'])) : ''; ?></td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('action' => 'view', $invoice['Invoice']['id']),array('escape'=>false)); ?>
					<?php if (Invoice::allowedToEdit($invoice, $userIsAdmin, $currentUser['User']['id'])): ?>
						<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $invoice['Invoice']['id']),array('escape'=>false)); ?>
					<?php endif; ?>
					<?php if ($invoice['Client']['duplicate_invoice_allowed']): ?>
						<?php echo $this->Html->link('<i class="fa fa-copy fa-2x"></i>', array('action' => 'add', $invoice['Invoice']['id']),array('escape'=>false), sprintf(__('Are you sure you want to duplicate this invoice?'))); ?>
					<?php endif; ?>
					<?php //echo $this->Html->link('<i class="fa fa-download fa-2x"></i>', array('action' => 'generateInvoice', $invoice['Invoice']['id']),array('escape'=>false)); ?>
					<?php //echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('action' => 'delete', $invoice['Invoice']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $invoice['Invoice']['invoice_num'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if( !$invoices ) : ?>
			<tr>
				<td colspan="9">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>	


