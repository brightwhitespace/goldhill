
<script type="text/javascript" src="/assets/js/jquery-1.10.1.min.js"></script>

<style>
	body {
		margin: 0 0 10px;
		background: #ddd;
		font-family: Helvetica, Arial, sans-serif;
	}
	.button {
		display: inline-block;
		margin: 0 5px 5px;
		padding: 0 10px;
		text-align: center;
		font-size: 13px;
		line-height: 30px;
		font-weight: bold;
		color: #FFF;
		background-color: #b88400;
		text-decoration: none;
	}
	.button-secondary {
		background-color: #b3b3b3;
	}
	.flash {
		max-width: 780px;
		display: block;
		margin: 0 auto 10px;
		padding: 5px 10px;
		text-align: center;
		font-size: 13px;
		font-weight: bold;
		color: #333;
	}
	.flash_success {
		color: #fff;
		background-color: #3b3;
	}
	.flash_failure {
		color: #fff;
		background-color: red;
	}

	@media print {
		.no-print { display: none }
	}
</style>

<?php if (!$invoice['Client']['billing_email']):  // DISPLAY WARNING IF BILLING_EMAIL NOT SET UP FOR CLIENT ?>
	<div class="flash flash_failure">
		THE CLIENT FOR THIS INVOICE DOES NOT HAVE A BILLING EMAIL ADDRESS SET UP
	</div>
<?php endif; ?>

<?php echo $this->Session->flash(); ?>

<div style="margin: 10px 0; text-align: center">
	<?php if ($action == 'preview'): ?>
		<?php echo $this->Html->link('SAVE', array('action'=>'index'), array('class'=>'button button-primary no-print')); ?>
	<?php else: ?>
		<a href="<?php echo $returnUrl ?>" class="button button-primary no-print">BACK</a>
	<?php endif; ?>

	<?php echo (Invoice::allowedToEdit($invoice, $userIsAdmin, $currentUser['User']['id'])) ? $this->Html->link('EDIT',array('action'=>'edit', $invoice['Invoice']['id']), array('class'=>'button button-primary no-print')) : ''; ?>

	<?php echo $this->Html->link('DOWNLOAD PDF',array('action'=>'generateInvoice', $invoice['Invoice']['id'], 'download'), array('class'=>'button button-primary no-print')); ?>

	<?php if ($userIsAdmin && $invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_DRAFT):  // Show APPROVE button if admin user and invoice is a Draft ?>
		<?php echo $this->Html->link('APPROVE',array('action'=>'view', $invoice['Invoice']['id'], $action ?: 'null', Invoice::STATUS_APPROVED), array('class'=>'button button-primary no-print')); ?>
	<?php endif; ?>

	<?php if ($invoice['Client']['billing_email'] && in_array($invoice['Invoice']['invoice_status_id'], array(Invoice::STATUS_APPROVED, Invoice::STATUS_SENT))): ?>
		<a href="#" onclick="return generateAndEmailInvoice(<?php echo $invoice['Invoice']['id'] ?>)" class="button button-primary no-print">
			<?php echo $invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_SENT ? 'RESEND PDF TO CLIENT' : 'EMAIL PDF TO CLIENT' ?>
		</a>
	<?php endif; ?>

	<?php if ($invoice['Invoice']['email_sent']): ?>
		<div class="flash">
			This invoice was sent to the client at <?php echo date('H:i \o\n j F Y ', strtotime($invoice['Invoice']['email_sent'])) ?>
		</div>
	<?php endif; ?>

	<?php if ($invoice['Client']['attach_project_timesheet_with_invoice'] && !empty($invoice['Project']['id']) && $invoice['Project']['project_type_id'] == Project::TYPE_DAYWORKS): ?>
		<div class="flash">
			NOTE: A Project Timesheet Report will also be sent with this invoice - <a target="_blank" href="/admin/reports/project_timesheet/view/<?php echo $invoice['Project']['id'] ?>">Preview in new window</a>
		</div>
	<?php endif; ?>
</div>

<script>
	function generateAndEmailInvoice(invoiceId) {

		<?php
		$clientEmail = $invoice['Client']['billing_email'];
		if ($invoice['Client']['billing_name']) {
			$clientEmail = sprintf("%s \\n<%s>", $invoice['Client']['billing_name'], $clientEmail);
		}

		// TODO Remove these hard-coded testing values
		$clientEmail = $_SERVER['SERVER_PORT'] == '8888' ? "Andy \\n<andrew+goldhill@brightwhitespace.co.uk>" : $clientEmail;
		?>

		var contactNameAndEmail = '<?php echo $clientEmail ?>';
		var includeProjectTimesheet = <?php echo $invoice['Client']['attach_project_timesheet_with_invoice'] && !empty($invoice['Project']['id']) ? 'true' : 'false' ?>;

		if (confirm('Please confirm you wish to email this invoice to:\n' + contactNameAndEmail + '\n\nPLEASE NOTE: This will also Archive the project')) {
			$('body').fadeTo(600, 0.4);
			$('a').on('click', function(e) { e.preventDefault(); }).removeAttr('onclick');

			// generate invoice and then redirect to send invoice
			$.get('/admin/invoices/generateInvoice/' + invoiceId + '/save')
				.done(function () {
					if (includeProjectTimesheet) {
						// generate project timesheet first before sending invoice
						$.get('/admin/reports/project_timesheet/save/<?php echo $invoice['Project']['id'] ?>')
							.done(function () {
								// redirect to send invoice
								$(location).attr('href', '/admin/invoices/sendInvoice/' + invoiceId);
							})
							.fail(function () {
								alert('Sorry - there was a problem generating the project timesheet. Please try again.')
							});
					} else {
						// redirect to send invoice
						$(location).attr('href', '/admin/invoices/sendInvoice/' + invoiceId);
					}
				})
				.fail(function () {
					alert('Sorry - there was a problem generating the invoice. Please try again.')
				});
		}

		return false;
	}
</script>

<table style="width: 98%; max-width: 800px; background: #fff; margin: 0 auto" border="0" cellpadding="10">
	<tbody>
		<tr>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td width="30%" valign="top">
						</td>
						<td valign="top" align="center">
						</td>
						<td width="30%" valign="top" align="right">
							<img src="/assets/images/logo.png" alt=""/>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<table width="100%" border="0" cellpadding="7" cellspacing="0">
					<tbody>
					<tr>
						<td>
							<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
								<?php echo $invoice['Client']['name']; ?><br>
								<?php echo $this->App->formatAddress($invoice['Client']) ?>
							</p>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<table width="100%" border="0" cellpadding="7" cellspacing="0">
					<tbody>
						<tr>
							<td width="20%" valign="top">
								<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
									<?php echo $this->Time->format('jS F Y',$invoice['Invoice']['invoice_date']); ?>
								</p>
							</td>
							<td valign="top" style="text-align: right">
								<?php if (!empty($invoice['Project']['purchase_order_no'])): ?>
									<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
										Purchase Order No: <?php echo $invoice['Project']['purchase_order_no']; ?>
									</p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td width="20%" valign="top">
								<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
									Invoice No: <?php echo $invoice['Invoice']['invoice_num']; ?>
								</p>
							</td>
							<td valign="top">
								<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
									Ref: <?php echo $invoice['Invoice']['ref']; ?>
								</p>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<table width="100%" border="0" cellpadding="7" cellspacing="0">
					<tbody>
					<tr>
						<td width="20%" style="border-bottom: 1px solid #000;"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">Date</p></td>
						<td colspan="2" style="border-bottom: 1px solid #000;"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">Description</p></td>
						<td style="border-bottom: 1px solid #000;" align="right"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">Cost (ex VAT)</p></td>
					</tr>
					<?php foreach($invoice['InvoiceItem'] as $invoiceItem): ?>
						<tr>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td style="vertical-align: top"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $this->Time->format('jS F Y',$invoiceItem['date']); ?></p></td>
							<td colspan="2" style="vertical-align: top"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $invoiceItem['description']; ?></p></td>
							<td style="vertical-align: top; text-align: right"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $invoiceItem['cost'] ? '&pound;' . $invoiceItem['cost'] : ''; ?></p></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr>
						<td width="20%"></td>
						<td width="50%"></td>
						<td width="15%"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">NETT</p></td>
						<td width="15%" align="right" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
							<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold;">&pound;<?php echo number_format($invoice['Invoice']['total'], 2) ?></p>
						</td>
					</tr>
					<tr>
						<td colspan="4"></td>
					</tr>
					<tr>
						<td width="20%"></td>
						<td width="45%"></td>
						<td width="15%"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">VAT @ 20%</p></td>
						<td width="15%" align="right">
							<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">&pound;<?php echo number_format($invoice['Invoice']['total'] / 100 * 20, 2) ?></p>
						</td>
					</tr>
					<tr>
						<td colspan="4"></td>
					</tr>
					<tr>
						<td width="20%"></td>
						<td width="45%"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold;"><?php echo $invoice['Invoice']['gross_status_text'] ?></p></td>
						<td width="15%"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">GROSS</p></td>
						<td width="20%" align="right" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
							<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold;">&pound;<?php echo number_format($invoice['Invoice']['total'] + ($invoice['Invoice']['total'] / 100 * 20), 2); ?></p>
						</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
					<?php if (!empty($invoice['Invoice']['banking_details'])): ?>
						<tr>
							<td colspan="4">
								<p style="font-size: 12px">
									<strong>Bank Details</strong><br>
									<?php echo nl2br(str_replace('  ', '&nbsp;&nbsp;', h($invoice['Invoice']['banking_details']))) ?>
								</p>
							</td>
						</tr>
					<?php endif; ?>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tbody>
					<tr>
						<td width="70px"><img width="100%" src="/assets/images/safe_contractor.png" alt=""/></td>
						<td>
							<table width="100%" border="0" cellpadding="1" cellspacing="0">
								<tbody>
									<tr>
										<td align="center"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold"><?php echo nl2br(str_replace('  ', '&nbsp;&nbsp;', h($invoice['Invoice']['footer_main_text']))) ?></p></td>
									</tr>
									<tr>
										<td align="center">
											<p style="font-size: 11px; line-height: 14px">
												<?php echo nl2br(str_replace('  ', '&nbsp;&nbsp;', h($invoice['Invoice']['footer_sub_text']))) ?>
											</p>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td width="78px"><img width="100%" src="/assets/images/bicsc.png" alt=""/></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>