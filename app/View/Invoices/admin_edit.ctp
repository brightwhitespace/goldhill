<div class="content">
	<h2>Edit Invoice</h2>

	<?php echo $this->Form->create('Invoice',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('project_id',array('label'=>'Project','type'=>'select','options'=>$projects,'empty'=>'Please select','class'=>'update-client-on-project-select'));
			echo $this->Form->input('client_id',array('label'=>'Client<em>*</em>','type'=>'select','options'=>$clients,'empty'=>'Please select','class'=>'project-client-to-update'));
			echo $this->Form->input('invoice_date',array('label'=>'Invoice Date<em>*</em>','type'=>'text','class'=>'datepicker'));
			echo $this->Form->input('ref',array('label'=>'Ref<em>*</em>','type'=>'text'));
			?>
		</div>
		<div class="col6">
			<?php
			echo $this->Form->input('gross_status_text',array('label'=>'"Gross Status" text','type'=>'text'));
			echo $this->Form->input('footer_main_text',array('label'=>'Main footer text','type'=>'textarea','style'=>'font-weight: bold'));
			echo $this->Form->input('footer_sub_text',array('label'=>'Sub footer text','type'=>'textarea','style'=>'height: 80px; font-size: 13px'));
			echo $this->Form->input('banking_details',array('label'=>'Bank Details','type'=>'textarea','style'=>'height: 80px; font-size: 13px'));
			?>
		</div>
	</div>

	<div class="group">
		<div class="col12">
			<h3 style="margin: 20px 0">Invoice Items</h3>
		</div>
	</div>

	<div class="group">
		<div class="col2">Date</div>
		<div class="col6">Description<em>*</em></div>
		<div class="col2">Cost (ex VAT)</div>
		<div class="col2"></div>
	</div>

	<div class="group">
		<div class="col12"><hr style="margin: 10px 0 20px"></div>
	</div>

	<div id="invoice-items-container">
		<?php $itemIdx = 0; ?>
		<?php foreach ($this->request->data['InvoiceItem'] as $invoiceItem): ?>

			<div class="invoice-item group">
				<div class="col2">
					<?php echo $this->Form->input("InvoiceItem.$itemIdx.date", array('label'=>false,'type'=>'text','class'=>'datepicker','style'=>'width: 100% !important')); ?>
				</div>
				<div class="col6">
					<?php echo $this->Form->input("InvoiceItem.$itemIdx.description", array('label'=>false,'type'=>'text')); ?>
				</div>
				<div class="col2">
					<?php echo $this->Form->input("InvoiceItem.$itemIdx.cost", array('label'=>false,'type'=>'text')); ?>
				</div>
				<div class="col2">
					<a href="#" class="button button-secondary" onclick="return deleteInvoiceItem(this)">Remove</a>
				</div>
			</div>

			<?php $itemIdx++; ?>
		<?php endforeach; ?>
	</div>

	<div class="group">
		<div class="col12">
			<p style="margin: 5px 0 40px">
				<a href="#" class="button button-primary" onclick="return addInvoiceItem()">Add new item</a>
			</p>
		</div>
	</div>

	<div class="group">
		<div class="col6">
			<?php echo $this->Html->link('Cancel', $returnUrl, array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Preview', array('name'=>'preview','type'=>'submit','value'=>'preview','class'=>'button button-primary')); ?>
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>

</div>

<script>
	var projectIdToClientIdMappings = <?php echo $projectIdToClientIdMappings ?>;
</script>

<?php // HTML template for adding invoice items ?>
<script id="invoice-item-template" type="text/html">
	<div class="invoice-item group">
		<div class="col2">
			<?php echo $this->Form->input("InvoiceItem.INDEX.date", array('label'=>false,'type'=>'text','class'=>'datepicker','style'=>'width: 100% !important')); ?>
		</div>
		<div class="col6">
			<?php echo $this->Form->input("InvoiceItem.INDEX.description", array('label'=>false,'type'=>'text')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input("InvoiceItem.INDEX.cost", array('label'=>false,'type'=>'text')); ?>
		</div>
		<div class="col2">
			<a href="#" class="button button-secondary" onclick="return deleteInvoiceItem(this)">Remove</a>
		</div>
	</div>
</script>
