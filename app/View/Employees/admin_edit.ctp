
<div class="content">
	<h2>Edit Sub-contractor</h2>

	<?php echo $this->Form->create('Employee',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col4">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('name',array('label'=>'Name<em>*</em>','type'=>'text'));
			echo $this->Form->input('rate_ph',array('label'=>'Rate per hour<em>*</em>','type'=>'number','class'=>'currency'));
			echo $this->Form->input('employee_number',array('label'=>'Sub-contractor No.','type'=>'text'));
			echo $this->Form->input('tel',array('label'=>'Telephone','type'=>'text'));
			echo $this->Form->input('email',array('label'=>'Email','type'=>'text'));
			echo $this->Form->input('address1',array('label'=>'Address 1','type'=>'text'));
			echo $this->Form->input('address2',array('label'=>'Address 2','type'=>'text'));
			echo $this->Form->input('address3',array('label'=>'Address 3','type'=>'text'));
			echo $this->Form->input('city',array('label'=>'City','type'=>'text'));
			echo $this->Form->input('postcode',array('label'=>'Postcode','type'=>'text'));
			?>
		</div>
		<div class="col5">
			<?php
			echo $this->Form->input('emergency_contact_name',array('label'=>'Next of Kin / Emergency Contact Name','type'=>'text'));
			echo $this->Form->input('emergency_contact_number',array('label'=>'Emergency Contact Telephone','type'=>'text'));
			echo $this->Form->input('utr_no',array('label'=>'UTR No.','type'=>'text'));
			echo $this->Form->input('ni_no',array('label'=>'NI No.','type'=>'text'));
			echo $this->Form->input('last_working_day_name',array('label'=>'Last Working Day of Week (for hours worked report)','type'=>'select','options'=>$daysList,'empty'=>'Do not send report'));
			echo $this->Form->input('employee_payment_method_id',array('label'=>'Payment Method (for wages report)','type'=>'select','options'=>$paymentMethodList));
			echo $this->Form->input('notes',array('label'=>'Notes','type'=>'textarea','style'=>'height: 269px'));
			?>
		</div>
		<div class="col3">
			<?php
			echo $this->Form->input('current_damages_to_repay',array('label'=>'Current damages to repay','type'=>'number','class'=>'currency'));
			echo $this->Form->input('current_repayment',array('label'=>'Weekly repayment','type'=>'number','class'=>'currency'));
			?>
		</div>
	</div>
	<div class="group">
		<div class="col6">
			<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>

</div>
