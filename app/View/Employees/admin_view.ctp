<div class="content">
	<div class="content-head group">
		<div class="col4">
			<h1>
				<?php echo $employee['Employee']['name']; ?>
				<?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $employee['Employee']['id']),array('escape'=>false)); ?>
				<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $employee['Employee']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $employee['Employee']['name'])); ?>
			</h1>
		</div>
		<div class="col8 right">
			<a href="#" onclick="return openAddCertificatePopup(this)" class="button button-primary" data-cert-id="" data-employee-id="<?php echo $employee['Employee']['id']; ?>" data-start-date="" data-end-date="">
				<i class="fa fa-plus-circle"></i> Add Certification
			</a>
			<a href="/admin/wage_adjustments/add/<?php echo $employee['Employee']['id']; ?>" class="button button-primary" data-cert-id="" data-employee-id="<?php echo $employee['Employee']['id']; ?>" data-start-date="" data-end-date="">
				<i class="fa fa-plus-circle"></i> Add Wage Adjustment
			</a>
		</div>
	</div>

	<div class="group">
		<div class="col3">
			<h3>Contact Details</h3>
			<div class="box">
				<p>
					<?php if(!empty($employee['Employee']['email'])): ?>
						<a href="mailto:<?php echo $employee['Employee']['email']; ?>"><?php echo $employee['Employee']['email']; ?></a><br>
					<?php endif; ?>
					<?php echo $employee['Employee']['tel']; ?>
				</p>
				<p>
					<?php echo $this->App->formatAddress($employee['Employee']) ?>
				</p>
				<?php if ($employee['Employee']['emergency_contact_name'] || $employee['Employee']['emergency_contact_number']): ?>
					<p>
						<b>Emergency Contact</b><br>
						<?php echo $employee['Employee']['emergency_contact_name'] ?><br>
						<?php echo $employee['Employee']['emergency_contact_number'] ?>
					</p>
				<?php endif; ?>
				<?php if ($employee['Employee']['utr_no'] || $employee['Employee']['ni_no']): ?>
					<p>
						<?php echo $employee['Employee']['utr_no'] ? '<b>UTR No: </b>' . $employee['Employee']['utr_no'] . '<br>' : '' ?>
						<?php echo $employee['Employee']['ni_no'] ? '<b>NI No: &nbsp;</b>' . $employee['Employee']['ni_no'] : '' ?>
					</p>
				<?php endif; ?>
				<p>
					<b>Last Day for Hours Worked Report</b><br>
					<?php echo $employee['Employee']['last_working_day_name'] ?: 'Do not send report' ?>
				</p>
				<?php if ($employee['Employee']['current_damages_to_repay'] > 0): ?>
					<p>
						<b>Damage to repay</b> &nbsp;<i>(Repayment per week)</i><br>
						&pound;<?php echo $employee['Employee']['current_damages_to_repay'] ?>
						&nbsp;<i>(&pound;<?php echo $employee['Employee']['current_repayment'] ?>)</i>
					</p>
				<?php endif; ?>
			</div>

			<?php if ($employee['Employee']['notes']): ?>
				<h3>Notes</h3>
				<div class="box">
					<p style="margin-bottom: 0">
						<?php echo nl2br($employee['Employee']['notes']); ?>
					</p>
				</div>
			<?php endif; ?>

			<h3>File Store</h3>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'employee', 'files'=>$files, 'objectId'=>$employee['Employee']['id'], 'allowDelete'=>$userIsAdmin)); ?>
			</div>
		</div>

		<div class="col9">

			<h3>Assigned Tools</h3>
			<table>
				<tr>
					<th>Serial No.</th>
					<th>Name</th>
					<th>Description</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php foreach ($employee['Tool'] as $tool): ?>
					<tr>
						<td><?php echo $tool['serial_number']; ?>&nbsp;</td>
						<td><?php echo $tool['name']; ?>&nbsp;</td>
						<td><?php echo $tool['description']; ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('controller'=>'tools', 'action' => 'view', $tool['id']), array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'tools', 'action' => 'edit', $tool['id']), array('escape'=>false)); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$employee['Tool']): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>

			<h3><br>Certifications</h3>
			<table>
				<tr>
					<th>Certificate</th>
					<th style="width: 110px">Start Date</th>
					<th style="width: 110px">End Date</th>
					<th style="width: 150px">Last updated</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php
				$certCount = 0;
				foreach ($employee['CertificatesEmployeeLatestUnique'] as $idx => $employeeCertRecord):

					$employeeId = $employee['Employee']['id'];
					$certId = $employeeCertRecord['certificate_id'];

					if (!$employeeCertRecord['ignore_previous_records']):
						$certCount++;
					?>
						<tr>
							<td><?php echo $employeeCertRecord['Certificate']['name'] ?></td>
							<td><?php echo $employeeCertRecord['start_date'] ? date('jS M Y', strtotime($employeeCertRecord['start_date'])) : '' ?></td>
							<td class="<?php echo $this->App->getCertExpiryClass($employeeCertRecord, $employeeCertRecord['Certificate']) ?>">
								<?php echo $employeeCertRecord['end_date'] ? date('jS M Y', strtotime($employeeCertRecord['end_date'])) : '<i class="fa fa-check" style="font-size: 18px; color: #b88400"></i>' ?>
							</td>
							<td><?php echo date('j-n-Y H:i', strtotime($employeeCertRecord['modified'])) . ' [' . $this->App->getInitials($this->App->getFullName($employeeCertRecord['User'])) . ']'; ?>&nbsp;</td>

							<td class="actions">
								<a href="#" onclick="return openAddCertificatePopup(this)" data-cert-id="<?php echo $certId ?>" data-employee-id="<?php echo $employeeId ?>"
								   data-start-date="<?php echo $employeeCertRecord['start_date'] ? date('j-n-Y', strtotime($employeeCertRecord['start_date'])) : '' ?>"
								   data-end-date="<?php echo $employeeCertRecord['end_date'] ? date('j-n-Y', strtotime($employeeCertRecord['end_date'])) : '' ?>">
									<i class="fa fa-edit fa-2x"></i>
								</a>
								<?php if($userIsAdmin): ?>
									<a href="#" onclick="return removeEmployeeCertificate(this, true)" data-cert-id="<?php echo $certId ?>" data-employee-id="<?php echo $employeeId ?>">
										<i class="fa fa-trash fa-2x"></i>
									</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>

				<?php if ($certCount == 0): ?>
					<tr>
						<td colspan="5">No certificates found</td>
					</tr>
				<?php endif; ?>
			</table>

			<h3><br>Wage Adjustments</h3>
			<table>
				<tr>
					<th>Date</th>
					<th>Description</th>
					<th>Value</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php foreach ($employee['WageAdjustment'] as $wageAdjustment): ?>
					<tr>
						<td><?php echo $wageAdjustment['date']; ?>&nbsp;</td>
						<td><?php echo $wageAdjustment['description']; ?>&nbsp;</td>
						<td style="<?php echo $wageAdjustment['value'] < 0 ? 'color: red' : '' ?>"><?php echo $this->Number->currency($wageAdjustment['value'], 'GBP'); ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'wage_adjustments', 'action' => 'edit', $wageAdjustment['id']),array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'wage_adjustments', 'action' => 'delete', $wageAdjustment['id']), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to delete this record?')); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$employee['WageAdjustment']): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>
		</div>
		
	</div>
	
</div>


<div id="add-cert-popup" class="overlay-block">
	<div class="overlay-content overlay-content-small vertical-centre">
		<a href="#" class="button button-primary small overlay-close"><i class="fa fa-close"></i></a>
		<h2>Add / Update Certificate</h2>
		<?php echo $this->Form->input('add_cert_employee_id', array('label'=>'Employee<em>*</em>','type'=>'select','options'=>$employeeList,'empty'=>'Select employee...','required'=>true,'disabled'=>true)) ?>
		<?php echo $this->Form->input('add_cert_cert_id', array('label'=>'Certificate<em>*</em>','type'=>'select','options'=>$certificateList,'empty'=>'Select certificate...','required'=>true)) ?>
		<?php echo $this->Form->input('add_cert_start_date', array('label'=>'Start Date','type'=>'text','class'=>'datepicker')) ?>
		<?php echo $this->Form->input('add_cert_end_date', array('label'=>'End Date','type'=>'text','class'=>'datepicker')) ?>
		<p>&nbsp;</p>
		<div class="group">
			<a href="#" class="button button-secondary overlay-close" style="float: none">CANCEL</a>
			<a href="#" onclick="addEmployeeCertificate(<?php echo empty($reloadPageAfterCertUpdate) ? 'false' : 'true' ?>)" class="button button-primary floatright">ADD / UPDATE</a>
		</div>
	</div>
</div>
