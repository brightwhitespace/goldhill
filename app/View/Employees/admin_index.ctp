<div class="content">
	<div class="group">
		<div class="col6">
			<h1><?php echo empty($archive) ? 'Sub-contractors' : 'Sub-contractor Archive' ?></h1>
		</div>
		<div class="col3 right">
			<a href="/admin/employees/add" class="button button-primary">Add Sub-contractor</a>
		</div>
		<div class="col3">
			<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
				<?php
					echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
					echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
				?>
				<?php if(!empty($this->request->data['Search'])): ?>
					<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
				<?php endif; ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<table>
		<style>
			td { vertical-align: top }
		</style>
		<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('employee_number', 'Id #');?></th>
			<th><?php echo $this->Paginator->sort('rate_ph','Rate (£)');?></th>
			<th><?php echo $this->Paginator->sort('tel', 'Telephone');?></th>
			<th><?php echo $this->Paginator->sort('email', 'Email');?></th>
			<th style="width: 30%"><?php echo $this->Paginator->sort('address1', 'Address & Notes');?></th>
			<th><?php echo $this->Paginator->sort('emergency_contact_name', 'Emergency Contact');?></th>
			<th>Tools?</th>
			<th style="width: 70px" class="actions"></th>
		</tr>
		<?php
			$i = 0;
			foreach ($employees as $employee):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($employee['Employee']['name'], array('action' => 'view', $employee['Employee']['id'])); ?>&nbsp;</td>
				<td><?php echo $employee['Employee']['employee_number']; ?>&nbsp;</td>
				<td><?php echo $employee['Employee']['rate_ph']; ?>&nbsp;</td>
				<td><?php echo $employee['Employee']['tel']; ?>&nbsp;</td>
				<td><?php echo $employee['Employee']['email']; ?>&nbsp;</td>
				<td><?php echo $this->App->formatAddress($employee['Employee'], ', ') . ($employee['Employee']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($employee['Employee']['notes']) . '</div>' : '') ?>&nbsp;</td>
				<td><?php echo $employee['Employee']['emergency_contact_name'] . ($employee['Employee']['emergency_contact_number'] ? '<br>' . $employee['Employee']['emergency_contact_number'] : ''); ?>&nbsp;</td>
				<td><?php echo !empty($employee['Tool']) ? '<i class="fa fa-wrench uploaded-files-tick"></i> ' . count($employee['Tool']) : ''; ?>&nbsp;</td>

				<td class="actions" style="white-space: nowrap">
					<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $employee['Employee']['id']),array('escape'=>false)); ?>
				<?php if($userIsAdmin): ?>
					<?php
					if ($employee['Employee']['archived']):
						// employee is archived so show Undo icon
						echo $this->Html->link('<i class="fa fa-undo fa-2x" title="Restore from Archive"></i>', array('controller' => 'employees', 'action' => 'archive', $employee['Employee']['id'],0), array('escape'=>false,'class'=>'deleteButton'));
					else:
						// show archive icon
						echo $this->Html->link('<i class="fa fa-archive fa-2x" title="Move to Archive"></i>', array('controller' => 'employees', 'action' => 'archive', $employee['Employee']['id'], 1), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this sub-contractor to Archive?'));
					endif;
					?>
				<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if( !$employees ) : ?>
			<tr>
				<td colspan="10">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>	
