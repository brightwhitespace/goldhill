<div class="content">
	<h2>PO Created: <?php echo $po['PurchaseOrder']['ref']; ?></h2>
	
	<div class="group">
		
		<p><strong>Project</strong><br>
		<?php echo $po['Project']['name']; ?></p>
		<p><strong>Supplier</strong><br>
		<?php echo $po['Supplier']['name']; ?></p>
		<p><strong>Supplier Contact</strong><br>
		<?php echo $po['Contact']['name']; ?></p>
		<p><strong>Type</strong><br>
		<?php echo $po['Type']['name']; ?></p>
		<p><strong>Delivery date</strong><br>
		<?php echo $po['PurchaseOrder']['start_date']; ?></p>
		
		<?php if($po['PurchaseOrder']['type_id'] != 2): ?>
		<p><strong>End date</strong><br>
		<?php echo $po['PurchaseOrder']['end_date']; ?></p>
		<?php endif; ?>
		
		<p><strong>Delivery address</strong><br>
		<?php echo $po['Project']['address1']; ?><br>
		<?php echo $po['Project']['address2']; ?><br>
		<?php echo $po['Project']['address3']; ?><br>
		<?php echo $po['Project']['city']; ?><br>
		<?php echo $po['Project']['postcode']; ?>
		</p>
		<p><strong>Description</strong><br>
		<?php echo $po['PurchaseOrder']['desc']; ?>
		</p>
		
	</div>
	
	<a href="/" class="button button-primary">DONE</a>
	
</div>
