<div class="content">
	<div class="content-head group">
		<div class="col8">
			<h1>
				<?php echo $purchase_order['PurchaseOrder']['ref']; ?> <?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $purchase_order['PurchaseOrder']['id']),array('escape'=>false)); ?>
				<?php //echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $purchase_order['PurchaseOrder']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $purchase_order['PurchaseOrder']['ref'])); ?>
			</h1>
		</div>
		<div class="col4 right">
			<a href="/admin/purchase_orders/" class="button button-primary">Continue</a>
		</div>
	</div>
	<div class="group">

			<div class="col12">

				<p><strong>Project</strong><br>
					<?php echo $purchase_order['Project']['name']; ?></p>
				<p><strong>Supplier</strong><br>
					<?php echo $purchase_order['Supplier']['name']; ?></p>
				<p><strong>Supplier Contact</strong><br>
					<?php echo $purchase_order['Contact']['name']; ?></p>
				<p><strong>Type</strong><br>
					<?php echo $purchase_order['Type']['name']; ?></p>
				<p><strong>Delivery date</strong><br>
					<?php echo $purchase_order['PurchaseOrder']['start_date']; ?></p>

				<?php if($purchase_order['PurchaseOrder']['type_id'] != 2): ?>
					<p><strong>End date</strong><br>
						<?php echo $purchase_order['PurchaseOrder']['end_date']; ?></p>
				<?php endif; ?>

				<p><strong>Delivery address</strong><br>
					<?php echo $purchase_order['Project']['address1']; ?><br>
					<?php echo $purchase_order['Project']['address2']; ?><br>
					<?php echo $purchase_order['Project']['address3']; ?><br>
					<?php echo $purchase_order['Project']['city']; ?><br>
					<?php echo $purchase_order['Project']['postcode']; ?>
				</p>
				<p><strong>Description</strong><br>
					<?php echo $purchase_order['PurchaseOrder']['desc']; ?>
				</p>
				
		</div>
		
	</div>
	
</div>	


