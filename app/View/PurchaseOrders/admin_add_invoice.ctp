<div class="content">
	<h2>Reconcile PO/Invoice</h2>
	
	<?php echo $this->Form->create('PurchaseOrder',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
				echo $this->Form->input('PurchaseOrderInvoice.purchase_order_id',array('type'=>'hidden'));
				echo $this->Form->input('PurchaseOrderInvoice.value',array('label'=>'Value<em>*</em>','type'=>'number','class'=>'currency'));
				echo $this->Form->input('PurchaseOrderInvoice.date',array('label'=>'Date<em>*</em>','type'=>'text','class'=>'datepicker'));
				echo $this->Form->input('PurchaseOrderInvoice.ref',array('label'=>'Inv No.<em>*</em>','type'=>'text'));
			?>
			
		</div>
		
	</div>
	<div class="group">
		<div class="col6">
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
		<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
