
<div class="group content">
	<h2>Edit Purchase Order: <?php echo $this->request->data['PurchaseOrder']['ref']; ?></h2>

	<div class="col9">
		<?php echo $this->Form->create('PurchaseOrder',array('novalidate'=>'novalidate'));?>
		<?php echo $this->Form->input('id'); ?>
		<div class="group<?php echo !$userIsAdmin ? ' readonly' : ''; ?>">
			<div class="col8">

				<?php // building the project dropdown so can add data-address to each project option ?>
				<label for="select-project">Project<em>*</em></label>
				<select id="select-project" name="data[PurchaseOrder][project_id]" required="required">
					<option value="">Please select</option>
					<?php foreach ($projects as $project): ?>
						<option value="<?php echo $project['Project']['id'] ?>"
								data-address="<?php echo $this->App->formatAddress($project['Project'], ',') ?>"
									<?php echo $project['Project']['id'] == $selectedProjectId ? 'selected' : '' ?>>
							<?php echo $project['Project']['name'] ?>
						</option>
					<?php endforeach; ?>
				</select>
				<?php echo !empty($this->validationErrors['PurchaseOrder']['project_id'][0]) ? '<div class="error-message">' . $this->validationErrors['PurchaseOrder']['project_id'][0] . '</div>' : '' ?>
				<p id="project-address" style="display: none">
				</p>

				<?php
					echo $this->Form->input('supplier_id',array('label'=>'Supplier<em>*</em>','type'=>'select','options'=>$suppliers,'empty'=>'Please select',
						'data-update'=>'supplierContacts','data-update-url'=>'/admin/suppliers/contacts/','data-selected-contact-id'=>$this->request->data['PurchaseOrder']['contact_id'],'class'=>'update-on-select'));
				?>
				<div id="supplierContacts">
				<?php
					echo $this->Form->input('contact_id',array('label'=>'Supplier Contact<em>*</em>','type'=>'select','options'=>array(),'empty'=>'Please select supplier first'));
				?>
				</div>
				<?php
				echo $this->Form->input('type_id',array('label'=>'Type<em>*</em>','type'=>'select','options'=>$types));
				echo $this->Form->input('user_id',array('label'=>'PO Contact<em>*</em>','type'=>'select','options'=>$users));
				echo $this->Form->input('employee_id',array('label'=>'Site Contact<em>*</em>','type'=>'select','options'=>$employees,'empty'=>'Please select'));
				echo $this->Form->input('desc',array('label'=>'Description','type'=>'text'));
				echo $this->Form->input('notes',array('type'=>'textarea','label'=> 'Notes'));
				?>
			</div>
			<div class="col4">
				<?php
				echo $this->Form->input('start_date',array('label'=>'Delivery date<em>*</em>','type'=>'text','class'=>'datepicker u-full-width'));
				?>
				<div class="type-hire not-readonly">
					<?php
					echo $this->Form->input('end_date',array('label'=>'End date<em>*</em>','type'=>'text','class'=>'datepicker u-full-width','onchange'=>'alert("*** \nPlease ensure you record the reason for changing the end date in the Notes field \n***")'));
					?>
				</div>
				<?php
				echo $this->Form->input('quantity',array('label'=>'Quantity<em>*</em>','type'=>'number','class'=>'u-full-width'));
				?>
				<div class="type-skips">
					<?php
					echo $this->Form->input('excess_tonage',array('label'=>'Excess Tonage - skips','type'=>'number','class'=>'currency u-full-width'));
					?>
				</div>
				<?php
				echo $this->Form->input('rate',array('label'=>'Rate<em>*</em>','type'=>'number','class'=>'currency u-full-width'));
				echo $this->Form->input('value',array('label'=>'PO Value','type'=>'number','class'=>'currency u-full-width'));
				?>
				<div class="not-readonly">
					<div class="type-hire">
						<?php
						echo $this->Form->input('off_hire',array('type'=>'checkbox','label'=> 'Off hire'));
						echo $this->Form->input('returned',array('type'=>'checkbox','label'=> 'Hire item returned'));
						?>
					</div>

					<?php
					echo $this->Form->input('complete',array('type'=>'checkbox','label'=> 'PO Complete'));
					echo $this->Form->input('void',array('type'=>'checkbox','label'=> 'Void'));
					echo $this->Form->input('recharge',array('type'=>'checkbox','label'=> 'Recharge item'));
					?>
				</div>
			</div>
		</div>

		<div class="group">
			<?php echo $this->Form->button('Reconcile PO/Invoice', array('name'=>'reconcile','type'=>'submit','value'=>'reconcile','class'=>'button button-primary floatright')); ?>
			<!--<a href="/admin/purchase_orders/edit/<?php echo $this->request->data['PurchaseOrder']['id']; ?>/reconcile" class="button button-primary floatright">Reconcile PO/Invoice</a>-->
			<h2>Invoices Received</h2>
			<div id="poInvoices">
				<?php if(!empty($this->request->data['PurchaseOrderInvoice'])): ?>
				<table>
					<tr>
						<th>NUMBER</th>
						<th>VALUE</th>
						<th>DATE</th>
						<th></th>
					</tr>
					<?php foreach($this->request->data['PurchaseOrderInvoice'] as $key => $invoice): ?>
					<tr>
						<td><?php echo $invoice['ref']; ?></td>
						<td><?php echo $this->Number->currency($invoice['value'],'GBP'); ?></td>
						<td><?php echo $this->Time->format('d/m/Y',$invoice['date']); ?></td>
						<td>
						<?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit_invoice', $invoice['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete_invoice', $invoice['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $invoice['ref'])); ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
				<?php endif; ?>
			</div>
		</div>
		<div class="group">
			<div class="col6">
			<?php echo $this->Html->link('Cancel',$returnUrl,array('class'=>'button button-secondary')); ?>
			</div>
			<div class="col6 right">
				<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?> &nbsp;
				<?php echo empty($po_is_confirmed) ? $this->Form->button('Confirm', array('name'=>'confirm','type'=>'submit','value'=>'confirm','class'=>'button button-primary')) : ''; ?>
			</div>
		</div>
		<?php echo $this->Form->end();?>
	</div>

	<div class="col3">
		<div class="input">
			<label>PO Files</label>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'purchase_order', 'files'=>$files, 'objectId'=>$this->request->data['PurchaseOrder']['id'], 'allowDelete'=>$userIsAdmin)); ?>
			</div>
		</div>
	</div>

</div>
