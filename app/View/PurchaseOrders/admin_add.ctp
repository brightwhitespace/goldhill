<div class="group content">
	<h2>Add Purchase Order</h2>

	<div class="col9">
		<?php echo $this->Form->create('PurchaseOrder',array('novalidate'=>'novalidate'));?>
		<div class="group">
			<div class="col8">

				<?php // building the project dropdown so can add data-address to each project option
					// if form is being redisplayed due to errors, make sure we preselect the project id that was previously chosen
					$selectedProjectId = !empty($this->request->data['PurchaseOrder']['project_id']) ? $this->request->data['PurchaseOrder']['project_id'] : 0;
				?>
				<label for="select-project">Project<em>*</em></label>
				<select id="select-project" name="data[PurchaseOrder][project_id]" required="required">
					<option value="">Please select</option>
					<?php foreach ($projects as $project): ?>
						<option value="<?php echo $project['Project']['id'] ?>"
								data-address="<?php echo $this->App->formatAddress($project['Project'], ',') ?>"
							<?php echo $project['Project']['id'] == $selectedProjectId ? 'selected' : '' ?>>
								<?php echo $project['Project']['name'] ?>
						</option>
					<?php endforeach; ?>
				</select>
				<?php echo !empty($this->validationErrors['PurchaseOrder']['project_id'][0]) ? '<div class="error-message">' . $this->validationErrors['PurchaseOrder']['project_id'][0] . '</div>' : '' ?>
				<p id="project-address" style="display: none">
				</p>

				<?php
					// if form is being redisplayed due to errors, make sure we preselect the supplier contact id that was previously chosen
					$selectedSupplierContactId = !empty($this->request->data['PurchaseOrder']['contact_id']) ? $this->request->data['PurchaseOrder']['contact_id'] : 0;
					echo $this->Form->input('supplier_id',array('label'=>'Supplier<em>*</em>','type'=>'select','options'=>$suppliers,'empty'=>'Please select',
						'data-update'=>'supplierContacts','data-update-url'=>'/admin/suppliers/contacts/','data-selected-contact-id'=>$selectedSupplierContactId,'class'=>'update-on-select'));
				?>
				<div id="supplierContacts">
				<?php
					echo $this->Form->input('contact_id',array('label'=>'Supplier Contact<em>*</em>','type'=>'select','options'=>array(),'empty'=>'Please select supplier first'));
				?>
				</div>
				<?php
				echo $this->Form->input('type_id',array('label'=>'Type<em>*</em>','type'=>'select','options'=>$types));
				echo $this->Form->input('user_id',array('label'=>'PO Contact<em>*</em>','type'=>'select','options'=>$users));
				echo $this->Form->input('employee_id',array('label'=>'Site Contact<em>*</em>','type'=>'select','options'=>$employees,'empty'=>'Please select'));
				echo $this->Form->input('desc',array('label'=>'Description','type'=>'text'));
				echo $this->Form->input('notes',array('type'=>'textarea','label'=> 'Notes'));
				?>
			</div>
			<div class="col4">
				<?php
					echo $this->Form->input('start_date',array('label'=>'Delivery date<em>*</em>','type'=>'text','class'=>'datepicker u-full-width'));
				?>
				<div class="type-hire not-readonly">
				<?php
					echo $this->Form->input('end_date',array('label'=>'End date','type'=>'text','class'=>'datepicker u-full-width'));
				?>
				</div>
				<?php
					echo $this->Form->input('quantity',array('label'=>'Quantity<em>*</em>','type'=>'number','class'=>'u-full-width'));
				?>
				<div class="type-skips">
				<?php
					echo $this->Form->input('excess_tonage',array('label'=>'Excess Tonage - skips','type'=>'number','class'=>'currency u-full-width'));
				?>
				</div>
				<?php
					echo $this->Form->input('rate',array('label'=>'Rate<em>*</em>','type'=>'number','class'=>'currency u-full-width'));
					echo $this->Form->input('value',array('label'=>'PO Value','type'=>'number','class'=>'currency u-full-width'));
				?>
				<div class="type-hire">
					<?php
						//echo $this->Form->input('returned',array('type'=>'checkbox','label'=> 'Hire item returned'));
					?>
				</div>
				<?php
					echo $this->Form->input('recharge',array('type'=>'checkbox','label'=> 'Recharge item'));
				?>

			</div>
		</div>
		<div class="group">
			<div class="col6">
			<?php echo $this->Html->link('Cancel',$returnUrl,array('class'=>'button button-secondary')); ?>
			</div>
			<div class="col6 right">
				<?php echo $this->Form->button('Save & Remain', array('name'=>'save_and_remain','type'=>'submit','value'=>'save','class'=>'button button-secondary')); ?>
				<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?> &nbsp;
				<?php echo $this->Form->button('TBC & Remain', array('name'=>'tbc_and_remain','type'=>'submit','value'=>'confirm','class'=>'button button-secondary')); ?>
				<?php echo $this->Form->button('TBC', array('name'=>'tbc','type'=>'submit','value'=>'confirm','class'=>'button button-primary')); ?>
			</div>
		</div>
		<?php echo $this->Form->end();?>
	</div>

	<div class="col3">
		<div class="input">
			<label>PO Files</label>
			<div class="box">
				<p style="word-break: normal !important"><i>
					New PO must be saved first to allow upload of files.<br><br>
					Click "SAVE &amp; REMAIN" or "TBC &amp; REMAIN" below to stay on PO page after saving.</i>
				</p>
			</div>
		</div>

		Note: TBC = To Be Confirmed<br>
		<em>(A "CONFIRM" button will appear on the Edit PO page and the PO will not be sent until this button is clicked)</em>
	</div>

</div>
