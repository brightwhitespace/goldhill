<div class="content">
	<div class="group">
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col5">
			<h1>Purchase Orders</h1>
		</div>
		<div class="col2 right">
			<?php echo $this->Form->input('filter', array('label'=>false, 'type'=>'select', 'onchange'=>'this.form.submit()', 'options'=>$searchFilterOptions, 'div'=>false,'empty'=>'Filter...')); ?>
		</div>
		<div class="col2 right">
			<a href="/admin/purchase_orders/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add PO</a>
		</div>
		<div class="col3">
			<?php
				echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
				echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if(!empty($this->request->data['Search'])): ?>
				<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
		
	<table class="cols-align-top">
		<tr>
			<th><?php echo $this->Paginator->sort('ref');?></th>
			<th style="width: 15%"><?php echo $this->Paginator->sort('project_id');?></th>
			<th style="width: 35%"><?php echo $this->Paginator->sort('desc');?></th>
			<th>Files?</th>
			<th><?php echo $this->Paginator->sort('supplier_id');?></th>
			<th><?php echo $this->Paginator->sort('value');?></th>
			<th><?php echo $this->Paginator->sort('invoiced');?></th>
			<th><?php echo $this->Paginator->sort('confirmed', 'TBC?');?></th>
			<th>Auth</th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th width="150" class="actions"></th>
		</tr>
		<?php
			$i = 0;
			foreach ($purchase_orders as $purchase_order): //debug($purchase_order);
				$class = null;
				if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] > 0) {
					$class = ' class="complete"';
				}
				if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] == 0) {
					$class = ' class="partial-complete"';
				}
				if ($purchase_order['PurchaseOrder']['void'] == 1) {
					$class = ' class="void"';
				}
				if($purchase_order['PurchaseOrder']['type_id'] == 1 && $purchase_order['PurchaseOrder']['returned'] == 0 && strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW + 3 days')):
					$class = ' class="warning"';
				endif;
				if($purchase_order['PurchaseOrder']['type_id'] == 1 && $purchase_order['PurchaseOrder']['returned'] == 0 && strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW')):
					$class = ' class="alert"';
				endif;

				$valuesClass = $purchase_order['PurchaseOrder']['invoiced'] > $purchase_order['PurchaseOrder']['value'] ? 'awaiting-credit-note' : '';
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($purchase_order['PurchaseOrder']['ref'], array('controller' => 'purchase_orders', 'action' => 'edit', $purchase_order['PurchaseOrder']['id'])); ?>&nbsp;</td>
				<td><?php echo $purchase_order['Project']['name']; ?>&nbsp;</td>
				<td><?php echo $purchase_order['PurchaseOrder']['desc']  . ($purchase_order['PurchaseOrder']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($purchase_order['PurchaseOrder']['notes']) . '</div>' : ''); ?>&nbsp;</td>
				<td><?php $files = glob(APP . "tmp/purchase_order/".$purchase_order['PurchaseOrder']['id']."/*"); echo !empty($files) ? '<i class="fa fa-check uploaded-files-tick"></i> ' . count($files) : '' ?>&nbsp;</td>
				<td><?php echo $purchase_order['Supplier']['name']; ?></td>
				<td class="<?php echo $valuesClass ?>"><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['value'],'GBP'); ?>&nbsp;</td>
				<td class="<?php echo $valuesClass ?>"><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['invoiced'],'GBP'); ?>&nbsp;</td>
				<td><?php echo !empty($purchase_order['PurchaseOrder']['confirmed']) ? '' : '<i style="font-size: 17px; font-weight: bold">TBC</i>'; ?>&nbsp;</td>
				<td><?php echo !empty($purchase_order['PurchaseOrder']['confirmed_by_user_id']) ? '[' . $this->App->getInitials($this->App->getFullName($purchase_order['ConfirmedByUser'])) . ']' : ''; ?>&nbsp;</td>
				<td><?php echo $this->Time->niceShort($purchase_order['PurchaseOrder']['created']); ?>&nbsp;</td>

				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $purchase_order['PurchaseOrder']['id']),array('escape'=>false)); ?>
					<?php if($userIsAdmin): ?>
						<?php //echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('action' => 'delete', $purchase_order['PurchaseOrder']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $purchase_order['PurchaseOrder']['ref'])); ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$purchase_orders): ?>
			<tr>
				<td colspan="10">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>	


