<div class="content">
	<div class="group">
		<div class="col9">
			<h1>Purchase Orders</h1>
		</div>
		<div class="col3 right">
			<a href="/purchase_orders/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Purchase Order</a>
		</div>
		
		
	</div>
	
		
		
		<table>
			<tr>
				<th><?php echo $this->Paginator->sort('ref');?></th>
				<th><?php echo $this->Paginator->sort('project_id');?></th>
				<th><?php echo $this->Paginator->sort('desc');?></th>
				
				<th><?php echo $this->Paginator->sort('value');?></th>
				<th><?php echo $this->Paginator->sort('created');?></th>
				
			</tr>
			<?php
				$i = 0;
				foreach ($purchase_orders as $purchase_order):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
				?>
				<tr<?php echo $class;?>>					
					<td><?php echo $purchase_order['PurchaseOrder']['ref']; ?>&nbsp;</td>
					<td><?php echo $purchase_order['Project']['name']; ?>&nbsp;</td>
					<td><?php echo $purchase_order['PurchaseOrder']['desc']; ?>&nbsp;</td>					
					<td><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['value'],'GBP'); ?>&nbsp;</td>
					<td><?php echo $this->Time->niceShort($purchase_order['PurchaseOrder']['created']); ?>&nbsp;</td>
					
					
				</tr>
			<?php endforeach; ?>
			
			<?php if( !$purchase_orders ) : ?>
				<tr>
					<td colspan="6">No records found</td>
				</tr>
			<?php endif; ?>
		</table>
		<div class="pagination group">
			<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
			<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
		</div>
</div>	


