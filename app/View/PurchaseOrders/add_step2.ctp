<div class="content">
	<h2>New PO</h2>
	
	<?php echo $this->Form->create('PurchaseOrder',array('novalidate'=>'novalidate'));?>
	<div class="group">
		
		<div class="col6 step2">
			<?php
				echo $this->Form->input('type_id',array('type'=>'hidden'));
				echo $this->Form->input('desc',array('label'=>'Description','type'=>'text'));
				echo $this->Form->input('start_date',array('label'=>'Delivery date<em>*</em>','type'=>'text','class'=>'datepicker'));
			?>
			<div class="type-hire">
			<?php
				echo $this->Form->input('end_date',array('label'=>'End date','type'=>'text','class'=>'datepicker'));
			?>
			</div>
			<?php
				echo $this->Form->input('quantity',array('label'=>'Quantity<em>*</em>','type'=>'number'));
			?>
			<div class="type-skips">
			<?php
				echo $this->Form->input('excess_tonage',array('label'=>'Excess Tonage - skips','type'=>'number','class'=>'currency'));
			?>
			</div>
			<?php
				echo $this->Form->input('rate',array('label'=>'Rate<em>*</em>','type'=>'number','class'=>'currency'));
				echo $this->Form->input('value',array('label'=>'PO Value','type'=>'number','class'=>'currency'));				
			?>
			
		</div>
	</div>
	<div class="group">
		<?php echo $this->Html->link('Back',array('action'=>'add'),array('class'=>'button button-secondary')); ?>
		<?php echo $this->Form->button('Continue', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
