<div class="content">
	<h2>Preview PO</h2>
	
	<?php echo $this->Form->create('PurchaseOrder',array('novalidate'=>'novalidate'));?>
	<div class="group">
		
		<p><strong>Project</strong><br>
		<?php echo $project['Project']['name']; ?></p>
		<p><strong>Supplier</strong><br>
		<?php echo $supplier['Supplier']['name']; ?></p>
		<p><strong>Supplier Contact</strong><br>
		<?php echo $suppliercontact['Contact']['name']; ?></p>
		<p><strong>Type</strong><br>
		<?php echo $type['Type']['name']; ?></p>
		<p><strong>Delivery date</strong><br>
		<?php echo $data['PurchaseOrder']['start_date']; ?></p>
		
		<?php if($data['PurchaseOrder']['type_id'] != 2): ?>
		<p><strong>End date</strong><br>
		<?php echo $data['PurchaseOrder']['end_date']; ?></p>
		<?php endif; ?>
		
		<p><strong>Delivery address</strong><br>
		<?php echo $project['Project']['address1']; ?><br>
		<?php echo $project['Project']['address2']; ?><br>
		<?php echo $project['Project']['address3']; ?><br>
		<?php echo $project['Project']['city']; ?><br>
		<?php echo $project['Project']['postcode']; ?>
		</p>
		<p><strong>Description</strong><br>
		<?php echo $data['PurchaseOrder']['desc']; ?>
		</p>
		
	</div>
	<div class="group">
	
		<?php echo $this->Html->link('Back',array('action'=>'add_step2'),array('class'=>'button button-secondary')); ?>
		<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
