<div class="content">
	<h2>New PO</h2>
	
	<?php echo $this->Form->create('PurchaseOrder',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6 step1">
			<?php
				echo $this->Form->input('project_id',array('label'=>'Project<em>*</em>','type'=>'select','options'=>$projects,'empty'=>'Please select'));
				echo $this->Form->input('supplier_id',array('label'=>'Supplier<em>*</em>','type'=>'select','options'=>$suppliers,'empty'=>'Please select','data-update'=>'supplierContacts','data-update-url'=>'/admin/suppliers/contacts/','class'=>'update-on-select'));
			?>
			<div id="supplierContacts">
			<?php
				echo $this->Form->input('contact_id',array('label'=>'Supplier Contact<em>*</em>','type'=>'select','options'=>array(),'empty'=>'Please select supplier first'));
				
			?>
			</div>	
			<?php
				echo $this->Form->input('type_id',array('label'=>'Type<em>*</em>','type'=>'select','options'=>$types));
				echo $this->Form->input('user_id',array('type'=>'hidden'));
				
			?>			
		</div>
		
	</div>
	<div class="group">
		
		<div class="col6 right">
		<?php echo $this->Form->button('Continue', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
