
<section class="section-medium bg-dark alt">
	<div class="container">

		<div class="col10">
			<ul class="breadcrumbs inline">
				<li><?php echo $this->Pages->breadcrumbs($page) ?></li>
			</ul>
			<div class="pad">
				<h1><?php echo $parentLandingPage['Page']['h1']; ?></h1>
				<?php echo $page['ContentBlock'][0]['content']; ?>
			</div>
		</div>
		<div class="col2">
			<?php if ($page['Page']['page_image']): ?>
				<img src="<?php echo $page['Page']['page_image']; ?>" alt="<?php echo $page['Page']['page_image_alt']; ?>" class="circle responsive">
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="section-medium">
	<div class="container">
		<div class="col3">
			<div class="sidebar">
				<div class="mobile-sidemenu mobile-only"><a href=""><span></span></a></div>
				<h3><?php echo $parentLandingPage['Page']['page_title']; ?></h3>
				<nav>
					<?php echo $this->Pages->getMenu($mainmenu_for_layout, array(
						'returnChildren' => true,
						'parentIds' => $parentIds,
						'showNonParentHierarchy' => false,
						'showFromLevel' => 2));
					?>
				</nav>
			</div>
		</div>
		<div class="col9">
			<h1><?php echo !empty($category) ? $category['PostCategory']['name'] : 'All News' ?></h1>
			<?php foreach ($posts as $post): ?>
				<?php echo $this->Blog->displaySummary($post, $currentSite) ?>
			<?php endforeach; ?>
			<div class="pagination clearfix">
				<?php $this->Paginator->options(array('url' => $paginatorOptions)); ?>
				<?php echo $this->Paginator->prev(__('&laquo; prev'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
				<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
			</div>
		</div>
	</div>
</section>
