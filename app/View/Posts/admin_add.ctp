
<div id="page-title">
	<div class="container clearfix">
		
		<h1>New Post</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<div id="tabs">
		<ul>
			<li class="tab"><a href="#body">Content</a></li>
			<li class="tab"><a href="#meta">Meta Data</a></li>
			<li class="tab"><a href="#options">Options</a></li>
		</ul>
		
		<?php echo $this->Form->create('Post',array('class'=>'cmxform'));?>
		<div id="body" class="tabArea">
			<fieldset>
				<ol>
					<?php
					echo $this->Form->input('title',array('label'=>'Post Title<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false,'onchange'=>"updateUrlField('PostUrl','PostTitle')"));
					echo $this->Form->input('url',array('label'=>'Post Url<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('is_external_url_link',array('type'=>'checkbox','label'=>'Link directly to external URL?',
						'class'=>'checkbox','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					echo $this->Form->input('external_url',array('label'=>'External Url','type'=>'text','class'=>'textbox','before'=>'<li id="externalUrlContainer">','after'=>'</li>','div'=>false));
					echo $this->Ckeditor->input('body',array('label'=>'','type'=>'textarea','class'=>'CKEDITOR','before'=>'<li id="bodyContentContainer">','after'=>'</li>','div'=>false));
					echo $this->Form->input('short_desc',array('label'=>'Brief description<br>(for list page)','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('link_text',array('label'=>'Link text (for list page)','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false,'default'=>'Read more'));
					echo $this->Form->input('source_name',array('label'=>'Name of Source (if any)','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('date',array('label'=>'Date','type'=>'date','dateFormat'=>'DMY','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('published',array('type'=>'checkbox','label'=>'Publish?','class'=>'checkbox','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					?>
				</ol>
			</fieldset>
			<script>
				$(document).ready(function() {
					// initially show/hide external url textbox based on checkbox
					$('#PostIsExternalUrlLink').on('change', function () {
						handleExternalUrlCheckboxChange(this);
					}).change();
				});

				function handleExternalUrlCheckboxChange(ele, duration) {
					var externalUrlTicked = $(ele).prop('checked');

					$('#bodyContentContainer').toggle(!externalUrlTicked);
					$('#externalUrlContainer').toggle(externalUrlTicked);
				}
			</script>
		</div>
		<div id="meta" class="tabArea">
			<fieldset>
				<legend><?php echo __('Description'); ?></legend>
				<ol>
					<?php
					echo $this->Form->input('meta_title',array('type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('meta_description',array('label'=>'Meta Description','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
					?>
				</ol>
			</fieldset>
		</div>
		<div id="options" class="tabArea">
			<fieldset>
				<legend><?php echo __('Sites to appear on'); ?></legend>
				<ol>
					<?php
					echo $this->Form->input('Site.Site',array('label'=>'Choose sites','type'=>'select','multiple'=>'checkbox','options'=>$sites,'class'=>'tickbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
					echo $this->Form->input('user_id',array('type'=>'hidden'));
					?>
				</ol>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Categories to appear in'); ?></legend>
				<ol>
					<?php
					echo $this->Form->input('PostCategory.PostCategory',array('label'=>'Choose categories','type'=>'select','multiple'=>'checkbox','options'=>$post_categories,'class'=>'tickbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
					echo $this->Form->input('user_id',array('type'=>'hidden'));
					?>
				</ol>
			</fieldset>
			<fieldset>
				<legend><?php echo __('Feature Box Info'); ?></legend>
				<ol>
					<?php
					echo $this->Form->input('featured',array('type'=>'checkbox','label'=>'Feature on homepage?','class'=>'checkbox','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					echo $this->Form->input('featured_landing',array('type'=>'checkbox','label'=>'Feature on landing page?','class'=>'checkbox','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
					echo $this->Form->input('alt_title',array('label'=>'Alternative short title','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					echo $this->Form->input('alt_short_desc',array('label'=>'Alternative short description','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
					?>
				</ol>
			</fieldset>
		</div>
		</div>
		<?php echo $this->Form->button('save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton')); ?>
		<?php echo $this->Form->button('save & close', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
		
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		
		<?php echo $this->Form->end();?>
	</div>
</div>
