
<section class="section-medium bg-dark alt">
	<div class="container">

		<div class="col10">
			<ul class="breadcrumbs inline">
				<li><?php echo $this->Pages->breadcrumbs($page) ?></li>
			</ul>
			<div class="pad">
				<h1><?php echo $parentLandingPage['Page']['h1']; ?></h1>
				<?php echo $page['ContentBlock'][0]['content']; ?>
			</div>
		</div>
		<div class="col2">
			<?php if ($page['Page']['page_image']): ?>
				<img src="<?php echo $page['Page']['page_image']; ?>" alt="<?php echo $page['Page']['page_image_alt']; ?>" class="circle responsive">
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="section-medium">
	<div class="container">
		<div class="col3">
			<div class="sidebar">
				<div class="mobile-sidemenu mobile-only"><a href=""><span></span></a></div>
				<h3><?php echo $parentLandingPage['Page']['page_title']; ?></h3>
				<nav>
					<?php echo $this->Pages->getMenu($mainmenu_for_layout, array(
						'returnChildren' => true,
						'parentIds' => $parentIds,
						'showNonParentHierarchy' => false,
						'showFromLevel' => 2));
					?>
				</nav>
			</div>
		</div>
		<div class="col9 post-full-view">
			<h1><?php echo $post['Post']['title'] ?></h1>
			<div class="post-info"><?php echo $this->Blog->getPostInfo($post, false) ?></div>
			<?php if ($post['Post']['is_external_url_link']): ?>
				<p><?php echo $post['Post']['short_desc'] ?></p>
				<p><?php echo $this->Blog->getPermalink($post, false, $currentSite, true) ?></p>
			<?php else: ?>
				<?php echo $post['Post']['body'] ?>
			<?php endif; ?>
		</div>
	</div>
</section>
