
<div id="page-title">
	<div class="container clearfix">
		
		<h1>Edit Advert</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		
		<?php echo $this->Form->create('Advert',array('class'=>'cmxform'));?>
		<?php echo $this->Form->input('id'); ?>
			
			<div id="tabs">
				<ul>  
					<li class="tab"><a href="#one">Content</a></li>
					<li class="tab"><a href="#two">Pages</a></li>
				</ul>
				<div id="one" class="tabArea">
					<fieldset>
						<ol>
							<?php
							echo $this->Form->input('name', array('label'=>'Advert Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Form->input('link', array('label'=>'Advert Link','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Ckeditor->browseField('image', array('id'=>'AdvertImage','type'=>'text','class'=>'textbox image','label'=> 'Browse for an image','before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Form->input('heading', array('label'=>'Advert Heading/Title','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Form->input('body', array('label'=>'Advert Body Text','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Form->input('caption', array('label'=>'Advert Caption Text','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Form->input('position', array('label'=>'Advert Position','type'=>'select','options'=>$positions,'before'=>'<li>','after'=>'</li>','div'=>false));
							echo $this->Form->input('published', array('type'=>'checkbox','label'=>'Publish?','class'=>'checkbox','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
							?>
						</ol>
					</fieldset>
				</div>
				<div id="two" class="tabArea">
					<fieldset>
						<legend>Display on pages</legend>
						<p>Tick the boxes for the pages you would like the advert to appear on</p>
						<p><a href="#" id="selectAll">All</a> | <a href="#" id="selectNone">None</a></p>
						<ol>
							<?php echo $this->Form->input('Page.Page',array('label'=>'','type'=>'select','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'multiple'=>'checkbox','class'=>'tickbox bulk-select','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false,'empty'=>false)); ?>
						</ol>
					</fieldset>
				</div>
			</div>
		<?php echo $this->Form->button('Save & close', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>

		<?php echo $this->Form->end();?>
		
	</div>
</div>
