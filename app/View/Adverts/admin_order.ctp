
<div id="page-title">
	<div class="container clearfix">
		
		<h1>Re-order Adverts</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<p>Drag and drop the adverts to change the order</p>
		<div id="ajax-status">order saved</div>
		
		<ol class="sortable" id="sortable_1">
		<?php
		$i = 0;
		foreach ($adverts as $advert):
			
		?>
			
			<li id="advert_<?php echo $advert['Advert']['id']; ?>">
				<div><?php echo $advert['Advert']['name']; ?></div>
				
			</li>
				
		<?php endforeach; ?>
		</ol>
		
		
		
		<?php echo $this->Form->button('save order', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton saveOrder','id'=>'saveOrder_adverts')); ?>
		
		<?php echo $this->Html->link('Back',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		
	</div>
</div>