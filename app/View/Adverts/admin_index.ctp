
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/adverts/order">Re-order Adverts</a></li>
			<li><a href="/admin/adverts/add">Add Advert</a></li>
		</ul>
		<h1>Adverts</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
	
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
			<?php
				echo $this->Form->input('term', array('label'=>'Search ', 'type'=>'text', 'div'=>false,'class'=>'textbox'));
			?>
			<?php echo $this->Form->button('Go', array('name'=>'search','type'=>'submit','value'=>'search','class'=>'filterButton')); ?>
		<?php echo $this->Form->end(); ?>
		<table cellpadding="0" cellspacing="0">
		<tr>
			<!--<th><?php echo $this->Paginator->sort('id');?></th>-->
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('position');?></th>
			<th><?php echo $this->Paginator->sort('published');?></th>
			
			<th class="actions"><?php echo __('Actions');?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($adverts as $advert):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
			<tr<?php echo $class;?>>
				<!--<td>
					<?php echo $advert['Advert']['id']; ?>
				</td>-->
				<td>
					<?php echo $this->Html->link($advert['Advert']['name'], array('action'=>'edit', $advert['Advert']['id']),array('escape'=>false),null,false); ?>
				</td>
				<td>
					<?php echo $advert['Advert']['position']; ?>
				</td>
				<td>
					<?php echo $this->Brightform->displayTrueOrFalse($advert['Advert']['published']); ?>
				</td>
				
				
				<td class="actions">
					<?php //echo $this->Html->link($this->Html->image('icon_order.gif'), array('action'=>'order'),null,null,false); ?>
					<?php echo $this->Html->link('EDIT', array('action'=>'edit', $advert['Advert']['id']),array('escape'=>false),null,false); ?>
					<?php echo $this->Html->link('DELETE', array('action'=>'delete', $advert['Advert']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?'), $advert['Advert']['id']),false); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<div class="pagination clearfix">
			<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
			<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
		</div>
	</div>
</div>