<div class="content">
	<h2>Add Agency Timesheet &nbsp;(<?php echo $this->request->data['Project']['name'] ?>)</h2>

	<?php echo $this->Form->create('AgencyTimesheet');?>
	<div class="group">
		<div class="col3">
			<?php
			echo $this->Form->input('date', array('label'=>'Agency Timesheet Date<em>*</em>','type'=>'text','class'=>'datepicker','style'=>'width: 100% !important'));
			?>
		</div>
		<div class="col9">

		</div>
	</div>

	<div class="group">
		<div class="col12">
			<h3 style="margin: 20px 0">Agency Timesheet Entries</h3>
		</div>
	</div>

	<div class="group">
		<div class="col2">Agency<em>*</em></div>
		<div class="col3">Sub-contractor<em>*</em></div>
		<div class="col1">Start time<em>*</em></div>
		<div class="col1">End time<em>*</em></div>
		<div class="col1">Rate p.h.<em>*</em></div>
		<div class="col2">Break? (hours)</div>
		<div class="col2"></div>
	</div>

	<div class="group">
		<div class="col12"><hr style="margin: 10px 0 20px"></div>
	</div>

	<div id="timesheet-entries-container">
		<?php $idx = -1; foreach ($this->request->data['AgencyEmployeesAgencyTimesheet'] as $timesheetEntry): $idx++ ?>

			<div class="timesheet-entry group">
				<div class="col2">
					<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.$idx.supplier_id", array('label'=>false,'type'=>'select','options'=>$agencies,
						'class'=>'agency-select', 'empty'=>'Select Agency...', 'onchange'=>"handleAgencyTimesheetAgencyChange(this)")); ?>
				</div>
				<div class="col3">
 					<div class="input select">
						<select name="data[AgencyEmployeesAgencyTimesheet][<?php echo $idx ?>][agency_employee_id]" class="agency-employee-select" onchange="handleAgencyTimesheetEmployeeChange(this)">
							<option value="">Select sub-contractor...</option>
							<?php foreach ($employees as $employee): ?>
								<option <?php echo $employee['AgencyEmployee']['id'] == $timesheetEntry['agency_employee_id'] ? 'selected' : ''; ?>
										value="<?php echo $employee['AgencyEmployee']['id']; ?>" data-rate-ph="<?php echo $employee['AgencyEmployee']['rate_ph']; ?>"
										data-supplier-id="<?php echo $employee['AgencyEmployee']['supplier_id']; ?>">
									<?php echo $employee['AgencyEmployee']['name']; ?>
								</option>
							<?php endforeach; ?>
							<option value="add-new-employee">--- ADD NEW SUB-CONTRACTOR ---</option>
						</select>
						<div class="new-agency-employee">
							<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.$idx.new_employee_name", array('label'=>false,'type'=>'text', 'placeholder'=>'Enter sub-contractor name...','div'=>false)) ?>
							&nbsp;<a href="#" onclick="return cancelAddNewAgencyEmployee(this)">cancel</a>
						</div>
					</div>
				</div>
				<div class="col1">
					<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.$idx.start_time", array('label'=>false,'type'=>'select','options'=>$startTimes,'default'=>'07:30')); ?>
				</div>
				<div class="col1">
					<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.$idx.end_time", array('label'=>false,'type'=>'select','options'=>$endTimes,'default'=>'16:30')); ?>
				</div>
				<div class="col1">
					<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.$idx.rate_ph", array('label'=>false,'type'=>'text','class'=>'currency rate_ph','style'=>'width: 100% !important')); ?>
				</div>
				<div class="col2 checkbox-and-input-container">
					<input type="checkbox" class="checkbox-to-enable-input" <?php echo !empty($timesheetEntry['break_hours']) ? 'checked' : '' ?> onclick="enableInputRelatedToCheckbox(this)" />
					<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.$idx.break_hours", array('label'=>false,'type'=>'text','class'=>'input-enabled-by-checkbox','data-default-value'=>'1.0')); ?>
				</div>
				<div class="col2">
					<a href="#" class="button button-secondary" onclick="return deleteAgencyTimesheetEntry(this)">Remove</a>
				</div>
			</div>

		<?php endforeach; ?>
	</div>

	<div class="group">
		<div class="col12">
			<p style="margin: 5px 0 40px">
				<a href="#" class="button button-primary" onclick="return addAgencyTimesheetEntry()">Add new entry</a>
			</p>
		</div>
	</div>

	<div class="group">
		<div class="col6">
			<a href="<?php echo $returnUrl ?>" class="button button-secondary">Cancel</a>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>

</div>


<?php // HTML template for adding timesheet entries ?>
<script id="timesheet-entry-template" type="text/html">
	<div class="timesheet-entry group">
		<div class="col2">
			<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.INDEX.supplier_id", array('label'=>false,'type'=>'select','options'=>$agencies,
				'class'=>'agency-select', 'empty'=>'Select Agency...', 'onchange'=>"handleAgencyTimesheetAgencyChange(this)")); ?>
		</div>
		<div class="col3">
			<select name="data[AgencyEmployeesAgencyTimesheet][INDEX][agency_employee_id]" class="agency-employee-select" onchange="handleAgencyTimesheetEmployeeChange(this)">
				<option value="">Select employee...</option>
				<?php foreach ($employees as $employee): ?>
					<option value="<?php echo $employee['AgencyEmployee']['id']; ?>" data-rate-ph="<?php echo $employee['AgencyEmployee']['rate_ph']; ?>"
						data-supplier-id="<?php echo $employee['AgencyEmployee']['supplier_id']; ?>">
						<?php echo $employee['AgencyEmployee']['name']; ?>
					</option>
				<?php endforeach; ?>
				<option value="add-new-employee">--- ADD NEW SUB-CONTRACTOR ---</option>
			</select>
			<div class="new-agency-employee">
				<input name="data[AgencyEmployeesAgencyTimesheet][INDEX][new_employee_name]" placeholder="Enter sub-contractor name..." type="text" />&nbsp;
				<a href="#" onclick="return cancelAddNewAgencyEmployee(this)">cancel</a>
			</div>
		</div>
		<div class="col1">
			<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.INDEX.start_time", array('label'=>false,'type'=>'select','options'=>$startTimes,'default'=>'07:30','required'=>true)); ?>
		</div>
		<div class="col1">
			<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.INDEX.end_time", array('label'=>false,'type'=>'select','options'=>$endTimes,'default'=>'16:30','required'=>true)); ?>
		</div>
		<div class="col1">
			<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.INDEX.rate_ph", array('label'=>false,'type'=>'text','required'=>true,'class'=>'currency rate_ph','style'=>'width: 100% !important')); ?>
		</div>
		<div class="col2 checkbox-and-input-container">
			<input type="checkbox" class="checkbox-to-enable-input" onclick="enableInputRelatedToCheckbox(this)" />
			<?php echo $this->Form->input("AgencyEmployeesAgencyTimesheet.INDEX.break_hours", array('label'=>false,'type'=>'text','class'=>'input-enabled-by-checkbox','data-default-value'=>'1.0')); ?>
		</div>
		<div class="col2">
			<a href="#" class="button button-secondary" onclick="return deleteAgencyTimesheetEntry(this)">Remove</a>
		</div>
	</div>
</script>
