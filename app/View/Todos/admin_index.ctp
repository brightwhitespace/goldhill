<div class="content todo-list">
	<div class="group">
		<div class="col3">
			<h1>To Dos</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/todos', 'class'=>'filter-form clearfix')); ?>
			<div class="col2 right">
				<?php if ($userIsAdmin): ?>
					<?php echo $this->Form->input('user_id', array('label'=>false,'type'=>'select','options'=>$users,'empty'=>'All Users','onchange'=>'this.form.submit()')); ?>
				<?php endif; ?>
				&nbsp;
			</div>
			<div class="col2 right">
				<?php echo $this->Form->input('archived', array('label'=>false,'type'=>'select','options'=>array(''=>'Current Tasks', 1=>'Archived Tasks'),'onchange'=>'this.form.submit()')); ?>
			</div>
			<div class="col2 right">
				<a href="/admin/todos/add" class="button button-primary">Add To Do</a>
			</div>
			<div class="col3">
				<?php
					echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
					echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
				?>
				<?php if(!empty($this->request->data['Search'])): ?>
					<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
				<?php endif; ?>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="group" style="margin-top: 20px">
		<div class="col7">
			<h2>Assigned Tasks</h2>
			<table class="todo">
				<tr>
					<th><?php echo $this->Paginator->sort('completed_date', 'Done?');?></th>
					<th><?php echo $this->Paginator->sort('due_date', 'Due');?></th>
					<th><?php echo $this->Paginator->sort('description');?></th>
					<th><?php echo $this->Paginator->sort('user_id', 'Assigned To');?></th>
					<th style="width: 20px"><?php echo $this->Paginator->sort('created_by_user_id', '<i class="fa fa-user-plus"></i>', array('escape'=>false, 'title'=>'Created By'));?></th>
					<th class="actions"></th>
				</tr>
				<?php
					$i = 0;
					foreach ($todos as $todo):
						$class = '';
						if ($i++ % 2 == 0) {
							//$class = 'altrow';
						}

						// add class for status
						$class .= ' todo-' . ($todo['Todo']['completed_date'] ? 'completed' : ($todo['Todo']['in_progress_by_user_id'] ? 'in-progress' : 'pending'));

						// add class for priority
						$class .= ' todo-priority-' . $todo['Todo']['priority'];

						if (!$todo['Todo']['completed_date']) {

							// add class for due date
							if (!empty($todo['Todo']['due_date_Y-m-d'])) {
								if ($todo['Todo']['due_date_Y-m-d'] < date('Y-m-d')) {
									$class .= ' todo-overdue';
								} else if ($todo['Todo']['due_date_Y-m-d'] == date('Y-m-d')) {
									$class .= ' todo-due-today';
								}
							}
						}
						?>
						<tr class="<?php echo $class;?>" data-todo-id="<?php echo $todo['Todo']['id']?>">

							<td class="todo-status">
								<i class="fa fa-exclamation-circle todo-exclamation"></i>
								<a href="#" class="todo-status-icon todo-status-icon-pending" title="Mark as COMPLETED"><i class="fa fa-square-o fa-2x"></i></a>
								<a href="#" class="todo-status-icon todo-status-icon-in-progress" title="In Progress by <?php echo !empty($todo['InProgressByUser']['id']) ? $this->App->getFullName($todo['InProgressByUser']) : '' ?> - click to COMPLETE">
									<i class="fa fa-caret-square-o-right fa-2x"></i>
								</a>
								<a href="#" class="todo-status-icon todo-status-icon-completed" title="Completed by <?php echo !empty($todo['CompletedByUser']['id']) ? $this->App->getFullName($todo['CompletedByUser']) : '' ?> - click to mark as PENDING">
									<i class="fa fa-check-square-o fa-2x"></i>
								</a>
							</td>
							<td class="todo-due-date"><?php echo $todo['Todo']['due_date'] ? date('j/n/y', strtotime($todo['Todo']['due_date'])) : ''; ?></td>
							<td class="todo-description"><?php echo $todo['Todo']['description']; ?>&nbsp;</td>
							<td class="todo-assigned-to"><?php echo $todo['User']['name']; ?>&nbsp;</td>
							<td class="todo-created-by"><?php echo !empty($todo['CreatedByUser']['id']) ? $this->App->getInitials($this->App->getFullName($todo['CreatedByUser'])) : ''; ?>&nbsp;</td>

							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $todo['Todo']['id']),array('escape'=>false)); ?>
								<?php if ($todo['Todo']['archived']): ?>
									<a href="#" class="todo-undo-archive-button archiveButton"><i class="fa fa-undo fa-2x"></i></a>
								<?php else: ?>
									<a href="#" class="todo-archive-button archiveButton"><i class="fa fa-archive fa-2x"></i></a>
								<?php endif; ?>
							</td>
						</tr>
				<?php endforeach; ?>

				<?php if (!$todos) : ?>
					<tr>
						<td colspan="6">No records found</td>
					</tr>
				<?php endif; ?>
			</table>

			<div class="pagination group">
				<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
				<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
			</div>
		</div>


		<div class="col5">
			<div style="padding-left: 20px">
				<h2>Unassigned Tasks</h2>
				<table class="todo" style="">
					<tr>
						<th>Done?</th>
						<th>Due</th>
						<th>Description</th>
						<th style="width: 20px"><?php echo $this->Paginator->sort('created_by_user_id', '<i class="fa fa-user-plus"></i>', array('escape'=>false, 'title'=>'Created By'));?></th>
						<th class="actions"></th>
					</tr>
					<?php
					$i = 0;
					foreach ($unassignedTasks as $todo):
						$class = '';
						if ($i++ % 2 == 0) {
							//$class = 'altrow';
						}

						// add class for status
						$class .= ' todo-' . ($todo['Todo']['completed_date'] ? 'completed' : ($todo['Todo']['in_progress_by_user_id'] ? 'in-progress' : 'pending'));

						// add class for priority
						$class .= ' todo-priority-' . $todo['Todo']['priority'];

						if (!$todo['Todo']['completed_date']) {

							// add class for due date
							if (!empty($todo['Todo']['due_date_Y-m-d'])) {
								if ($todo['Todo']['due_date_Y-m-d'] < date('Y-m-d')) {
									$class .= ' todo-overdue';
								} else if ($todo['Todo']['due_date_Y-m-d'] == date('Y-m-d')) {
									$class .= ' todo-due-today';
								}
							}
						}
						?>
						<tr class="<?php echo $class;?>" data-todo-id="<?php echo $todo['Todo']['id']?>">

							<td class="todo-status">
								<i class="fa fa-exclamation-circle todo-exclamation"></i>
								<a href="#" class="todo-status-icon todo-status-icon-pending" title="Mark as COMPLETED"><i class="fa fa-square-o fa-2x"></i></a>
								<a href="#" class="todo-status-icon todo-status-icon-in-progress" title="In Progress by <?php echo !empty($todo['InProgressByUser']['id']) ? $this->App->getFullName($todo['InProgressByUser']) : '' ?> - click to COMPLETE">
									<i class="fa fa-caret-square-o-right fa-2x"></i>
								</a>
								<a href="#" class="todo-status-icon todo-status-icon-completed" title="Completed by <?php echo !empty($todo['CompletedByUser']['id']) ? $this->App->getFullName($todo['CompletedByUser']) : '' ?> - click to mark as PENDING">
									<i class="fa fa-check-square-o fa-2x"></i>
								</a>
							</td>
							<td class="todo-due-date"><?php echo $todo['Todo']['due_date'] ? date('j/n/y', strtotime($todo['Todo']['due_date'])) : ''; ?></td>
							<td class="todo-description"><?php echo $todo['Todo']['description']; ?>&nbsp;</td>
							<td class="todo-created-by"><?php echo !empty($todo['CreatedByUser']['id']) ? $this->App->getInitials($this->App->getFullName($todo['CreatedByUser'])) : ''; ?>&nbsp;</td>

							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $todo['Todo']['id']),array('escape'=>false)); ?>
								<?php if ($userIsAdmin): ?>
									<?php if ($todo['Todo']['archived']): ?>
										<a href="#" class="todo-undo-archive-button archiveButton"><i class="fa fa-undo fa-2x"></i></a>
									<?php else: ?>
										<a href="#" class="todo-archive-button archiveButton"><i class="fa fa-archive fa-2x"></i></a>
									<?php endif; ?>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>

					<?php if (!$unassignedTasks) : ?>
						<tr>
							<td colspan="6">No records found</td>
						</tr>
					<?php endif; ?>
				</table>
			</div>
		</div>
	</div>

</div>	


