<div class="content">
	<h2>Edit Todo</h2>
	
	<?php echo $this->Form->create('Todo',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('id');
			// allow edit of "created by user" if not previously set
			echo empty($this->request->data['Todo']['created_by_user_id']) ? $this->Form->input('created_by_user_id',array('label'=>'Created By','type'=>'select', 'options'=>$users, 'empty'=>$emptyUserOption)) : '';
			echo $this->Form->input('description',array('label'=>'Description','type'=>'textarea','style'=>'height: 150px'));
			echo $this->Form->input('user_id',array('label'=>'Assign To','type'=>'select', 'options'=>$users, 'empty'=>$emptyUserOption));
			echo $this->Form->input('due_date',array('label'=>'Due Date','type'=>'text','class'=>'datepicker','style'=>'width: 50%'));
			echo $this->Form->input('priority',array('label'=>'Priority','type'=>'select', 'options'=>Todo::getPriorities(),'style'=>'width: 50%'));
			echo $this->Form->input('status',array('label'=>'Status','type'=>'select','options'=>Todo::getStatuses(),'style'=>'width: 50%'));
			?>
		</div>
		<div class="col6">
		</div>
	</div>
	<div class="group">
		<div class="col6">
			<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>

</div>
