<div id="page-title">
	<div class="container clearfix">
		
		<h1>Add Post Category</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
<?php echo $this->Form->create('PostCategory',array('class'=>'cmxform'));?>
	<fieldset>
 		<legend><?php echo __('Add Post Category');?></legend>
		<ol>
			<?php
			echo $this->Form->input('name',array('label'=>'Category Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false,'onchange'=>"updateUrlField('PostCategoryUrl','PostCategoryName')"));
			echo $this->Form->input('url',array('label'=>'Category Url<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Form->input('displayed', array('type'=>'checkbox','label'=>'Display in menu?','class'=>'checkbox','before'=>'<li class="tickbox">','after'=>'</li>','div'=>false));
			echo $this->Ckeditor->input('body',array('label'=>'','type'=>'textarea','class'=>'CKEDITOR','before'=>'<li>','after'=>'</li>','div'=>false));
			?>
		</ol>
	</fieldset>
<?php echo $this->Form->button('Save Category', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
	<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
<?php echo $this->Form->end();?>

</div>
</div>
