
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/post_categories/order">Re-order Categories</a></li>
			<li><a href="/admin/post_categories/add">New Category</a></li>
		</ul>
		<h1>Post Categories</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->Paginator->sort('id');?></th>
	<th><?php echo $this->Paginator->sort('name');?></th>
	<th><?php echo $this->Paginator->sort('displayed', 'Display in menu?');?></th>
	<th><?php echo $this->Paginator->sort('modified');?></th>
	<th class="actions"><?php echo __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($post_categories as $post_category):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $post_category['PostCategory']['id']; ?>
		</td>
		<td>
			<?php echo $post_category['PostCategory']['name']; ?>
		</td>
		<td>
			<?php echo $this->Brightform->displayTrueOrFalse($post_category['PostCategory']['displayed']); ?>
		</td>
		<td>
			<?php echo $this->Time->timeAgoInWords($post_category['PostCategory']['modified']); ?>
		</td>
		<td class="actions">
			<?php //echo $this->Html->link($this->Html->image('icon_order.gif'), array('action'=>'order'),null,null,false); ?>
			<?php echo $this->Html->link('EDIT', array('action'=>'edit', $post_category['PostCategory']['id']),array('escape'=>false),null,false); ?>
			<?php echo $this->Html->link('DELETE', array('action'=>'delete', $post_category['PostCategory']['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?'), $post_category['PostCategory']['id']),false); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
<div class="pagination clearfix">
	<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
	<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
	<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
</div>

</div>
</div>