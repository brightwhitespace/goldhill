
<div id="page-title">
	<div class="container clearfix">
		
		<h1>Re-order Projects</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<p>Drag and drop the categories to change the order</p>
		<div id="ajax-status">order saved</div>
		
		<ol class="sortable" id="sortable_1">
		<?php
		$i = 0;
		foreach ($categories as $category):
			
		?>
			
			<li id="postcategory_<?php echo $category['PostCategory']['id']; ?>">
				<div><?php echo $category['PostCategory']['name']; ?></div>
			</li>
				
		<?php endforeach; ?>
		</ol>
		
		
		
		<?php echo $this->Form->button('save order', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton saveOrder','id'=>'saveOrder_post_categories')); ?>
		
		<?php echo $this->Html->link('Back',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		
	</div>
</div>