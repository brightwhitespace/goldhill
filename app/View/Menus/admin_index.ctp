<?php 
$this->Html->addCrumb('admin','/admin/');
$this->Html->addCrumb('menus','/admin/menus'); 
?>
<div id="breadcrumbs"><?php echo $this->Html->getCrumbs(' &raquo; '); ?></div>
	<h2><?php echo __('Menus');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($menus as $menu):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $menu['Menu']['id']; ?>&nbsp;</td>
		<td><?php echo $menu['Menu']['name']; ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($menu['Menu']['created']); ?>&nbsp;</td>
		<td><?php echo $this->Time->timeAgoInWords($menu['Menu']['modified']); ?>&nbsp;</td>
		<td class="actions">			
			<?php echo $this->Html->link($this->Html->image('/assets/admin/icon_edit.png'), array('action' => 'edit', $menu['Menu']['id']),array('escape'=>false)); ?>
			<?php echo $this->Html->link($this->Html->image('/assets/admin/icon_delete.png'), array('action' => 'delete', $menu['Menu']['id']), array('escape'=>false), sprintf(__('Are you sure you want to delete # %s?'), $menu['Menu']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>