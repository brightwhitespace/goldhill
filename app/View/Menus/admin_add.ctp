<?php 
$this->Html->addCrumb('admin','/admin/');
$this->Html->addCrumb('menus','/admin/menus');
$this->Html->addCrumb('add','/admin/menus/add'); 
?>
<div id="breadcrumbs"><?php echo $this->Html->getCrumbs(' &raquo; '); ?></div>
<h2><?php echo __('Add Menu');?></h2>

<?php echo $this->Form->create('Menu',array('class'=>'cmxform'));?>
	<fieldset>
 		<legend><?php echo __('Admin Add Menu'); ?></legend>
	<?php
		echo $this->Form->input('name',array('label'=>'Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Save'));?>

