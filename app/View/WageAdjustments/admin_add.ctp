<div class="content">
	<h2>Add Wage Adjustment</h2>

	<?php echo $this->Form->create('WageAdjustment',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('date', array('label'=>'Date<em>*</em>','type'=>'text','class'=>'datepicker'));
			echo $this->Form->input('value', array('label'=>'Value<em>*</em> (enter minus value for a deduction)','type'=>'number','class'=>'currency'));
			echo $this->Form->input('description', array('label'=>'Description','type'=>'text'));
			echo $this->Form->input('is_damage_repayment', array('label'=>'Damage Repayment? <em>(N.B. this will NOT be deducted from the current damage value on save)</em>','type'=>'checkbox'));
			?>
		</div>

	</div>
	<div class="group" style="margin-top: 20px">
			<div class="col6">
				<?php echo $this->Html->link('Cancel', array('controller' => 'employees', 'action'=>'view', $employeeId),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>

</div>
