
<div id="page-title">
	<div class="container clearfix">
		
		<h1>Add Photo</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
	<?php echo $this->Form->create('Photo', array('class'=>'cmxform') );?>
	<fieldset>
		<ol>
		<?php
			echo $this->Form->input('product_id',array('type'=>'hidden'));
			echo $this->Form->input('project_id',array('type'=>'hidden'));
			echo $this->Form->input('gallery_id',array('type'=>'hidden'));
			echo $this->Ckeditor->browseField('image_thumb',array('id'=>'PhotoImageThumb','type'=>'text','class'=>'textbox','label'=> 'Browse for an thumbnail','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Ckeditor->browseField('image_src',array('id'=>'PhotoImageSrc','type'=>'text','class'=>'textbox','label'=> 'Browse for an medium image','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Ckeditor->browseField('image_enlargement',array('id'=>'PhotoImageEnlargement','type'=>'text','class'=>'textbox','label'=> 'Browse for an enlargement','before'=>'<li>','after'=>'</li>','div'=>false));
		?>
		</ol>
	</fieldset>
	<?php echo $this->Form->button('Save Photo', array('name'=>'add_photo','type'=>'submit','value'=>'add_photo','class'=>'submitButton')); ?>
	
	<?php echo $this->Form->end();?>
	</div>
</div>