
<div class="group" style="position: relative">
	<div class="col6">
		<h1 style="position: absolute; top: -130px; left: 320px">Client Area</h1>
		<h1><?php echo $currentUser['Client']['name'] ?></h1>
	</div>
	<div class="col6 right">
		<a href="/clients/home" class="button button-primary">View Projects</a> &nbsp;
		<a href="/clients/invoices?clear=1" class="button button-primary">View Invoices</a>
	</div>
</div>

<div class="content">
	<div class="group">
		<?php echo $this->Form->create('Search', array('url' => '/clients/invoices', 'class'=>'filter-form clearfix')); ?>
		<div class="col2">
			<h1>Invoices</h1>
		</div>
		<div class="col5">
			<?php if (!empty($project)): ?>
				<h2><?php echo $project['Project']['name'] ?></h2>
			<?php endif; ?>
			&nbsp;
		</div>
		<div class="col2 right">
			<?php echo count($contactList) > 1 ? $this->Form->input('contact_id', array('label'=>false, 'type'=>'select', 'options'=>$contactList, 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by contact...')) : '&nbsp;'; ?>
		</div>
		<div class="col3">
			<?php
				echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
				echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if(!empty($this->request->data['Search'])): ?>
			<?php echo $this->Html->link('Clear search', array('action' => 'invoices', '?' => array('clear'=>1))); ?>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
		
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('invoice_num');?></th>
			<th><?php echo $this->Paginator->sort('invoice_date');?></th>
			<th style="width: 16%"><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('ref', 'Reference');?></th>
			<th><?php echo $this->Paginator->sort('total', 'Total (ex VAT)');?></th>
			<th><?php echo $this->Paginator->sort('invoice_status_id');?></th>
			<th><?php echo $this->Paginator->sort('email_sent', 'Date Sent');?></th>
			<th style="width: 60px" class="actions"></th>
		</tr>
		<?php
			$i = 0;
			foreach ($invoices as $invoice):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($invoice['Invoice']['invoice_num'], array('controller' => 'clients', 'action' => 'view_invoice', $invoice['Invoice']['id'])); ?></td>
				<td><?php echo $invoice['Invoice']['invoice_date']; ?></td>
				<td><?php echo $invoice['Project']['name']; ?></td>

				<td><?php echo $invoice['Invoice']['ref']; ?></td>
				<td><?php echo $this->Number->currency($invoice['Invoice']['total'],'GBP'); ?></td>
				<td style="white-space: nowrap">
					<i class="fa <?php echo $invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_DRAFT ? 'fa-question fa-lg' : ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_APPROVED ? 'fa-check fa-lg' : 'fa-envelope-o') ?>"></i>
					<?php echo $invoice['InvoiceStatus']['name']; ?>
				</td>
				<td><?php echo $invoice['Invoice']['email_sent'] ? date('j M Y', strtotime($invoice['Invoice']['email_sent'])) : ''; ?></td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('action' => 'view_invoice', $invoice['Invoice']['id']),array('escape'=>false)); ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if( !$invoices ) : ?>
			<tr>
				<td colspan="9">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>	


