<div class="content">
	<div class="content-head group">
		<div class="col8">
			<h1><?php echo $client['Client']['name']; ?> <?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $client['Client']['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $client['Client']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $client['Client']['name'])); ?></h1>
		</div>
		<div class="col4 right">
			<a href="/admin/projects/add/<?php echo $client['Client']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Project</a>
		</div>
	</div>
	<div class="group">
			<div class="col3">
				<h3>Contact Details</h3>
				<div class="box">
					<p>
					<?php if(!empty($client['Client']['email'])): ?>
					<a href="mailto:<?php echo $client['Client']['email']; ?>"><?php echo $client['Client']['email']; ?></a><br>
					<?php endif; ?>
					<?php echo $client['Client']['tel']; ?>
					</p>
					<p>
					<?php echo !empty($client['Client']['address1']) ? $client['Client']['address1'].'<br>' : ''; ?>
					<?php echo !empty($client['Client']['address2']) ? $client['Client']['address2'].'<br>' : ''; ?>
					<?php echo !empty($client['Client']['address3']) ? $client['Client']['address3'].'<br>' : ''; ?>
					<?php echo !empty($client['Client']['city']) ? $client['Client']['city'].'<br>' : ''; ?>
					<?php echo !empty($client['Client']['postcode']) ? $client['Client']['postcode'] : ''; ?>
					</p>
				</div>
				
				<h3>Project Managers</h3>
				<table>
					<?php foreach($client['Contact'] as $contact): ?>
					<tr class="bg">
						<td><a href="/admin/contacts/edit/<?php echo $contact['id']; ?>"><?php echo $contact['name']; ?></a></td>
						<td><?php echo $contact['tel']; ?></td>
						<td><?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('controller' => 'contacts', 'action' => 'delete', $contact['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $contact['name'])); ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
				<a href="/admin/contacts/add/client_id:<?php echo $client['Client']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add</a>
			</div>
			<div class="col9">
				
				<h3>Recent Projects</h3>
				<table>
					<tr>
						<th>Name</th>
						<th>Client</th>
						<?php if($userIsAdmin): ?>
						<th>Value</th>
						<th>Profit</th>
						<?php endif; ?>
						<th>Added</th>
						<th width="150" class="actions"></th>
					</tr>
					<?php
						$i = 0;
						foreach ($projects as $project):
							$class = null;
							if ($i++ % 2 == 0) {
								$class = ' class="altrow"';
							}
						?>
						<tr<?php echo $class;?>>
							
							<td><?php echo $this->Html->link($project['Project']['name'], array('controller' => 'projects', 'action' => 'view', $project['Project']['id'])); ?>&nbsp;</td>
							<td><?php echo $project['Client']['name']; ?>&nbsp;</td>
							<?php if($userIsAdmin): ?>
							<td><?php echo $this->Number->currency($project['Project']['value'],'GBP'); ?>&nbsp;</td>
							<td><?php echo isset($project['Project']['profit']) ? $project['Project']['profit'].'%' : ''; ?></td>
							<?php endif; ?>
							<td><?php echo $this->Time->niceShort($project['Project']['created']); ?>&nbsp;</td>
							
							<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller' => 'projects', 'action' => 'edit', $project['Project']['id']),array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller' => 'projects', 'action' => 'delete', $project['Project']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $project['Project']['name'])); ?>
							<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
					
					<?php if( !$projects ) : ?>
						<tr>
							<td colspan="6">No projects found</td>
						</tr>
					<?php endif; ?>
				</table>
				<a href="/admin/projects/index/client_id:<?php echo $client['Client']['id']; ?>" class="button button-primary">See All</a>
				
		</div>
		
	</div>
	
</div>	


