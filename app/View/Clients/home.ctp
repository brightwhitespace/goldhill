
<div class="group" style="position: relative">
    <div class="col6">
        <h1 style="position: absolute; top: -130px; left: 320px">Client Area</h1>
        <h1><?php echo $currentUser['Client']['name'] ?></h1>
    </div>
    <div class="col6 right">
        <a href="/clients/home" class="button button-primary">View Projects</a> &nbsp;
        <a href="/clients/invoices" class="button button-primary">View Invoices</a>
    </div>
</div>

<div class="content">
    <div class="group">
        <?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
        <div class="col3">
            <h1>Projects</h1>
        </div>
        <div class="col2 right">
            <?php echo count($contactList) > 1 ? $this->Form->input('contact_id', array('label'=>false, 'type'=>'select', 'options'=>$contactList, 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by contact...')) : '&nbsp;'; ?>
        </div>
        <div class="col2 right">
            <?php echo empty($archive) ? $this->Form->input('project_status', array('label'=>false, 'type'=>'select', 'options'=>$projectStatusGroupList, 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by status...')) : '&nbsp;'; ?>
        </div>
        <div class="col2 right">
            <?php echo $this->Form->input('project_type_id', array('label'=>false, 'type'=>'select', 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by type...')); ?>
        </div>
        <div class="col3">
            <?php
            echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
            echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
            ?>
            <?php if (!empty($this->request->data['Search'])): ?>
                <?php echo $this->Html->link('Clear search', array('action' => 'home', '?' => array('clear'=>1))); ?>
            <?php endif; ?>
        </div>
        <?php echo $this->Form->end(); ?>

    </div>

    <table class="cols-align-top">
        <tr>
            <th><?php echo $this->Paginator->sort('start_date_Ymd', 'Start date');?></th>
            <th><?php echo $this->Paginator->sort('end_date_Ymd', 'End date');?></th>
            <th><?php echo $this->Paginator->sort('name');?></th>
            <th><?php echo $this->Paginator->sort('description');?></th>
            <th><?php echo $this->Paginator->sort('address1', 'Address');?></th>
            <th><?php echo $this->Paginator->sort('project_type_id');?></th>
            <th><?php echo $this->Paginator->sort('project_status_id', 'Status');?></th>
            <th width="60"><?php echo $this->Paginator->sort('created');?></th>
            <th width="50" class="actions"></th>
        </tr>
        <?php
        $i = 0;
        foreach ($projects as $project):
            $class = null;
        ?>
            <tr<?php echo $class;?>>

                <td style="width: 120px"><?php echo $project['Project']['start_date_Ymd'] ? date('j M Y', strtotime($project['Project']['start_date_Ymd'])) : '' ?></td>
                <td style="width: 120px"><?php echo $project['Project']['end_date_Ymd'] ? date('j M Y', strtotime($project['Project']['end_date_Ymd'])) : '' ?></td>
                <td style="width: 15%"><?php echo $project['Project']['name'] ?></td>
                <td style="width: 20%"><?php echo $project['Project']['description']  . ($project['Project']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($project['Project']['notes']) . '</div>' : ''); ?></td>
                <td style="width: 20%"><?php echo $this->App->formatAddress($project['Project'], ', '); ?></td>
                <td style="width: 100px"><?php echo $project['ProjectType']['name']; ?></td>
                <?php
                // If project status is 'Holding' then change it to 'Completed'
                $projectStatus = $project['ProjectStatus']['name'] == 'Holding' ? 'Completed' : $project['ProjectStatus']['name'];
                ?>
                <td style="width: 100px"><span class="<?php echo $projectStatus; ?>"><?php echo $projectStatus; ?></span></td>
                <td><?php echo date('j/n/y', strtotime($project['Project']['created'])); ?></td>
                <td class="actions">
                    <?php
                    $gotSentInvoice = false;
                    foreach ($project['Invoice'] as $invoice):
                        $gotSentInvoice = $gotSentInvoice || $invoice['invoice_status_id'] == Invoice::STATUS_SENT;
                    endforeach;
                    if ($gotSentInvoice):
                    ?>
                        <a href="/clients/invoices/<?php echo $project['Project']['id'] ?>" title="View Invoices"><i class="fa fa-file-o fa-2x"></i></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php if (!$projects): ?>
            <tr>
                <td colspan="8">No records found</td>
            </tr>
        <?php endif; ?>
    </table>
    <div class="pagination group">
        <?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
        <?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
        <?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
    </div>
</div>




<?php /*
<div class="content-group group">
    <div class="col12">
        <div class="content">
            <h3>Projects</h3>
            <table class="cols-align-top">
                <tr>
                    <th style="width: 100px">Start date</th>
                    <th width="20%">Name</th>
                    <th width="30%">Description</th>
                    <th width="30%">Address</th>
                    <th style="width: 100px">Status</th>
                    <th width="50" class="actions"></th>
                </tr>
                <?php
                $i = 0;
                foreach ($projects as $project):
                    $class = null;
                    ?>
                    <tr<?php echo $class;?>>
                        <td><?php echo $this->Time->format('d/m/Y',$project['Project']['start_date']); ?>&nbsp;</td>
                        <td><?php echo $this->Html->link($project['Project']['name'], array('controller' => 'projects', 'action' => 'view', $project['Project']['id'])); ?>&nbsp;</td>
                        <td><?php echo $project['Project']['description']  . ($project['Project']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($project['Project']['notes']) . '</div>' : ''); ?></td>
                        <td><?php echo $this->App->formatAddress($project['Project'], ', '); ?></td>
                        <td><span class="<?php echo $project['ProjectStatus']['name']; ?>"><?php echo $project['ProjectStatus']['name']; ?></span></td>
                        <td class="actions">
                            <a href="/clients/invoices/<?php echo $project['Project']['id'] ?>" title="View Invoices"><i class="fa fa-file-o fa-2x"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>

                <?php if( !$projects ) : ?>
                    <tr>
                        <td colspan="8">No projects found</td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>

    <div class="col12">
        <div class="content">
            <h3>Day Works</h3>
            <table class="cols-align-top">
                <tr>
                    <th style="width: 100px">Start date</th>
                    <th width="20%">Name</th>
                    <th width="30%">Description</th>
                    <th width="30%">Address</th>
                    <th style="width: 100px">Status</th>
                    <th width="50" class="actions"></th>
                </tr>
                <?php
                $i = 0;
                foreach ($projectsDayWorks as $project):
                    $class = null;
                    ?>
                    <tr<?php echo $class;?>>
                        <td><?php echo $this->Time->format('d/m/Y',$project['Project']['start_date']); ?>&nbsp;</td>
                        <td><?php echo $this->Html->link($project['Project']['name'], array('controller' => 'projects', 'action' => 'view', $project['Project']['id'])); ?>&nbsp;</td>
                        <td><?php echo $project['Project']['description']  . ($project['Project']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($project['Project']['notes']) . '</div>' : ''); ?></td>
                        <td><?php echo $this->App->formatAddress($project['Project'], ', '); ?></td>
                        <td><span class="<?php echo $project['ProjectStatus']['name']; ?>"><?php echo $project['ProjectStatus']['name']; ?></span></td>
                        <td class="actions">
                            <a href="/clients/invoices/<?php echo $project['Project']['id'] ?>" title="View Invoices"><i class="fa fa-file-o fa-2x"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>

                <?php if (!$projectsDayWorks): ?>
                    <tr>
                        <td colspan="9">No projects found</td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
*/ ?>
