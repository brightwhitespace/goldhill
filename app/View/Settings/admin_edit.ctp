<div id="page-title">
	<div class="container clearfix">
		<h1>Edit Setting</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<?php echo $this->Form->create('Setting',array('class'=>'cmxform'));?>
			<fieldset>
				<legend><?php echo __('Admin Edit Setting'); ?></legend>
			<?php
			echo $this->Form->input('id');
				echo $this->Form->input('name',array('label'=>'Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
				echo $this->Form->input('value',array('label'=>'Value<em>*</em>','type'=>'textarea','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
			?>
			</fieldset>
			<?php echo $this->Form->button('Save Setting', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
			<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		<?php echo $this->Form->end();?>
	</div>
</div>
