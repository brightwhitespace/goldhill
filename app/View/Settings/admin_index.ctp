<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/settings/add">Add Setting</a></li>
		</ul>
		<h1>Settings</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th><?php echo $this->Paginator->sort('id');?></th>
				<th><?php echo $this->Paginator->sort('name');?></th>
				<th><?php echo $this->Paginator->sort('value');?></th>
				<th><?php echo $this->Paginator->sort('created');?></th>
				<th><?php echo $this->Paginator->sort('modified');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($settings as $setting):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
				<tr<?php echo $class;?>>
					<td><?php echo $setting['Setting']['id']; ?>&nbsp;</td>
					<td><?php echo h($setting['Setting']['name']); ?>&nbsp;</td>
					<td><?php echo nl2br(h($setting['Setting']['value'])); ?>&nbsp;</td>
					<td><?php echo $this->Time->niceShort($setting['Setting']['created']); ?>&nbsp;</td>
					<td><?php echo $this->Time->timeAgoInWords($setting['Setting']['modified']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link('EDIT', array('action' => 'edit', $setting['Setting']['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('DELETE', array('action' => 'delete', $setting['Setting']['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?'), $setting['Setting']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>

		<div class="pagination clearfix">
			<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
			<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
		</div>

	</div>
</div>