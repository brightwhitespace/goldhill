<div class="content">
	<h2>Add Timesheet &nbsp;(<?php echo $this->request->data['Project']['name'] ?>)</h2>

	<?php echo $this->Form->create('Timesheet');?>
	<div class="group">
		<div class="col3">
			<?php
			echo $this->Form->input('date', array('label'=>'Timesheet Date<em>*</em>','type'=>'text','class'=>'datepicker','style'=>'width: 100% !important'));
			?>
		</div>
		<div class="col9">

		</div>
	</div>

	<div class="group">
		<div class="col12">
			<h3 style="margin: 20px 0">Timesheet Entries</h3>
		</div>
	</div>

	<div class="group">
		<div class="col3">Employee<em>*</em></div>
		<div class="col2">Start time<em>*</em></div>
		<div class="col2">End time<em>*</em></div>
		<div class="col1">Rate p.h.<em>*</em></div>
		<div class="col2">Break? (hours)</div>
		<div class="col2"></div>
	</div>

	<div class="group">
		<div class="col12"><hr style="margin: 10px 0 20px"></div>
	</div>

	<div id="timesheet-entries-container">
		<?php foreach ($this->request->data['EmployeesTimesheet'] as $idx => $timesheetEntry): ?>

			<div class="timesheet-entry group">
				<div class="col3">
					<div class="input select">
						<select name="data[EmployeesTimesheet][<?php echo $idx ?>][employee_id]" onchange="handleTimesheetEmployeeChange(this)">
							<option value="">Select employee...</option>
							<?php foreach ($employees as $employee): ?>
								<option <?php echo $employee['Employee']['id'] == $timesheetEntry['employee_id'] ? 'selected' : ''; ?> value="<?php echo $employee['Employee']['id']; ?>" data-rate-ph="<?php echo $employee['Employee']['rate_ph']; ?>">
									<?php echo $employee['Employee']['name']; ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col2">
					<?php echo $this->Form->input("EmployeesTimesheet.$idx.start_time", array('label'=>false,'type'=>'select','options'=>$startTimes,'default'=>'07:30')); ?>
				</div>
				<div class="col2">
					<?php echo $this->Form->input("EmployeesTimesheet.$idx.end_time", array('label'=>false,'type'=>'select','options'=>$endTimes,'default'=>'16:30')); ?>
				</div>
				<div class="col1">
					<?php echo $this->Form->input("EmployeesTimesheet.$idx.rate_ph", array('label'=>false,'type'=>'text','class'=>'currency rate_ph','style'=>'width: 100% !important')); ?>
				</div>
				<div class="col2 checkbox-and-input-container">
					<input type="checkbox" class="checkbox-to-enable-input" <?php echo !empty($timesheetEntry['break_hours']) ? 'checked' : '' ?> onclick="enableInputRelatedToCheckbox(this)" />
					<?php echo $this->Form->input("EmployeesTimesheet.$idx.break_hours", array('label'=>false,'type'=>'text','class'=>'input-enabled-by-checkbox','data-default-value'=>'1.0')); ?>
				</div>
				<div class="col1">
					<a href="#" class="button button-secondary" onclick="return deleteTimesheetEntry(this)">Remove</a>
				</div>
			</div>

		<?php endforeach; ?>
	</div>

	<div class="group">
		<div class="col12">
			<p style="margin: 5px 0 40px">
				<a href="#" class="button button-primary" onclick="return addTimesheetEntry()">Add new entry</a>
			</p>
		</div>
	</div>

	<div class="group">
		<div class="col6">
			<a href="<?php echo $returnUrl ?>" class="button button-secondary">Cancel</a>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>

</div>


<?php // HTML template for adding timesheet entries ?>
<script id="timesheet-entry-template" type="text/html">
	<div class="timesheet-entry group">
		<div class="col3">
			<div class="input select required">
				<select name="data[EmployeesTimesheet][INDEX][employee_id]" required="required" onchange="handleTimesheetEmployeeChange(this)">
					<option value="">Select employee...</option>
					<?php foreach ($employees as $employee): ?>
						<option value="<?php echo $employee['Employee']['id']; ?>" data-rate-ph="<?php echo $employee['Employee']['rate_ph']; ?>">
							<?php echo $employee['Employee']['name']; ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col2">
			<?php echo $this->Form->input("EmployeesTimesheet.INDEX.start_time", array('label'=>false,'type'=>'select','options'=>$startTimes,'default'=>'07:30','required'=>true)); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input("EmployeesTimesheet.INDEX.end_time", array('label'=>false,'type'=>'select','options'=>$endTimes,'default'=>'16:30','required'=>true)); ?>
		</div>
		<div class="col1">
			<?php echo $this->Form->input("EmployeesTimesheet.INDEX.rate_ph", array('label'=>false,'type'=>'text','required'=>true,'class'=>'currency rate_ph','style'=>'width: 100% !important')); ?>
		</div>
		<div class="col2 checkbox-and-input-container">
			<input type="checkbox" class="checkbox-to-enable-input" onclick="enableInputRelatedToCheckbox(this)" />
			<?php echo $this->Form->input("EmployeesTimesheet.INDEX.break_hours", array('label'=>false,'type'=>'text','class'=>'input-enabled-by-checkbox','data-default-value'=>'1.0')); ?>
		</div>
		<div class="col2">
			<a href="#" class="button button-secondary" onclick="return deleteTimesheetEntry(this)">Remove</a>
		</div>
	</div>
</script>
