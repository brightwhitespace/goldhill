<?php echo $this->Html->docType('xhtml-trans'); ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php
    echo '<link rel="stylesheet" href="/app/webroot/assets/style/normalize.css" />';
    echo '<link rel="stylesheet" href="/app/webroot/assets/style/reports.css" />';
    ?>

    <?php echo isset($meta_for_layout['head']) ? $meta_for_layout['head'] : ''; ?>
</head>

<body>

<style>
    body { font-family: Helvetica, Arial, sans-serif; }
</style>

<div class="pdf-report">

    <?php echo $content_for_layout ?>

</div>

</body>

</html>