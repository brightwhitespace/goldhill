<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title_for_layout; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="robots" content="all" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<?php 
	
	$this->AdminCombinator->add_libs('js', array(
		'jquery-1.8.0.min',
		'jquery-ui-1.8.23.custom.min',
		'jquery.mjs.nestedSortable',
		'jquery.multiupload',
		'jquery.jeditable',
		'jquery.cookie',
		'brightedit'
	));
	echo $this->AdminCombinator->scripts('js');
	
	
	echo isset($this->Ckeditor) ? $this->Ckeditor->link() : '';
	
	?>
	
	
	<?php
	$this->Combinator->add_libs('css', array(			
		'normalize',
		'base',
		'font-awesome.min',
		'main',
		'reports'
	));
	echo $this->Combinator->scripts('css');
	?>
	
	<link href="/assets/admin/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" >
	
</head>

<body>

	<header>
		<div class="container group">
			<div id="logo" class="col6" itemscope itemtype="http://schema.org/Organization"> 
				<a itemprop="url" href="/">
					<img itemprop="logo" src="/assets/images/logo.svg" alt="Goldhill Contracting" />
				</a>
			</div>
			<div class="col6 right">
				<a href="/users/logout" class="button button-primary">LOGOUT</a>
				
			</div>
		</div>	
		
	</header>
	
	<main class="container">
	<?php echo $content_for_layout; ?>
	</main>		
	<?php echo $this->Session->flash(); ?>

</body>
</html>