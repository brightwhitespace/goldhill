<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title_for_layout; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="robots" content="all" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<?php 
	
	$this->AdminCombinator->add_libs('js', array(
		'jquery-1.8.0.min',
		'jquery-ui-1.8.23.custom.min',
		'jquery.mjs.nestedSortable',
		'jquery.multiupload',
		'jquery.jeditable',
		'jquery.cookie',
		'brightedit'
	));
	echo $this->AdminCombinator->scripts('js');
	
	
	echo isset($this->Ckeditor) ? $this->Ckeditor->link() : '';
	
	?>
	
	
	<?php
	$this->Combinator->add_libs('css', array(
		'normalize',
		'base',
		'font-awesome.min',
		'main',
		'reports',
		'print',
	));
	echo $this->Combinator->scripts('css');
	?>
	
	<link href="/assets/admin/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" >
	
</head>

<body>

	<header>
		<div class="container group">
			<div id="logo" class="col6" itemscope itemtype="http://schema.org/Organization"> 
				<a itemprop="url" href="/">
					<img itemprop="logo" src="/assets/images/logo.svg" alt="Goldhill Contracting" />
				</a>
			</div>
			<div class="col6 right">
				<?php echo date('D, d M Y'); ?>
				<a href="/admin/todos" class="button button-primary">To Do <?php echo !empty($userTodos) ? '<b>( ' . count($userTodos) . ' )</b>' : '( 0 )' ?></a>
				<a href="/users/logout" class="button button-primary">LOGOUT</a>
			</div>
		</div>
		<nav>
			<div class="container">
				<div class="mobile-menu mobile-only"><a href=""><span></span></a></div>
				<ul class="nav group">
					<li<?php echo $this->params['controller'] == 'pages' ? ' class="selected"' : '' ?>>
						<a href="/admin">Dashboard</a>
						<ul>
							<li><a href="/admin/pages/admin/current">Current Projects</a></li>
							<li><a href="/admin/pages/admin/holding">Holding Projects</a></li>
						</ul>
					</li>
					<li<?php echo $this->params['controller'] == 'clients' ? ' class="selected"' : '' ?>><a href="/admin/clients">Clients</a></li>
					<li<?php echo $this->params['controller'] == 'suppliers' ? ' class="selected"' : '' ?>><a href="/admin/suppliers">Suppliers</a></li>

					<li<?php echo $this->params['controller'] == 'projects' ? ' class="selected"' : '' ?>>
						<a href="/admin/projects">Projects</a>
						<ul>
							<li><a href="/admin/projects">Projects</a></li>
							<li><a href="/admin/projects/index/1">Project Archive</a></li>
						</ul>
					</li>

					<li<?php echo $this->params['controller'] == 'purchase_orders' ? ' class="selected"' : '' ?>><a href="/admin/purchase_orders">Purchase Orders</a></li>

					<?php if ($userIsAdmin|| $userIsOfficeAdmin): ?>
						<li<?php echo $this->params['controller'] == 'labour' ? ' class="selected"' : '' ?>><a href="/admin/labour">Labour</a></li>
						<li<?php echo $this->params['controller'] == 'invoices' ? ' class="selected"' : '' ?>><a href="/admin/invoices">Invoices</a></li>
					<?php endif; ?>

					<?php if($userIsAdmin): ?>
						<li<?php echo $this->params['controller'] == 'users' ? ' class="selected"' : '' ?>><a href="/admin/users">Users</a></li>
					<?php endif; ?>

					<li<?php echo $this->params['controller'] == 'employees' ? ' class="selected"' : '' ?>>
						<a href="/admin/employees">Sub-contractors</a>
						<ul>
							<li><a href="/admin/employees">Sub-contractors</a></li>
							<li><a href="/admin/certificates/employee_matrix">Training Matrix</a></li>
							<li><a href="/admin/certificates">Certificates</a></li>
							<li><a href="/admin/employees/index/1">Sub-contractor Archive</a></li>
						</ul>
					</li>

					<li<?php echo in_array($this->params['controller'], array('tools', 'vehicles')) ? ' class="selected"' : '' ?>>
						<a href="/admin/tools" style="<?php echo count($unassignedTools) ? 'text-decoration: underline; color: red' : '' ?>">Equipment</a>
						<ul>
							<li><a href="/admin/tools" style="<?php echo count($unassignedTools) ? 'text-decoration: underline; color: red' : '' ?>">Tools</a></li>
							<li><a href="/admin/vehicles">Vehicles</a></li>
						</ul>
					</li>

					<?php if ($userIsAdmin|| $userIsOfficeAdmin): ?>
						<li<?php echo $this->params['controller'] == 'quotes' ? ' class="selected"' : '' ?>>
							<a href="/admin/quotes">Quotes</a>
							<ul>
								<li><a href="/admin/quotes">Quotes</a></li>
								<li><a href="/admin/quotes/index/1">Quote Archive</a></li>
							</ul>
						</li>
					<?php endif; ?>

					<?php if ($userIsAdmin|| $userIsOfficeAdmin): ?>
						<li<?php echo $this->params['controller'] == 'reports' ? ' class="selected"' : '' ?>><a href="/admin/reports">Reports</a></li>
					<?php endif; ?>
				</ul>
			</div>
		</nav>
	</header>
	
	<main class="container">
		<?php echo $content_for_layout; ?>
	</main>
	<?php echo $this->Session->flash(); ?>

	<?php if ($this->Session->consume('justLoggedIn') && $userTodos): ?>
		<div id="to-do-popup" class="overlay-block show-on-page-load">
			<div class="overlay-content vertical-centre centre">
				<a href="#" class="button button-primary small overlay-close" onclick="$('#to-do-popup').hide()"><i class="fa fa-close"></i></a>
				<p>You have <?php echo count($userTodos) ?> item<?php echo count($userTodos) !== 1 ? 's' : '' ?> in your To Do list</p>
				<a href="/admin/todos/index/<?php echo $currentUser['User']['id'] ?>" class="button button-primary">VIEW YOUR TO DO LIST</a>
			</div>
		</div>
	<?php endif; ?>

</body>
</html>