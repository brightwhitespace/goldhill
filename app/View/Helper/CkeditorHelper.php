<?php

class CkeditorHelper extends Helper {
	
	var $helpers = array('Form','Html');
	
	function link(){
		
		$html = $this->Html->script('/assets/js/ckeditor/ckeditor');
		$html .= $this->Html->script('/assets/js/ckfinder/ckfinder');
		return $html;
		
	}
	
	function input($field,$options){
		$html = $this->Form->input($field,$options);
		
		$did = $this->Form->_name($field);
		//$did = $this->domId(null, $field);
		
		$html.= $this->Html->scriptBlock("CKEDITOR.replace('$did')");
		
		return $html;
	}
	
	function browseField($field,$options){
		
		$name = $this->_name($field);
		//$id = $this->domId($field);
		$id = $options['id'];
		
		$options['after'] = '<input type="button" onclick="BrowseServer(\'#' .$id .'\');" value="Browse"/>'.$options['after'];
		
		$html = $this->Form->input($field,$options);
		
		$html .= $this->Html->scriptBlock("
				function BrowseServer(field){
					CKFinder.popup( '/assets/js/ckfinder/', null, null, function(url) { $(field).val(url); $(field).change() });
				}
		");
		
		return $html;
		
	}
	
	function filemanager(){
		$html = $this->Html->scriptBlock("
			var finder = new CKFinder();
			finder.basePath = '/assets/js/ckfinder/';
			finder.create();								 
		");
		
		return $html;
	}
	
}

?>