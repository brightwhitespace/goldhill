<?php

class BlogHelper extends Helper {
	
	var $helpers = array('Html');
	
	function getPermalink(
		$post,
		$includeTitleAsLinkText = true,
		$site = null,
		$includeDefaultLinkText = false,
		$linkClass = '',
		$linkTextSuffix = '',
		$externalUrlMessage = ' [links to external site]',
		$targetIfExternalLink = '_blank'
	) {
		if ($post['Post']['is_external_url_link']) {
			$url = $post['Post']['external_url'];
			$spanClass = 'external-post-link';
			$target = "target='$targetIfExternalLink'";
			$externalUrlMessage = " <span>$externalUrlMessage</span>";
		} else {
			$urlPrefix = $site ? '/' . $site['Site']['url'] . '/' . $site['Site']['blog_url'] : '';
			$url = $urlPrefix . '/' . $post['Post']['url'];
			$spanClass = 'internal-post-link';
			$target = '';
			$externalUrlMessage = '';
		}

		$linkText = $includeTitleAsLinkText ? $post['Post']['title'] : '';
		if ($includeDefaultLinkText) {
			$linkText = $post['Post']['link_text'] ? $post['Post']['link_text'] : 'Read more';
		}

		return $linkText ? "<span class='$spanClass'><a href='$url' class='$linkClass' $target>$linkText$linkTextSuffix</a>$externalUrlMessage</span>" : $url;
	}
	
	function getExcerpt($post,$charLimit = 100) {
		if(!empty($post['Post']['short_desc'])){
			return $post['Post']['short_desc'];
		}else{
			return strip_tags(substr($post['Post']['body'],0,$charLimit)).'...';
		}
	}

	function getCategories($post){
		$str = '';
		foreach($post['PostCategory'] as $cat){
			$str .= $cat['name'].', ';
		}
		
		return rtrim($str,", ");
	}

	/**
	 * @param $post
	 * @param $site
	 * @return string
	 */
	public function displaySummary($post, $site = null)
	{
		$summary = '<div class="post-summary">';
		$summary .= '<h3>' . $post['Post']['title'] . '</h3>';
		$summary .= '<div class="post-info">' . $this->getPostInfo($post) . '</div>';
		$summary .= '<p>' . $post['Post']['short_desc'] . '</p>';

		$linkAttributes = $post['Post']['is_external_url_link'] ? 'class="post-external-link" target="_blank"' : '';
		$summary .= '<p>' . $this->getPermalink($post, false, $site, true) . '</p>';

		$summary .= '</div>';

		return $summary;
	}

	/**
	 * @param $post
	 * @param $includeCategories
	 * @return string
	 */
	public function getPostInfo($post, $includeCategories = true)
	{
		$postInfo = $post['Post']['source_name'] ? $post['Post']['source_name'] . ', ' : '';
		$postInfo .= date('j F Y', strtotime($post['Post']['date'])) . ' &nbsp;';
		if ($includeCategories) {
			foreach ($post['PostCategory'] as $postCategory) {
				$postInfo .= '<span>' . $postCategory['name'] . '</span>';
			}
		}

		return $postInfo;
	}

	/**
	 * @param $post
	 * @param $includeCategories
	 * @return string
	 */
	public function getPost($post, $includeCategories = true)
	{
		$postInfo = date('j F Y', strtotime($post['Post']['date'])) . ' &nbsp;';
		if ($includeCategories) {
			foreach ($post['PostCategory'] as $postCategory) {
				$postInfo .= '<span>' . $postCategory['name'] . '</span>';
			}
		}

		return $postInfo;
	}
}
