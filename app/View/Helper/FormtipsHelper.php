<?php

class FormtipsHelper extends Helper {
	
	var $helpers = array('Form');
	
	function input($fieldName,$options,$tip){
		
		$this->setEntity($fieldName);

		$modelKey = $this->model();
		$fieldKey = $this->domId();
		
		$name = $this->_name($fieldName);
		
		$options['after'] = '<div id="tip'.$fieldKey.'" class="formTip" style="display:none">'.$tip.'</div></li>';
		$options['class'] .= ' tipField';
		
		$html = $this->Form->input($fieldName,$options);
		
		return $html;
	}
	
}

?>