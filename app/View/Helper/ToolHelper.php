<?php

class ToolHelper extends AppHelper {

    var $helpers = array('Html');

    /**
     * If tool is assigned to another object (Employee / Project / Vehicle) then return the identifier value of that object
     *
     * @param string $tool
     * @param int $maxLength
     * @return string
     */
    function getAssignedToName($tool, $maxLength = null)
    {
        $assignedToName = '';
        $assignedToController = '';
        $assignedToId = null;

        switch ($tool['Tool']['tool_status_id']) {
            case Tool::STATUS_INDIVIDUAL;
                $assignedToName = !empty($tool['Employee']['name']) ? $tool['Employee']['name'] : 'Unknown';
                $assignedToId = !empty($tool['Employee']['id']) ? $tool['Employee']['id'] : null;
                $assignedToController = 'employees';
                break;
            case Tool::STATUS_IN_VEHICLE:
                $assignedToName = !empty($tool['Vehicle']['reg_no']) ? $tool['Vehicle']['reg_no'] : 'Unknown';
                $assignedToId = !empty($tool['Vehicle']['id']) ? $tool['Vehicle']['id'] : null;
                $assignedToController = 'vehicles';
                break;
            case Tool::STATUS_PROJECT:
                $assignedToName = !empty($tool['Project']['name']) ? $tool['Project']['name'] : 'Unknown';
                $assignedToId = !empty($tool['Project']['id']) ? $tool['Project']['id'] : null;
                $assignedToController = 'projects';
                break;
        }

        return $assignedToId ? $this->Html->link($this->shortenString($assignedToName, $maxLength), array('controller'=>$assignedToController, 'action'=>'view', $assignedToId), array('title' => $assignedToName)) : '';
    }

}
