<?php
class TreeHelper extends HtmlHelper {

	var $html = null;
	var $listarray = array();
	var $i;

	function table($data,$model,$title_field = 'name'){
		$this->tableData($data,$model,$title_field,0);
		return $this->html;
	}
	
	function tableData($data,$model,$title_field,$level){
		
		foreach ($data as $row):
		
			$indent = $level*10;
			$this->html .= "<tr class='level{$level}'>
				<td>
					<span style='padding-left: {$indent}px'>&raquo; {$this->link($row[$model][$title_field], array('action'=>'edit', $row[$model]['id'],'#four'))}</span>
				</td>
				<td>";
				if($row[$model]['published'] == 1){
					$this->html .= "yes";
				}else{
					$this->html .= "no";
				}
			$this->html .= "</td>
				<td class='actions'>
					
					
					{$this->link('EDIT', array('action'=>'edit', $row[$model]['id']),array('escape'=>false),null)}
					{$this->link('DELETE', array('action'=>'delete', $row[$model]['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?'), $row[$model]['id']))}
				</td>
			</tr>";
			if(isset($row['children'][0])){
				$newLevel = $level + 1;
				$this->html .= $this->tableData($row['children'],$model,$title_field,$newLevel);
			}
			
		endforeach;
	}
	
	function threadedList($data,$model,$field){
		$this->listarray[0] = '';
		$this->threadedItem($data,$model,$field,0);
		return $this->listarray;
	}
	
	function threadedItem($data,$model,$field,$level){
		foreach ($data as $row):
			$prefix = '';
			for($i = 0; $i < $level; $i++){
				$prefix .= '&raquo; ';
			}
			$this->listarray[$row[$model]['id']] = $prefix.$row[$model][$field];
			
			if(isset($row['children'][0])){
				$newLevel = $level + 1;
				$this->threadedItem($row['children'],$model,$field,$newLevel);
			}			
		endforeach;
	}
	
	
	function expandable($data,$model,$title_field = 'name',$checkbox = false){
		$this->html = '<ul class="expandableList">';
		$this->expandableData($data,$model,$title_field,0,$checkbox);
		$this->html .= '</ul>';
		return $this->html;
	}
	
	function expandableData($data,$model,$title_field,$level,$checkbox){
		$i = 0;
		foreach ($data as $row):
			$indent = $level*10;
			
			$this->html .= '<li id="expandable_'.$row[$model]['id'].'"><div class="expandableName clearfix">';			
			
			if(isset($row['children'][0])){
				$this->html .= '<a href="#" onclick="return false" class="expandable" id="'.$row[$model]['id'].'"><span>open</span></a> ';
			}else{
				$this->html .= '<img class="nonexpandable" src="/assets/admin/nonexpandable.png" />';
			}
			
			$this->html .= "{$row[$model][$title_field]}";
				
			if($checkbox){
				$this->html .= '<div class="expandableActions actions"><input type="checkbox" id="PageId" name="data[Page]['.$i.'][id]" value="'.$row[$model]['id'].'"></div>';
			}else{
				$this->html .= "<div class='expandableActions actions'>
					
					{$this->link('EDIT', array('action'=>'edit', $row[$model]['id']),array('escape'=>false),null)}
					{$this->link('DELETE', array('action'=>'delete', $row[$model]['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?'), $row[$model]['id']))}
				</div></div>";
			}
			
			if(isset($row['children'][0])){
				$newLevel = $level + 1;
				$this->html .= '<ul id="expandable'.$row[$model]['id'].'">';
				$this->html .= $this->expandableData($row['children'],$model,$title_field,$newLevel,$checkbox);
				$this->html .= '</ul>';
			}
			
			$this->html .= '</li>';
			$i++;
			
		endforeach;
	}
	
	function sortable($data,$model,$title_field = 'name'){
		$this->html = '<ol class="sortable" id="sortable_5">';
		$this->sortableData($data,$model,$title_field,0);
		$this->html .= '</ol>';
		return $this->html;
	}
	
	function sortableData($data,$model,$title_field,$level){
		
		foreach ($data as $row):
			$indent = $level*10;
			
			$this->html .= '<li id="'.strtolower($model).'_'.$row[$model]['id'].'">';			
			
			
			$this->html .= "<div>{$row[$model][$title_field]}</div>";
				
						
			
			if(isset($row['children'][0])){
				$newLevel = $level + 1;
				$this->html .= '<ol id="sortable'.$row[$model]['id'].'">';
				$this->html .= $this->sortableData($row['children'],$model,$title_field,$newLevel);
				$this->html .= '</ol>';
			}
			
			$this->html .= '</li>';
			
		endforeach;
	}
	
}
?>