<?php

class BrightformHelper extends Helper {
	
	var $helpers = array('Javascript','Form');
	var $results = null;
	
	function input($field,$options){
		
		$required = '';
		$class = $field['type'].'Field';
		
		if($field['required'] == 1):
			$required = '<em>*</em>';
			$class .= ' required';
		endif;
		
		$opts = array('label'=>$field['label'].$required,'type'=>$field['type'],'class'=>$class);
		
		if($field['type'] == 'select'){
			$opts['options'] = $this->getSelectOptions($field['options']);
		}
		
		$options = array_merge($opts,$options);
			
		return $this->Form->input($field['name'],$options); 
	
	}
	
	function getSelectOptions($opts){
		
		$opts = explode(",",$opts);
		
		$options = array();
		
		foreach($opts as $opt){
			$options[$opt] = trim($opt," ");
		}
		
		return $options;
	}

	function getValue($results, $field){
		if(!empty($results['result'])){
			$form = unserialize($results['result']);
			if(isset($form['Form'][$field])){
				return $form['Form'][$field];
			}
		}
		return null;
	}

	function displayTrueOrFalse($value, $falseIsBlank = false)
	{
		$falseValue = $falseIsBlank ? '' : '<img src="/assets/admin/icon_fail_sm.png">';

		return $value ? '<img src="/assets/admin/icon_pass_sm.png">' : $falseValue;
	}
}
