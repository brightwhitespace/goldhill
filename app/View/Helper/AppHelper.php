<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Helper', 'View');

/**
 * This is a placeholder class.
 * Create the same file in app/View/Helper/AppHelper.php
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       Cake.View.Helper
 */
class AppHelper extends Helper {

	var $yesNoOptions = array(1 => 'Yes', 0 => 'No');

	function nl2p($text) {

		// Converts single newlines in text to <br /> and double newlines to </p><p>
		// NOTE: Does not enclose text with opening and closing <p> and </p> tags

		return str_replace( array("\n\n", "\n"), array('</p><p>', '<br />'), str_replace("\r", '', $text) );
	}

	function snippet($str,$length){
		$rstr = substr($str,0,$length);
		$rstr = rtrim($rstr," ");
		return $rstr ."...";
	}

	/**
	 * @param array $array Array containing the address key=>value pairs
	 * @param string $separator
	 * @param array|null $addressFields Provided in case you want to override the default address key values
	 * @return string
	 */
	function formatAddress($array, $separator = '<br>', $addressFields = null)
	{
		// include all possible fields by default (those that don't exist will be ignored)
		if (!$addressFields) {
			$addressFields = array(
				'address1',
				'address2',
				'address3',
				'address4',
				'address5',
				'town',
				'city',
				'county',
				'postcode',
				'country',
			);
		}

		$validAddressStrings = array();

		foreach ($addressFields as $addressField) {
			if (!empty($array[$addressField])) {
				$validAddressStrings[] = h($array[$addressField]);
			}
		}

		return implode($separator, $validAddressStrings);
	}

	/**
	 * @param array $arrayContainingNameParts Of format [ ..., 'firstname' => value, 'lastname' => value, ... ]
	 * @return string
	 */
	function getFullName($arrayContainingNameParts)
	{
		$keysToKeep = array('firstname' => '', 'lastname' => '');

		$nameParts = array_intersect_key($arrayContainingNameParts, $keysToKeep);

		return implode(' ', $nameParts);
	}

	function getInitials($str)
	{
		$words = explode(" ", $str);
		$initials = "";

		foreach ($words as $word) {
			$initials .= !empty($word[0]) ? $word[0] : '';
		}

		return $initials;
	}

	function shortenString($str, $maxLength, $ellipsis = '...')
	{
		return !$maxLength ? $str : substr($str, 0, $maxLength) . (strlen($str) > $maxLength ? $ellipsis : '');
	}

	function getCertExpiryClass($employeeCertRecord, $certificate)
	{
		$certificate = !empty($certificate['Certificate']) ? $certificate : array('Certificate' => $certificate);

		$certExpiryWarningDays = $certificate['Certificate']['expiry_warning_days'];
		$daysUntilExpiry = null;
		$today = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));

		$certClass = 'has-cert ';
		if (!$employeeCertRecord['ignore_previous_records']): // check it's not a "deleted" record
			$endDate = $employeeCertRecord['end_date'] ? DateTime::createFromFormat('Y-m-d', $employeeCertRecord['end_date']) : null;
			$daysUntilExpiry = $endDate ? $today->diff($endDate)->days : $certExpiryWarningDays + 1;
			if ($endDate && $endDate < $today):
				$certClass .= 'cert-expired';
			elseif ($daysUntilExpiry <= $certExpiryWarningDays):
				$certClass .= 'cert-warning';
			else:
				$certClass .= 'cert-ok';
			endif;
		endif;

		return $certClass;
	}

}
