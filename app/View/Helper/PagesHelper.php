<?php

class PagesHelper extends AppHelper {
	
	var $helpers = array('Html','Brightform','Form','Javascript');

	function content($content,$page){
		//$content = $page['Page']['page_body'];

		if(!empty($page['Form']) && $page['Form']['id']){
			$shortCode = '[form]';

			$form = $this->Form->create('Form',array('url'=>'/forms/send','class'=>'validate'));

			$form .= $this->Form->input('id',array('type'=>'hidden','value'=>$page['Form']['id']));
			$form .= $this->Form->input('test',array('type'=>'hidden','value'=>true));

			$form .= '<fieldset>';

			foreach($page['Form']['FormField'] as $field):
				if($field['type'] == 'content'){

					$form .= '</fieldset>';


					$form .= '<fieldset>';
					$form .= '<h3>'.$field['label'].'</h3>';


				}else{
					$form .= $this->Brightform->input($field,array('between'=>'<div class="'.$field['type'].'-box">','after'=>'</div>'));
				}

			endforeach;

			$form .= '</fieldset>';
			$form .= $this->Form->button('submit', array('name'=>'save','type'=>'submit','value'=>'send','class'=>'cta'));
			$form .= $this->Form->end();

			$content = str_replace(array($shortCode), array($form), $content);
		}

		return $content;
	}

	function contentBlocks($contentBlocks, $page)
	{
		$content = '';

		foreach ($contentBlocks as $contentBlock) {
			$content .= $this->content($contentBlock['content'], $page);
		}

		return $content;
	}

	function _getForms(){
		
	}


	function pageUrl($page, $absolute = false, $parentUrl = '', $returnDirectUrl = true)
	{
		if (!isset($page['Page'])) {
			$page['Page'] = $page;
		}

		if ($returnDirectUrl && !empty($page['Page']['direct_url'])) {
			$url = $page['Page']['direct_url'];
		} else {
			$url = $this->getSiteUrlPrefix($page) . '/' . $page['Page']['page_url'];
		}

		return $url;
	}

	function getMenu($pages, $options = array())
	{
		$defaults = array(
			'class' => '',
			'first' => 'first',
			'last' => 'last',
			'dropdowns' => 'drop',
			'returnChildren' => false,
			'includeUL' => true,
			'parentIds' => array(),
			'showNonParentHierarchy' => true,
			'showFromLevel' => 1,
			'showToLevel' => 9999,
			'currentLevel' => 1
		);
		
		$options = array_merge($defaults, $options);

		// check if we have reached the level where output can start to be displayed (NOTE: root level = 1)
		$displayingThisLevel = $options['currentLevel'] >= $options['showFromLevel'] &&
			$options['currentLevel'] <= $options['showToLevel'];

		if ($displayingThisLevel && $options['includeUL'] == true) {
			$output = "\n<ul".' class="'.$options['class'].'">';
		} else {
			$output = '';
		}
		
		foreach ($pages as $page) {

			// work out if this page is "active" i.e. is itself the current page OR is in its direct parent hierarchy
			//   OR the current page's direct_url is the start of the requested url (copes with blog posts)
			$isActiveOrParentPage = in_array($page['Page']['id'], $options['parentIds'])
				|| (isset($this->request->params['slug']) && $this->request->params['slug'] == $page['Page']['page_url'])
				|| stripos($this->Html->url(null), $page['Page']['direct_url']) === 0;

			// will only display page IF we are displaying ALL pages, OR if the page is within the "active' hierarchy
			if ($isActiveOrParentPage || in_array($page['Page']['parent_id'], $options['parentIds']) || $options['showNonParentHierarchy']) {

				// work out if child pages are being displayed for this page
				$displayingChildren = ($isActiveOrParentPage || $options['showNonParentHierarchy']) && $options['returnChildren'] && $page['children'];

				// work out class(es) for the <li> tag
				$class = $displayingChildren ? $options['dropdowns'] . ' ' : '';
				$class .= $isActiveOrParentPage ? 'active' : '';

				$output .= $displayingThisLevel ? "\n<li class='$class'>" : '';
				if (!empty($page['Page']['direct_url'])) {
					$url = $page['Page']['direct_url'];
				} else {
					$url = $this->pageUrl($page);
				}

				$linkOptions = $isActiveOrParentPage ? array('class' => 'active') : array();
				$output .= $displayingThisLevel ? ($this->Html->link($page['Page']['menu_title'] ? $page['Page']['menu_title'] : $page['Page']['page_title'], $url, $linkOptions)) : '';

				if ($displayingChildren) {
					// increment level
					$options['currentLevel'] = $options['currentLevel'] + 1;

					// add current page id to list of parent ids so pages under it will be displayed
					if ($isActiveOrParentPage) {
						$options['parentIds'][] = $page['Page']['id'];
					}

					$output .= $this->getMenu($page['children'], $options);
				}

				$output .= $displayingThisLevel ? '</li>' : '';
			}
		}
		
		if ($options['includeUL'] == true) {
			$output .= $displayingThisLevel ? '</ul>' : '';
		}
		
		return $output;
	}

	function getSnippet($page,$snippetName){
		foreach($page['Snippet'] as $snippet){
			if($snippet['name'] == $snippetName){
				return $snippet['snippet'];
			}
		}
	}
	
	function breadcrumbs($page)
	{
		$baseUrl = $this->getSiteUrlPrefix($page);

		$this->Html->addCrumb('Home', $baseUrl);

		if(!empty($page['Parent']['Parent']['page_title'])){
			$url = $baseUrl . '/' . $page['Parent']['Parent']['page_url'];
			$this->Html->addCrumb($page['Parent']['Parent']['page_title'], $url);
		}
		if(!empty($page['Parent']['page_title'])){
			$url = $baseUrl . '/' . $page['Parent']['page_url'];
			$this->Html->addCrumb($page['Parent']['page_title'], $url);
		}
		$this->Html->addCrumb('<span>'.$page['Page']['page_title'].'</span>', null);

		return $this->Html->getCrumbs('</li> <li>');
	}
	
	function getBreadCrumbs($list = false){
		if ($list) {
			return '<li>'.$this->Html->getCrumbs('</li><li>').'</li>';
		} else {
			return $this->Html->getCrumbs(' &raquo; ');
		}
	}

	function getExcerpt($sub, $chars = 100){
		$desc = preg_replace('/\s+?(\S+)?$/', '', substr(strip_tags($sub['ContentBlock'][0]['content']), 0, $chars)).'...';
		if(!empty($sub['short_desc'])): 
			$desc = $sub['short_desc']; 
		endif;
		if(!empty($sub['Page']['short_desc'])): 
			$desc = $sub['Page']['short_desc']; 
		endif;
		
		return $desc;
	}

	/**
	 * @param $page
	 * @return string
	 */
	public function getSiteUrlPrefix($page = null)
	{
		$urlPrefix = '/';

		if (!empty($page['Site'])) {
			$urlPrefix .= $page['Site']['url'];

		} else if (!empty($this->request->params['site'])) {
			$siteCode = $this->request->params['site'];
			$countryCode = !empty($this->request->params['country']) ? $this->request->params['country'] : 'uk';
			$urlPrefix .= $siteCode . '/' . $countryCode;
		}

		return $urlPrefix;
	}

}