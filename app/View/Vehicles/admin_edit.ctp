
<div class="content">
	<h1>Edit Vehicle</h1>

	<?php echo $this->Form->create('Vehicle', array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('reg_no', array('label'=>'Registration Number','type'=>'text'));
			echo $this->Form->input('make', array('label'=>'Make<em>*</em>','type'=>'text'));
			echo $this->Form->input('model', array('label'=>'Model<em>*</em>','type'=>'text'));
			echo $this->Form->input('mot_expiry_date',array('label'=>'MOT Expiry Date','type'=>'text','class'=>'datepicker','style'=>'width: 50%'));
			echo $this->Form->input('tax_expiry_date',array('label'=>'Tax Expiry Date','type'=>'text','class'=>'datepicker','style'=>'width: 50%'));
			?>
		</div>
		<div class="col6">
			<?php
			echo $this->Form->input('notes', array('label'=>'Notes','type'=>'textarea','style'=>'height: 193px'));
			?>
		</div>
	</div>
	<div class="group" style="margin-top: 30px">
		<div class="col6">
			<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>
