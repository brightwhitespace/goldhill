<div class="content">
	<div class="content-head group">
		<div class="col8">
			<h1>
				<?php echo $vehicle['Vehicle']['reg_no']; ?>
				<?php if($userIsAdmin): ?>
					<?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $vehicle['Vehicle']['id']),array('escape'=>false)); ?>
				<?php endif; ?>
			</h1>
		</div>
		<div class="col4 right">
		</div>
	</div>

	<div class="group">
		<div class="col3">
			<h3>Vehicle Details</h3>
			<div class="box">
				<p>
					<b>Reg. No.: </b>
					<?php echo h($vehicle['Vehicle']['reg_no']) ?><br>
				</p>
				<p>
					<b>Make & Model: </b>
					<?php echo h($vehicle['Vehicle']['make'] . ' ' . $vehicle['Vehicle']['model']) ?>
				</p>
				<p>
					<b>MOT Expires: </b>
					<?php echo $vehicle['Vehicle']['mot_expiry_date'] ? date('d/m/Y', strtotime($vehicle['Vehicle']['mot_expiry_date'])) : '' ?>
				</p>
				<p>
					<b>Tax Expires: </b>
					<?php echo $vehicle['Vehicle']['tax_expiry_date'] ? date('d/m/Y', strtotime($vehicle['Vehicle']['tax_expiry_date'])) : '' ?>
				</p>
			</div>

			<?php if ($vehicle['Vehicle']['notes']): ?>
				<h3>Notes</h3>
				<div class="box">
					<p style="margin-bottom: 0">
						<?php echo nl2br(h($vehicle['Vehicle']['notes'])); ?>
					</p>
				</div>
			<?php endif; ?>

			<h3>File Store</h3>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'vehicle', 'files'=>$files, 'objectId'=>$vehicle['Vehicle']['id'], 'allowDelete'=>$userIsAdmin)); ?>
			</div>
		</div>

		<div class="col9">

			<h3>Assigned Tools</h3>
			<table>
				<tr>
					<th>Serial No.</th>
					<th>Name</th>
					<th>Description</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php foreach ($vehicle['Tool'] as $tool): ?>
					<tr>
						<td><?php echo $tool['serial_number']; ?>&nbsp;</td>
						<td><?php echo $tool['name']; ?>&nbsp;</td>
						<td><?php echo $tool['description']; ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('controller'=>'tools', 'action' => 'view', $tool['id']), array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'tools', 'action' => 'edit', $tool['id']), array('escape'=>false)); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$vehicle['Tool']): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>

		</div>
		
	</div>
	
</div>
