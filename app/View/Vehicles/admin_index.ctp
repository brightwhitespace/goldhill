<div class="content">
	<div class="group">
		<div class="col6">
			<h2><?php echo empty($archive) ? 'Vehicles' : 'Vehicle Archive' ?></h2>
		</div>
		<div class="col3 right">
			<a href="/admin/vehicles/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Vehicle</a>
		</div>
		<div class="col3">
			<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
			<?php
			echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
			echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if (!empty($this->request->data['Search'])): ?>
				<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
			<?php endif; ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<table class="cols-align-top">
		<tr>
			<th style="width: 80px"><?php echo $this->Paginator->sort('reg_no', 'Reg. No.');?></th>
			<th style="width: 20%"><?php echo $this->Paginator->sort('make');?></th>
			<th><?php echo $this->Paginator->sort('model');?></th>
			<th><?php echo $this->Paginator->sort('notes');?></th>
			<th><?php echo $this->Paginator->sort('mot_expiry_date');?></th>
			<th><?php echo $this->Paginator->sort('tax_expiry_date');?></th>
			<th>Tools?</th>
			<th style="width: 62px" class="actions"></th>
		</tr>
		<?php
		$i = 0;
		foreach ($vehicles as $vehicle):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($vehicle['Vehicle']['reg_no'], array('action' => 'view', $vehicle['Vehicle']['id'])); ?>&nbsp;</td>
				<td><?php echo $vehicle['Vehicle']['make']; ?>&nbsp;</td>
				<td><?php echo $vehicle['Vehicle']['model']; ?>&nbsp;</td>
				<td><?php echo $vehicle['Vehicle']['notes'] ?></td>
				<td><?php echo $vehicle['Vehicle']['mot_expiry_date'] ? date('d/m/Y', strtotime($vehicle['Vehicle']['mot_expiry_date'])) : ''?></td>
				<td><?php echo $vehicle['Vehicle']['tax_expiry_date'] ? date('d/m/Y', strtotime($vehicle['Vehicle']['tax_expiry_date'])) : ''?></td>
				<td><?php echo !empty($vehicle['Tool']) ? '<i class="fa fa-wrench uploaded-files-tick"></i> ' . count($vehicle['Tool']) : ''; ?>&nbsp;</td>
				<td class="actions">
					<?php if ($userIsAdmin): ?>
						<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $vehicle['Vehicle']['id']),array('escape'=>false)); ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$vehicles): ?>
			<tr>
				<td colspan="8">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>
