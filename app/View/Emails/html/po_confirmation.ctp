

<table width="98%" border="0" cellpadding="10">
	<tbody>
		<tr>
			<td width="50%" valign="top"><img src="<?php echo $url; ?>/assets/images/logo.png" alt=""/></td>
			<td width="50%"><h1 style="font-family: Helvetica, Arial, sans-serif; font-size: 30px;">Purchase Order</h1>
				<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-top: 1px solid #000; border-bottom: 1px solid #000; ">
					<tbody>
						<tr>
							<td style="border-bottom: 1px solid #000; "><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Date</span></td>
							<td style="border-bottom: 1px solid #000; "><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $this->Time->format('d/m/Y',$po['PurchaseOrder']['start_date']); ?></span></td>
						</tr>
						<tr>
							<td style="border-bottom: 1px solid #000; "><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">P.O. Number</span></td>
							<td style="border-bottom: 1px solid #000; "><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $po['PurchaseOrder']['ref']; ?></span></td>
						</tr>
						<?php if(isset($po['User'])): ?>
							<tr>
								<td style="border-bottom: 1px solid #000; "><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">P.O. Contact</span></td>
								<td style="border-bottom: 1px solid #000; "><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $po['User']['firstname'].' '.$po['User']['lastname']; ?></span></td>
							</tr>
						<?php endif; ?>
						<?php if(isset($po['Employee']['name'])): ?>
							<tr>
								<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Site Contact</span></td>
								<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $po['Employee']['name'] . ($po['Employee']['tel'] ? ' (' . $po['Employee']['tel'] . ')' : ''); ?></span></td>
							</tr>
						<?php endif; ?>
					</tbody>
			</table></td>
		</tr>
		<tr>
			<td valign="top">
			<table width="50%" border="0" cellpadding="7" cellspacing="0">
				<tbody>
					<tr>
						<td bgcolor="#B88400" valign="top">
						<span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold; color: #fff;">VENDOR</span>
						</td>
					</tr>
					<tr>
						<td valign="top">
						<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">
						<?php echo $po['Supplier']['name']; ?><br>
						<?php echo $this->App->formatAddress($po['Supplier']); ?>
						</p>
						</td>
					</tr>
				</tbody>
			</table></td>
			<td><table width="50%" border="0" cellpadding="7" cellspacing="0">
				<tbody>
					<tr>
						<td bgcolor="#B88400" valign="top"><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold; color: #fff;">DELIVERY ADDRESS</span></td>
					</tr>
					<tr>
						<td valign="top">
						<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">
							<?php echo $this->App->formatAddress($po['Project']); ?>
						</p>
						</td>
					</tr>
				</tbody>
			</table></td>
		</tr>
		<tr>
			<td colspan="2" valign="top"><table width="100%" border="0" cellpadding="7" cellspacing="0">
				<tbody>
					<tr>
						<td bgcolor="#B88400"><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold; color: #fff;">Item</span></td>
						<td bgcolor="#B88400"><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #fff; font-weight: bold;">Quantity</span></td>
					</tr>
					<tr>
						<td style="border-bottom: 1px solid #000; "><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $po['PurchaseOrder']['desc']; ?></p></td>
						<td style="border-bottom: 1px solid #000; "><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $po['PurchaseOrder']['quantity']; ?></p></td>
					</tr>
				</tbody>
			</table></td>
		</tr>
		<tr>
			<td valign="top">&nbsp;</td>
			<td>
			<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-top: 1px solid #000; border-bottom: 1px solid #000; ">
				<tbody>
					<tr>
						<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Rate</span></td>
						<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $this->Number->currency($po['PurchaseOrder']['rate'],'GBP'); ?></span></td>
					</tr>
					<?php if($po['PurchaseOrder']['type_id'] == 2): ?>
					<tr>
						<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Excess Tonage</span></td>
						<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $this->Number->currency($po['PurchaseOrder']['excess_tonage'],'GBP'); ?></span></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Total</span></td>
						<td><span style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $this->Number->currency($po['PurchaseOrder']['value'],'GBP'); ?></span></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" valign="top"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; text-align: center;">Unit 4A Adams Way, Springfield Business Park, Alcester, B49 6PU</p></td>
		</tr>
	</tbody>
</table>