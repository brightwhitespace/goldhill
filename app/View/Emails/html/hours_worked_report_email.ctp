
<p style="font-size: 13px; font-family: arial, helvetica, sans-serif"><b><?php echo str_replace(' ', '&nbsp;', $reportHeading) ?><br>&nbsp;</b></p>

<table width="600px" border="1" cellpadding="5">
	<tr>
		<th style="width: 140px; text-align: left; font-size: 11px; font-family: arial, helvetica, sans-serif">Date</th>
		<th style="text-align: left; font-size: 11px; font-family: arial, helvetica, sans-serif">&nbsp;Start&nbsp;&nbsp;-&nbsp;&nbsp;End&nbsp;&nbsp;&nbsp;(&nbsp;Hrs&nbsp;)&nbsp;Project</th>
	</tr>

	<?php
	$totalHours = 0;
	if (!empty($days)):
	?>
		<?php foreach($days as $dayKey => $day): ?>
			<tr>
				<td style="font-size: 11px; font-family: arial, helvetica, sans-serif"><?php echo $dayKey ?></td>
				<td>
				<?php foreach ($day as $projectKey => $timesheetEntries): ?>
					<table border="0" cellpadding="2">
					<?php
					$idx = 0;
					foreach ($timesheetEntries as $timesheetEntry):
						$totalHours += $timesheetEntry['hours_worked'];
					?>
						<tr>
							<td valign="top" style="font-size: 11px; font-family: arial, helvetica, sans-serif"><?php echo $timesheetEntry['start_time'] ?>&nbsp;-&nbsp;<?php echo $timesheetEntry['end_time'] ?></td>
							<td valign="top" style="font-size: 11px; font-family: arial, helvetica, sans-serif"><b>(&nbsp;<?php echo str_replace(array('.00', '.50'), array('.0', '.5'), number_format($timesheetEntry['hours_worked'], 2)) ?>&nbsp;)</b></td>
							<?php if ($idx++ == 0): ?>
								<td valign="top" style="font-size: 11px; font-family: arial, helvetica, sans-serif" rowspan="<?php echo count($timesheetEntries) ?>"><?php echo h($projectKey) ?></td>
							<?php endif; ?>
						</tr>
					<?php endforeach; ?>
					</table>
				<?php endforeach; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td valign="top" style="font-size: 11px; font-family: arial, helvetica, sans-serif" colspan="2"><i>No results for this date range</i></td>
		</tr>
	<?php endif; ?>
</table>

<!--<p style="font-size: 13px; font-family: arial, helvetica, sans-serif"><b>&nbsp;<br>Total Hours = --><?php //echo str_replace(array('.00', '.50'), array('.0', '.5'), number_format($totalHours, 2)) ?><!--</b></p>-->

