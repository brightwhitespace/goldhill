

<?php foreach ($quotesByType as $key => $quotes): ?>

	<p style="font-size: 18px; font-weight: bold; font-family: arial, sans-serif"><?php echo $key ?></p>

	<?php if ($quotes): ?>
		<table width="700px" border="1" cellpadding="5">
			<tbody>
				<tr>
					<td width="300px"><b>Project</b></td>
					<td width="200px"><b>Client</b></td>
					<td width="100px"><b>Site Visit</b></td>
					<td width="100px"><b>Due Back</b></td>
				</tr>
				<?php foreach ($quotes as $quote): ?>
					<tr>
						<td style="vertical-align: top"><?php echo $quote['Quote']['project_name'] ?></td>
						<td style="vertical-align: top"><?php echo $quote['Client']['name'] ?></td>
						<td style="vertical-align: top"><?php echo ($quote['Quote']['site_visit_date'] ? date('d M Y', strtotime($quote['Quote']['site_visit_date'])) : '') . '<br>' . $quote['Quote']['site_visit_time'] ?></td>
						<td style="vertical-align: top"><?php echo ($quote['Quote']['quote_due'] ? date('d M Y', strtotime($quote['Quote']['quote_due'])) : '') . '<br>' . $quote['Quote']['quote_due_time'] ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php else: ?>
		<p style="padding-bottom: 10px; font-size: 14px; font-style: italic; font-family: arial, sans-serif">No <?php echo strtolower($key) ?></p>
	<?php endif; ?>

<?php endforeach; ?>