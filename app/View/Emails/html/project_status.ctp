

<?php foreach ($projectsByType as $key => $projects): ?>

	<p style="font-size: 18px; font-weight: bold; font-family: arial, sans-serif"><?php echo $key ?></p>

	<?php if ($projects): ?>
		<table width="700px" border="1" cellpadding="5">
			<tbody>
				<tr>
					<td width="300px"><b>Project</b></td>
					<td width="200px"><b>Client</b></td>
					<td width="100px"><b>Start Date</b></td>
					<td width="100px"><b>End Date</b></td>
				</tr>
				<?php foreach ($projects as $project): ?>
					<tr>
						<td style="vertical-align: top"><?php echo $project['Project']['name'] ?></td>
						<td style="vertical-align: top"><?php echo $project['Client']['name'] ?></td>
						<td style="vertical-align: top"><?php echo ($project['Project']['start_date'] ? date('d M Y', strtotime($project['Project']['start_date'])) : '') ?></td>
						<td style="vertical-align: top"><?php echo ($project['Project']['end_date'] ? date('d M Y', strtotime($project['Project']['end_date'])) : '')  ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php else: ?>
		<p style="padding-bottom: 10px; font-size: 14px; font-style: italic; font-family: arial, sans-serif">No <?php echo strtolower($key) ?></p>
	<?php endif; ?>

<?php endforeach; ?>