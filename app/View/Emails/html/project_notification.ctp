

<table width="600px" border="0" cellpadding="6">
	<tbody>
		<tr>
			<td valign="top" colspan="3"><img src="<?php echo $url; ?>/assets/images/logo.png" alt=""/></td>
		</tr>
		<tr>
			<td valign="top" colspan="3"><h1 style="font-family: Helvetica, Arial, sans-serif; font-size: 24px;">Project Notification</h1></td>
		</tr>
		<tr>
			<td valign="top" width="100px">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Project Name:</p>
			</td>
			<td valign="top" width="420px">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $project['Project']['name']; ?></p>
			</td>
			<td valign="top" width="80px" align="right"></td>
		</tr>
		<tr>
			<td valign="top">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Site Address:</p>
			</td>
			<td valign="top" colspan="2">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo $this->App->formatAddress($project['Project']); ?></p>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Description:</p>
			</td>
			<td valign="top" colspan="2">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo nl2br($project['Project']['description']); ?></p>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Start Date:</p>
			</td>
			<td valign="top" colspan="2">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;"><?php echo date('jS F Y', strtotime($project['Project']['start_date'])); ?></p>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">Cost:</p>
			</td>
			<td valign="top" colspan="2">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">&pound;<?php echo number_format($project['Project']['value'], 2); ?></p>
			</td>
		</tr>
		<?php if ($project['ProjectItem']): ?>
			<tr>
				<td valign="top" colspan="3" style="border-bottom: 1px solid #000; font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: bold">
					Breakdown:
				</td>
			</tr>
			<?php foreach ($project['ProjectItem'] as $projectItem): ?>
				<tr>
					<td valign="top">
						<?php echo $projectItem['date'] ? date('jS M Y', strtotime($projectItem['date'])) : ''; ?>
					</td>
					<td valign="top">
						<?php echo $projectItem['description']; ?>
					</td>
					<td valign="top" align="right">
						<?php echo $projectItem['cost'] ? '&pound;' . $projectItem['cost'] : ''; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		<tr>
	</tbody>
</table>