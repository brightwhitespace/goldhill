

<table width="700px" border="1" cellpadding="5">
	<tbody>
		<tr>
			<td><b>Employee</b></td>
			<td><b>Certificate</b></td>
			<td><b>Expires on</b></td>
			<td><b>Has Expired?</b></td>
		</tr>
		<?php foreach ($certsToNotify as $certToNotify): ?>
			<tr>
				<td><?php echo $certToNotify['employee_name'] ?></td>
				<td><?php echo $certToNotify['cert_name'] ?></td>
				<td><?php echo $certToNotify['expiry_date'] ?></td>
				<td><?php echo $certToNotify['has_expired'] ? 'YES' : '' ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>