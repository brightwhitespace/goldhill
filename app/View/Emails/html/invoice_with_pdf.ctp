

<table width="98%" border="0" cellpadding="6">
	<tbody>
		<tr>
			<td valign="top"><img src="<?php echo $url; ?>/assets/images/logo.png" alt=""/></td>
		</tr>
		<tr>
			<td valign="top"><h1 style="font-family: Helvetica, Arial, sans-serif; font-size: 15px;">Invoice <?php echo $invoice['Invoice']['invoice_num']; ?> from Goldhill Contracting</h1></td>
		</tr>
		<tr>
			<td valign="top">
				<p style="font-family: Helvetica, Arial, sans-serif; font-size: 13px;">
					Dear <?php echo $billingName; ?>,<br>
					<br>
					Please find attached our invoice dated <?php echo $invoice['Invoice']['invoice_date']; ?>.<br>
					<br>
					Regards,<br>
					<br>
					Goldhill Contracting
				</p>
			</td>
		</tr>
	</tbody>
</table>