
<div id="column2alt" class="column">

	<?php 
		$searchCount = $this->Paginator->counter(array('format' => '{:count}'));
		$searchCount = $searchCount ? $searchCount : 0;
		$searchNumPages = $this->Paginator->counter(array('format' => '{:pages}'));
	?>
	
	<?php if( !$searchCount ) : ?>
		<h1><?php echo $resultsTitle ?></h1>
		<?php if( isset($starStatusNotSelected) && $starStatusNotSelected ) : ?>
			<p>Please select either Professional or Budding Star in the star search tool</p>
		<?php else: ?>
			<p>No stars matching your search criteria were found</p>
		<?php endif; ?>
	<?php else: ?>
		
		<div class="searchOptions">
			<?php echo $searchCount.' Star'.($searchCount != 1 ? 's' : '') ?> Found  
			<?php if( $searchNumPages > 1 ): ?>
				&nbsp;| &nbsp;<a href="/users/search/all">View all</a>
			<?php endif; ?>
			<!--&nbsp;| &nbsp;<a href="/users/search/100">View 100 per page</a>-->
		</div>

		<h1><?php echo $resultsTitle ?></h1>
	
		<div id="searchResults" class="clearfix">
	
		<?php foreach( $stars as $idx=>$star ) : ?>
			<div class="profileOverview<?php echo (($idx+1) % 5 == 0) ? ' lastProfile' : '' ?>">
				<?php echo $this->User->getSearchImgLink($star, $settings_for_layout); ?>
				<p><?php echo htmlentities($star['User']['firstname'].' '.$star['User']['lastname']); ?></p>
			</div>
		<?php endforeach; ?>
	
		<?php if( $searchCount ) : ?>
			<div class="pagination">
				<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
				<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
			</div>
			
			<div class="searchOptions">
			<?php echo $searchCount.' Star'.($searchCount != 1 ? 's' : '') ?> Found  
			<?php if( $searchNumPages > 1 ): ?>
				&nbsp;| &nbsp;<a href="/users/search/all">View all</a>
			<?php endif; ?>
			<!--&nbsp;| &nbsp;<a href="/users/search/100">View 100 per page</a>-->
			</div>
		<?php endif; ?>
	
	<?php endif; ?>
		
	<?php //debug($searchCount); ?>
	<?php //debug($stars); ?>
</div>
