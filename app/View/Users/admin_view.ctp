<div class="users view">
<h2><?php echo __('User');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Username'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['username']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('User Password'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['user_password']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('User Alias'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['user_alias']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('User Email'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['user_email']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('User Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['user_type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action'=>'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete User'), array('action'=>'delete', $user['User']['id']), null, sprintf(__('Are you sure you want to delete # %s?'), $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action'=>'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action'=>'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Profiles'), array('controller'=> 'user_profiles', 'action'=>'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Profile'), array('controller'=> 'user_profiles', 'action'=>'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Orders'), array('controller'=> 'orders', 'action'=>'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order'), array('controller'=> 'orders', 'action'=>'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buying Groups'), array('controller'=> 'buying_groups', 'action'=>'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buying Group'), array('controller'=> 'buying_groups', 'action'=>'add')); ?> </li>
	</ul>
</div>
	<div class="related">
		<h3><?php echo __('Related User Profiles');?></h3>
	<?php if (!empty($user['UserProfile'])):?>
		<dl>	<?php $i = 0; $class = ' class="altrow"';?>
			<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('User Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['user_id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Address1');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['address1'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Address2');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['address2'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Address3');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['address3'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('City');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['city'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Postcode');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['postcode'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Telephone');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['telephone'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Mobile');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $user['UserProfile']['mobile'];?>
&nbsp;</dd>
		</dl>
	<?php endif; ?>
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('Edit User Profile'), array('controller'=> 'user_profiles', 'action'=>'edit', $user['UserProfile']['id'])); ?></li>
			</ul>
		</div>
	</div>
	<div class="related">
	<h3><?php echo __('Related Orders');?></h3>
	<?php if (!empty($user['Order'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Transaction Id'); ?></th>
		<th><?php echo __('Auth Id'); ?></th>
		<th><?php echo __('Order Details'); ?></th>
		<th><?php echo __('Order Amount'); ?></th>
		<th><?php echo __('Order Tax'); ?></th>
		<th><?php echo __('Order Complete'); ?></th>
		<th><?php echo __('Order Status'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Order'] as $order):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $order['id'];?></td>
			<td><?php echo $order['user_id'];?></td>
			<td><?php echo $order['transaction_id'];?></td>
			<td><?php echo $order['auth_id'];?></td>
			<td><?php echo $order['order_details'];?></td>
			<td><?php echo $order['order_amount'];?></td>
			<td><?php echo $order['order_tax'];?></td>
			<td><?php echo $order['order_complete'];?></td>
			<td><?php echo $order['order_status'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller'=> 'orders', 'action'=>'view', $order['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller'=> 'orders', 'action'=>'edit', $order['id'])); ?>
				<?php echo $this->Html->link(__('Delete'), array('controller'=> 'orders', 'action'=>'delete', $order['id']), null, sprintf(__('Are you sure you want to delete # %s?'), $order['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Order'), array('controller'=> 'orders', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Buying Groups');?></h3>
	<?php if (!empty($user['BuyingGroup'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['BuyingGroup'] as $buyingGroup):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $buyingGroup['product_id'];?></td>
			<td><?php echo $buyingGroup['id'];?></td>
			<td><?php echo $buyingGroup['start_date'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller'=> 'buying_groups', 'action'=>'view', $buyingGroup['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller'=> 'buying_groups', 'action'=>'edit', $buyingGroup['id'])); ?>
				<?php echo $this->Html->link(__('Delete'), array('controller'=> 'buying_groups', 'action'=>'delete', $buyingGroup['id']), null, sprintf(__('Are you sure you want to delete # %s?'), $buyingGroup['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Buying Group'), array('controller'=> 'buying_groups', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
