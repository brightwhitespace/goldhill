	<div class="content">
		<?php echo $this->Form->create('User', array('class'=>'loginForm')); ?>
			<fieldset>
			<?php echo $this->Session->flash(); ?>
			
				<?php echo $this->Form->input('username',array('label'=>'Email','type'=>'text','class'=>'textbox','div'=>false));?>
				<?php echo $this->Form->input('password',array('label'=>'Password','type'=>'password','class'=>'textbox','div'=>false));?>
				<?php //echo $this->Form->input('rememberme', array('label' => ' Keep me logged in', 'type' => 'checkbox','class'=>'tickbox')); ?>
			
			</fieldset>
				<?php echo $this->Form->button('Login', array('name'=>'login','type'=>'submit','value'=>'Login','class'=>'submitButton')); ?>
		<?php echo $this->Form->end(); ?>
	</div>
