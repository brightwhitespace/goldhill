
<div class="content">
	<h2>Add User</h2>

	<?php echo $this->Form->create('User',array('novalidate'=>'novalidate'));?>

	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('access_group_id',array('id'=>'accessGroupSelect','label'=>'Account Type','type'=>'select','options'=>$accessgroups,'class'=>'selectbox','empty'=>'Please select...'));
			echo $this->Form->input('username',array('label'=>'Email<em>*</em>','class'=>'textbox'));
			echo $this->Form->input('firstname',array('label'=>'First Name<em>*</em>','class'=>'textbox'));
			echo $this->Form->input('lastname',array('label'=>'Last Name<em>*</em>','class'=>'textbox'));
			echo $this->Form->input('password_new',array('label'=>'Password<em>*</em>','type'=>'password','class'=>'textbox'));
			echo $this->Form->input('password_confirm',array('label'=>'Confirm Password<em>*</em>','type'=>'password','class'=>'textbox'));
			echo $this->Form->input('project_manager',array('type'=>'checkbox','label'=> 'Is the user a project manager?'));
			?>
		</div>

		<div class="col6">
			<?php
			echo $this->Form->input('employee_id',array('label'=>'Associated Sub-contractor (if any - for Tools management)','type'=>'select','options'=>$employeeList,'empty'=>'Please select...'));
			echo $this->Form->input('client_id',array('label'=>'Associated Client','type'=>'select','options'=>$clientList,'empty'=>'Please select...'));
			?>
		</div>
	</div>

	<div class="group" style="margin-top: 30px">
		<div class="col6">
			<?php echo $this->Html->link('Cancel', array('action'=>'index'), array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save User', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>

	<?php echo $this->Form->end();?>

</div>
