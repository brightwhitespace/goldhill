<div class="content">
	<div class="group">
		<div class="col6">
			<h2>Users</h2>
		</div>
		<div class="col6 right">
			<a href="/admin/users/add/" class="button button-primary">Add User</a>
		</div>
	</div>
<table>
<tr>
	<th><?php echo $this->Paginator->sort('id');?></th>
	<th><?php echo $this->Paginator->sort('username', 'Email');?></th>
	<th><?php echo $this->Paginator->sort('firstname', 'Name');?></th>
	<th><?php echo $this->Paginator->sort('access_group_id', 'Role');?></th>
	<th><?php echo $this->Paginator->sort('employee_id', 'Sub-contractor?');?></th>
	<th><?php echo $this->Paginator->sort('created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($users as $user): ?>
	<tr>
		<td>
			<?php echo $user['User']['id']; ?>
		</td>
		<td>
			<?php echo $user['User']['username']; ?>
		</td>
		<td>
			<?php echo htmlentities($user['User']['firstname'].' '.$user['User']['lastname']); ?>
		</td>
		<td>
			<?php echo $user['AccessGroup']['group_name']; ?>
		</td>
		<td>
			<?php echo !empty($user['User']['employee_id']) ? '<i class="fa fa-check uploaded-files-tick"></i>' : ''; ?>&nbsp;</td>
		</td>
		<td>
			<?php echo $this->Time->niceShort($user['User']['created']); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link('EDIT', array('action'=>'edit', $user['User']['id']),array('escape'=>false), null, false); ?>
			<?php echo $userIsAdmin ? $this->Html->link('DELETE', array('action'=>'delete', $user['User']['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $user['User']['firstname'].' '.$user['User']['lastname']),false) : ''; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
<div class="pagination group">
	<?php echo $this->Paginator->prev(__('&laquo; previous',true), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
	<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
	<?php echo $this->Paginator->next(__('next &raquo;',true), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
</div>
