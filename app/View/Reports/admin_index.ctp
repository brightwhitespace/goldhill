<div class="content report diary-report">
	<div class="group noprint">
		<div class="col4">
			<h1>Reports</h1>
		</div>
	</div>

	<div class="group">
		<div class="col12">
			<ul>
				<li>Labour
					<ul>
						<li><a href="/admin/reports/labour">Weekly Labour</a></li>
						<li><a href="/admin/reports/wages">Sub-contractor - Wages</a></li>
						<li><a href="/admin/reports/employee_hours_worked">Sub-contractor - Days and Times Worked</a></li>
						<li><a href="/admin/reports/diary">Diary</a></li>
						<li><a href="/admin/reports/project_timesheet">Project Timesheet</a></li>
						<li><a href="/admin/reports/weekly_analysis">Weekly Labour Analysis</a></li>
						<li><a href="/admin/reports/agency_labourers">Agency Labourers</a></li>
					</ul>
				</li>
				<li>POs
					<ul>
						<li><a href="/admin/reports/pos_on_hire">On Hire</a></li>
						<li><a href="/admin/purchase_orders/index/filter:pos_not_completed?clear=1">POs not completed</a></li>
					</ul>
				</li>
				<li>Suppliers
					<ul>
						<li><a href="/admin/reports/supplier_spend">Spend</a></li>
					</ul>
				</li>
				<li>Sales
					<ul>
						<li><a href="/admin/reports/monthly_sales">Monthly Sales</a></li>
					</ul>
				</li>
				<li>Quotes
					<ul>
						<li><a href="/admin/reports/quotes_due_back">Quotes due back in</a></li>
						<li><a href="/admin/reports/quotes_with_site_visits">Site visits</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

</div>

