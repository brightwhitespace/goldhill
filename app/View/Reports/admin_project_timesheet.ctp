
<div class="content report">

	<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/project_timesheet', 'class'=>'filter-form clearfix')); ?>

		<div class="group">
			<div class="col3">
				<h1>Project Timesheets</h1>
			</div>
			<div class="col3">
				<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select week...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
				<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
			</div>
			<div class="col2">
				<?php echo $this->Form->input('start_date',array('label'=>'From ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
			</div>
			<div class="col2">
				<?php echo $this->Form->input('end_date',array('label'=>'To ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
			</div>
			<div class="col1">
				<button type="submit" name="prev-week" class="button button-primary"><i class="fa fa-minus"></i> Week</button>
			</div>
			<div class="col1">
				<button type="submit" name="next-week" class="button button-primary"><i class="fa fa-plus"></i> Week</button>
			</div>
		</div>

		<div class="group">
			<div class="col3">
				&nbsp;
			</div>
			<div class="col3">
				<?php echo $this->Form->input('client_id', array('label'=>false,'type'=>'select','options'=>$clients,'empty'=>'Select Client...','onchange'=>'$("#SearchProjectId").val(""); this.form.submit()')); ?>
			</div>
			<div class="col4">
				<?php echo $this->Form->input('project_id', array('label'=>false,'type'=>'select','options'=>!empty($projects) ? $projects : array(),'empty'=>'Select Project...','onchange'=>'this.form.submit()')); ?>
			</div>
			<div class="col2" style="text-align: right">
				<?php if (!empty($this->request->data['Search']['project_id'])): ?>
					<a href="/admin/reports/project_timesheet/pdf">Download PDF</a><br>
					<a href="/admin/reports/project_timesheet/view" target="_blank">View PDF</a>
				<?php endif; ?>
			</div>
		</div>

	<?php echo $this->Form->end(); ?>

	<div class="group">
		<div class="col12">

			<?php if (isset($days)): ?>

				<h2><br><?php echo $reportHeading ?></h2>

				<div class="proj-timesheet-report-container">

					<div class="group proj-timesheet-day proj-timesheet-day-header">
						<div class="proj-timesheet-day-name">Date</div>
						<div class="proj-timesheet-entries group">
							<div class="proj-timesheet-operative">Operative</div>
							<div class="proj-timesheet-time">Start</div>
							<div class="proj-timesheet-time">End</div>
							<div class="proj-timesheet-break">Break <span style="font-style: italic">(hrs)</span></div>
						</div>
						<div class="proj-timesheet-entries">
							<div class="proj-timesheet-project-item">Work carried out <span style="font-style: italic">(i.e. project items)</span></div>
						</div>
					</div>

					<?php if (!empty($days)): ?>
						<?php foreach($days as $dayKey => $day): ?>
							<div class="group proj-timesheet-day">
								<div class="proj-timesheet-day-name"><?php echo $dayKey ?></div>
								<div class="proj-timesheet-entries group">
									<?php if (!empty($day['employee_timesheets'])): ?>
										<?php foreach ($day['employee_timesheets'] as $timesheetEntry): ?>
											<div style="clear: left" class="proj-timesheet-operative"><?php echo h($timesheetEntry['employee_name']) ?></div>
											<div class="proj-timesheet-time"><?php echo $timesheetEntry['start_time'] ?></div>
											<div class="proj-timesheet-time"><?php echo $timesheetEntry['end_time'] ?></div>
											<div class="proj-timesheet-break"><?php echo $timesheetEntry['break_hours'] ?></div>
										<?php endforeach; ?>
									<?php else: ?>
										<div class="proj-timesheet-operative">&nbsp;</div>
										<div class="proj-timesheet-time">&nbsp;</div>
										<div class="proj-timesheet-time">&nbsp;</div>
										<div class="proj-timesheet-break">&nbsp;</div>
									<?php endif; ?>
								</div>
								<div class="proj-timesheet-project-items">
									<?php if (!empty($day['project_items'])): ?>
										<?php foreach ($day['project_items'] as $projectItem): ?>
											<div class="proj-timesheet-project-item"><?php echo h($projectItem['description']) ?></div>
										<?php endforeach; ?>
									<?php else: ?>
										<div class="proj-timesheet-project-item">&nbsp;</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div class="group lab-day">
							<i>No results for this date range</i>
						</div>
					<?php endif; ?>

				</div>

			<?php else: ?>
				<p><i><?php echo (empty($this->request->data['Search']['project_id'])) ? '' : 'From date is after To date' ?></i></p>
			<?php endif; ?>

		</div>
	</div>

</div>

