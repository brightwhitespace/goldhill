<div class="content report hw-report">
	<div class="group noprint">
		<div class="col4">
			<h1>Hours Worked Report</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/employee_hours_worked', 'class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('employee_id', array('label'=>false,'type'=>'select','options'=>$employees,'empty'=>'Select Sub-contractor...','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('start_date',array('label'=>'From ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('end_date',array('label'=>'To ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col1">
			&nbsp;
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<?php if (isset($days)): ?>

		<div class="group">
			<div class="col12">

				<h2><?php echo $reportHeading ?></h2>

				<?php echo $this->element('employee_hours_worked_table', array('days' => $days)) ?>

			</div>
		</div>

	<?php else: ?>
		<p><i><?php echo (empty($this->request->data['Search']['employee_id'])) ? '' : 'From date is after To date' ?></i></p>
	<?php endif; ?>


</div>

