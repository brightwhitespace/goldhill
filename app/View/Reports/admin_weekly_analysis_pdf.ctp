
<htmlpageheader name="defaultheader">
	<table style="width: 95%; margin: 0 auto;">
		<tr>
			<td><h2 style="font-size: 15px"><?php echo $reportHeading ?></h2></td>
			<td style="text-align: right"><img style="height: 75px" src="../../app/webroot/assets/images/logo.png" alt=""/></td>
		</tr>
	</table>
</htmlpageheader>

<sethtmlpageheader name="defaultheader" page="ALL" value="1" show-this-page="1" />

<div class="content report lab-analysis-report">

	<?php if (!empty($days)): ?>

		<table class="lab-analysis-report">
			<tr>
				<th>Name</th>
				<?php foreach ($days as $day): ?>
					<th colspan="2"><?php echo $day->format('l') ?><br><?php echo $day->format('d/m/Y') ?></th>
				<?php endforeach; ?>
			</tr>
			<?php $rowIndex = 0 ?>
			<?php foreach ($employees as $employeeName => $employeeDaysWorked): $rowIndex++ // a single "row" is actually two table rows as the employee name is spanned across both ?>
				<tr class="<?php echo $rowIndex % 2 == 0 ? 'even-row' : 'odd-row'; ?>">
					<td rowspan="2"><?php echo h($employeeName) ?></td>
					<?php foreach ($days as $day): ?>
						<?php if (!empty($employeeDaysWorked[$day->format('d-m-Y')]['D'])): ?>
							<td class="project-type active" style="background: #fbc97f">D</td>
							<td class="project-name">
								<?php foreach ($employeeDaysWorked[$day->format('d-m-Y')]['D'] as $projectName): ?>
									<div class="ellipsis" style="font-size: 11px"><?php echo substr($projectName, 0, 22) . (strlen($projectName) > 22 ? '&hellip;' : '') ?></div>
								<?php endforeach; ?>
							</td>
						<?php else: ?>
							<td class="project-type">D</td>
							<td class="project-name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<?php endif; ?>
					<?php endforeach; ?>
				</tr>
				<tr class="<?php echo $rowIndex % 2 == 0 ? 'even-row' : 'odd-row'; ?>">
					<?php foreach ($days as $day): ?>
						<?php if (!empty($employeeDaysWorked[$day->format('d-m-Y')]['P'])): ?>
							<td class="project-type active" style="background: #fbc97f">P</td>
							<td class="project-name">
								<?php foreach ($employeeDaysWorked[$day->format('d-m-Y')]['P'] as $projectName): ?>
									<div class="ellipsis" style="font-size: 11px"><?php echo substr($projectName, 0, 22) . (strlen($projectName) > 22 ? '&hellip;' : '') ?></div>
								<?php endforeach; ?>
							</td>
						<?php else: ?>
							<td class="project-type">P</td>
							<td class="project-name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<?php endif; ?>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</table>

	<?php else: ?>
		<p><i><?php echo (empty($this->request->data['Search']['project_id'])) ? '' : 'From date is after To date' ?></i></p>
	<?php endif; ?>

</div>	


