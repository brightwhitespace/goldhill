
<htmlpageheader name="defaultheader">
	<table style="width: 95%; margin: 0 auto;">
		<tr>
			<td><h2 style="font-size: 15px"><?php echo $reportHeading ?></h2></td>
			<td style="text-align: right"><img style="height: 75px" src="../../app/webroot/assets/images/logo.png" alt=""/></td>
		</tr>
	</table>
</htmlpageheader>

<sethtmlpageheader name="defaultheader" page="ALL" value="1" show-this-page="1" />

<div class="content report">

	<?php if (isset($days)): ?>

		<div class="proj-timesheet-report-container">

			<div class="group proj-timesheet-day proj-timesheet-day-header">
				<div class="proj-timesheet-day-name">Date</div>
				<div class="proj-timesheet-entries group">
					<div class="proj-timesheet-operative">Operative</div>
					<div class="proj-timesheet-time">Start</div>
					<div class="proj-timesheet-time">End</div>
					<div class="proj-timesheet-break">Break (hrs)</div>
				</div>
				<div class="proj-timesheet-entries">
					<div class="proj-timesheet-project-item">Work carried out</div>
				</div>
			</div>

			<?php if (!empty($days)): ?>
				<?php foreach($days as $dayKey => $day): ?>
					<div class="group proj-timesheet-day">
						<div class="proj-timesheet-day-name"><?php echo $dayKey ?></div>
						<div class="proj-timesheet-entries group">
							<?php if (!empty($day['employee_timesheets'])): ?>
								<?php foreach ($day['employee_timesheets'] as $timesheetEntry): ?>
									<div class="proj-timesheet-operative"><?php echo h($timesheetEntry['employee_name']) ?></div>
									<div class="proj-timesheet-time"><?php echo $timesheetEntry['start_time'] ?></div>
									<div class="proj-timesheet-time"><?php echo $timesheetEntry['end_time'] ?></div>
									<div class="proj-timesheet-break"><?php echo $timesheetEntry['break_hours'] ?></div>
								<?php endforeach; ?>
							<?php else: ?>
								<div class="proj-timesheet-operative">&nbsp;</div>
								<div class="proj-timesheet-time">&nbsp;</div>
								<div class="proj-timesheet-time">&nbsp;</div>
								<div class="proj-timesheet-break">&nbsp;</div>
							<?php endif; ?>
						</div>
						<div class="proj-timesheet-project-items">
							<?php if (!empty($day['project_items'])): ?>
								<?php foreach ($day['project_items'] as $projectItem): ?>
									<div class="proj-timesheet-project-item"><?php echo h($projectItem['description']) ?></div>
								<?php endforeach; ?>
							<?php else: ?>
								<div class="proj-timesheet-project-item">&nbsp;</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<div class="group lab-day">
					<i>No results for this date range</i>
				</div>
			<?php endif; ?>

		</div>

		<p style="padding: 12px 0 0 30px">Please obtain a signature from Contract Manager / Site Foreman where possible &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			Signed:&nbsp; ....................................................................................</p>

	<?php else: ?>
		<p><i><?php echo (empty($this->request->data['Search']['project_id'])) ? '' : 'From date is after To date' ?></i></p>
	<?php endif; ?>

</div>

