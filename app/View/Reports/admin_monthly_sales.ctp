<style type="text/css" media="print">
	@page {
		size: portrait;
	}
</style>

<div class="content report">
	<div class="group noprint">
		<div class="col3">
			<h1>Sales</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/monthly_sales', 'class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select month...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
			<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('start_date',array('label'=>'From ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('end_date',array('label'=>'To ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col1">
			<button type="submit" name="prev-month" class="button button-primary"><i class="fa fa-minus"></i> Month</button>
		</div>
		<div class="col1">
			<button type="submit" name="next-month" class="button button-primary"><i class="fa fa-plus"></i> Month</button>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="group">
		<div class="col12">

			<?php if (isset($invoices)): $grandTotal = 0; ?>
				<h2><?php echo $reportHeading ?></h2>
				<?php if (!empty($invoices)): ?>
					<table class="report">
						<tr>
							<th style="width: 90px">Date</th>
							<th>Number</th>
							<th>Reference</th>
							<th>Company</th>
							<th style="width: 90px">&pound; Nett</th>
							<th style="width: 80px">&pound; VAT</th>
							<th style="width: 90px">&pound; Gross</th>
						</tr>
						<?php foreach ($invoices as $invoice): ?>
							<tr>
								<td><?php echo date('j M Y', strtotime($invoice['Invoice']['invoice_date'])) ?></td>
								<td><?php echo $invoice['Invoice']['invoice_num'] ?></td>
								<td><?php echo $invoice['Invoice']['ref'] ?></td>
								<td><?php echo h($invoice['Client']['name']) ?></td>
								<td><?php
									$grandTotal += $invoice['Invoice']['total'];
									echo number_format($invoice['Invoice']['total'], 2);
									?>
								</td>
								<td><?php echo number_format($invoice['Invoice']['total'] * 0.2, 2); ?></td>
								<td><?php echo number_format($invoice['Invoice']['total'] * 1.2, 2); ?></td>
							</tr>
						<?php endforeach; ?>
						<tr style="border-top: 1px solid #aaa">
							<td><b>Total</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td><b><?php echo number_format($grandTotal, 2) ?></b></td>
							<td><b><?php echo number_format($grandTotal * 0.2, 2) ?></b></td>
							<td><b><?php echo number_format($grandTotal * 1.2, 2) ?></b></td>
						</tr>
					</table>
				<?php else: ?>
					<p><i>No results in this date range</i></p>
				<?php endif; ?>
			<?php else: ?>
				<p><i>From date is after To date</i></p>
			<?php endif; ?>

		</div>
	</div>

</div>

