
<div class="content report lab-analysis-report">
	<div class="group noprint">
		<div class="col3">
			<h1>Agency Labourers</h1>
		</div>
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select week...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
			<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
		</div>
		<div class="col2">
			<button type="submit" name="prev-week" class="button button-primary"><i class="fa fa-minus"></i> Week</button> &nbsp;
			<button type="submit" name="next-week" class="button button-primary"><i class="fa fa-plus"></i> Week</button>
		</div>
		<div class="col2">
			&nbsp;
		</div>
		<div class="col2 right noprint">
			<a href="/admin/reports/agency_labourers/csv" target="_blank"><i class="fa fa-print"></i> Download as CSV</a>
			<?php echo $this->Form->hidden('start_date'); ?>
			<?php echo $this->Form->hidden('end_date'); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="group" style="overflow: auto">
		<div class="col12">

			<h2><?php echo $reportHeading ?></h2>

			<?php if ($projects): ?>
				<table class="agency-labourers-report">
					<tr class="date-headings">
						<td></td>
						<td></td>
						<?php foreach ($days as $day): ?>
							<td><?php echo $day->format('M j') ?></td>
						<?php endforeach; ?>
					</tr>

					<tr>
						<td>&nbsp;</td>
					</tr>

					<?php foreach ($projects as $projectName => $projectData): ?>
						<tr class="general-heading">
							<td>Site:</td>
							<td colspan="8"><?php echo $projectName ?></td>
						</tr>

						<tr class="general-heading">
							<td>Labourer</td>
							<td>Agency</td>
							<?php foreach ($days as $day): ?>
								<td><?php echo $day->format('l'); ?></td>
							<?php endforeach; ?>
							<td>Total Hours</td>
							<td>Total Cost</td>
							<td>Rate</td>
						</tr>

						<tr class="general-heading">
							<td><?php echo ($projectData['project_type_id'] == '1' ? 'Strip Out' : 'Day Works') ?></td>
						</tr>

						<?php foreach ($projectData['suppliers'] as $supplierName => $supplierData): ?>
							<?php foreach ($supplierData['employees'] as $employeeName => $employeeData): ?>
								<?php // output a timesheet line e.g. "John Smith,Redrock Agency,07:00 - 16:00,...,07:00 - 16:00,40.5,450,9.95 p/h" ?>
								<tr>
									<td><?php echo $employeeName ?></td>
									<td><?php echo $supplierName ?></td>
									<?php foreach ($days as $day): ?>
										<td>
											<?php if (!empty($employeeData['days'][$day->format('d-m-Y')])): ?>
												<?php $dayData = $employeeData['days'][$day->format('d-m-Y')]; ?>
												<?php echo sprintf('%s - %s', $dayData['start_time'], $dayData['end_time']); ?>
											<?php endif; ?>
										</td>
									<?php endforeach; ?>
									<td><?php echo $employeeData['total_hours_worked'] ?></td>
									<td>&pound;<?php echo number_format($employeeData['total_cost'], 2) ?></td>
									<td>&pound;<?php echo number_format($employeeData['average_rate_ph'], 2) ?> p/h</td>
								</tr>
							<?php endforeach; ?>
						<?php endforeach; ?>

						<tr>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td>&nbsp;</td>
						</tr>
					<?php endforeach; ?>
				</table>
			<?php else: ?>
				<p><i>No results for this date range</i></p>
			<?php endif; ?>

		</div>
	</div>

</div>	


