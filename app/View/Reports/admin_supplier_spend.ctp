<style type="text/css" media="print">
	@page {
		size: portrait;
	}
</style>

<div class="content report">
	<div class="group noprint">
		<div class="col3">
			<h1>Supplier Spend</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/supplier_spend', 'class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select month...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
			<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('start_date',array('label'=>'From ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('end_date',array('label'=>'To ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col1">
			<button type="submit" name="prev-month" class="button button-primary"><i class="fa fa-minus"></i> Month</button>
		</div>
		<div class="col1">
			<button type="submit" name="next-month" class="button button-primary"><i class="fa fa-plus"></i> Month</button>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="group">
		<div class="col12">

			<?php if (isset($suppliers)): $grandTotal = 0; ?>
				<h2><?php echo $reportHeading ?></h2>
				<?php if (!empty($suppliers)): ?>
					<table class="report">
						<tr>
							<th>Supplier</th>
							<th>Spend £</th>
						</tr>
						<?php foreach ($suppliers as $supplier): ?>
							<tr>
								<td><?php echo h($supplier['Supplier']['name']) ?></td>
								<td><?php
									$grandTotal += $supplier[0]['spend'];
									echo number_format($supplier[0]['spend'], 2);
									?>
								</td>
							</tr>
						<?php endforeach; ?>
						<tr>
							<td><b>Total</b></td>
							<td><b><?php echo number_format($grandTotal, 2) ?></b></td>
						</tr>
					</table>
				<?php else: ?>
					<p><i>No results in this date range</i></p>
				<?php endif; ?>
			<?php else: ?>
				<p><i>From date is after To date</i></p>
			<?php endif; ?>

		</div>
	</div>

</div>

