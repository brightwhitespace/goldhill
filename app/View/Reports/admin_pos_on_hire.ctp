
<div class="content">
	<div class="group">
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col5">
			<h1>POs On Hire</h1>
		</div>
		<div class="col2 right noprint">
			&nbsp;
		</div>
		<div class="col2 right noprint">
			&nbsp;
		</div>
		<div class="col3">
			<?php
				echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
				echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if(!empty($this->request->data['Search'])): ?>
				<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
		
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('ref');?></th>
			<th style="width: 15%"><?php echo $this->Paginator->sort('Project.project_name', 'Project');?></th>
			<th style="width: 35%"><?php echo $this->Paginator->sort('desc');?></th>
			<th><?php echo $this->Paginator->sort('supplier_id');?></th>
			<th><?php echo $this->Paginator->sort('User.firstname', 'PO Contact');?></th>
			<th><?php echo $this->Paginator->sort('Employee.name', 'Site Contact');?></th>
			<th style="width: 90px"><?php echo $this->Paginator->sort('end_date', 'End Date');?></th>
			<th width="50" class="actions"></th>
		</tr>
		<?php
			$i = 0;
			foreach ($purchase_orders as $purchase_order):
				$class = null;
				if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] > 0) {
					$class = ' class="complete"';
				}
				if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] == 0) {
					$class = ' class="partial-complete"';
				}
				if ($purchase_order['PurchaseOrder']['void'] == 1) {
					$class = ' class="void"';
				}
				if($purchase_order['PurchaseOrder']['type_id'] == 1 && $purchase_order['PurchaseOrder']['returned'] == 0 && strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW + 3 days')):
					$class = ' class="warning"';
				endif;
				if($purchase_order['PurchaseOrder']['type_id'] == 1 && $purchase_order['PurchaseOrder']['returned'] == 0 && strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW')):
					$class = ' class="alert"';
				endif;
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($purchase_order['PurchaseOrder']['ref'], array('controller' => 'purchase_orders', 'action' => 'edit', $purchase_order['PurchaseOrder']['id'])); ?>&nbsp;</td>
				<td><?php echo $purchase_order['Project']['name']; ?>&nbsp;</td>
				<td><?php echo $purchase_order['PurchaseOrder']['desc']; ?>&nbsp;</td>
				<td><?php echo $purchase_order['Supplier']['name']; ?></td>
				<td><?php echo $purchase_order['User']['name']; ?>&nbsp;</td>
				<td><?php echo $purchase_order['Employee']['name']; ?>&nbsp;</td>
				<td><?php echo $purchase_order['PurchaseOrder']['end_date']; ?>&nbsp;</td>

				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller' => 'purchase_orders', 'action' => 'edit', $purchase_order['PurchaseOrder']['id']),array('escape'=>false)); ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$purchase_orders): ?>
			<tr>
				<td colspan="8">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
</div>	


