
<div class="content report lab-analysis-report">
	<div class="group noprint">
		<div class="col3">
			<h1>Labour Analysis</h1>
		</div>
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('client_id', array('label'=>false,'type'=>'select','options'=>$clients,'empty'=>'Select Client...','onchange'=>'$("#SearchProjectId").val(""); this.form.submit()')); ?>
		</div>
		<div class="col3">
			<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select week...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
			<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
		</div>
		<div class="col2">
			<button type="submit" name="prev-week" class="button button-primary"><i class="fa fa-minus"></i> Week</button> &nbsp;
			<button type="submit" name="next-week" class="button button-primary"><i class="fa fa-plus"></i> Week</button>
			<?php echo $this->Form->hidden('start_date'); ?>
			<?php echo $this->Form->hidden('end_date'); ?>
		</div>
		<div class="col1 right noprint">
			<?php if (!empty($this->request->data['Search']['client_id'])): ?>
				<a href="/admin/reports/weekly_analysis/pdf">Download PDF</a><br>
				<a href="/admin/reports/weekly_analysis/view" target="_blank">View PDF</a>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<?php if (!empty($days)): ?>
		<div class="group" style="overflow: auto">
			<div class="col12">

				<h2><?php echo $reportHeading ?></h2>

				<table class="lab-analysis-report">
					<tr>
						<th>Name</th>
						<?php foreach ($days as $day): ?>
							<th colspan="2"><?php echo $day->format('l') ?><br><?php echo $day->format('d/m/Y') ?></th>
						<?php endforeach; ?>
					</tr>
					<?php $rowIndex = 0 ?>
					<?php foreach ($employees as $employeeName => $employeeDaysWorked): $rowIndex++ // a single "row" is actually two table rows as the employee name is spanned across both ?>
						<tr class="<?php echo $rowIndex % 2 == 0 ? 'even-row' : 'odd-row'; ?>">
							<td rowspan="2"><?php echo h($employeeName) ?></td>
							<?php foreach ($days as $day): ?>
								<?php if (!empty($employeeDaysWorked[$day->format('d-m-Y')]['D'])): ?>
									<td class="project-type active">D</td>
									<td class="project-name">
										<?php foreach ($employeeDaysWorked[$day->format('d-m-Y')]['D'] as $projectName): ?>
											<div class="ellipsis"><?php echo $projectName ?></div>
										<?php endforeach; ?>
									</td>
								<?php else: ?>
									<td class="project-type">D</td>
									<td class="project-name"></td>
								<?php endif; ?>
							<?php endforeach; ?>
						</tr>
						<tr class="<?php echo $rowIndex % 2 == 0 ? 'even-row' : 'odd-row'; ?>">
							<?php foreach ($days as $day): ?>
								<?php if (!empty($employeeDaysWorked[$day->format('d-m-Y')]['P'])): ?>
									<td class="project-type active">P</td>
									<td class="project-name">
										<?php foreach ($employeeDaysWorked[$day->format('d-m-Y')]['P'] as $projectName): ?>
											<div class="ellipsis"><?php echo $projectName ?></div>
										<?php endforeach; ?>
									</td>
								<?php else: ?>
									<td class="project-type">P</td>
									<td class="project-name"></td>
								<?php endif; ?>
							<?php endforeach; ?>
						</tr>
					<?php endforeach; ?>
				</table>

			</div>
		</div>
	<?php else: ?>
		<p><i><?php echo (empty($this->request->data['Search']['project_id'])) ? '' : 'From date is after To date' ?></i></p>
	<?php endif; ?>

</div>	


