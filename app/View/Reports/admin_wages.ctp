
<div class="content report wages-report">
	<div class="group noprint">
		<div class="col3">
			<h1>Wages Report</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/wages', 'class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select week...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
			<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('start_date',array('label'=>'From ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('end_date',array('label'=>'To ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col1">
			<button type="submit" name="prev-week" class="button button-primary"><i class="fa fa-minus"></i> Week</button>
		</div>
		<div class="col1">
			<button type="submit" name="next-week" class="button button-primary"><i class="fa fa-plus"></i> Week</button>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<?php
		if ($days):
			$grandTotal = 0;
	?>
		<div class="group" style="overflow: auto">
			<div class="col12">

				<h2><?php echo $reportHeading ?></h2>

				<table class="wages-report">
					<tr>
						<th>Name</th>
						<?php foreach($days as $day): ?>
							<th style="text-align: center"><?php echo $day->format('l') ?><br><?php echo $day->format('d/m/Y') ?></th>
						<?php endforeach; ?>
						<th>Damages<br>Repayment</th>
						<th>Adjustment</th>
						<th>Total</th>
					</tr>
					<?php
					$currentPaymentMethodName = '';
					foreach ($employees as $employee): //debug($employee);
					?>
						<?php if (empty($employee['Employee']['archived']) || $employee['wage_total']): ?>
							<?php
							if ($employee['EmployeePaymentMethod']['name'] != $currentPaymentMethodName):
								$currentPaymentMethodName = $employee['EmployeePaymentMethod']['name']
							?>
								<tr>
									<td style="font-size: 1.6rem; font-weight: bold; padding: 20px 0 8px 8px"><?php echo h($currentPaymentMethodName) ?></td>
									<?php foreach($employee['wage_by_day'] as $date => $wage): ?>
										<td></td>
									<?php endforeach; ?>
									<td></td>
									<td></td>
									<td>&nbsp;</td>
								</tr>
							<?php endif; ?>
							<tr>
								<td><?php echo h($employee['Employee']['name']) ?></td>
								<?php foreach($employee['wage_by_day'] as $date => $wage): ?>
									<td>
										<?php echo $employee['hours_by_day'][$date] > 8 ? "<span style='color: red'>[" . $employee['hours_by_day'][$date] . "]</span>&nbsp;" : '' ?>
										<?php echo $wage ? number_format($wage, 2) : '' ?>
									</td>
								<?php endforeach; ?>
								<td style="<?php echo $employee['damage_repayments_total'] < 0 ? 'color: red' : '' ?>"><?php echo $employee['damage_repayments_total'] ? number_format($employee['damage_repayments_total'], 2) : '' ?>
								<td style="<?php echo $employee['adjustments_total'] < 0 ? 'color: red' : '' ?>"><?php echo $employee['adjustments_total'] ? number_format($employee['adjustments_total'], 2) : '' ?>
								</td>
								<td><?php
									$grandTotal += $employee['wage_total'];
									echo $employee['wage_total'] ? number_format($employee['wage_total'], 2) : '-'
									?>
								</td>
							</tr>
						<?php endif; ?>
					<?php endforeach; ?>
					<tr>
						<td><b>Total</b></td>
						<?php foreach($days as $day): ?>
							<td></td>
						<?php endforeach; ?>
						<td></td>
						<td><b><?php echo $grandTotal ? number_format($grandTotal, 2) : '-' ?></b></td>
					</tr>
				</table>

			</div>
		</div>
	<?php else: ?>
		<p><i>From date is after To date</i></p>
	<?php endif; ?>

</div>	


