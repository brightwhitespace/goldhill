<style type="text/css" media="print">
	@page {
		size: portrait;
	}
</style>

<div class="content report">
	<div class="group noprint">
		<div class="col3">
			<h1>Labour Report</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/labour', 'class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<?php echo $this->Form->input('date_range', array('label'=>false,'type'=>'select','options'=>$dateRangeOptions,'empty'=>'Select week...','onchange'=>'$("#SearchDateRangeSelected").val(1); this.form.submit()')); ?>
			<?php echo $this->Form->input('date_range_selected', array('type'=>'hidden','value'=>0)); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('start_date',array('label'=>'From ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col2">
			<?php echo $this->Form->input('end_date',array('label'=>'To ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col1">
			<button type="submit" name="prev-week" class="button button-primary"><i class="fa fa-minus"></i> Week</button>
		</div>
		<div class="col1">
			<button type="submit" name="next-week" class="button button-primary"><i class="fa fa-plus"></i> Week</button>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="group">
		<div class="col12">

			<h2><?php echo $reportHeading ?></h2>

			<div class="lab-report-container">

				<?php if (isset($days)): ?>

					<div class="group lab-day lab-day-header">
						<div class="lab-day-name">Date</div>
						<div class="lab-projects">
							<div class="lab-project">
								<div class="lab-project-name">Project</div>
								<div class="lab-labour-entries">
									<div class="lab-labour-entry group">
										<div>Qty</div>
										<div>Rate</div>
										<div>Total</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php if (!empty($days)): ?>
						<?php foreach($days as $dayKey => $day): ?>
							<div class="group lab-day">
								<div class="lab-day-name"><?php echo $dayKey ?></div>
								<div class="lab-projects">
									<?php foreach ($day as $projectKey => $labours): ?>
										<div class="lab-project group">
											<div class="lab-project-name"><?php echo h($projectKey) ?></div>
											<div class="lab-labour-entries">
												<?php foreach ($labours as $labour): ?>
													<div class="lab-labour-entry group">
														<div><?php echo $labour['Labour']['quantity'] ?></div>
														<div><?php echo number_format($labour['Labour']['rate'], 2) ?></div>
														<div><?php echo number_format($labour['Labour']['total'], 2) ?></div>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div class="group lab-day">
							<i>No results for this date range</i>
						</div>
					<?php endif; ?>

				<?php else: ?>
					<p><i><?php echo (empty($this->request->data['Search']['employee_id'])) ? '' : 'From date is after To date' ?></i></p>
				<?php endif; ?>

			</div>

		</div>
	</div>

</div>

