
<div class="content">
	<div class="group">
		<div class="col12">
			<h1>Site Visits</h1>
		</div>
	</div>
		
	<table>
		<tr>
			<th style="min-width: 100px"><?php echo $this->Paginator->sort('site_visit_date', 'Visit date/time');?></th>
			<th><?php echo $this->Paginator->sort('project_name', 'Project name');?></th>
			<th style="min-width: 120px"><?php echo $this->Paginator->sort('Client.name', 'Client');?></th>
			<th style="min-width: 130px"><?php echo $this->Paginator->sort('Quote.contact_name', 'Contact');?></th>
			<th><?php echo $this->Paginator->sort('address1', 'Address');?></th>
			<th><?php echo $this->Paginator->sort('notes');?></th>
		</tr>
		<?php foreach ($quotes as $quote): ?>
			<tr>
				<td><?php echo ($quote['Quote']['site_visit_date'] ? date('d M Y', strtotime($quote['Quote']['site_visit_date'])) : '') . '<br>' . $quote['Quote']['site_visit_time']; ?>&nbsp;</td>
				<td><?php echo $quote['Quote']['project_name']; ?>&nbsp;</td>
				<td><?php echo $quote['Client']['name']; ?>&nbsp;</td>
				<td><?php echo $quote['Quote']['contact_name'] . '<br>(' . $quote['Quote']['contact_tel'] . ')'; ?>&nbsp;</td>
				<td><?php echo $this->App->formatAddress($quote['Quote'], ', ') ?>&nbsp;</td>
				<td><?php echo nl2br($quote['Quote']['notes']) ?>&nbsp;</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$quotes): ?>
			<tr>
				<td colspan="5">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
</div>	


