Agency Labourers
,,<?php
	foreach ($days as $day):
		echo $day->format('j M y') . ',';
	endforeach;
?>

<?php
	foreach ($projects as $projectName => $projectData):
		echo "Site:,$projectName\n";

		echo "Labourer,Agency";
		foreach ($days as $day):
			echo ',' . $day->format('l');
		endforeach;
		echo ",,Total Hours,Total Cost,Rate\n";

		echo ($projectData['project_type_id'] == '1' ? 'Strip Out' : 'Day Works') . "\n";

		foreach ($projectData['suppliers'] as $supplierName => $supplierData):
			foreach ($supplierData['employees'] as $employeeName => $employeeData):
				// output a timesheet line e.g. "John Smith,Redrock Agency,07:00 - 16:00,...,07:00 - 16:00,40.5,9.95 p/h"
				echo "$employeeName,$supplierName,";
				foreach ($days as $day):
					if (!empty($employeeData['days'][$day->format('d-m-Y')])):
						$dayData = $employeeData['days'][$day->format('d-m-Y')];
						echo sprintf('%s - %s', $dayData['start_time'], $dayData['end_time']);
					endif;
					echo ',';
				endforeach;
				echo ',' . $employeeData['total_hours_worked'] . ',' . $employeeData['total_cost'] . ',' . $employeeData['average_rate_ph'] . " p/h\n";
			endforeach;
		endforeach;
		echo "\n\n";
	endforeach;
?>
