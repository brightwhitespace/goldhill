<style type="text/css" media="print">
	@page {
		size: portrait;
	}
</style>

<div class="content report diary-report">
	<div class="group noprint">
		<div class="col4">
			<h1>Diary Report</h1>
		</div>
		<?php echo $this->Form->create('Search', array('url'=>'/admin/reports/diary', 'class'=>'filter-form clearfix')); ?>
		<div class="col2 noprint">
			<?php echo $this->Form->input('date',array('label'=>'Date ','type'=>'text','class'=>'datepicker','style'=>'width: 70% !important','onchange'=>'this.form.submit()')); ?>
		</div>
		<div class="col6 right noprint">
		</div>
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="group">
		<div class="col12">
			<h2><?php echo $dateHeading ?></h2>
		</div>
	</div>

	<div class="diary-container">
		<div class="group">
			<?php if (!empty($projects)): ?>

				<?php
					$projectTypes = array(
						1 => 'Part 1 - Strip Out Works',
						2 => 'Part 2 - Day Works'
					);
				?>

				<?php foreach ($projectTypes as $projectTypeId => $projectTypeName): ?>
					<div class="col6">
						<h3><?php echo h($projectTypeName) ?></h3>
						<?php foreach ($projects as $project): ?>
							<?php if ($project['Project']['project_type_id'] == $projectTypeId): ?>

								<div class="diary-project">
									<p><b><?php echo h($project['Project']['name']) ?></b></p>

									<?php $previousTitle = ''; ?>
									<?php foreach ($project['items'] as $item): ?>
										<?php if (!empty($item['title']) || !empty($item['description'])): ?>
											<?php echo ((!empty($item['title']) && $item['title'] != $previousTitle) ? '<p class="title">' . h($item['title']) . '</p>' : '') . $item['description'] . '<br>'; ?>
											<?php $previousTitle = !empty($item['title']) ? $item['title'] : '' ?>
										<?php endif; ?>
									<?php endforeach; ?>
								</div>

							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>

			<?php else: ?>
				<div class="col12">
					<p><i>No results for this day</i></p>
				</div>
			<?php endif; ?>
		</div>
	</div>

</div>

