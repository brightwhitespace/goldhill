<div class="content">
	<h2>Add Labour</h2>
	
	<?php echo $this->Form->create('Labour',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
				echo $this->Form->input('project_id',array('label'=>'Project<em>*</em>','type'=>'select','options'=>$projects,'empty'=>'Please select'));
				echo $this->Form->input('date',array('label'=>'Date<em>*</em>','type'=>'text','class'=>'datepicker'));
				echo $this->Form->input('quantity',array('label'=>'Quantity<em>*</em>','type'=>'number'));
				echo $this->Form->input('rate',array('label'=>'Rate<em>*</em>','type'=>'number','class'=>'currency'));
				echo $this->Form->input('total',array('label'=>'Total<em>*</em>','type'=>'number','class'=>'currency'));
			?>
		</div>
		
	</div>
	<div class="group">
		<div class="col6">
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
		<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
