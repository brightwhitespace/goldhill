
<div class="content">
	<h2>Edit Certificate</h2>

	<?php echo $this->Form->create('Certificate');?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('name',array('label'=>'Name<em>*</em>','type'=>'text'));
			echo $this->Form->input('expiry_warning_days',array('label'=>'Email warning if expires within<em>*</em>','type'=>'select','options'=>$expiryNotificationTimeOptions,'empty'=>'Please select...'));
			?>
		</div>
	</div>
	<div class="group">
		<div class="col6">
			<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>
