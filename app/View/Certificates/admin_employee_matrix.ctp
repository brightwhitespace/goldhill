<div class="content">
	<div class="group">
		<div class="col2">
			<h2>Training Matrix</h2>
		</div>
		<div class="col2">
			<a href="#" onclick="return openAddCertificatePopup(this)" class="button button-primary" data-cert-id="" data-employee-id="" data-start-date="" data-end-date="">
				<i class="fa fa-plus-circle"></i> ADD CERTIFICATION
			</a>
		</div>
	</div>

	<div style="xoverflow-x: auto">

		<table class="cols-align-top employee-matrix">
			<tr>
				<th class="employee-no">#</th>
				<th class="employee-name">Sub-contractor</th>
				<?php foreach ($certificates as $certificate): ?>
					<th class="rotate">
						<div><span><?php echo $certificate['Certificate']['name']; ?></span></div>
					</th>
				<?php endforeach; ?>
				<th>&nbsp;</th>
			</tr>

			<?php foreach ($employees as $employee): ?>
				<tr>
					<td class="employee-no">
						<?php echo $employee['Employee']['employee_number'] ?>
					</td>
					<td class="employee-name">
						<?php echo $this->Html->link($employee['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $employee['Employee']['id'])); ?>
					</td>
					<?php foreach ($certificates as $certificate):  // add a column for each available certificate ?>
						<?php echo $this->element('training_matrix_table_cell', array('employee'=>$employee, 'certificate'=>$certificate)) ?>
					<?php endforeach; ?>
					<td>
						&nbsp;
					</td>
				</tr>
			<?php endforeach; ?>
		</table>

	</div>

	<?php if (empty($employees)): ?>
		<p>No employees found</p>
	<?php endif; ?>

</div>

<div id="add-cert-popup" class="overlay-block">
	<div class="overlay-content overlay-content-small vertical-centre">
		<a href="#" class="button button-primary small overlay-close"><i class="fa fa-close"></i></a>
		<h2>Add / Update Certificate</h2>
		<?php echo $this->Form->input('add_cert_employee_id', array('label'=>'Employee<em>*</em>','type'=>'select','options'=>$employeeList,'empty'=>'Select employee...','required'=>true)) ?>
		<?php echo $this->Form->input('add_cert_cert_id', array('label'=>'Certificate<em>*</em>','type'=>'select','options'=>$certificateList,'empty'=>'Select certificate...','required'=>true)) ?>
		<?php echo $this->Form->input('add_cert_start_date', array('label'=>'Start Date','type'=>'text','class'=>'datepicker')) ?>
		<?php echo $this->Form->input('add_cert_end_date', array('label'=>'End Date','type'=>'text','class'=>'datepicker')) ?>
		<p>&nbsp;</p>
		<div class="group">
			<a href="#" class="button button-secondary overlay-close" style="float: none">CANCEL</a>
			<a href="#" onclick="addEmployeeCertificate(<?php echo empty($reloadPageAfterCertUpdate) ? 'false' : 'true' ?>)" class="button button-primary floatright">ADD / UPDATE</a>
		</div>
	</div>
</div>
