<div class="content">
	<div class="group">
		<div class="col3">
			<h2><?php echo empty($archive) ? 'Certificates' : 'Certificate Archive' ?></h2>
		</div>
		<div class="col6 right">
			<a href="/admin/certificates/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Certificate</a>
			<a href="/admin/certificates/order" class="button button-primary"><i class="fa fa-reorder"></i> Re-order</a>
		</div>
		<div class="col3">
			<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
			<?php
			echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
			echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if (!empty($this->request->data['Search'])): ?>
				<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
			<?php endif; ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<table>
		<style>
			td { vertical-align: top }
		</style>
		<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('expiry_warning_days', 'Email warning if expires within');?></th>
			<th style="width: 62px" class="actions"></th>
		</tr>
		<?php
		$i = 0;
		foreach ($certificates as $certificate):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($certificate['Certificate']['name'], array('action' => 'edit', $certificate['Certificate']['id'])); ?>&nbsp;</td>
				<td><?php echo $expiryNotificationTimeOptions[$certificate['Certificate']['expiry_warning_days']]; ?>&nbsp;</td>
				<td class="actions">
					<?php if ($userIsAdmin): ?>
						<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $certificate['Certificate']['id']),array('escape'=>false)); ?>
					<?php endif; ?>

					<?php /*if ($userIsAdmin): ?>
						<?php
						if ($certificate['Certificate']['archived']):
							// certificate is archived so show Undo icon
							echo $this->Html->link('<i class="fa fa-undo fa-2x" title="Restore from Archive"></i>', array('controller' => 'certificates', 'action' => 'archive', $certificate['Certificate']['id'],0), array('escape'=>false,'class'=>'deleteButton'));
						else:
							// show archive icon
							echo $this->Html->link('<i class="fa fa-archive fa-2x" title="Move to Archive"></i>', array('controller' => 'certificates', 'action' => 'archive', $certificate['Certificate']['id'], 1), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this certificate to Archive?'));
						endif;
						?>
					<?php endif; */ ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$certificates): ?>
			<tr>
				<td colspan="3">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>
