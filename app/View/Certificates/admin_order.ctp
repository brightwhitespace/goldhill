
<div class="content">
	<?php echo $this->Form->create('Certificate');?>
	<div class="group">
		<div class="col6">
			<h2>Re-order Certificates</h2>
			<p>Drag and drop the certificates to change the order</p>
			<div id="ajax-status">order saved</div>

			<ol class="sortable" id="sortable_1">
				<?php
				$i = 0;
				foreach ($certificates as $certificate):
				?>
					<li id="certificate_<?php echo $certificate['Certificate']['id']; ?>">
						<div><?php echo $certificate['Certificate']['name']; ?></div>
					</li>
				<?php endforeach; ?>
			</ol>

			<?php echo $this->Form->button('save order', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton saveOrder','id'=>'saveOrder_certificates')); ?>
			<?php echo $this->Html->link('Back',array('action'=>'index'),array('class'=>'cancelLink')); ?>
		</div>
		<div class="col6">
		</div>
	</div>
</div>