<div class="content">
	<h2>Edit Contact</h2>
	
	<?php echo $this->Form->create('Contact',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			
			<?php
				echo $this->Form->input('Contact.id');
				echo isset($this->request->data['Contact']['client_id']) ? $this->Form->input('Contact.client_id',array('type'=>'hidden')) : '';
				echo isset($this->request->data['Contact']['supplier_id']) ? $this->Form->input('Contact.supplier_id',array('type'=>'hidden')) : '';	
				echo $this->Form->input('Contact.firstname',array('label'=>'First name<em>*</em>','type'=>'text'));
				echo $this->Form->input('Contact.lastname',array('label'=>'Last name<em>*</em>','type'=>'text'));
				echo $this->Form->input('Contact.tel',array('label'=>'Mobile<em>*</em>','type'=>'text'));
				echo $this->Form->input('Contact.email',array('label'=>'Email<em>*</em>','type'=>'text'));
			?>
		</div>
		
	</div>
	<div class="group">
		<div class="col6">
		<?php echo $this->Html->link('Cancel',$returnUrl,array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
		<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
