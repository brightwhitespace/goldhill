
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/forms/add">Add Form</a></li>
		</ul>
		<h1>Forms</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
	
	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th>Short Code</th>
			<th><?php echo $this->Paginator->sort('email');?></th>			
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($forms as $form):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $form['Form']['id']; ?>&nbsp;</td>
		<td><?php echo $this->Html->link($form['Form']['name'],array('action'=>'view', $form['Form']['id'])); ?>&nbsp;</td>
		<td>[form]</td>
		<td><?php echo $form['Form']['email']; ?>&nbsp;</td>
		<td><?php echo $this->Time->timeAgoInWords($form['Form']['modified']); ?>&nbsp;</td>
		<td><?php echo $this->Time->format('d/m/Y',$form['Form']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('EXPORT', array('action'=>'export', $form['Form']['id']),array('escape'=>false,'title'=>'Save Results to CSV'),null,false); ?>
			<?php echo $this->Html->link('VIEW', array('action'=>'view', $form['Form']['id']),array('escape'=>false),null,false); ?>
			<?php echo $this->Html->link('EDIT', array('action'=>'edit', $form['Form']['id']),array('escape'=>false),null,false); ?>
			<?php echo $this->Html->link('DELETE', array('action'=>'delete', $form['Form']['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete # %s?', true), $form['Form']['id']),false); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
	<div class="pagination clearfix">
		<?php echo $this->Paginator->prev(__('&laquo; previous',true), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;',true), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>

</div>

</div>