<div id="page-title">
	<div class="container clearfix">
		
		<h1>Edit Form</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
	<div id="tabs">
<ul class="subsection_tabs">  
     <li class="tab"><a href="#desc">Details</a></li>
     <li class="tab"><a href="#fields">Fields</a></li>  
</ul>
<?php echo $this->Form->create('Form',array('class'=>'cmxform'));?>
<div id="desc" class="tabArea">
	<fieldset>
 		<legend><?php __('Edit Form'); ?></legend>
		<ol>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('page_id',array('label'=>'Page','type'=>'select','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
			echo $this->Form->input('name',array('label'=>'Form Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Form->input('description',array('type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Form->input('email',array('label'=>'Email address<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
			echo $this->Form->input('response',array('label'=>'Response Message<em>*</em>','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
		?>
		</ol>
	</fieldset>
</div>
<div id="fields" class="tabArea">
	<fieldset>
		<table id="formFields">
			<tr>
				<th>Name</th>
				<th>Label</th>
				<th>Type</th>
				<th>Options</th>
				<th>Required</th>
				<th></th>
			</tr>
			<tbody id="sortableRows">
			<?php if(empty($this->data['FormField'])): ?>
			<tr>
				<td>
				<?php 
				echo $this->Form->input('FormField.0.name',array('label'=>false,'type'=>'text','class'=>'medtextbox','div'=>false)); 
				echo $this->Form->input('FormField.0.order_num',array('type'=>'hidden','class'=>'fieldOrderNum'));
				?>
				</td>
				<td><?php echo $this->Form->input('FormField.0.label',array('label'=>false,'type'=>'text','class'=>'medtextbox','div'=>false)); ?></td>
				<td><?php echo $this->Form->input('FormField.0.type',array('label'=>false,'type'=>'select','options'=>$fieldTypes,'div'=>false)); ?></td>
				<td><?php echo $this->Form->input('FormField.0.options',array('label'=>false,'type'=>'textarea','class'=>'bigtextbox','div'=>false)); ?></td>
				<td><?php echo $this->Form->input('FormField.0.required',array('label'=>false,'type'=>'checkbox','div'=>false)); ?></td>
				<td></td>
			</tr>
			<?php else: ?>
			
				<?php 
				$count = 0;
				
				foreach($this->data['FormField'] as $field): ?>
				
				<tr id="field_<?php echo $count; ?>">
					<td>
					<?php
					echo $this->Form->input('FormField.'.$count.'.id',array('type'=>'hidden'));
					echo $this->Form->input('FormField.'.$count.'.order_num',array('type'=>'hidden')); 
					echo $this->Form->input('FormField.'.$count.'.name',array('label'=>false,'type'=>'text','class'=>'medtextbox','div'=>false)); ?>
					</td>
					<td><?php echo $this->Form->input('FormField.'.$count.'.label',array('label'=>false,'type'=>'text','class'=>'medtextbox','div'=>false)); ?></td>
					<td><?php echo $this->Form->input('FormField.'.$count.'.type',array('label'=>false,'type'=>'select','options'=>$fieldTypes,'div'=>false)); ?></td>
					<td><?php echo $this->Form->input('FormField.'.$count.'.options',array('label'=>false,'type'=>'textarea','class'=>'bigtextbox','div'=>false)); ?></td>
					<td><?php echo $this->Form->input('FormField.'.$count.'.required',array('label'=>false,'type'=>'checkbox','div'=>false)); ?></td>
					<td><?php echo $this->Html->link($this->Html->image('/assets/admin/icon_delete.png'), array('action'=>'deletefield', $field['id']), array('escape'=>false), sprintf(__('Are you sure you want to delete # %s?\n\nMake sure you save all changes to the form before proceeding.', true), $field['name']),false); ?>
					<?php echo $this->Html->image('/assets/admin/icon_up_sm.png',array('class'=>'dragHandle','onclick'=>'moveTableRowUp("field_'.$count.'")')); ?>
					<?php echo $this->Html->image('/assets/admin/icon_down_sm.png',array('class'=>'dragHandle','onclick'=>'moveTableRowDown("field_'.$count.'")')); ?>
					</td>
				</tr>
				
				<?php $count++; endforeach; ?>
			
			<?php endif; ?>
			</tbody>
		</table>
		<div class="textButton">
			<?php echo $this->Html->link('ADD FIELD','#',array('id'=>'addFormField','escape'=>false)); ?>
		</div>
		
	</fieldset>
</div>
</div>
	<?php echo $this->Form->button('save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton')); ?>

	<?php echo $this->Form->button('save & close', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
	
	<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
<?php echo $this->Form->end();?>

</div>

</div>