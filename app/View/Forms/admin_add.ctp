<div id="page-title">
	<div class="container clearfix">
		
		<h1>Add Form</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
<p>Fill in the form below to add a new form. Once the form is added you can embed it into pages of the website. If you enter an email address, the form responses will be emailed to this address. If you do not enter an email address, the responses will be stored in the database.</p>
<?php echo $this->Form->create('Form',array('class'=>'cmxform'));?>
	<fieldset>
 		<legend><?php __('Add Form'); ?></legend>
		<ol>
	<?php
		echo $this->Form->input('page_id',array('label'=>'Page','type'=>'select','options'=>$this->Tree->threadedList($pages,'Page','page_title'),'class'=>'selectbox','before'=>'<li>','after'=>'</li>','div'=>false,'escape'=>false));
		echo $this->Form->input('name',array('label'=>'Form Name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		echo $this->Form->input('description',array('type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
		echo $this->Form->input('email',array('label'=>'Email address<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		echo $this->Form->input('response',array('label'=>'Response Message<em>*</em>','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false));
	?>
		</ol>
	</fieldset>
	<?php echo $this->Form->button('save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'submitButton')); ?>

	<?php echo $this->Form->button('save & close', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
	
	<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>

<?php echo $this->Form->end();?>

</div>

</div>
