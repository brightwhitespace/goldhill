<?php if($page['Page']['page_title'] != 'Home'): ?> 
	<div class="breadcrumbs"><strong>You Are Here:</strong> <?php echo $this->Pages->breadCrumbs($page); ?></div>
<?php endif; ?>

<div id="column1alt" class="column">
	<div id="section">		
		<?php 
		if(!empty($page['Page']['page_image'])):
			echo $this->Html->image($page['Page']['page_image'],array('id'=>'sectionImage','alt'=>$page['Page']['page_title']));			
		endif; ?>
		<h2><?php echo h($page['Page']['page_title']); ?></h2>
		
		<?php if(!empty($page['Sub'])): ?>
		<p>In this section:</p>
		<ul>
			<?php foreach($page['Sub'] as $subPage): ?>
				<li<?php echo $page['Page']['id'] == $subPage['id'] ? ' class="subSelected"' : ''; ?>><a href="<?php echo $this->Pages->pageUrl($subPage); ?>" title="<?php echo $subPage['page_title']; ?>"><?php echo $subPage['page_title']; ?></a></li>
			<?php endforeach; ?>			
		</ul>
		<?php endif; ?>
	</div>
</div>

<div id="column2alt" class="column">
	<?php echo $page['Page']['page_body']; ?>
	
	<?php echo $this->Form->create('Form',array('class'=>'cmxform'));?>
	<p><em>*</em> indicates required field</p>
	<fieldset>
		<legend><?php echo __('Subscription Type'); ?></legend>
		<ol>
		<?php echo $this->Formtips->input('subType',array('legend'=>false,'type'=>'radio','options'=>$subs,'class'=>'radiobutton','before'=>'<li class="bigLabel">','after'=>'</li>','div'=>false,'separator'=>'</li><li class="bigLabel">'),'Please select which subscription package you would like');?>
		</ol>
	</fieldset>
	<fieldset>
 		<legend><?php echo __('Your personal details');?></legend>
		
	<?php
		echo $this->Formtips->input('firstname',array('label'=>'Your first name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter your first name/christian name in the box');
		echo $this->Formtips->input('lastname',array('label'=>'Your last name<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter your last name/surname name in the box');
		echo $this->Formtips->input('address1',array('label'=>'Address line 1<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter the first line of your address e.g. house number and street');
		echo $this->Form->input('address2',array('label'=>'Address line 2','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		echo $this->Form->input('address3',array('label'=>'Address line 3','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		echo $this->Formtips->input('city',array('label'=>'City/Town<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter the city or town you live in e.g. London');
		echo $this->Formtips->input('county',array('label'=>'County<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter the county you live e.g. Warwickshire');
		echo $this->Formtips->input('postcode',array('label'=>'Postcode<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter your postcode');
		echo $this->Formtips->input('telephone',array('label'=>'Telephone<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter a contact telephone number');
		echo $this->Formtips->input('email',array('label'=>'Email address<em>*</em>','type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please enter your email address');
		echo $this->Formtips->input('Your Date of Birth',array('type'=>'date','dateFormat'=>'DMY','before'=>'<li>','after'=>'</li>','div'=>false,'class'=>'dateTime','minYear'=>'1900'),'Please enter your email address');
	?>	
	</fieldset>
	
	<fieldset>
		<legend><?php echo __('More Details');?></legend>		
		<ol>
		<?php
		
		echo $this->Formtips->input('reason',array('label'=>'Reason For Taking Warfarin','type'=>'textarea','class'=>'bigtextbox','before'=>'<li>','after'=>'</li>','div'=>false),'Please tell us the reason you are taking Warfarin'); 
		
		echo $this->Form->input('attend_hospital',array('label'=>'I attend a hospital clinic','type'=>'select','options'=>$yesNo,'before'=>'<li>','after'=>'</li>','div'=>false,'empty'=>'-- Please Select --'));
		echo $this->Form->input('attend_clinic',array('label'=>'I attend a GP clinic','type'=>'select','options'=>$yesNo,'before'=>'<li>','after'=>'</li>','div'=>false,'empty'=>'-- Please Select --'));
		echo $this->Form->input('attend_pharmacy',array('label'=>'I attend a pharmacy clinic','type'=>'select','options'=>$yesNo,'before'=>'<li>','after'=>'</li>','div'=>false,'empty'=>'-- Please Select --'));
		echo $this->Formtips->input('other',array('label'=>'Other (please state)','class'=>'bigtextbox','type'=>'textarea','before'=>'<li>','after'=>'</li>','div'=>false),'If non of the above are applicable please give details');
		echo $this->Form->input('self_test',array('label'=>'Do you self-test?','type'=>'select','options'=>$yesNo,'before'=>'<li>','after'=>'</li>','div'=>false,'empty'=>'-- Please Select --'));
		echo $this->Form->input('self_manage',array('label'=>'Do you self-manage?','type'=>'select','options'=>$yesNo,'before'=>'<li>','after'=>'</li>','div'=>false,'empty'=>'-- Please Select --'));
		echo $this->Form->input('enough_info',array('label'=>'Do you feel you were given enough information at the start of your treatment','type'=>'select','options'=>$yesNo,'before'=>'<li>','after'=>'</li>','div'=>false,'empty'=>'-- Please Select --'));
		?>
		</ol>
		
	</fieldset>
	<fieldset>
		<legend><?php echo __('Data Protection');?></legend>	
		<p>In accordance with the Data Protection Act, AntiCoagulation Europe will not pass your details to another party. However, from time to time we may wish to contact you with anticoagulation surveys or new information that may interest you.</p>
		<ol>
		<?php echo $this->Form->input('data_privacy',array('label'=>'If you do not wish to receive such information please tick this box.','type'=>'checkbox','before'=>'<li class="bigLabel">','after'=>'</li>','div'=>false)); ?>
		</ol>		
	</fieldset>
	
	<input type="image" name="submit" src="/assets/images/button_payment.gif" alt="continue to payment" />
	
<?php echo $this->Form->end();?>
	
</div>



<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick-subscriptions" />
<!--<input type="hidden" name="sra" value="1" />-->
<input type="hidden" name="src" value="1">


<input type="hidden" name="a3" value=" " />
<input type="hidden" name="p3" value="1" />
<input type="hidden" name="t3" value="Y" />

<input type="hidden" name="business" value="eve.knight@ntlworld.com" /><!--?php echo $rows ;robins_1246369464_biz@gmail.com?>-->
<!--<input type="hidden" name="invoice" value="IYTA1MmYxZmNjZjQ3" />-->
<!--<input type="hidden" name="invoice" value="" />-->
<!--<input type="hidden" name="cancel_return" value="" />--> <!--? echo JURI::root()?>-->
<!--<input type="hidden" name="notify_url" value="" />--><!--? echo JURI::root()?>-->
<input type="hidden" name="item_number" value="1" />
<input type="hidden" name="custom" value="358">
<input type="hidden" name="item_name" value=" to http://www.anticoagulationeurope.org/" />
<input type="hidden" name="no_shipping" value="1" />

<input type="hidden" name="no_note" value="1" />
<input type="hidden" name="rm" value="2" />
<input type="hidden" name="return" value="http://www.anticoagulationeurope.org/index.php?option=com_members&task=paypalthanks" />
<input type="hidden" name="currency_code" value="GBP" />
<!--<input type="hidden" name="lc" value="US" />-->
<center>

<br>
<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="center" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
	<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
	<!--<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="center" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
	<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">-->
</center>
</form>
