
<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><?php echo $this->Html->link('Edit Form',array('action'=>'edit',$form['Form']['id']),array('escape'=>false)); ?></li>
		</ul>
		<h1>View Form</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
	
<div id="tabs">
<ul>  
     <li class="tab"><a href="#desc">Details</a></li>
     <li class="tab"><a href="#results">Results</a></li>  
</ul>

<div id="desc" class="tabArea">
<div class="contentsection clearfix">
	<h2>Details</h2>
	<dl class="clearfix"><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Id</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $form['Form']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Name</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $form['Form']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Description</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $form['Form']['description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Created</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $form['Form']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Modified</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $form['Form']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
	
	<div class="textButton">
	
	</div>
</div>



</div>

<div id="results" class="tabArea">
	<?php if(!empty($form['FormResult'])): ?>
		
		<table>
			<tr>
				<?php foreach($form['FormField'] as $field): ?>
				<th><?php echo $field['label']; ?></th>
				<?php endforeach; ?>
				<th>Date Submitted</th>
			</tr>
			<?php foreach($form['FormResult'] as $result): ?>
			<tr>
				<?php foreach($form['FormField'] as $field): ?>
				<td><?php echo $this->Brightform->getValue($result,$field['name']); ?></td>
				<?php endforeach; ?>
				<td><?php echo $this->Time->format('H:i d/m/Y',$result['created']); ?></td>
			</tr>
			<?php endforeach; ?>
		</table>
		
		<div class="textButton">
			<?php echo $this->Html->link($this->Html->image('/assets/admin/icon_save.png').' Export Results', array('action'=>'export', $form['Form']['id']),array('escape'=>false,'title'=>'Save Results to CSV'),null,false); ?>
		</div>
		
	<?php else: ?>
		<p><em>No forms submitted yet.</em></p>
	<?php endif; ?>
</div>

</div>
</div>
</div>