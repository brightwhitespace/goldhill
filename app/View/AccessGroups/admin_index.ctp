<div id="page-title">
	<div class="container clearfix">
		<ul id="function-buttons">
			<li><a href="/admin/access_groups/add">Add Group</a></li>
		</ul>
		<h1>Access Groups</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
<table>
	<thead>
    <tr>
        <th>ID</th>
		<th>Name</th>
        <th colspan="2">Actions</th>
    </tr>
	</thead>
	<tbody>
    <?php 
	$i = 0;
	foreach ($accessGroups as $accessGroup): $group = $accessGroup['AccessGroup'];
		$class = null; 
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>   
	<tr<?php echo $class;?>>
        <td>
            <?php echo $group['id'] ?>    
        </td>
		<td>
            <?php echo $group['group_name'] ?>    
        </td>    
        <td class="actions">
			<?php echo $this->Html->link('EDIT','/admin/access_groups/edit/'.$group['id']); ?> 
			<?php echo $this->Html->link('PERMISSIONS','/admin/access_groups/permissions/'.$group['id']); ?>
           
			<?php echo $this->Html->link('DELETE', array('action'=>'delete', $group['id']), array('class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $group['group_name']),false); ?>
        </td>
    </tr>
    <?php endforeach; ?>
	</tbody>
</table>

</div>
</div>