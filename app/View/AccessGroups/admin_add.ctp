<div id="page-title">
	<div class="container clearfix">
		
		<h1>Add Access Group</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
<?php echo $this->Form->create('AccessGroup', array('class'=>'cmxform') );?>
	<fieldset>
 		<legend><?php echo __('Add Access Group');?></legend>
		<ol>
		<?php 
		echo $this->Form->input('group_name',array('type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		echo $this->Ckeditor->input('menu_html',array('label'=>'','type'=>'textarea','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		
		echo $this->Form->input('landing_page',array('type'=>'text','class'=>'textbox','before'=>'<li>','after'=>'</li>','div'=>false));
		?>
		</ol>
	</fieldset>
	<?php echo $this->Form->button('Create Group', array('name'=>'save & close','type'=>'submit','value'=>'close','class'=>'submitButton')); ?>
	<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'cancelLink')); ?>
<?php echo $this->Form->end();?>

</div>
</div>