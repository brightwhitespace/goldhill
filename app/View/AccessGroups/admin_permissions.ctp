<div id="page-title">
	<div class="container clearfix">
		
		<h1>Edit Access Group Permissions</h1>
	</div>
</div>	

<div id="content-wrapper">
	<div id="content" class="container clearfix">
<script type="text/javascript">
<!--
function CheckAll()
{
    for (i=0; i < document.permissionForm.elements.length; i++){
		document.permissionForm.elements[i].checked = 1;
	}
}
function UncheckAll(){
	for (i=0; i < document.permissionForm.elements.length; i++){
		document.permissionForm.elements[i].checked = 0;
	}
}
-->
</script>
<?php $group = $group['AccessGroup']; ?>
<form action="/admin/access_groups/permissions/<?php echo $group['id']?>" method="post" name="permissionForm" id="permissionForm">
<table>
	<tr>
	<th>Pages / Actions</th>
	<th>
	<a href="#" onclick="CheckAll()">allow all</a> | <a href="#"  onclick="UncheckAll()">deny all</a>
	</th>
	</tr>
	<?php
	foreach($accessList as $page => $access):
	?>
		<tr>
		<td><?php echo $page; ?></td>
			<?php if ($access == true): ?>
			<td class="positive"><input type="checkbox" name="<?php echo "data[{$group['group_name']}][{$i}]"; ?>" value="<?php echo $page; ?>" checked /></td>
			<?php else: ?>
			<td class="negative"><input type="checkbox" name="<?php echo "data[{$group['group_name']}][{$i}]"; ?>" value="<?php echo $page; ?>" /></td>
			<?php endif; ?>
		</tr>
	<?php
		$i++;
	endforeach;
	?>
</table>
<input type="hidden" name="group" value="<?php echo $group['group_name']; ?>" />
<input type="submit" value="Update permissions" />
</form>
</div>
</div>