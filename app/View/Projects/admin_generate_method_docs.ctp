<div class="content">
	<h2>Generate Method Statement & Risk Assessment</h2>
	
	<?php echo $this->Form->create('ProjectMethodDoc', array('novalidate'=>'novalidate')); ?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('client_name', array('label'=>'Client name','type'=>'text','default'=>$project['Client']['name']));
			echo $this->Form->input('pre_address_text', array('label'=>'Pre-address text','type'=>'text','default'=>'The demolition and associated works at'));
			echo $this->Form->input('project_address', array('label'=>'Project address','type'=>'textarea','style'=>'height: 105px','default'=>$this->App->formatAddress($project['Project'], "\n")));
			echo $this->Form->input('cover_date', array('label'=>'Date on first page','type'=>'text', 'default'=>date('jS F Y')));
			echo $this->Form->input('contract_ref', array('label'=>'Contract ref','type'=>'text', 'default'=>'GCL/'));

			echo $this->Form->input('site_contact_id',array('label'=>'Site contact (select to auto-populate fields below)','type'=>'select','options'=>$employeesList,'empty'=>'Please select...',
				'default'=>$defaultSiteContactId,'onchange'=>'updateMethodSiteContactFields()', 'style'=>'width: 50%'));
			echo $this->Form->input('site_contact_name', array('label'=>'Site contact name','type'=>'text'));
			echo $this->Form->input('site_contact_tel', array('label'=>'Site contact telephone','type'=>'text'));

			?>
			<div class="method-radio-options">
				<h3 style="margin-top: 2rem">Sections to include:</h3>
				<?php
				echo $this->Form->input('include_floor_coverings',array('legend'=>'Floor Coverings','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_suspended_ceilings',array('legend'=>'Suspended Ceilings','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_metal_stud_partitions',array('legend'=>'Metal Stud Partitions','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_blockwork_walls',array('legend'=>'Blockwork Walls','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_fixtures_and_fittings',array('legend'=>'Fixtures &amp; Fittings','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_sanitaryware',array('legend'=>'Kitchen/Sanitaryware','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_mech_and_electrical',array('legend'=>'Mech &amp; Electrical','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				echo $this->Form->input('include_mobile_towers',array('legend'=>'Mobile Towers','type'=>'radio','options'=>$this->App->yesNoOptions,'default'=>1));
				?>
			</div>
			<?php
			?>
		</div>
		<div class="col6">
			<?php
			echo $this->Form->input('review_date', array('label'=>'Initial review date','type'=>'text', 'default'=>date('jS F Y')));
			echo $this->Form->input('timescale', array('label'=>'Contract timescale','type'=>'text'));
			echo $this->Form->input('start_date', array('label'=>'Project start date','type'=>'text','default'=>date('jS F Y', $project['Project']['start_date'] ? strtotime($project['Project']['start_date']) : null)));
			echo $this->Form->input('max_num_employees_on_site',array('label'=>'Maximum no. of employees on site','type'=>'text','default'=>2));
			echo $this->Form->input('transport_of_tools',array('label'=>'Transport of tools - additional text','type'=>'text',
				'default'=>'All equipment will be taken up to project floor ...'));
			echo $this->Form->input('removal_of_waste',array('label'=>'Removal of waste - additional text','type'=>'text',
				'default'=>'Movements shall be from the project floor ...'));
			echo $this->Form->input('noise_restrictions',array('label'=>'Noise restrictions - initial text','type'=>'textarea','style'=>'height: 105px',
				'default'=>'The building and project floor are occupied. In view of the buildings work needs, out of hours working shall be required; this shall be by agreement and shall be approved in advance.'));
			echo $this->Form->input('health_safety_name',array('label'=>'Health &amp; safety consultant name','type'=>'text','default'=>'Jason Bassett'));
			echo $this->Form->input('health_safety_tel',array('label'=>'Health &amp; safety consultant telephone','type'=>'text','default'=>'07900 188105'));
			echo $this->Form->input('is_injury_risk',array('label'=>'Do operations involve significant risk of injury?','type'=>'text','default'=>'Yes'));
			echo $this->Form->input('can_be_avoided',array('label'=>'Can operations be avoided / mechanised / automated?','type'=>'text','default'=>'No'));
			echo $this->Form->input('within_acceptable_limits',array('label'=>'Are operations clearly within acceptable limits?','type'=>'text','default'=>'No'));
			echo $this->Form->input('overall_risk',array('label'=>'Overall assessment of risk?','type'=>'text','default'=>'Medium'));
			?>
		</div>
	</div>

	<p>&nbsp;</p>

	<div class="group">
		<div class="col6">
		<?php echo $this->Html->link('Cancel', $returnUrl, array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
		<?php echo $this->Form->button('Generate', array('name'=>'generate','type'=>'generate','value'=>'generate','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>

<script>
	window.onload = function () {
		// if site contact name has already been set then don't initially update name & tel fields based on dropdown
		if (!$('#ProjectMethodDocSiteContactName').val()) {
			updateMethodSiteContactFields();
		}
	};

	// employee data array will be of form { 1: { name: 'Name of employee id 1', tel: 'tel no' }, ... }
	var employeesData = <?php echo json_encode($employeesData) ?>;

	function updateMethodSiteContactFields() {
		// user has selected a different employee from the site contact dropdown so update the name & tel fields
		var selectedEmployeeId = $('#ProjectMethodDocSiteContactId').val();
		if (selectedEmployeeId) {
			var employee = employeesData[selectedEmployeeId];
			console.log(employee);
			console.log(employee.name);
			$('#ProjectMethodDocSiteContactName').val(employee.name);
			$('#ProjectMethodDocSiteContactTel').val(employee.tel);
		}
	}
</script>