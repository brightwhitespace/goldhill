<div class="content">
	<div class="content-head group">
		<div class="col6">
			<h1><?php echo $project['Project']['name']; ?>
				<?php if($userIsAdmin): ?>
					<?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $project['Project']['id']),array('escape'=>false)); ?>
					<?php
					if ($project['Project']['archived']):
						// project is archived so show Undo icon
						echo $this->Html->link('<i class="fa fa-undo" title="Restore from Archive"></i>', array('action' => 'archive', $project['Project']['id'],0), array('escape'=>false,'class'=>'deleteButton'));
					else:
						// show archive icon
						echo $this->Html->link('<i class="fa fa-archive" title="Move to Archive"></i>', array('controller' => 'projects', 'action' => 'archive', $project['Project']['id'], 1), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this project to Archive?'));
					endif;
					?>
					<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $project['Project']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $project['Project']['name'])); ?>

				<?php endif; ?>
			</h1>
		</div>
		<div class="col6 right">
			<a href="/admin/purchase_orders/add/<?php echo $project['Project']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add PO</a>
			<a href="/admin/labour/add/project_id:<?php echo $project['Project']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Labour</a>
			<a href="/admin/timesheets/add/<?php echo $project['Project']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Timesheet</a>
			<a href="/admin/agency_timesheets/add/<?php echo $project['Project']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Agency T/S</a>
		</div>
	</div>
	<div class="group">
		<div class="col3">
			<h3>Contact Details</h3>
			<div class="box">
				<p>
				<strong>Client</strong><br>
				<?php echo $project['Client']['name']; ?>
				</p>
				<?php if($userIsAdmin): ?>
					<p>
					<strong>Project Value</strong><br>
					<?php echo $this->Number->currency($project['Project']['value'],'GBP'); ?>
					</p>
					<p><strong>Project Profit</strong><br>
					<?php echo $project['Project']['profit']; ?>%
					</p>
				<?php endif; ?>
				<p>
					<?php echo $this->App->formatAddress($project['Project']) ?>
				</p>
				<?php if(!empty($project['Project']['spec'])): ?>
					<p><a href="<?php echo $project['Project']['spec'] ?>" class="button button-primary" target="_blank">View Spec</a></p>
				<?php endif; ?>
			</div>

			<h3>Contact</h3>
			<div class="box">
				<p>
					<strong><?php echo $project['Contact']['name']; ?></strong>
				</p>
				<p>
					<?php if(!empty($project['Contact']['email'])): ?>
						<a href="mailto:<?php echo $project['Contact']['email']; ?>"><?php echo $project['Contact']['email']; ?></a><br>
					<?php endif; ?>
					<?php echo $project['Contact']['tel']; ?>
				</p>
				<?php if (!empty($project['Contact']['email'])): ?>
					<p>
						<a href="/admin/projects/view/<?php echo $project['Project']['id']; ?>/notify" class="button button-primary" onclick="return confirm('Please confirm you wish to email the project details to this contact...')">SEND NOTIFICATION</a>
						<br><i><?php echo $project['Project']['notification_sent_time'] ? 'Notification sent at ' . date('H:i \o\n jS M Y', strtotime($project['Project']['notification_sent_time'])) : '' ?></i>
					</p>
				<?php endif; ?>
			</div>

			<h3>Project Files</h3>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'project', 'files'=>$files, 'objectId'=>$project['Project']['id'], 'allowDelete'=>$userIsAdmin)); ?>
				<p style="margin: 20px 0 0">
					<a href="/admin/projects/generate_method_docs/<?php echo $project['Project']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> METHOD &amp; RISK DOCS</a>
				</p>
			</div>

			<?php if($userIsAdmin): ?>
				<h3>Confidential Files</h3>
				<div class="box">
					<?php echo $this->element('file_uploader', array('objectType'=>'project_confidential', 'files'=>$files_confidential, 'objectId'=>$project['Project']['id'], 'allowDelete'=>$userIsAdmin)); ?>
				</div>
			<?php endif; ?>

			<?php if ($project['Project']['notes']): ?>
				<h3>Notes</h3>
				<div class="box">
					<p style="margin-bottom: 0">
						<?php echo nl2br($project['Project']['notes']); ?>
					</p>
				</div>
			<?php endif; ?>

		</div>
		<div class="col9">
			<h3>Recent Purchase Orders</h3>
			<table>
				<tr>
					<th>PO</th>
					<th>Description</th>
					<th>Type</th>
					<th>Value</th>
					<th>Invoiced</th>
					<th style="width: 150px" class="actions"></th>
				</tr>
				<?php
					$i = 0;
					foreach ($purchaseOrders as $purchase_order):
						$class = null;
						if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] > 0) {
							$class = ' class="complete"';
						}
						if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] == 0) {
							$class = ' class="partial-complete"';
						}
						if ($purchase_order['PurchaseOrder']['void'] == 1) {
							$class = ' class="void"';
						}
						if($purchase_order['PurchaseOrder']['type_id'] == 1 && $purchase_order['PurchaseOrder']['returned'] == 0 && strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW + 3 days')):
							$class = ' class="warning"';
						endif;
						if($purchase_order['PurchaseOrder']['type_id'] == 1 && $purchase_order['PurchaseOrder']['returned'] == 0 && strtotime($purchase_order['PurchaseOrder']['end_date']) <= strtotime('NOW')):
							$class = ' class="alert"';
						endif;

						$valuesClass = $purchase_order['PurchaseOrder']['invoiced'] > $purchase_order['PurchaseOrder']['value'] ? 'awaiting-credit-note' : '';
					?>
					<tr<?php echo $class;?>>

						<td><?php echo $this->Html->link($purchase_order['PurchaseOrder']['ref'], array('controller' => 'purchase_orders', 'action' => 'edit', $purchase_order['PurchaseOrder']['id'])); ?>&nbsp;</td>

						<td><?php echo $purchase_order['PurchaseOrder']['desc']; ?>&nbsp;</td>
						<td><?php echo $purchase_order['Type']['name']; ?></td>
						<td class="<?php echo $valuesClass ?>"><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['value'],'GBP'); ?>&nbsp;</td>
						<td class="<?php echo $valuesClass ?>"><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['invoiced'],'GBP'); ?>&nbsp;</td>

						<td class="actions">
						<?php if($userIsAdmin): ?>
							<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'purchase_orders','action' => 'edit', $purchase_order['PurchaseOrder']['id']),array('escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'purchase_orders','action' => 'delete', $purchase_order['PurchaseOrder']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $purchase_order['PurchaseOrder']['ref'])); ?>
						<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if( !$purchaseOrders ) : ?>
					<tr>
						<td colspan="6">No POs found</td>
					</tr>
				<?php endif; ?>
			</table>
			<a href="/admin/purchase_orders/index/project_id:<?php echo $project['Project']['id']; ?>" class="button button-primary">See All</a>

			<h3><br>Labour</h3>
			<table>
				<tr>
					<th>Date</th>
					<th>Total</th>
					<th style="width: 150px" class="actions"></th>
				</tr>
				<?php
				$i = 0;
				foreach ($labours as $labour):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class;?>>
						<td><?php echo $labour['Labour']['date']; ?>&nbsp;</td>
						<td><?php echo $this->Number->currency($labour['Labour']['total'],'GBP'); ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'labour','action' => 'edit', $labour['Labour']['id']),array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'labour','action' => 'delete', $labour['Labour']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $labour['Labour']['id'])); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$labours): ?>
					<tr>
						<td colspan="3">No records found</td>
					</tr>
				<?php endif; ?>
			</table>
			<a href="/admin/labour/index/project_id:<?php echo $project['Project']['id']; ?>" class="button button-primary">See All</a>


			<h3><br>Assigned Tools</h3>
			<table>
				<tr>
					<th>Serial No.</th>
					<th>Name</th>
					<th>Description</th>
					<th style="width: 140px" class="actions"></th>
				</tr>
				<?php foreach ($project['Tool'] as $tool): ?>
					<tr>
						<td><?php echo $tool['serial_number']; ?>&nbsp;</td>
						<td><?php echo $tool['name']; ?>&nbsp;</td>
						<td><?php echo $tool['description']; ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('controller'=>'tools', 'action' => 'view', $tool['id']), array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'tools', 'action' => 'edit', $tool['id']), array('escape'=>false)); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$project['Tool']): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>


			<h3><br>Timesheets</h3>
			<table>
				<tr>
					<th style="width: 100px">Date</th>
					<th></th>
					<th class="right">Total</th>
					<th style="width: 120px" class="actions"></th>
				</tr>
				<?php
				$i = 0;
				foreach ($timesheets as $timesheet):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class;?>>
						<td style="vertical-align: top; padding-top: 15px"><?php echo $timesheet['Timesheet']['date']; ?>&nbsp;</td>
						<td>
							<table class="timesheet-entries timesheet-entry-details">
								<?php foreach($timesheet['EmployeesTimesheet'] as $timesheetEntry): ?>
									<tr style="background: transparent !important">
										<td style="width: 37%"><?php echo $employeesList[$timesheetEntry['employee_id']] ?></td>
										<td style="width: 55px"><?php echo $timesheetEntry['start_time'] ?></td>
										<td style="width: 50px"><?php echo $timesheetEntry['end_time'] ?></td>
										<td style="width: 75px"><?php echo $timesheetEntry['break_hours'] ? '(-'.$timesheetEntry['break_hours'].' hrs)' : '' ?></td>
										<td><?php echo (float)(number_format($timesheetEntry['hours_worked'], 2)) ?> hours @ &pound;<?php echo number_format($timesheetEntry['rate_ph'], 2) ?></td>
									</tr>
								<?php endforeach; ?>
							</table>
							&nbsp;
						</td>
						<td class="right">
							<table class="timesheet-entries timesheet-entry-costs">
								<?php foreach($timesheet['EmployeesTimesheet'] as $timesheetEntry): ?>
									<tr style="background: transparent !important">
										<td style="text-align: right"><?php echo $timesheetEntry['cost'] ?></td>
									</tr>
								<?php endforeach; ?>
							</table>
							<?php echo $this->Number->currency($timesheet['Timesheet']['total'],'GBP'); ?></td>
						<td style="vertical-align: top; padding-top: 14px" class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'timesheets','action' => 'edit', $timesheet['Timesheet']['id']),array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'timesheets','action' => 'delete', $timesheet['Timesheet']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete timesheet %s?'), $timesheet['Timesheet']['date'])); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$timesheets): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>


			<h3><br>Agency Timesheets</h3>
			<table>
				<tr>
					<th style="width: 100px">Date</th>
					<th></th>
					<th class="right">Total</th>
					<th style="width: 120px" class="actions"></th>
				</tr>
				<?php
				$i = 0;
				foreach ($agencyTimesheets as $agencyTimesheet):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class;?>>
						<td style="vertical-align: top; padding-top: 15px"><?php echo $agencyTimesheet['AgencyTimesheet']['date']; ?>&nbsp;</td>
						<td>
							<table class="timesheet-entries timesheet-entry-details">
								<?php foreach($agencyTimesheet['AgencyEmployeesAgencyTimesheet'] as $agencyTimesheetEntry): ?>
									<tr style="background: transparent !important">
										<td style="width: 28%"><?php echo $agenciesList[$agencyTimesheetEntry['supplier_id']] ?></td>
										<td style="width: 24%"><?php echo $agencyEmployeesList[$agencyTimesheetEntry['agency_employee_id']] ?></td>
										<td style="width: 55px"><?php echo $agencyTimesheetEntry['start_time'] ?></td>
										<td style="width: 50px"><?php echo $agencyTimesheetEntry['end_time'] ?></td>
										<td style="width: 75px"><?php echo $agencyTimesheetEntry['break_hours'] ? '(-'.$agencyTimesheetEntry['break_hours'].' hrs)' : '' ?></td>
										<td><?php echo (float)(number_format($agencyTimesheetEntry['hours_worked'], 2)) ?> hours @ &pound;<?php echo number_format($agencyTimesheetEntry['rate_ph'], 2) ?></td>
									</tr>
								<?php endforeach; ?>
							</table>
							&nbsp;
						</td>
						<td class="right">
							<table class="timesheet-entries timesheet-entry-costs">
								<?php foreach($agencyTimesheet['AgencyEmployeesAgencyTimesheet'] as $agencyTimesheetEntry): ?>
									<tr style="background: transparent !important">
										<td style="text-align: right"><?php echo $agencyTimesheetEntry['cost'] ?></td>
									</tr>
								<?php endforeach; ?>
							</table>
							<?php echo $this->Number->currency($agencyTimesheet['AgencyTimesheet']['total'],'GBP'); ?></td>
						<td style="vertical-align: top; padding-top: 14px" class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'agency_timesheets','action' => 'edit', $agencyTimesheet['AgencyTimesheet']['id']),array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'agency_timesheets','action' => 'delete', $agencyTimesheet['AgencyTimesheet']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete timesheet %s?'), $agencyTimesheet['AgencyTimesheet']['date'])); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$agencyTimesheets): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>


			<?php if ($userIsAdmin|| $userIsOfficeAdmin): ?>
				<h3><br>Project Items</h3>
				<table>
					<tr>
						<th>Date</th>
						<th>Description</th>
						<th>Cost</th>
						<th style="width: 150px" class="actions"></th>
					</tr>
					<?php
					$totalCost = 0;
					foreach ($project['ProjectItem'] as $projectItem):
						$totalCost += $projectItem['cost'] ?: 0;
					?>
						<tr>
							<td><?php echo $projectItem['date']; ?>&nbsp;</td>
							<td><?php echo $projectItem['description']; ?>&nbsp;</td>
							<td><?php echo $projectItem['cost'] !== null ? $this->Number->currency($projectItem['cost'],'GBP') : ''; ?>&nbsp;</td>
							<td class="actions">
								<?php if($userIsAdmin): ?>
									<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'projects','action' => 'edit', $project['Project']['id'], '#' => 'project-items-heading'),array('escape'=>false)); ?>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>

					<?php if (!$project['ProjectItem']): ?>
						<tr>
							<td colspan="4">No records found</td>
						</tr>
					<?php else: ?>
						<td></td>
						<td style="text-align: right; font-weight: bold">Total</td>
						<td style="font-weight: bold; <?php echo $totalCost != $project['Project']['value'] ? 'color: red' : '' ?>"><?php echo $this->Number->currency($totalCost,'GBP'); ?>&nbsp;</td>
						<td></td>
					<?php endif; ?>
				</table>
				<?php if (!empty($project['ProjectItem']) && empty($project['Invoice'])):  // only allow "create invoice..." if no invoices already exist for this project ?>
					<a href="/admin/projects/createInvoice/<?php echo $project['Project']['id']; ?>" class="button button-primary" onclick="return confirm('Please confirm you wish to create the invoice...\n\nPLEASE NOTE: You can only do this once!')">
						Create Draft Invoice from Project Items &amp; Recharged PO's
					</a>
				<?php endif; ?>
			<?php endif; ?>


			<?php if ($userIsAdmin|| $userIsOfficeAdmin): ?>
				<h3><br>Invoices</h3>
				<table>
					<tr>
						<th>Number</th>
						<th>Date</th>
						<th>Reference</th>
						<th>Total</th>
						<th>Status</th>
						<th>Date Sent</th>
						<th style="width: 120px" class="actions"></th>
					</tr>
					<?php
					$i = 0;
					foreach ($project['Invoice'] as $invoice):
						$class = null;
						if ($i++ % 2 == 0) {
							$class = ' class="altrow"';
						}
						?>
						<tr<?php echo $class;?>>
							<td><?php echo $invoice['invoice_num']; ?>&nbsp;</td>
							<td><?php echo $invoice['invoice_date']; ?>&nbsp;</td>
							<td><?php echo $invoice['ref']; ?>&nbsp;</td>
							<td><?php echo $this->Number->currency($invoice['total'],'GBP'); ?>&nbsp;</td>
							<td><?php echo Invoice::getStatuses()[$invoice['invoice_status_id']]; ?>&nbsp;</td>
							<td><?php echo $invoice['email_sent'] ? date('j M Y', strtotime($invoice['email_sent'])) : ''; ?></td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('controller'=>'invoices','action' => 'view', $invoice['id']), array('escape'=>false)); ?>
								<?php if (Invoice::allowedToEdit($invoice, $userIsAdmin, $currentUser['User']['id'])): ?>
									<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'invoices','action' => 'edit', $invoice['id']), array('escape'=>false)); ?>
								<?php endif; ?>
								<?php if (false): ?>
									<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'invoices','action' => 'delete', $invoice['id']),
											array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete invoice %s?'), $invoice['invoice_num'])); ?>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>

					<?php if (!$project['Invoice']): ?>
						<tr>
							<td colspan="7">No records found</td>
						</tr>
					<?php endif; ?>
				</table>
			<?php endif; ?>

		</div>
		
	</div>
	
</div>	


