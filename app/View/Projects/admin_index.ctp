<div class="content">
	<div class="group">
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col3">
			<h1><?php echo empty($archive) ? 'Projects' : 'Project Archive'; ?></h1>
		</div>
		<div class="col2 right">
			<?php echo empty($archive) ? $this->Form->input('project_status', array('label'=>false, 'type'=>'select', 'options'=>$projectStatusGroupList, 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by status...')) : '&nbsp;'; ?>
		</div>
		<div class="col2 right">
			<?php echo $this->Form->input('project_type_id', array('label'=>false, 'type'=>'select', 'onchange'=>'this.form.submit()', 'div'=>false,'empty'=>'Filter by type...')); ?>
		</div>
		<div class="col2 right">
			<a href="/admin/projects/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Project</a>
		</div>
		<div class="col3">
			<?php
				echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
				echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if(!empty($this->request->data['Search'])): ?>
				<?php echo $this->Html->link('Clear search', array('action' => 'index', $archive, '?' => array('clear'=>1))); ?>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
		
	</div>
		
	<table class="cols-align-top">
		<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('description');?></th>
			<th>Files?</th>
			<th><?php echo $this->Paginator->sort('client_id');?></th>
			<th><?php echo $this->Paginator->sort('project_type_id');?></th>
			<?php if($userIsAdmin): ?>
				<th><?php echo $this->Paginator->sort('value','Project Value');?></th>
				<th><a href="#">Cost</a></th>
				<th><a href="#">Profit</a></th>
			<?php endif; ?>
			<th><?php echo $this->Paginator->sort('project_status_id', 'Status');?></th>
			<th width="80"><?php echo $this->Paginator->sort('created');?></th>
			<th width="160" class="actions"></th>
		</tr>
		<?php
			$i = 0;
			foreach ($projects as $project):
				$class = null;
				if ($project['Project']['profit'] <= $settings_for_layout['profit_warning']) {
					$class = ' class="alert"';
				} else if ($project['Project']['profit'] <= $settings_for_layout['profit_warning_amber']) {
					$class = ' class="warning"';
				}
			?>
			<tr<?php echo $class;?>>

				<td><?php echo $this->Html->link($project['Project']['name'], array('controller' => 'projects', 'action' => 'view', $project['Project']['id'])); ?>&nbsp;</td>
				<td><?php echo $project['Project']['description']  . ($project['Project']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($project['Project']['notes']) . '</div>' : ''); ?></td>
				<td><?php $files = glob(APP . "tmp/project/".$project['Project']['id']."/*"); echo !empty($files) ? '<i class="fa fa-check uploaded-files-tick"></i> ' . count($files) : '' ?>&nbsp;</td>
				<td><?php echo $project['Client']['name']; ?>&nbsp;</td>
				<td><?php echo $project['ProjectType']['name']; ?></td>

				<?php if ($userIsAdmin): ?>
					<td><?php echo $this->Number->currency($project['Project']['value'],'GBP'); ?>&nbsp;</td>
					<td><?php echo isset($project['Project']['cost']) ? $this->Number->currency($project['Project']['cost'],'GBP') : ''; ?></td>
					<td><?php echo isset($project['Project']['profit']) ? $project['Project']['profit'].'%' : ''; ?></td>
				<?php endif; ?>

				<td><span class="<?php echo $project['ProjectStatus']['name']; ?>"><?php echo $project['ProjectStatus']['name']; ?></span></td>

				<td><?php echo $this->Time->niceShort($project['Project']['created']); ?>&nbsp;</td>

				<td class="actions">
					<?php if ($userIsAdmin): ?>
						<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('action' => 'summary', $project['Project']['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $project['Project']['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('action' => 'delete', $project['Project']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $project['Project']['name'])); ?>
						<?php
						if ($project['Project']['archived']):
							// project is archived so show Undo icon
							echo $this->Html->link('<i class="fa fa-undo fa-2x" title="Restore from Archive"></i>', array('action' => 'archive', $project['Project']['id'],0), array('escape'=>false,'class'=>'deleteButton'));
						else:
							// show archive icon
							echo $this->Html->link('<i class="fa fa-archive fa-2x" title="Move to Archive"></i>', array('controller' => 'projects', 'action' => 'archive', $project['Project']['id'], 1), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this project to Archive?'));
						endif;
						?>
					<?php endif; ?>

					<?php if (!empty($project['Invoice']) && ($userIsAdmin|| $userIsOfficeAdmin)): ?>
						<?php echo $this->element('invoice_icon', array('invoice'=>end($project['Invoice']))) ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if( !$projects ) : ?>
			<tr>
				<td colspan="11">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>	


