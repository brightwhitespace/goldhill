<div class="content">
	<div class="content-head group">
		<div class="col7">
			<h1><?php echo $project['Project']['name']; ?>

			</h1>
		</div>

	</div>
	<div class="group">

			<div class="col12">

				<table>
					<tr>
						<th>Name</th>
						<th>Qty</th>
						<th>Total Cost</th>
					</tr>
					<tr>
						<td>Labour</td>
						<td><?php echo $labourTotals['qty']; ?></td>
						<td><?php echo $this->Number->currency($labourTotals['total'],'GBP'); ?></td>
					</tr>
					<tr>
						<td>Skip</td>
						<td><?php echo $skipTotals['qty']; ?></td>
						<td><?php echo $this->Number->currency($skipTotals['total'],'GBP'); ?></td>
					</tr>
					<tr>
						<td>Hire</td>
						<td><?php echo $hireTotals['qty']; ?></td>
						<td><?php echo $this->Number->currency($hireTotals['total'],'GBP'); ?></td>
					</tr>
					<tr>
						<td>Sundry</td>
						<td><?php echo $sundryTotals['qty']; ?></td>
						<td><?php echo $this->Number->currency($sundryTotals['total'],'GBP'); ?></td>
					</tr>
				</table>

			</div>
		
	</div>
	
</div>	


