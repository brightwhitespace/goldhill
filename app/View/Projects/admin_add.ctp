<div class="content">
	<h2>Add Project</h2>
	
	<?php echo $this->Form->create('Project',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
				// if form is being redisplayed due to errors, make sure we preselect the client contact id that was previously chosen
				$selectedClientContactId = !empty($this->request->data['Project']['contact_id']) ? $this->request->data['Project']['contact_id'] : 0;
				echo $this->Form->input('client_id',array('label'=>'Client<em>*</em>','type'=>'select','options'=>$clients,'empty'=>'Please select',
					'data-update'=>'projectManagers','data-update-url'=>'/admin/clients/contacts/','data-selected-contact-id'=>$selectedClientContactId,'class'=>'update-on-select'));
			?>
			<div id="projectManagers"></div>
			<?php
			echo $this->Form->input('user_id',array('label'=>'Project manager<em>*</em>','type'=>'select','options'=>$users));
			echo $this->Form->input('name',array('label'=>'Project name<em>*</em>','type'=>'text'));
			echo $this->Form->input('project_type_id',array('label'=>'Project type<em>*</em>','type'=>'select','options'=>$projectTypes,'empty'=>'Please select'));
			echo $this->Ckeditor->browseField('spec',array('id'=>'ProjectSpec','type'=>'text','class'=>'smalltextbox file','label'=> 'Project spec','before'=>'','after'=>'','div'=>true));
			echo $this->Form->input('value_labour',array('label'=>'Labour cost','type'=>'number','class'=>'currency'));
			echo $this->Form->input('value_other',array('label'=>'Other cost','type'=>'number','class'=>'currency'));
			echo $this->Form->input('value',array('label'=>'Project Value<em>*</em>','type'=>'number','class'=>'currency'));
			echo $this->Form->input('max_spend',array('label'=>'Max Spend','type'=>'number','class'=>'currency'));
			echo $this->Form->input('man_hours_allowed',array('label'=>'Man Hours Allowed','type'=>'number'));
			?>
		</div>
		<div class="col6">
			<?php
			echo $this->Form->input('address1',array('label'=>'Address 1','type'=>'text'));
			echo $this->Form->input('address2',array('label'=>'Address 2','type'=>'text'));
			echo $this->Form->input('address3',array('label'=>'Address 3','type'=>'text'));
			echo $this->Form->input('city',array('label'=>'City','type'=>'text'));
			echo $this->Form->input('postcode',array('label'=>'Postcode','type'=>'text'));
			echo $this->Form->input('description',array('label'=>'Description','type'=>'textarea'));
			echo $this->Form->input('notes',array('label'=>'Notes','type'=>'textarea','style'=>'height: 90px'));
			echo $this->Form->input('purchase_order_no',array('label'=>'Purchase Order No.','type'=>'text'));
			?>
			<div class="group">
				<div class="col6" style="margin-left: 0px">
					<?php echo $this->Form->input('start_date',array('label'=>'Start date','type'=>'text','class'=>'datepicker')); ?>
				</div>
				<div class="col6">
					<?php echo $this->Form->input('end_date',array('label'=>'End date','type'=>'text','class'=>'datepicker')); ?>
				</div>
			</div>
			<?php
			echo $this->Form->input('project_status_id',array('label'=>'Project Status','type'=>'select','options'=>$projectStatuses,'empty'=>'Please select...','style'=>'width: 50%'));
			?>
		</div>
	</div>

	<div class="group">
		<div class="col8">
			<h3 id="project-items-heading" style="margin: 20px 0">Project Items</h3>
		</div>
	</div>

	<div class="group">
		<div class="col2">Date</div>
		<div class="col4">Description<em>*</em></div>
		<div class="col2">Cost (ex VAT)</div>
	</div>

	<div class="group">
		<div class="col8"><hr style="margin: 10px 0 20px"></div>
	</div>

	<div id="project-items-container">
		<div id="project-items-container">
			<?php $itemIdx = 0; ?>
			<?php foreach ($this->request->data['ProjectItem'] as $projectItem): ?>

				<div class="project-item group">
					<div class="col2">
						<?php echo $this->Form->input("ProjectItem.$itemIdx.date", array('label'=>false,'type'=>'text','class'=>'datepicker','style'=>'width: 100% !important')); ?>
					</div>
					<div class="col4">
						<?php echo $this->Form->input("ProjectItem.$itemIdx.description", array('label'=>false,'type'=>'text')); ?>
					</div>
					<div class="col2 input-and-delete-container">
						<?php echo $this->Form->input("ProjectItem.$itemIdx.cost", array('label'=>false,'type'=>'text', 'class'=>'project-item-cost', 'onchange'=>'updateProjectValue()')); ?>
						<a href="#" class="button button-secondary button-delete-project-item" style="" onclick="return deleteProjectItem(this)">X</a>
					</div>
				</div>

				<?php $itemIdx++; ?>
			<?php endforeach; ?>
		</div>
	</div>

	<div class="group">
		<div class="col6">
			<p style="margin: 5px 0 40px">
				<a href="#" class="button button-primary" onclick="return addProjectItem()">Add new item</a>
			</p>
		</div>
	</div>

	<div class="group">
		<div class="col6">
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
		<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>

<?php // HTML template for adding project items ?>
<script id="project-item-template" type="text/html">
	<div class="project-item group">
		<div class="col2">
			<?php echo $this->Form->input("ProjectItem.INDEX.date", array('label'=>false,'type'=>'text','class'=>'datepicker','style'=>'width: 100% !important')); ?>
		</div>
		<div class="col4">
			<?php echo $this->Form->input("ProjectItem.INDEX.description", array('label'=>false,'type'=>'text')); ?>
		</div>
		<div class="col2 input-and-delete-container">
			<?php echo $this->Form->input("ProjectItem.INDEX.cost", array('label'=>false,'type'=>'text', 'class'=>'project-item-cost', 'onchange'=>'updateProjectValue()')); ?>
			<a href="#" class="button button-secondary button-delete-project-item" onclick="return deleteProjectItem(this)">X</a>
		</div>
	</div>
</script>
