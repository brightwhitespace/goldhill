
<div class="content">
	<h1>Edit Tool</h1>

	<?php echo $this->Form->create('Tool', array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('serial_number', array('label'=>'Serial Number (leave blank to use next available id)','type'=>'text'));
			echo $this->Form->input('name', array('label'=>'Name<em>*</em>','type'=>'text'));
			echo $this->Form->input('description', array('label'=>'Description','type'=>'text'));
			echo $this->Form->input('tool_status_id', array('id'=>'tool-status-select','label'=>'Where / Who With?','type'=>'select','options'=>Tool::getStatuses()));
			echo $this->Form->input('employee_id', array('label'=>'Assigned Individual','type'=>'select','options'=>$employeeList,'empty'=>'Please select...', 'data-display-status-id'=>Tool::STATUS_INDIVIDUAL));
			echo $this->Form->input('project_id', array('label'=>'Project','type'=>'select','options'=>$projectList,'empty'=>'Please select...', 'data-display-status-id'=>Tool::STATUS_PROJECT));
			echo $this->Form->input('vehicle_id', array('label'=>'Vehicle','type'=>'select','options'=>$vehicleList,'empty'=>'Please select...', 'data-display-status-id'=>Tool::STATUS_IN_VEHICLE));
			?>
		</div>
		<div class="col6">
			<?php
			echo $this->Form->input('notes', array('label'=>'Notes','type'=>'textarea','style'=>'height: 193px'));
			?>
		</div>
	</div>
	<div class="group" style="margin-top: 30px">
		<div class="col6">
			<?php echo $this->Html->link('Cancel', $returnUrl, array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>
