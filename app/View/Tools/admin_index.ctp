<div class="content">
	<div class="group">
		<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
		<div class="col1">
			<h2><?php echo empty($archive) ? 'Tools' : 'Tool Archive' ?></h2>
		</div>
		<div class="col2">
			<div class="group floatright noprint">
				<span class="noprint" style="display: block; margin-top: 8px; float: left"><?php echo $this->Form->input('show_all_records', array('type'=>'checkbox', 'onchange'=>'this.form.submit()', 'div'=>false)) ?></span>
				<label class="noprint" style="display: block; margin-top: 9px; float: left; padding-left: 6px" for="SearchShowAllRecords">Show all</label>
			</div>
			&nbsp;
		</div>
		<div class="col2">
			<?php echo $this->Form->input('tool_status_id', array('id'=>'tool-status-select', 'label' => false, 'type' => 'select', 'options' => Tool::getStatuses(), 'empty' => 'Filter by status...', 'onchange'=>'this.form.submit()')) ?>
		</div>
		<div class="col2">
			<?php
			echo $this->Form->input('employee_id', array('label'=>false,'type'=>'select','options'=>$employeeList,'empty'=>'Filter by assigned to...', 'data-display-status-id'=>Tool::STATUS_INDIVIDUAL, 'onchange'=>'this.form.submit()'));
			echo $this->Form->input('project_id', array('label'=>false,'type'=>'select','options'=>$projectList,'empty'=>'Filter by assigned to...', 'data-display-status-id'=>Tool::STATUS_PROJECT, 'onchange'=>'this.form.submit()'));
			echo $this->Form->input('vehicle_id', array('label'=>false,'type'=>'select','options'=>$vehicleList,'empty'=>'Filter by assigned to...', 'data-display-status-id'=>Tool::STATUS_IN_VEHICLE, 'onchange'=>'this.form.submit()'));
			echo empty($this->request->data['Search']['tool_status_id']) || !in_array($this->request->data['Search']['tool_status_id'], array(Tool::STATUS_INDIVIDUAL, Tool::STATUS_PROJECT, Tool::STATUS_IN_VEHICLE)) ? '&nbsp;' : '';
			?>
		</div>
		<div class="col2 right">
			<a href="/admin/tools/add" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Tool</a>
		</div>
		<div class="col3">
			<?php
			echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
			echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
			?>
			<?php if (!empty($this->request->data['Search'])): ?>
				<a class="noprint" href="<?php echo $this->request->here ?>?clear=1">Clear search</a>
			<?php endif; ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
	<table class="cols-align-top">
		<tr>
			<th style="width: 80px"><?php echo $this->Paginator->sort('serial_number', 'Serial No');?></th>
			<th style="width: 20%"><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('description');?></th>
			<th><?php echo $this->Paginator->sort('notes');?></th>
			<th style="width: 140px"><?php echo $this->Paginator->sort('tool_status_id', 'Status');?></th>
			<th style="width: 20%"><?php echo $this->Paginator->sort('employee_id', 'Assigned To');?></th>
			<th style="width: 62px" class="actions"></th>
		</tr>
		<?php foreach ($tools as $tool): ?>
			<tr>
				<td><?php echo $this->Html->link($tool['Tool']['serial_number'], array('action' => 'view', $tool['Tool']['id'])); ?>&nbsp;</td>
				<td><?php echo $tool['Tool']['name']; ?>&nbsp;</td>
				<td><?php echo $tool['Tool']['description']; ?>&nbsp;</td>
				<td><?php echo $tool['Tool']['notes'] ?></td>
				<td><?php echo $tool['ToolStatus']['name']; ?>&nbsp;</td>
				<td><?php echo $this->Tool->getAssignedToName($tool, 35); ?>&nbsp;</td>
				<td class="actions">
					<?php if ($userIsAdmin): ?>
						<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $tool['Tool']['id']),array('escape'=>false)); ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$tools): ?>
			<tr>
				<td colspan="7">No records found</td>
			</tr>
		<?php endif; ?>
	</table>

	<?php if (empty($this->request->data['Search']['show_all_records'])): ?>
		<div class="pagination group">
			<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
			<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
		</div>
	<?php endif; ?>
</div>
