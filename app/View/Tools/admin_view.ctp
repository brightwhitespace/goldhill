<div class="content">
	<div class="content-head group">
		<div class="col8">
			<h1>
				<?php echo $tool['Tool']['name']; ?>
				<?php if($userIsAdmin): ?>
					<?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $tool['Tool']['id']),array('escape'=>false)); ?>
				<?php endif; ?>
			</h1>
		</div>
		<div class="col4 right">
		</div>
	</div>

	<div class="group">
		<div class="col3">
			<h3>Tool Details</h3>
			<div class="box">
				<p>
					<b>Serial Number: </b>
					<?php echo h($tool['Tool']['serial_number']) ?><br>
				</p>
				<p>
					<b>Status: </b>
					<?php echo h($tool['ToolStatus']['name']) ?>
				</p>
				<?php if ($this->Tool->getAssignedToName($tool)): ?>
					<p>
						<b>Assigned To: </b>
						<?php echo $this->Tool->getAssignedToName($tool) ?>
					</p>
				<?php endif; ?>
				<?php if ($tool['Tool']['description']): ?>
					<p>
						<b>Description: </b>
						<?php echo h($tool['Tool']['description']) ?><br>
					</p>
				<?php endif; ?>
			</div>

			<?php if ($tool['Tool']['notes']): ?>
				<h3>Notes</h3>
				<div class="box">
					<p style="margin-bottom: 0">
						<?php echo nl2br(h($tool['Tool']['notes'])); ?>
					</p>
				</div>
			<?php endif; ?>

			<h3>File Store</h3>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'tool', 'files'=>$files, 'objectId'=>$tool['Tool']['id'], 'allowDelete'=>$userIsAdmin)); ?>
			</div>
		</div>

		<div class="col9">

			<h3>Tool History &nbsp; <span style="font-weight: normal; font-size: 1.4rem; font-style: italic">(only changes are shown, most recent first)</span></h3>
			<table>
				<thead>
					<tr>
						<th style="width: 80px">Serial No</th>
						<th>Name</th>
						<th>Description</th>
						<th>Notes</th>
						<th style="width: 140px">Status</th>
						<th style="width: 20%">Assigned To</th>
						<th>Modified [User]</th>
					</tr>
				</thead>
				<tbody id="tool-history-body" style="display: none">
					<?php
					// NOTE: on page load will reverse the table rows using jquery so most recent shown first

					// need to remember last history tool item displayed as only want to show changes for next one
					$prevTool = array(
						'serial_number' => '',
						'name' => '',
						'description' => '',
						'notes' => '',
						'tool_status_id' => 0,
						'Tool' => array('tool_status_id' => 0),
					);

					foreach ($tool['HistoryTool'] as $tool):
						$tool = array('Tool' => $tool) + $tool;
					?>
						<tr>
							<td><?php echo $tool['serial_number'] != $prevTool['serial_number'] ? $tool['serial_number'] : ''; ?>&nbsp;</td>
							<td><?php echo $tool['name'] != $prevTool['name'] ? $tool['name'] : ''; ?>&nbsp;</td>
							<td><?php echo $tool['description'] != $prevTool['description'] ? $tool['description'] : ''; ?>&nbsp;</td>
							<td><?php echo $tool['notes'] != $prevTool['notes'] ? $tool['notes'] : ''; ?></td>
							<td><?php echo $tool['tool_status_id'] != $prevTool['tool_status_id'] ? Tool::getStatuses()[$tool['tool_status_id']] : ''; ?>&nbsp;</td>
							<td><?php echo $this->Tool->getAssignedToName($tool, 35) != $this->Tool->getAssignedToName($prevTool, 35) ? $this->Tool->getAssignedToName($tool, 35) : ''; ?>&nbsp;</td>
							<td><?php echo date('j/n/y H:i', strtotime($tool['Tool']['modified'])) . ' [' . $this->App->getInitials($this->App->getFullName($tool['User'])) . ']' ?></td>
						</tr>
					<?php
						$prevTool = $tool;
					endforeach;
					?>
				</tbody>
			</table>

			<script>
				window.onload = function () {
					// want most recent changes first so reverse order of history table rows
					var historyBody = $('#tool-history-body');
					historyBody.html($('tr',historyBody).get().reverse()).show();
				}
			</script>

		</div>
		
	</div>
	
</div>
