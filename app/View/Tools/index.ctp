<div class="content">
	<div class="group">
		<div class="col6">
			<h2>My Tools</h2>
		</div>
	</div>
	<table class="cols-align-top">
		<tr>
			<th style="width: 80px"><?php echo $this->Paginator->sort('serial_number', 'Serial No');?></th>
			<th style="width: 20%"><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('description');?></th>
			<th><?php echo $this->Paginator->sort('notes');?></th>
			<th style="width: 140px"><?php echo $this->Paginator->sort('tool_status_id', 'Status');?></th>
			<th style="width: 20%"><?php echo $this->Paginator->sort('employee_id', 'Assigned To');?></th>
			<th style="width: 62px" class="actions"></th>
		</tr>
		<?php foreach ($tools as $tool): ?>
			<tr>
				<td><?php echo $this->Html->link($tool['Tool']['serial_number'], array('action' => 'view', $tool['Tool']['id'])); ?>&nbsp;</td>
				<td><?php echo $tool['Tool']['name']; ?>&nbsp;</td>
				<td><?php echo $tool['Tool']['description']; ?>&nbsp;</td>
				<td><?php echo $tool['Tool']['notes'] ?></td>
				<td><?php echo $tool['ToolStatus']['name']; ?>&nbsp;</td>
				<td><?php echo $this->Tool->getAssignedToName($tool, 35); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $tool['Tool']['id']),array('escape'=>false)); ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if (!$tools): ?>
			<tr>
				<td colspan="7">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>
