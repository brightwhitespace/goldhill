
<?php foreach ($files as $filepath): ?>
	<div>
		<?php if (!empty($allowDelete)): ?>
			<a class="deleteLink" href="/admin/files/delete/<?php echo $objectType ?>/<?php echo $objectId; ?>/<?php echo urlencode(basename($filepath)); ?>" onclick="return confirm('Are you sure you wish to delete this file?')"><i class="fa fa-times"></i></a>
		<?php endif; ?>
		<a href="/admin/files/view/<?php echo $objectType; ?>/<?php echo $objectId; ?>/<?php echo urlencode(basename($filepath)); ?>"><i class="fa fa-download"></i></a>
		&nbsp;<span class="editable" rel="/admin/files/rename/<?php echo $objectType; ?>/<?php echo $objectId; ?>/<?php echo urlencode(basename($filepath)); ?>" id="name"><?php echo basename($filepath) ?></span>
	</div>
<?php endforeach; ?>
