<div class="content">
	<div class="group">
		<div class="col6">
			<h1>Suppliers</h1>
		</div>
		<div class="col3 right">
			<a href="/admin/suppliers/add" class="button button-primary">Add Supplier</a>
		</div>
		<div class="col3">
			<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
				<?php
					echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
					echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
				?>
				<?php if(!empty($this->request->data['Search'])): ?>
				<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
				<?php endif; ?>
			<?php echo $this->Form->end(); ?>
		</div>
		
	</div>
	
		
		
		<table>
			<tr>
				<th><?php echo $this->Paginator->sort('name');?></th>
				<th><?php echo $this->Paginator->sort('tel');?></th>
				<th><?php echo $this->Paginator->sort('email');?></th>
				<th><?php echo $this->Paginator->sort('modified');?></th>
				<th width="150" class="actions"></th>
			</tr>
			<?php
				$i = 0;
				foreach ($suppliers as $supplier):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
				?>
				<tr<?php echo $class;?>>
					
					<td><?php echo $this->Html->link($supplier['Supplier']['name'], array('controller' => 'suppliers', 'action' => 'view', $supplier['Supplier']['id'])); ?>&nbsp;</td>
					<td><?php echo $supplier['Supplier']['tel']; ?>&nbsp;</td>
					<td><?php echo $supplier['Supplier']['email']; ?>&nbsp;</td>
					<td><?php echo $this->Time->niceShort($supplier['Supplier']['created']); ?>&nbsp;</td>
					
					<td class="actions">
						<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $supplier['Supplier']['id']),array('escape'=>false)); ?>
					<?php if($userIsAdmin): ?>
						<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('action' => 'delete', $supplier['Supplier']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $supplier['Supplier']['name'])); ?>
					<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
			
			<?php if( !$suppliers ) : ?>
				<tr>
					<td colspan="6">No records found</td>
				</tr>
			<?php endif; ?>
		</table>
		<div class="pagination group">
			<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
			<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
		</div>
</div>	


