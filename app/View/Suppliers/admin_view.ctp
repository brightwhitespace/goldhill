<div class="content">
	<div class="content-head group">
		<div class="col8">
			<h1><?php echo $supplier['Supplier']['name']; ?> <?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $supplier['Supplier']['id']),array('escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $supplier['Supplier']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $supplier['Supplier']['name'])); ?></h1>
		</div>
		<div class="col4 right">
			<a href="/admin/suppliers/" class="button button-primary"><i class="fa fa-plus-circle"></i> See All</a>
		</div>
	</div>
	<div class="group">
			<div class="col3">
				<h3>Contact Details</h3>
				<div class="box">
					<p>
					<?php if(!empty($supplier['Supplier']['email'])): ?>
						<a href="mailto:<?php echo $supplier['Supplier']['email']; ?>"><?php echo $supplier['Supplier']['email']; ?></a><br>
					<?php endif; ?>
					<?php echo $supplier['Supplier']['tel']; ?>
					</p>
					<p>
					<?php echo !empty($supplier['Supplier']['address1']) ? $supplier['Supplier']['address1'].'<br>' : ''; ?>
					<?php echo !empty($supplier['Supplier']['address2']) ? $supplier['Supplier']['address2'].'<br>' : ''; ?>
					<?php echo !empty($supplier['Supplier']['address3']) ? $supplier['Supplier']['address3'].'<br>' : ''; ?>
					<?php echo !empty($supplier['Supplier']['city']) ? $supplier['Supplier']['city'].'<br>' : ''; ?>
					<?php echo !empty($supplier['Supplier']['postcode']) ? $supplier['Supplier']['postcode'] : ''; ?>
					</p>
				</div>
				
				<h3>Contacts</h3>
				<table>
					<?php foreach($supplier['Contact'] as $contact): ?>
					<tr class="bg">
						<td><a href="/admin/contacts/edit/<?php echo $contact['id']; ?>"><?php echo $contact['name']; ?></a></td>
						<td><?php echo $contact['tel']; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
				<a href="/admin/contacts/add/supplier_id:<?php echo $supplier['Supplier']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Contact</a>

				<?php if ($supplier['Supplier']['is_agency']): ?>
					<h3><br>Sub-contractors</h3>
					<table>
						<?php if (!empty($agencyEmployees)): ?>
							<?php foreach ($agencyEmployees as $employee): ?>
								<tr class="bg">
									<td>
										<a class="archiveButton" href="/admin/suppliers/archiveEmployee/<?php echo $employee['AgencyEmployee']['id']; ?>" title="Archive sub-contractor" onclick="return confirm('Are you sure you wish to archive this sub-contractor?')"><i class="fa fa-archive"></i></a>
										&nbsp;<a href="/admin/suppliers/editEmployee/<?php echo $employee['AgencyEmployee']['id']; ?>"><?php echo $employee['AgencyEmployee']['name']; ?></a>
									</td>
									<td><?php echo $employee['AgencyEmployee']['tel']; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
							<tr class="bg">
								<td><i>No sub-contractors found</i></td>
							</tr>
						<?php endif; ?>
					</table>
					<a href="/admin/suppliers/addEmployee/supplier_id:<?php echo $supplier['Supplier']['id']; ?>" class="button button-primary"><i class="fa fa-plus-circle"></i> Add Sub-contractor</a>
				<?php endif; ?>

				<h3><br>File Store</h3>
				<div class="box">
					<?php echo $this->element('file_uploader', array('objectType'=>'supplier', 'files'=>$files, 'objectId'=>$supplier['Supplier']['id'], 'allowDelete'=>$userIsAdmin)); ?>
				</div>

			</div>

			<div class="col9">
				
				<h3>Recent Purchase Orders</h3>
				<table>
					<tr>
						<th>PO</th>
						<th>Project</th>
						<th>Value</th>
						<th>Invoiced</th>
						<th width="150" class="actions"></th>
					</tr>
					<?php
						$i = 0;
						foreach ($purchaseOrders as $purchase_order):
							$class = null;
							if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] > 0) {
								$class = ' class="complete"';
							}
							if ($purchase_order['PurchaseOrder']['complete'] == 1 && $purchase_order['PurchaseOrder']['invoiced'] == 0) {
								$class = ' class="partial-complete"';
							}
							if ($purchase_order['PurchaseOrder']['void'] == 1) {
								$class = ' class="void"';
							}
						?>
						<tr<?php echo $class;?>>
							
							<td><?php echo $this->Html->link($purchase_order['PurchaseOrder']['ref'], array('controller' => 'purchase_orders', 'action' => 'edit', $purchase_order['PurchaseOrder']['id'])); ?>&nbsp;</td>
							<td><?php echo $purchase_order['Project']['name']; ?>&nbsp;</td>
							<td><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['value'],'GBP'); ?>&nbsp;</td>
							<td><?php echo $this->Number->currency($purchase_order['PurchaseOrder']['invoiced'],'GBP'); ?>&nbsp;</td>
							
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'purchase_orders','action' => 'edit', $purchase_order['PurchaseOrder']['id']),array('escape'=>false)); ?>
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'purchase_orders','action' => 'delete', $purchase_order['PurchaseOrder']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $purchase_order['PurchaseOrder']['ref'])); ?>
							<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
					
					<?php if( !$purchaseOrders ) : ?>
						<tr>
							<td colspan="6">No POs found</td>
						</tr>
					<?php endif; ?>
				</table>
				<a href="/admin/purchase_orders/index/supplier_id:<?php echo $supplier['Supplier']['id']; ?>" class="button button-primary">See All</a>
				
		</div>
		
	</div>
	
</div>	


