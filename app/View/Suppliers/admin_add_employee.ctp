<div class="content">
	<h2>Add Sub-contractor</h2>

	<?php echo $this->Form->create('AgencyEmployee', array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('name', array('label'=>'Name<em>*</em>','type'=>'text'));
			echo $this->Form->input('rate_ph', array('label'=>'Rate p.h.<em>*</em>','type'=>'text','class'=>'currency rate_ph','default'=>$defaultAgencyEmployeeRatePH));
			echo $this->Form->input('tel', array('label'=>'Mobile','type'=>'text'));
			?>
		</div>

	</div>
	<div class="group">
		<div class="col6">
			<?php echo $this->Html->link('Cancel', $returnUrl, array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
			<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>

</div>
