<div class="content">
	<h2>Add Supplier</h2>
	
	<?php echo $this->Form->create('Supplier',array('novalidate'=>'novalidate'));?>
	<div class="group">
		<div class="col6">
			<?php
			echo $this->Form->input('name',array('label'=>'Company name<em>*</em>','type'=>'text'));
			echo $this->Form->input('address1',array('label'=>'Address 1','type'=>'text'));
			echo $this->Form->input('address2',array('label'=>'Address 2','type'=>'text'));
			echo $this->Form->input('address3',array('label'=>'Address 3','type'=>'text'));
			echo $this->Form->input('city',array('label'=>'City','type'=>'text'));
			echo $this->Form->input('postcode',array('label'=>'Postcode','type'=>'text'));
			echo $this->Form->input('email',array('label'=>'Email address<em>*</em>','type'=>'text'));
			echo $this->Form->input('tel',array('label'=>'Telephone<em>*</em>','type'=>'text'));
			echo $this->Form->input('is_agency',array('label'=>'Is Agency?','type'=>'checkbox', 'style'=>'margin: 15px 0 30px'));
			?>
		</div>
		<div class="col6">
			<h4>First Contact</h4>
			<?php
			echo $this->Form->input('Contact.0.firstname',array('label'=>'First name<em>*</em>','type'=>'text'));
			echo $this->Form->input('Contact.0.lastname',array('label'=>'Last name<em>*</em>','type'=>'text'));
			echo $this->Form->input('Contact.0.tel',array('label'=>'Mobile<em>*</em>','type'=>'text'));
			echo $this->Form->input('Contact.0.email',array('label'=>'Email<em>*</em>','type'=>'text'));
			?>
		</div>
	</div>
	<div class="group">
		<div class="col6">
		<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
		</div>
		<div class="col6 right">
		<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end();?>
	
</div>
