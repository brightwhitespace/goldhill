<div class="content">
	<div class="content-head group">
		<div class="col4">
			<h1>
				<?php echo $quote['Quote']['name']; ?>
				<?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('action' => 'edit', $quote['Quote']['id']),array('escape'=>false)); ?>
				<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $quote['Quote']['id']), array('escape'=>false,'class'=>'deleteButton'), sprintf(__('Are you sure you want to delete %s?'), $quote['Quote']['name'])); ?>
			</h1>
		</div>
		<div class="col8 right">
			<a href="#" onclick="return openAddCertificatePopup(this)" class="button button-primary" data-cert-id="" data-quote-id="<?php echo $quote['Quote']['id']; ?>" data-start-date="" data-end-date="">
				<i class="fa fa-plus-circle"></i> Add Certification
			</a>
			<a href="/admin/wage_adjustments/add/<?php echo $quote['Quote']['id']; ?>" class="button button-primary" data-cert-id="" data-quote-id="<?php echo $quote['Quote']['id']; ?>" data-start-date="" data-end-date="">
				<i class="fa fa-plus-circle"></i> Add Wage Adjustment
			</a>
		</div>
	</div>

	<div class="group">
		<div class="col3">
			<h3>Contact Details</h3>
			<div class="box">
				<p>
					<?php if(!empty($quote['Quote']['email'])): ?>
						<a href="mailto:<?php echo $quote['Quote']['email']; ?>"><?php echo $quote['Quote']['email']; ?></a><br>
					<?php endif; ?>
					<?php echo $quote['Quote']['tel']; ?>
				</p>
				<p>
					<?php echo $this->App->formatAddress($quote['Quote']) ?>
				</p>
				<?php if ($quote['Quote']['emergency_contact_name'] || $quote['Quote']['emergency_contact_number']): ?>
					<p>
						<b>Emergency Contact</b><br>
						<?php echo $quote['Quote']['emergency_contact_name'] ?><br>
						<?php echo $quote['Quote']['emergency_contact_number'] ?>
					</p>
				<?php endif; ?>
				<?php if ($quote['Quote']['utr_no'] || $quote['Quote']['ni_no']): ?>
					<p>
						<?php echo $quote['Quote']['utr_no'] ? '<b>UTR No: </b>' . $quote['Quote']['utr_no'] . '<br>' : '' ?>
						<?php echo $quote['Quote']['ni_no'] ? '<b>NI No: &nbsp;</b>' . $quote['Quote']['ni_no'] : '' ?>
					</p>
				<?php endif; ?>
				<p>
					<b>Last Day for Hours Worked Report</b><br>
					<?php echo $quote['Quote']['last_working_day_name'] ?: 'Do not send report' ?>
				</p>
			</div>

			<?php if ($quote['Quote']['notes']): ?>
				<h3>Notes</h3>
				<div class="box">
					<p style="margin-bottom: 0">
						<?php echo nl2br($quote['Quote']['notes']); ?>
					</p>
				</div>
			<?php endif; ?>

			<h3>File Store</h3>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'quote', 'files'=>$files, 'objectId'=>$quote['Quote']['id'], 'allowDelete'=>$userIsAdmin)); ?>
			</div>
		</div>

		<div class="col9">

			<h3>Assigned Tools</h3>
			<table>
				<tr>
					<th>Serial No.</th>
					<th>Name</th>
					<th>Description</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php foreach ($quote['Tool'] as $tool): ?>
					<tr>
						<td><?php echo $tool['serial_number']; ?>&nbsp;</td>
						<td><?php echo $tool['name']; ?>&nbsp;</td>
						<td><?php echo $tool['description']; ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-search fa-2x"></i>', array('controller'=>'tools', 'action' => 'view', $tool['id']), array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'tools', 'action' => 'edit', $tool['id']), array('escape'=>false)); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$quote['Tool']): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>

			<h3><br>Certifications</h3>
			<table>
				<tr>
					<th>Certificate</th>
					<th style="width: 110px">Start Date</th>
					<th style="width: 110px">End Date</th>
					<th style="width: 150px">Last updated</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php
				$certCount = 0;
				foreach ($quote['CertificatesQuoteLatestUnique'] as $idx => $quoteCertRecord):

					$quoteId = $quote['Quote']['id'];
					$certId = $quoteCertRecord['certificate_id'];

					if (!$quoteCertRecord['ignore_previous_records']):
						$certCount++;
					?>
						<tr>
							<td><?php echo $quoteCertRecord['Certificate']['name'] ?></td>
							<td><?php echo $quoteCertRecord['start_date'] ? date('jS M Y', strtotime($quoteCertRecord['start_date'])) : '' ?></td>
							<td class="<?php echo $this->App->getCertExpiryClass($quoteCertRecord, $quoteCertRecord['Certificate']) ?>">
								<?php echo $quoteCertRecord['end_date'] ? date('jS M Y', strtotime($quoteCertRecord['end_date'])) : '<i class="fa fa-check" style="font-size: 18px; color: #b88400"></i>' ?>
							</td>
							<td><?php echo date('j-n-Y H:i', strtotime($quoteCertRecord['modified'])) . ' [' . $this->App->getInitials($this->App->getFullName($quoteCertRecord['User'])) . ']'; ?>&nbsp;</td>

							<td class="actions">
								<a href="#" onclick="return openAddCertificatePopup(this)" data-cert-id="<?php echo $certId ?>" data-quote-id="<?php echo $quoteId ?>"
								   data-start-date="<?php echo $quoteCertRecord['start_date'] ? date('j-n-Y', strtotime($quoteCertRecord['start_date'])) : '' ?>"
								   data-end-date="<?php echo $quoteCertRecord['end_date'] ? date('j-n-Y', strtotime($quoteCertRecord['end_date'])) : '' ?>">
									<i class="fa fa-edit fa-2x"></i>
								</a>
								<?php if($userIsAdmin): ?>
									<a href="#" onclick="return removeQuoteCertificate(this, true)" data-cert-id="<?php echo $certId ?>" data-quote-id="<?php echo $quoteId ?>">
										<i class="fa fa-trash fa-2x"></i>
									</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>

				<?php if ($certCount == 0): ?>
					<tr>
						<td colspan="5">No certificates found</td>
					</tr>
				<?php endif; ?>
			</table>

			<h3><br>Wage Adjustments</h3>
			<table>
				<tr>
					<th>Date</th>
					<th>Description</th>
					<th>Value</th>
					<th style="width: 100px" class="actions"></th>
				</tr>
				<?php foreach ($quote['WageAdjustment'] as $wageAdjustment): ?>
					<tr>
						<td><?php echo $wageAdjustment['date']; ?>&nbsp;</td>
						<td><?php echo $wageAdjustment['description']; ?>&nbsp;</td>
						<td style="<?php echo $wageAdjustment['value'] < 0 ? 'color: red' : '' ?>"><?php echo $this->Number->currency($wageAdjustment['value'], 'GBP'); ?>&nbsp;</td>
						<td class="actions">
							<?php if($userIsAdmin): ?>
								<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('controller'=>'wage_adjustments', 'action' => 'edit', $wageAdjustment['id']),array('escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('controller'=>'wage_adjustments', 'action' => 'delete', $wageAdjustment['id']), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to delete this record?')); ?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php if (!$quote['WageAdjustment']): ?>
					<tr>
						<td colspan="4">No records found</td>
					</tr>
				<?php endif; ?>
			</table>
		</div>
		
	</div>
	
</div>


<div id="add-cert-popup" class="overlay-block">
	<div class="overlay-content overlay-content-small vertical-centre">
		<a href="#" class="button button-primary small overlay-close"><i class="fa fa-close"></i></a>
		<h2>Add / Update Certificate</h2>
		<?php echo $this->Form->input('add_cert_quote_id', array('label'=>'Quote<em>*</em>','type'=>'select','options'=>$quoteList,'empty'=>'Select quote...','required'=>true,'disabled'=>true)) ?>
		<?php echo $this->Form->input('add_cert_cert_id', array('label'=>'Certificate<em>*</em>','type'=>'select','options'=>$certificateList,'empty'=>'Select certificate...','required'=>true)) ?>
		<?php echo $this->Form->input('add_cert_start_date', array('label'=>'Start Date','type'=>'text','class'=>'datepicker')) ?>
		<?php echo $this->Form->input('add_cert_end_date', array('label'=>'End Date','type'=>'text','class'=>'datepicker')) ?>
		<p>&nbsp;</p>
		<div class="group">
			<a href="#" class="button button-secondary overlay-close" style="float: none">CANCEL</a>
			<a href="#" onclick="addQuoteCertificate(<?php echo empty($reloadPageAfterCertUpdate) ? 'false' : 'true' ?>)" class="button button-primary floatright">ADD / UPDATE</a>
		</div>
	</div>
</div>
