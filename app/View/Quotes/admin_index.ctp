<div class="content">
	<div class="group">
		<div class="col6">
			<h1><?php echo empty($archive) ? 'Quotes' : 'Quote Archive' ?></h1>
		</div>
		<div class="col3 right">
			<a href="/admin/quotes/add" class="button button-primary">Add Quote</a>
		</div>
		<div class="col3">
			<?php echo $this->Form->create('Search', array('class'=>'filter-form clearfix')); ?>
				<?php
					echo $this->Form->input('term', array('label'=>'Search', 'type'=>'text', 'div'=>false,'class'=>'textbox','placeholder'=>'Search'));
					echo $this->Form->button('<i class="fa fa-search"></i>', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'search-button button-primary'));
				?>
				<?php if(!empty($this->request->data['Search'])): ?>
					<?php echo $this->Html->link('Clear search', array('action' => 'index', '?' => array('clear'=>1))); ?>
				<?php endif; ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<table>
		<style>
			td { vertical-align: top }
		</style>
		<tr>
			<th><?php echo $this->Paginator->sort('project_name', 'Project Name');?></th>
			<th><?php echo $this->Paginator->sort('Client.name', 'Client');?></th>
			<th style="min-width: 130px"><?php echo $this->Paginator->sort('contact_name', 'Contact');?></th>
			<th><?php echo $this->Paginator->sort('address1', 'Address & Notes');?></th>
			<th style="min-width: 20px"><?php echo $this->Paginator->sort('site_visit', 'Site visit?');?></th>
			<th style="min-width: 100px"><?php echo $this->Paginator->sort('site_visit_date', 'Visit date/time');?></th>
			<th style="min-width: 100px"><?php echo $this->Paginator->sort('quote_due', 'Quote due date/time');?></th>
			<th style="min-width: 30px"><?php echo $this->Paginator->sort('completed', 'Completed?');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th style="width: 90px" class="actions"></th>
		</tr>
		<?php
		foreach ($quotes as $quote):
			$class = null;
			// do not show warning/alert marked as completed
			if (!$quote['Quote']['completed']):
				if ($quote['Quote']['quote_due'] && strtotime($quote['Quote']['quote_due']) <= strtotime('NOW + 2 days')):
					$class = ' class="warning"';
				endif;

				if ($quote['Quote']['quote_due'] && strtotime($quote['Quote']['quote_due']) <= strtotime('NOW + 1 day')):
					$class = ' class="alert"';
				endif;
			endif;
		?>
			<tr <?php echo $class;?>>

				<td><?php echo $this->Html->link($quote['Quote']['project_name'], array('action' => 'edit', $quote['Quote']['id']),array('escape'=>false)); ?>&nbsp;</td>
				<td><?php echo $quote['Client']['name']; ?>&nbsp;</td>
				<td><?php echo $quote['Quote']['contact_name'] . '<br>(' . $quote['Quote']['contact_tel'] . ')'; ?>&nbsp;</td>
				<td><?php echo $this->App->formatAddress($quote['Quote'], ', ') . ($quote['Quote']['notes'] ? '<br><a href="#" onclick="$(this).html(\'<br>\').next().slideDown(); return false">Click to see notes</a><div style="display: none">' . nl2br($quote['Quote']['notes']) . '</div>' : '') ?>&nbsp;</td>
				<td><?php echo $quote['Quote']['site_visit'] ? '<i class="fa fa-check uploaded-files-tick"></i>' : ''; ?>&nbsp;</td>
				<td><?php echo ($quote['Quote']['site_visit_date'] ? date('d M Y', strtotime($quote['Quote']['site_visit_date'])) : '') . ($quote['Quote']['site_visit_time'] ? '<br>'.$quote['Quote']['site_visit_time'] : ''); ?>&nbsp;</td>
				<td><?php echo ($quote['Quote']['quote_due'] ? date('d M Y', strtotime($quote['Quote']['quote_due'])) : '') . ($quote['Quote']['quote_due_time'] ? '<br>'.$quote['Quote']['quote_due_time'] : ''); ?>&nbsp;</td>
				<td><?php echo $quote['Quote']['completed'] ? '<i class="fa fa-check uploaded-files-tick"></i>' : ''; ?>&nbsp;</td>
				<td><?php echo $quote['Quote']['created'] ? date('j/n/Y', strtotime($quote['Quote']['created'])) : ''; ?>&nbsp;</td>

				<td class="actions" style="white-space: nowrap">
					<?php echo $this->Html->link('<i class="fa fa-edit fa-2x"></i>', array('action' => 'edit', $quote['Quote']['id']),array('escape'=>false)); ?>
					<?php if ($userIsAdmin): ?>
						<?php
						if ($quote['Quote']['archived']):
							// quote is archived so show Undo icon
							echo $this->Html->link('<i class="fa fa-undo fa-2x" title="Restore from Archive"></i>', array('controller' => 'quotes', 'action' => 'archive', $quote['Quote']['id'],0), array('escape'=>false,'class'=>'deleteButton'));
						else:
							// show archive icon
							echo $this->Html->link('<i class="fa fa-archive fa-2x" title="Move to Archive"></i>', array('controller' => 'quotes', 'action' => 'archive', $quote['Quote']['id'], 1), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this quote to Archive?'));
						endif;
						?>
					<?php endif; ?>
					<?php if($userIsAdmin): ?>
						<?php echo $this->Html->link('<i class="fa fa-trash fa-2x"></i>', array('action' => 'delete', $quote['Quote']['id']), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to delete this quote?')); ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<?php if( !$quotes ) : ?>
			<tr>
				<td colspan="9">No records found</td>
			</tr>
		<?php endif; ?>
	</table>
	<div class="pagination group">
		<?php echo $this->Paginator->prev(__('&laquo; previous'), array('tag'=>'div','id'=>'prev','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'prev','class'=>'disabled','escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('first'=>3,'last'=>3,'ellipsis'=>'<span>......</span>','before'=> null,'after'=> null,'separator'=>null));?>
		<?php echo $this->Paginator->next(__('next &raquo;'), array('tag'=>'div','id'=>'next','class'=>'enabled','escape'=>false), null, array('tag'=>'div','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</div>
</div>	
