
<div class="group content">
	<h2>Edit Quote</h2>

	<div class="col9">
		<?php echo $this->Form->create('Quote',array('novalidate'=>'novalidate'));?>
		<div class="group">
			<div class="col6">
				<?php
				echo $this->Form->input('id');
				echo $this->Form->input('client_id',array('label'=>'Client<em>*</em>','type'=>'select','options'=>$clients,'empty'=>'Please select'));
				echo $this->Form->input('contact_name',array('label'=>'Contact name','type'=>'text'));
				echo $this->Form->input('contact_tel',array('label'=>'Contact telephone','type'=>'text'));

				echo $this->Form->input('site_visit',array('type'=>'checkbox','label'=> 'Site visit?', 'style'=>'margin-top: 20px'));
				echo $this->Form->input('site_visit_date',array('label'=>'Site visit date','type'=>'text','class'=>'datepicker'));
				echo $this->Form->input('site_visit_time',array('label'=>'Site visit time','type'=>'text','style'=>'width: 50%; margin-bottom: 35px'));
				echo $this->Form->input('quote_due',array('label'=>'Quote due back date','type'=>'text','class'=>'datepicker'));
				echo $this->Form->input('quote_due_time',array('label'=>'Quote due back time','type'=>'text','style'=>'width: 50%'));
				echo $this->Form->input('completed',array('type'=>'checkbox','label'=> 'Completed?', 'style'=>'margin-top: 15px'));
				?>
			</div>
			<div class="col6">
				<?php
				echo $this->Form->input('project_name',array('label'=>'Project name','type'=>'text'));
				echo $this->Form->input('address1',array('label'=>'Address 1','type'=>'text'));
				echo $this->Form->input('address2',array('label'=>'Address 2','type'=>'text'));
				echo $this->Form->input('address3',array('label'=>'Address 3','type'=>'text'));
				echo $this->Form->input('city',array('label'=>'City','type'=>'text'));
				echo $this->Form->input('postcode',array('label'=>'Postcode','type'=>'text'));
				echo $this->Form->input('notes',array('label'=>'Notes','type'=>'textarea','style'=>'height: 150px'));
				?>
			</div>
		</div>
		<div class="group" style="margin-top: 20px">
			<div class="col6">
				<?php echo $this->Html->link('Cancel',array('action'=>'index'),array('class'=>'button button-secondary')); ?>
			</div>
			<div class="col6 right">
				<?php echo $this->Form->button('Save', array('name'=>'save','type'=>'submit','value'=>'save','class'=>'button button-primary')); ?>
			</div>
		</div>
		<?php echo $this->Form->end();?>
	</div>

	<div class="col3">
		<div class="input">
			<label>Quote Files</label>
			<div class="box">
				<?php echo $this->element('file_uploader', array('objectType'=>'quote', 'files'=>$files, 'objectId'=>$this->request->data['Quote']['id'], 'allowDelete'=>$userIsAdmin)); ?>
			</div>
		</div>
	</div>

</div>
