
<ul class="container clearfix">
	<?php if( $name == 'pages' || $name == 'snippets' || $name == 'adverts'  || $name == 'forms' || $name == 'testimonials' || $name == 'people' || $name == 'signposts' || $name == 'project_categories' || $name == 'galleries' || $name == 'glossaries' || $name == 'resources' || $name == 'events') : ?>				
		<li class="<?php echo $name == 'pages' ? 'selected' : '' ?>"><a href="/admin/pages">Pages</a></li>	
		<li class="<?php echo $name == 'events' ? 'selected' : '' ?>"><a href="/admin/events">Events</a></li>			
		<li class="<?php echo $name == 'adverts' ? 'selected' : '' ?>"><a href="/admin/adverts">Adverts</a></li>
		<li class="<?php echo $name == 'signposts' ? 'selected' : '' ?>"><a href="/admin/signposts">Sign Posts</a></li>
		<li class="<?php echo $name == 'galleries' ? 'selected' : '' ?>"><a href="/admin/galleries">Galleries</a></li>
		<li class="<?php echo $name == 'forms' ? 'selected' : '' ?>"><a href="/admin/forms">Forms</a></li>
		<li class="<?php echo $name == 'snippets' ? 'selected' : '' ?>"><a href="/admin/snippets">Snippets</a></li>
		<li class="<?php echo $name == 'glossaries' ? 'selected' : '' ?>"><a href="/admin/glossaries">Glossary</a></li>
		<li class="<?php echo $name == 'resources' ? 'selected' : '' ?>"><a href="/admin/resources">Resources</a></li>	
		<?php /*
		<li class="<?php echo $name == 'projects' ? 'selected' : '' ?><?php echo $name == 'project_categories' ? 'selected' : '' ?>"><a href="/admin/projects">Case Studies</a></li>
		
		
		<li class="<?php echo $name == 'testimonials' ? 'selected' : '' ?>"><a href="/admin/testimonials">Testimonials</a></li>
		<li class="<?php echo $name == 'people' ? 'selected' : '' ?>"><a href="/admin/people">People</a></li> */ ?>
	
	<?php endif; ?>
	
	<?php if( $name == 'posts' || $name == 'post_categories' ) : ?>				
		<li class="<?php echo $name == 'posts' ? 'selected' : '' ?>"><a href="/admin/posts">Posts</a></li>				
		<li class="<?php echo $name == 'post_categories' ? 'selected' : '' ?>"><a href="/admin/post_categories">Categories</a></li>	
	<?php endif; ?>
	
	<?php if( $name == 'users' || $name == 'access_groups' ) : ?>				
		<li class="<?php echo $name == 'users' ? 'selected' : '' ?>"><a href="/admin/users">Users</a></li>				
		<?php if ($userIsAdmin): ?>
			<li class="<?php echo $name == 'access_groups' ? 'selected' : '' ?>"><a href="/admin/access_groups">Access Groups</a></li>
		<?php endif; ?>
	<?php endif; ?>
	
	<?php if( $name == 'products' || $name == 'categories' || $name == 'orders' || $name == 'delivery_rules' ) : ?>				
		<li class="<?php echo $name == 'products' ? 'selected' : '' ?>"><a href="/admin/products">Products</a></li>				
		<li class="<?php echo $name == 'categories' ? 'selected' : '' ?>"><a href="/admin/categories">Categories</a></li>	
		<li class="<?php echo $name == 'orders' ? 'selected' : '' ?>"><a href="/admin/orders">Orders</a></li>	
		<li class="<?php echo $name == 'delivery_rules' ? 'selected' : '' ?>"><a href="/admin/delivery_rules">Delivery Rules</a></li>	
	<?php endif; ?>
</ul>