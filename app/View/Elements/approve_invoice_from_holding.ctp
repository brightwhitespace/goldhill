
<?php $invoice = $invoice && !isset($invoice['Invoice']) ? array('Invoice' => $invoice) : $invoice; ?>

<?php if (!$invoice): ?>
    NO INVOICE
<?php elseif ($userIsAdmin && $invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_DRAFT): ?>
    <a href="/admin/invoices/setStatus/<?php echo $invoice['Invoice']['id'] . '/' . Invoice::STATUS_APPROVED ?>" class="button button-primary"><i class="fa fa-file-o"></i> APPROVE</a>
<?php elseif ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_APPROVED): ?>
    <i class="fa fa-file-o"></i> APPROVED
<?php elseif ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_SENT): ?>
    <i class="fa fa-file-o"></i> SENT
<?php else: ?>
    <i class="fa fa-file-o"></i> DRAFT
<?php endif; ?>