<?php if ($this->Paginator->hasPage(2)) : ?>
<div class="row pagination">
	<ul class="group">
	<?php echo $this->Paginator->prev(__('&laquo; previous',true), array('tag'=>'li','id'=>'prevPage','class'=>'enabled','escape'=>false), null, array('tag'=>'li','id'=>'prev','class'=>'disabled','escape'=>false));?>
	
	<?php echo $this->Paginator->numbers(array('before'=> '<li>','after'=> '</li>','separator'=>'</li><li>'));?>
	
	<?php echo $this->Paginator->next(__('next &raquo;',true), array('tag'=>'li','id'=>'nextPage','class'=>'enabled','escape'=>false), null, array('tag'=>'li','id'=>'next','escape'=>false,'class'=>'disabled'));?>
	</ul>
</div>
<?php endif; ?>