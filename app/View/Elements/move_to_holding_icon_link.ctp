
<?php
$cannotMoveToHoldingReasons = array();

if (empty($project['ProjectItem'])):
    $cannotMoveToHoldingReasons[] = 'no project items exist to create invoice';
endif;

if (!empty($project['Tool'])):
    $cannotMoveToHoldingReasons[] = 'tools are assigned to this project';
endif;

if (!$cannotMoveToHoldingReasons):
    echo $this->Html->link('<i class="fa fa-folder-o fa-2x" title="Move to Holding"></i>', array('controller' => 'projects', 'action' => 'createInvoice', $project['Project']['id'], 1), array('escape'=>false,'class'=>'deleteButton'), __('Are you sure you want to move this project to Holding?'));
else:
    $message = 'Cannot move to holding as ' . implode(' AND ', $cannotMoveToHoldingReasons);
?>
    <a href="#" onclick="alert('<?php echo $message ?>'); return false;" title="<?php echo $message ?>">
        <span class="fa-stack" style="margin-top: -8px">
            <i class="fa fa-folder-o fa-stack-2x"></i>
            <i class="fa fa-times fa-stack-1x"></i>
        </span>
    </a>
<?php endif; ?>