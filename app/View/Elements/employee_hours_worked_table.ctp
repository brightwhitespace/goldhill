
<div class="hw-report-container">

    <div class="group hw-day hw-day-header">
        <div class="hw-day-name">Date</div>
        <div class="hw-projects">
            <div class="hw-project">
                <div class="hw-project-name">Project</div>
                <div class="hw-timesheet-entries">
                    <div class="hw-timesheet-entry group">
                        <div>Start</div>
                        <div>End</div>
                        <?php echo empty($hideBreakColumn) ? '<div>Break</div>' : '' ?>
                        <div>Worked</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($days)): ?>
        <?php foreach($days as $dayKey => $day): ?>
            <div class="group hw-day">
                <div class="hw-day-name"><?php echo $dayKey ?></div>
                <div class="hw-projects">
                    <?php foreach ($day as $projectKey => $timesheetEntries): ?>
                        <div class="hw-project group">
                            <div class="hw-project-name"><?php echo h($projectKey) ?></div>
                            <div class="hw-timesheet-entries">
                                <?php foreach ($timesheetEntries as $timesheetEntry): ?>
                                    <div class="hw-timesheet-entry group">
                                        <div><?php echo $timesheetEntry['start_time'] ?></div>
                                        <div><?php echo $timesheetEntry['end_time'] ?></div>
                                        <?php echo empty($hideBreakColumn) ? '<div>' . $timesheetEntry['break_hours'] . '</div>' : '' ?>
                                        <div><?php echo str_replace(array('.00', '.50'), array('.0', '.5'), number_format($timesheetEntry['hours_worked'], 2)) ?></div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="group hw-day">
            <i>No results for this date range</i>
        </div>
    <?php endif; ?>

</div>