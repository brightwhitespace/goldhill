
<div id="uploadUpdatable_<?php echo $objectType; ?>" class="uploadedFileList" data-file-uploader-object-type="<?php echo $objectType; ?>">
    <?php foreach ($files as $filepath): ?>
        <div>
            <?php if (!empty($allowDelete)): ?>
                <a class="deleteLink" href="/admin/files/delete/<?php echo $objectType; ?>/<?php echo $objectId; ?>/<?php echo urlencode(basename($filepath)); ?>" onclick="return confirm('Are you sure you wish to delete this file?')"><i class="fa fa-times"></i></a>
            <?php endif; ?>
            <a href="/admin/files/view/<?php echo $objectType; ?>/<?php echo $objectId; ?>/<?php echo urlencode(basename($filepath)); ?>"><i class="fa fa-download"></i></a>
            &nbsp;<span class="editable" rel="/admin/files/rename/<?php echo $objectType; ?>/<?php echo $objectId; ?>/<?php echo urlencode(basename($filepath)); ?>" id="name"><?php echo basename($filepath) ?></span>
        </div>
    <?php endforeach; ?>
</div>

<div id="dragAndDropFiles_<?php echo $objectType; ?>" class="uploadArea">
    <h3>Drag & drop Files here to upload</h3>
</div>

<form name="demoFiler_<?php echo $objectType; ?>" id="demoFiler_<?php echo $objectType; ?>" enctype="multipart/form-data" rel="<?php echo $objectType; ?>/<?php echo $objectId; ?>" method="post">
    <input type="file" name="multiUpload_<?php echo $objectType; ?>" id="multiUpload_<?php echo $objectType; ?>" multiple />

    <button name="submitHandler" class="submitButton buttonUpload">Upload</button>
</form>

<div class="progressBar">
    <div class="status"></div>
</div>