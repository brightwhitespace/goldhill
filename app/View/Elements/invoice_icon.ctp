
<?php $invoice = !isset($invoice['Invoice']) ? array('Invoice' => $invoice) : $invoice; ?>

<a href="/admin/invoices/view/<?php echo $invoice['Invoice']['id'] ?>">
    <span class="fa-stack" style="margin-top: -8px">
        <i class="fa fa-file-o fa-stack-2x"></i>
        <?php if ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_DRAFT): ?>
            <i class="fa fa-question-circle fa-lg fa-stack-1x" title="View Invoice (DRAFT)"></i>
        <?php elseif ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_APPROVED): ?>
            <i class="fa fa-check fa-lg fa-stack-1x" title="View Invoice (Approved)"></i>
        <?php else: ?>
            <i class="fa fa-envelope-o fa-stack-1x" title="View Invoice (Sent)"></i>
        <?php endif; ?>
    </span>
</a>