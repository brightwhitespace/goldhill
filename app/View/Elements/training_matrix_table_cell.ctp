
<?php
$employeeId = $employee['Employee']['id'];
$certId = $certificate['Certificate']['id'];

$certCellId = sprintf('employee-%s-cert-%s', $employeeId, $certId);
$certClass = 'no-cert';
$employeeCertRecord = array('id' => null, 'start_date' => null, 'end_date' => null);

if (!empty($employee['CertificatesEmployee'][$certId])): // employee has this certificate
    $employeeCertRecord = $employee['CertificatesEmployee'][$certId];
    $endDate = $employeeCertRecord['end_date'] ? DateTime::createFromFormat('Y-m-d', $employeeCertRecord['end_date']) : null;
    $certClass = $this->App->getCertExpiryClass($employeeCertRecord, $certificate);
endif;
?>
<td id="<?php echo $certCellId ?>" class="<?php echo $certClass ?>">
    <?php if ($employeeCertRecord['id'] && empty($employeeCertRecord['ignore_previous_records'])): ?>
        <div><?php echo !empty($endDate) ? $endDate->format('j-n-y') : '<i class="fa fa-check" style="font-size: 18px; line-height: 12px; color: #b88400"></i>' ?></div>
    <?php endif; ?>
    <div class="cert-actions">
        <?php // use explicit onclick to open modal or call removeCertificate etc as the training matrix cells are recreated dynamically when changes are made ?>
        <a class="add-cert" href="#" onclick="return openAddCertificatePopup(this)" data-cert-id="<?php echo $certId ?>" data-employee-id="<?php echo $employeeId ?>"
                data-start-date="<?php echo $employeeCertRecord['start_date'] ? date('j-n-Y', strtotime($employeeCertRecord['start_date'])) : '' ?>"
                data-end-date="<?php echo $employeeCertRecord['end_date'] ? date('j-n-Y', strtotime($employeeCertRecord['end_date'])) : '' ?>">
            <i class="fa <?php echo !empty($employeeCertRecord['id']) ? 'fa-edit' : 'fa-plus-circle' ?>"></i>
        </a>
        <a class="delete-cert" href="#" onclick="return removeEmployeeCertificate(this, false)" data-cert-id="<?php echo $certId ?>" data-employee-id="<?php echo $employeeId ?>">
            <i class="fa fa-times-circle"></i>
        </a>
    </div>
</td>
