/* ---- init ---- */

var menuSlideSpeed = 400;
var mobileBreakPoint = 768;

$(document).ready(function(){
	initUpdateOnSelect();
	initUpdateClientOnProjectSelect();
	initCheckboxesThatEnableRelatedInputField();
	initDatePicker();
	initProjectForm();
	initProjectSelector();
	initPOForm();
	initLabourForm();
	initInvoiceForm();
	initTimesheetForm();
	initAgencyTimesheetForm();
	initTodoPage();
	initToolsAddEdit();
	initUserAdminForm();
	initTables();
	initUtils();
	initTabs();
	initExpandable();
	initSortables();
	initFormEditor();
	initMultiUploadForm();
	initEditableTables();	
	initContentBlocks();
	initTestimonials();
	initResources();
	initBulkSelect();
	initSiteSwitcher();
	initDisplayImageOnBrowseSelect();
	setReadOnly();
	initModalWindow();
	initMobileMenu();
});

function initUpdateOnSelect(){
	$('.update-on-select').off('change');

	$('.update-on-select').on('change',function(){
		handleUpdateOnSelect(this);
	});
	
	$('.update-on-select').each(function(i,ob){
		handleUpdateOnSelect(ob);
	});
}

function handleUpdateOnSelect(ob){
	var urlToLoad = $(ob).data('updateUrl') + $(ob).val();

	// if data-project-id exists then add as extra parameter to url
	if (typeof $(ob).data('selectedContactId') !== 'undefined') {
		urlToLoad += '/' + $(ob).data('selectedContactId');
	}

	domToUpdate = '#'+$(ob).data('update');
	if($(ob).val() > 0){
		$.ajax({
			type: 'POST',
			url: urlToLoad,
			success: function(data){
				$(domToUpdate).html(data);
				setReadOnly();
			}
		});
	}else{
		$(domToUpdate).html();
	}
}

function initUpdateClientOnProjectSelect() {
	$('.update-client-on-project-select').on('change', updateClientOnProjectSelect);

	updateClientOnProjectSelect();

	// make sure to re-enable client dropdown on submit otherwise client id is not passed through
	$('.update-client-on-project-select').parents('form').on('submit', function() {
		$('.project-client-to-update').prop('disabled', false);
	});
}

function updateClientOnProjectSelect() {
	// NOTE: needs json object called "projectIdToClientIdMappings" to be set up on page
	//  - this is effectively an array of project id => client id mappings

	if (typeof projectIdToClientIdMappings !== 'undefined') {
		var selectedProjectId = $('.update-client-on-project-select').val();

		if (selectedProjectId) {
			// select appropriate client option from dropdown and then disable it
			var clientIdOfProject = projectIdToClientIdMappings[selectedProjectId];
			$('.project-client-to-update').val(clientIdOfProject).prop('disabled', true);
		} else {
			// re-enable client dropdown if no project selected
			$('.project-client-to-update').prop('disabled', false);
		}
	}
}

function initCheckboxesThatEnableRelatedInputField() {
	// NOTE: A checkbox with a related input must be initially checked/unchecked as required
	$('.checkbox-to-enable-input').each(function () {
		enableInputRelatedToCheckbox($(this), true);
	});
}

function enableInputRelatedToCheckbox(checkbox) {
	// get input (or select) related to this checkbox
	var correspondingInput = $(checkbox).parent().find('input[type="text"]').first();
	correspondingInput = correspondingInput ? correspondingInput : $(checkbox).parent().find('select').first();

	// if checkbox ticked then enable related input (and set its default value if value is not already set and
	// the related input has a data-default-value attribute present)
	if ($(checkbox).prop('checked')) {
		var currentVal = correspondingInput.val();
		if (!currentVal && correspondingInput.data('defaultValue')) {
			correspondingInput.val(correspondingInput.data('defaultValue'));
		}
		correspondingInput.prop('disabled', false);
	} else {
		// checkbox is not ticked so clear related input and disable it
		correspondingInput.val('').prop('disabled', true);
	}
}
function initDatePicker(){
    $( ".datepicker" ).datepicker({ dateFormat: "d-m-yy",changeYear: true,yearRange: "1945:2100",showButtonPanel: true });
}

function initLabourForm(){
	calcLabourForm();
	$('#LabourRate').on('change',function(){
		calcLabourForm();
	});
	$('#LabourQuantity').on('change',function(){
		calcLabourForm();
	});
}

function calcLabourForm(){
	var rate = $('#LabourRate').val();
	var quantity = $('#LabourQuantity').val();
	var total = rate * quantity;
	$('#LabourTotal').val(total.toFixed(2));
}

function initInvoiceForm(){
	// use this global variable to store next index value for new invoice item
	invoiceItemIndex = $('#invoice-items-container').find('.invoice-item').length;
}

function addInvoiceItem() {
	$('#invoice-items-container').append(
		$($('#invoice-item-template').html().replace(/INDEX/g, invoiceItemIndex++))
	);

	$( ".datepicker" ).datepicker({ dateFormat: "d-m-yy",changeYear: true,yearRange: "1945:2020",showButtonPanel: true });

	return false;
}

function deleteInvoiceItem(ob) {
	if (confirm('Please confirm you wish to remove this item...')) {
		$(ob).parentsUntil('.invoice-item').parent().remove();
	}

	return false;
}


/** Standard Timesheets **/

function initTimesheetForm() {
	// use this global variable to store next index value for new timesheet entry
	timesheetEntryIndex = $('#timesheet-entries-container').find('.timesheet-entry').length;
}

function handleTimesheetEmployeeChange(ob) {
	var timesheetEntry = $(ob).parents('.timesheet-entry').first();
	console.log(timesheetEntry);
	var employeeRate = $(ob).find('option:selected').data('ratePh');

	$(timesheetEntry).find('.rate_ph').first().val(employeeRate);
}

function addTimesheetEntry() {
	$('#timesheet-entries-container').append($('#timesheet-entry-template').html().replace(/INDEX/g, timesheetEntryIndex++));

	return false;
}

function deleteTimesheetEntry(ob) {
	if (confirm('Please confirm you wish to remove this entry...')) {
		$(ob).parentsUntil('.timesheet-entry').parent().remove();
	}

	ob.preventDefault();
}


/** Agency Timesheets **/

function initAgencyTimesheetForm() {
	isInitialisingAgencyTimesheetForm = true;

	// use this global variable to store next index value for new timesheet entry
	timesheetEntryIndex = $('#timesheet-entries-container').find('.timesheet-entry').length;

	filterAllAgencyEmployeeDropdowns();

	isInitialisingAgencyTimesheetForm = false;
}

function filterAllAgencyEmployeeDropdowns() {
	// go through agency dropdowns and call the "handleChange..." function on each to filter the corresponding employee list
	$('.agency-select').each(function (idx, ele) {
		handleAgencyTimesheetAgencyChange($(ele));
	});

	// go through employee dropdowns and call the "handleChange..." function if ADD NEW EMPLOYEE was selected (to show text input field)
	$('.agency-employee-select').each(function (idx, ele) {
		if ($(ele).val() == 'add-new-employee') {
			handleAgencyTimesheetEmployeeChange($(ele));
		}
	})
}

function handleAgencyTimesheetAgencyChange(ob) {
	// get the employee dropdown corresponding to this agency dropdown
	var timesheetEntry = $(ob).parents('.timesheet-entry').first();
	var employeeSelect = $(timesheetEntry).find('.agency-employee-select').first();

	// show or hide employee option based on whether its supplier_id is the same as the selected agency/supplier_id
	// (always show ADD NEW EMPLOYEE option providing an agency is selected)
	var selectedAgencyId = $(ob).val();
	$(employeeSelect).find('option').each(function (idx, ele) {
		if (($(ele).data('supplierId') == selectedAgencyId) || (selectedAgencyId && $(ele).val() == 'add-new-employee')) {
			$(ele).show();  // not using jquery toggle(bool) function as animation seems to be screwing up the show/hide
		} else {
			$(ele).hide();
		}
	});

	if (!isInitialisingAgencyTimesheetForm) {
		$(employeeSelect).val('');
	}
}

function handleAgencyTimesheetEmployeeChange(ob) {
	var timesheetEntry = $(ob).parents('.timesheet-entry').first();

	if ($(ob).val() == 'add-new-employee') {
		$(ob).hide();
		$(ob).siblings().show();
	} else {
		var employeeRate = $(ob).find('option:selected').data('ratePh');
	}

	$(timesheetEntry).find('.rate_ph').first().val(employeeRate);
}

function cancelAddNewAgencyEmployee(ob) {
	$(ob).parent().hide();
	var employeeSelect = $(ob).parent().siblings().first();
	$(employeeSelect).val('').show();

	return false;
}

function addAgencyTimesheetEntry() {
	$('#timesheet-entries-container').append($('#timesheet-entry-template').html().replace(/INDEX/g, timesheetEntryIndex++));

	return false;
}

function deleteAgencyTimesheetEntry(ob) {
	if (confirm('Please confirm you wish to remove this entry...')) {
		$(ob).parentsUntil('.timesheet-entry').parent().remove();
	}

	ob.preventDefault();
}

/** Projects **/

function initProjectForm(){
	$('#ProjectValue').on('change',function(){
		var val = $('#ProjectValue').val();
		if(val > 0){
			var maxSpend = val * 0.75;
			$('#ProjectMaxSpend').val(maxSpend.toFixed(2));
		}
	});
	$('#ProjectValueLabour').on('change',function(){
		updateProjectValue();
	});
	$('#ProjectValueOther').on('change',function(){
		updateProjectValue();
	});

	// use this global variable to store next index value for new project item
	projectItemIndex = $('#project-items-container').find('.project-item').length;
}

function addProjectItem() {
	$('#project-items-container').append(
		$($('#project-item-template').html().replace(/INDEX/g, projectItemIndex++))
	);

	$( ".datepicker" ).datepicker({ dateFormat: "d-m-yy",changeYear: true,yearRange: "1945:2020",showButtonPanel: true });

	return false;
}

function deleteProjectItem(ob) {
	if (confirm('Please confirm you wish to remove this item...')) {
		$(ob).parentsUntil('.project-item').parent().remove();
		updateProjectValue();
	}

	return false;
}

function updateProjectValue(){
	var val1 = $('#ProjectValueLabour').val();
	var val2 = $('#ProjectValueOther').val();

	var valProjectItems = 0;
	//$('.project-item-cost').each(function () {
	//	if (!isNaN($(this).val()) && $(this).val() != '') {
	//		var projectItemVal =  Number($(this).val());
	//		$(this).val(projectItemVal.toFixed(2));
	//		valProjectItems += projectItemVal;
	//	} else {
	//		$(this).val('');
	//	}
	//});
	
	$('#ProjectValue').val((Number(val1) + Number(val2) + valProjectItems).toFixed(2));
	
	var val = $('#ProjectValue').val();
	if(val > 0){
		var maxSpend = val * 0.75;
		$('#ProjectMaxSpend').val(maxSpend.toFixed(2));
	}
	
}

function initProjectSelector() {
	// handle change event on project selector (event containing target element is passed through to handler function)
	$('#select-project').on('change', function() {
		updateProjectAddress($(this));
	});

	// fire change event to show address if project has been pre-selected
	$('#select-project').change();
}

function updateProjectAddress(projectSelector) {
	$('#project-address').hide();
	var selectedOption = projectSelector.find('option:selected');
	if (selectedOption.data('address')) {
		var addressHtml = selectedOption.data('address').split(',').join('<br>');
		$('#project-address').html(addressHtml).show();
	}
}

function setReadOnly(){
	$('.readonly input, .readonly select').attr('readonly','readonly').attr('disabled','disabled');
	$('.not-readonly input, .not-readonly select').removeAttr('readonly').removeAttr('disabled');
}


function initPOForm(){
	
	//set read only based on admin access
	// Reference for PO type ids: 1 => hire, 2 => skip, 3 => sundry, 4 => labour
	
	if($('#PurchaseOrderTypeId').val() == 2){
		$('.type-skips').show();
		$('.type-hire').hide();
	}else{
		$('.type-skips').hide();
		$('.type-hire').show();
	}
	
	$('#PurchaseOrderTypeId').on('change',function(){

		if($('#PurchaseOrderTypeId').val() == 2){
			$('.type-skips').show();
			$('.type-hire').hide();
		}else{
			$('.type-skips').hide();
			$('.type-hire').show();
		}
		calcPoForm();
	});
	
	if($('#PurchaseOrderStartDate').length > 0){
		calcPoForm();
	}
	
	$('#PurchaseOrderQuantity').on('change',function(){
		calcPoForm();
	});
	
	$('#PurchaseOrderRate').on('change',function(){
		calcPoForm();
	});
	
	$('#PurchaseOrderStartDate').on('change',function(){
		calcPoForm();
	});
	
	$('#PurchaseOrderEndDate').on('change',function(){
		calcPoForm();
	});
	
	$('#PurchaseOrderExcessTonage').on('change',function(){
		calcPoForm();
	});
	
	$('#PurchaseOrderAdminAddForm').on('submit',function(){
		calcPoForm();
		$(this).submit();
	});
}

function calcPoForm(){
	if($('#PurchaseOrderStartDate').val() != ""){
		dateParts = $('#PurchaseOrderStartDate').val().split('-');
		var startDate = new Date(dateParts[2],(dateParts[1]-1),dateParts[0]);
	}

	if($('#PurchaseOrderEndDate').val() != ""){		
		dateParts = $('#PurchaseOrderEndDate').val().split('-');
		var endDate = new Date(dateParts[2],(dateParts[1]-1),dateParts[0],23,59,0);		
	}
	
	var days = Math.round((endDate - startDate)/1000/60/60/24);

	var quantity = $('#PurchaseOrderQuantity').val();
	var rate = $('#PurchaseOrderRate').val();
	var excess = $('#PurchaseOrderExcessTonage').val();
	var total = 0;
	
	
	switch($('#PurchaseOrderTypeId').val()) {

		case '2':  // Skip Hire...
			total = Number(quantity) * (Number(rate) + (Number(excess) * 5));
			break;

		// This is for the 7 day rate for Hire items (cost based on number of days hired for)
		//case '1':  // Standard Hire...
		//	total = Number(quantity) * (Number(rate) / 7) * (isNaN(days) ? 1 : days);
		//	break;

		//case '4':  // Labour...
		//	total = (days * 8) * Number(quantity) * Number(rate);
		//	break;

		default:
			total = quantity * rate;
	}
	
	$('#PurchaseOrderValue').val(total.toFixed(2));
}


function initTodoPage() {
	$('.todo-status-icon-pending, .todo-status-icon-in-progress').on('click', function() {
		var tr = $(this).closest('tr');
		tr.removeClass('todo-pending todo-in-progress todo-overdue todo-due-today');
		tr.addClass('todo-completed');
		tr.find('.todo-status-icon-completed').first().prop('title', 'Completed by you - click to mark as PENDING');
		var todoId = tr.data('todoId');

		$.get('/admin/todos/complete/' + todoId + '/1')
			.done(function(data) {
				if (data) {
					alert(data);
				}
			});

		return false;
	});
	$('.todo-status-icon-completed').on('click', function() {
		var tr = $(this).closest('tr');
		tr.removeClass('todo-completed');
		tr.addClass('todo-pending');
		var todoId = tr.data('todoId');

		$.get('/admin/todos/complete/' + todoId + '/0')
			.done(function(data) {
				if (data) {
					alert(data);
				}
			});

		return false;
	});

	$('.todo-archive-button').on('click', function() {
		var tr = $(this).closest('tr');
		tr.fadeOut(function () { tr.remove() }).slideUp().find('*').slideUp();
		var todoId = tr.data('todoId');

		$.get('/admin/todos/archive/' + todoId + '/1')
			.done(function(data) {
				if (data) {
					alert(data);
				}
			});

		return false;
	});
	$('.todo-undo-archive-button').on('click', function() {
		var tr = $(this).closest('tr');
		tr.fadeOut(function () { tr.remove() }).slideUp().find('*').slideUp();
		var todoId = tr.data('todoId');

		$.get('/admin/todos/archive/' + todoId + '/0')
			.done(function(data) {
				if (data) {
					alert(data);
				}
			});

		return false;
	});
}

function initBulkSelect(){
	
	$('#selectAll').click(function(e){
			
			$('.bulk-select').each(function(i,ob){
				
					$(ob).find('input').prop('checked',true);
				
			});
			
			e.preventDefault();
			e.stopPropagation();
		
	});
	$('#selectNone').click(function(e){
		
			$('.bulk-select').each(function(i,ob){
				
					$(ob).find('input').prop('checked',false);
				
			});
			
			e.preventDefault();
			e.stopPropagation();
	});
	
	
}

function initTestimonials(){
	$('.addTestimonial').unbind('click').click(function(){
		var pos = $('#product-reviews fieldset').length;
		
		var html = '<fieldset><ol><li><label for="Testimonial' + pos + 'Author">Author<em>*</em></label><input name="data[Testimonial][' + pos + '][author]" class="textbox" type="text" value="" id="Testimonial' + pos + 'Author" required="required"/></li><li><label for="Testimonial' + pos + 'Quote">Quote</label><textarea name="data[Testimonial][' + pos + '][quote]" class="bigtextbox" cols="30" rows="6" id="Testimonial' + pos + 'Quote" required="required"></textarea><br><img src="/assets/admin/icon_minus.png" class="minusButton removeContentBlock" rel=""></li>	</ol></fieldset>';
		
		$('#product-reviews fieldset:last').after(html);
		
		initContentBlocks();
	});
	
	$('.removeTestimonial').unbind('click').click(function(){
		if (confirm("Are you sure?")){
			var id = $(this).attr('rel');
			if(id > 0){
				$.ajax({
					type: 'POST',
					url: '/admin/testimonials/delete/' + id,			
					success: function(data){				
						$('#testimonial' + id).slideUp(200);
						setTimeout(function(){$('#testimonial' + id).remove()},300);
					}
				});
			}else{
				$(this).parent().parent().slideUp(200);
				setTimeout(function(){$(this).parent().remove()},300);
			}
			
		}
	});
}

function initContentBlocks(){
	$('.addContentBlock').unbind('click').click(function(){
		var pos = $('#contentBlocks li').length;
		var html = '<li><label for="ContentBlock' + pos + 'Content"></label><textarea name="data[ContentBlock][' + pos + '][content]" cols="30" rows="6" id="ContentBlock' + pos + 'Content"></textarea><img src="/assets/admin/icon_minus.png" class="minusButton removeContentBlock" rel=""><img src="/assets/admin/icon_plus.png" class="plusButton addContentBlock"><input name="data[ContentBlock][' + pos + '][published]" type="hidden" value="1"></li>';
		
		$('#contentBlocks li:last').after(html);
		
		CKEDITOR.replace('data[ContentBlock][' + pos + '][content]');
		
		initContentBlocks();
	});
	
	$('.removeContentBlock').unbind('click').click(function(){
		if (confirm("Are you sure?")){
			var id = $(this).attr('rel');
			if(id > 0){
				$.ajax({
					type: 'POST',
					url: '/admin/pages/deletecontent/' + id,			
					success: function(data){				
						$('#content' + id).slideUp(200);
						setTimeout(function(){$('#content' + id).remove()},300);
					}
				});
			}else{
				$(this).parent().slideUp(200);
				setTimeout(function(){$(this).parent().remove()},300);
			}
			
		}
	});
}

function initTables(){
	$('tr').click(function(){
		//$(this).toggleClass('highlight');
	});
	
	$('tr').dblclick(function(){
	  var id = $(this).attr('id');
	  if(id.length > 0){
		 var parts = id.split('-');
		 window.location.href = '/admin/' + parts[0] + '/' + parts[1] + '/' + parts[2]; 
	  }
	});
}

function initEditableTables(){
     $('.editable').each(function(i,ob){
			$(ob).editable($(ob).attr('rel'), 
			{ 
				type      : 'text',
				width		: 150,
				cancel    : 'Cancel',
				submit    : 'OK',
				indicator : '<img src="/assets/admin/indicator.gif">',
				tooltip   : 'Click to edit...',
				cssclass : 'inplaceEditor',
				callback : function(value, settings) {
					//refreshUploadUpdatable();
					//console.log(this);
					//console.log(value);
					//console.log(settings);
				}
			}
		)
     });
}

function initMultiUploadForm()
{
	var ver = getIEVersion();

	$('[data-file-uploader-object-type]').each(function(idx, ele) {

		var objectType = $(ele).data('fileUploaderObjectType');

		// get unique ids for each uploader
		var demoFilerId = 'demoFiler_' + objectType;
		var dragAndDropFilesId = 'dragAndDropFiles_' + objectType;
		var uploadUpdatableId = 'uploadUpdatable_' + objectType;

		if ($('#'+demoFilerId).length > 0 && ver == -1) {
			var support = "image/jpg,image/png,image/bmp,image/jpeg,image/gif,message/rfc822"
				+ ',application/zip,application/x-compressed-zip,application/pdf,text/plain,'
				+ 'application/rtf,application/msword,application/vnd.ms-excel,application/vnd.ms-powerpoint,'
				+ 'application/vnd.openxmlformats-officedocument.wordprocessingml.document,'
				+ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,'
				+ 'application/vnd.openxmlformats-officedocument.presentationml.presentation';

			initMultiUploader({
				support : support,
				form: demoFilerId,					// Form ID
				dragArea: dragAndDropFilesId,		// Upload Area ID
				uploadUrl: "/admin/files/upload/" + $('#'+demoFilerId).attr('rel'),  // Server side upload url
				onComplete: function(){
					refreshUploadUpdatable(objectType);
				}
			});
		}
	});
}

function refreshUploadUpdatable(objectType)
{
	var demoFilerId = 'demoFiler_' + objectType;
	var uploadUpdatableId = 'uploadUpdatable_' + objectType;

	if ($('#'+uploadUpdatableId).length > 0) {
		$.ajax({
			type: 'POST',
			url: "/admin/files/updateAfterUpload/" + $('#'+demoFilerId).attr('rel'),
			success: function(data){
				$('#'+uploadUpdatableId).html(data);
				initEditableTables();
			}
		});
	}
}

function initUtils(){
	//hide ajax messages
	$("#ajax-status").hide();
	
	//disable form buttons once clicked
	$('form.cmxform').submit(function(){
		$('button[type=submit]', this).attr('disabled', 'disabled');
		$('button[type=submit]').addClass('submitDisabled');
	});
	
	//hide flash messages after displaying for 2 secs
	$('#flashMessage').delay(2000).slideUp(200);
	
	$('.currencybox').keyup(function () {
		if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
		   this.value = this.value.replace(/[^0-9\.]/g, '');
		}
	});

}


function initSortables(){
	$('ol.sortable').each(function(){
		maxLevels = $(this).attr('id').substr($(this).attr('id').length - 1, 1);
		//console.log($(this).nestedSortable.options);
		$(this).nestedSortable({
			
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			maxLevels: maxLevels,
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div'
		});
	});
	
	$('.saveOrder').unbind('click');
	
	$('.saveOrder').click(function(){
		subUrl = $(this).attr('id').split("saveOrder_");
		serialized = $('ol.sortable').nestedSortable('serialize');
		$(this).attr('disabled', 'disabled');
		$('.saveOrder').addClass('submitDisabled');
		$.ajax({
			type: 'POST',
			url: '/admin/'+ subUrl[1] +'/changeorder/',
			data: serialized,
			success: function(data){
				$("#ajax-status").slideDown(200).delay(1000).slideUp(200);
				$('.saveOrder').removeClass('submitDisabled');
				$('.saveOrder').removeAttr('disabled');
			}
		});
	});
	
	$('.sortableDelete').click(function(e,ob){
		id = $(this).attr('id');
		e.stopPropagation(); e.preventDefault();
		$.ajax({
			type: 'POST',
			url: $(this).attr('href'),			
			success: function(data){				
				$('#'+id).parent().slideUp(200);
			}
		});
	});
}

/* ---- Expanding Collapsing ---- */



function initExpandable(){
	//hide all sub branches first
	$('.expandableList ul').hide();
	
	//check cookies for ones to show
	
	if($.cookie('expandedElements') == undefined){
		//first visit
		var expandedElements = [];
	}else{
		cookie = unescape($.cookie('expandedElements'));
		expandedElements = cookie.split(',');
	}
	
	for(var i = 0; i < expandedElements.length; i++){
		$('#' + expandedElements[i]).find('ul').first().slideDown(500);
		$('#' + expandedElements[i]).find('a.expandable').first().addClass('shrinkable');
	}
	
	//add actions
	$('.expandable').click(function(){
		if($.cookie('expandedElements') == undefined){
			var expandedElements = [];
		}else{
			cookie = unescape($.cookie('expandedElements'));
			expandedElements = cookie.split(',');
		}
		
		if($(this).hasClass('shrinkable')){
			//expanded
			$itemId = $(this).parent().parent().attr('id');
			$(this).parent().parent().find('ul').slideUp(500);
			$(this).removeClass('shrinkable');
			
			if(expandedElements.indexOf($itemId) > -1){
				
				expandedElements.splice(expandedElements.indexOf($itemId),1);
			}
		}else{
			$itemId = $(this).parent().parent().attr('id');
			$(this).parent().parent().find('ul').first().slideDown(500);
			$(this).addClass('shrinkable');
			
			if(expandedElements.indexOf($itemId) == -1){
				expandedElements.push($itemId);
			}
		}
		
		console.log(expandedElements);
		$.cookie('expandedElements', escape(expandedElements.join(',')), { expires: 7 });
		
		
	});
	
}


function initTabs(){
	//subsection_tabs
	$( "#tabs" ).tabs();
}

function initResources(){
	switchResourceForm();
	$('#ResourceType').on('change',function(){
		switchResourceForm();
	});
}

function switchResourceForm(){
	if($('#ResourceType').val() == 1){ //file resource
		$('#ResourceResourceFile').parent().show();
		$('#ResourceResourceLink').parent().hide();
	}
	if($('#ResourceType').val() == 2){ //link resource
		$('#ResourceResourceFile').parent().hide();
		$('#ResourceResourceLink').parent().show();
	}
}


function initModalWindow(){
	$('.overlay-block').not('.show-on-page-load').hide();

	$('.overlay-open').on('click',function(e){

		$($(this).attr('href')).fadeIn();

		e.preventDefault();
	});

	$('.overlay-close').on('click',function(e){

		$(this).parents('.overlay-block').fadeOut();

		e.preventDefault();
	});

	$('.overlay-block').on('click',function(){
		$(this).fadeOut();
	});

	$('.overlay-content').on('click',function(e){
		e.stopPropagation();
	});
}


/* ---- Employee Certificate Functions ---- */

function openAddCertificatePopup(addButton) {
	// populate employee and cert data into fields within popup
	$('#add_cert_employee_id').val($(addButton).data('employee-id'));
	$('#add_cert_cert_id').val($(addButton).data('cert-id'));
	$('#add_cert_start_date').val($(addButton).data('start-date'));
	$('#add_cert_end_date').val($(addButton).data('end-date'));

	// display popoup
	$('#add-cert-popup').fadeIn();
}

function addEmployeeCertificate(reloadPage) {
	var employeeId = $('#add_cert_employee_id').val();
	var certId = $('#add_cert_cert_id').val();

	if (!employeeId || !certId) {
		alert('Employee and certificate must both be specified');
		return false;
	}

	var endDate = $('#add_cert_end_date').val();
	var startDate = $('#add_cert_start_date').val();

	var urlParams = employeeId + '/' + certId + (endDate ? '/' + endDate : '');
	urlParams += (endDate && startDate) ? '/' + startDate : '';

	$.ajax({
		type: 'POST',
		url: "/admin/certificates/add_employee_certificate/" + urlParams,
		success: function(data) {
			if (data) {
				var certCellId = '#employee-' + employeeId + '-cert-' + certId;
				$(certCellId).replaceWith($(data));
				$('#add-cert-popup').fadeOut();
			} else {
				alert('There was a problem adding the certificate');
			}

			if (reloadPage) {
				location.reload();
			}
		}
	});

	return false;
}

function removeEmployeeCertificate(removeButton, reloadPage) {
	if (confirm('Are you sure you wish to remove this certificate?')) {
		var employeeId = $(removeButton).data('employee-id');
		var certId = $(removeButton).data('cert-id');

		// use false values for start and end date and a true value to mark the record as "ignore previous versions"
		var urlParams = employeeId + '/' + certId + '/0/0/1';

		$.ajax({
			type: 'POST',
			url: "/admin/certificates/add_employee_certificate/" + urlParams,
			success: function (data) {
				if (data) {
					var certCellId = '#employee-' + employeeId + '-cert-' + certId;
					$(certCellId).replaceWith($(data));
				} else {
					alert('There was a problem removing the certificate');
				}

				if (reloadPage) {
					location.reload();
				}
			}
		});
	}

	return false;
}


/* ---- Tools ---- */

function initToolsAddEdit() {

	$('#tool-status-select').on('change', function () {
		handleToolStatusChange();
	});

	handleToolStatusChange();
}

function handleToolStatusChange() {

	// loop through dropdowns and show/hide based on the selected status (compare with the data a
	$('select[data-display-status-id]').each(function (idx, ele) {
		var isVisible = $(ele).data('displayStatusId') == $('#tool-status-select').val();
		$(ele).parents('div.input').toggle(isVisible);
	});
}


function initUserAdminForm() {

	updateUserAdminForm();

	$('#accessGroupSelect').on('change', updateUserAdminForm);
}

function updateUserAdminForm() {
	// If 'Client' selected (id = 37) then show client dropdown, otherwise show Sub-contractor dropdown
	var isClient = ($('#accessGroupSelect').val() == '37');
	$('#UserClientId').parent().toggle(isClient);
	$('#UserEmployeeId').parent().toggle(!isClient);
}

/* ---- Form Editor Functions ---- */

function initFormEditor(){
	
	$("#addFormField").click(function(e){
		
		var numRows = $('#formFields tr').length - 1;
		var nextRow = numRows ++;			
		
		var row = '<tr><td><input type="hidden" name="data[FormField]['+ nextRow +'][order_num]" value="'+ nextRow +'" id="FormField'+ nextRow +'OrderNum" /><input name="data[FormField]['+ nextRow +'][name]" type="text" class="medtextbox" id="FormField'+ nextRow +'Name" /></td>';
		row += '<td><input name="data[FormField]['+ nextRow +'][label]" type="text" class="medtextbox" id="FormField'+ nextRow +'Label" /></td>';
		row += '<td><select name="data[FormField]['+ nextRow +'][type]" id="FormField'+ nextRow +'Type"><option value="text">Small Text Box</option><option value="textarea">Large Text Box</option><option value="radio">Radio button</option><option value="checkbox">Tick Box</option><option value="select">Select Box</option><option value="content">Content Section</option></select></td>';
		row += '<td><textarea name="data[FormField]['+ nextRow +'][options]" class="bigtextbox" cols="30" rows="6" id="FormField'+ nextRow +'Options" ></textarea></td>';
		row += '<td><input type="hidden" name="data[FormField]['+ nextRow +'][required]" id="FormField'+ nextRow +'Required_" value="0" /><input type="checkbox" name="data[FormField]['+ nextRow +'][required]" value="1" id="FormField'+ nextRow +'Required" /></td><td><a href="#" class="deleteMe"><img src="/assets/admin/icon_delete.png" alt="" /></a></td></tr>';
		
		$('#formFields tr:last').after(row); 
		
		$('.deleteMe').on('click',function(e){
			$(this).parent().parent().remove();
			
			e.preventDefault();
			e.stopPropagation();
		});
		
		e.preventDefault();
		
	});
	
	
	
}

function moveTableRowDown(id){	
	var row = $('#'+id);
	row.insertAfter(row.next());
	
	reOrderItemsFields();	
}

function moveTableRowUp(id){	
	var row = $('#'+id);
	

	row.insertBefore(row.prev());
	reOrderItemsFields();	
}

function reOrderItemsFields(){
	/*for(var i = 0; i < $('sortableRows').rows.length; i++){		
		var bits = $('sortableRows').rows[i].id.split("_");
		$('FormField' + bits[1] +'OrderNum').value = i;
	}*/
	
	
	$('#sortableRows tr').each(function(i,ob){
		var bits = $(ob).attr('id').split("_");
		console.log(i);
		$('#FormField' + bits[1] +'OrderNum').val(i);
		
	});
	
}



function updateUrlField(field1,field2){
	$('#'+field1).val(slugify($('#'+field2).val()));
}


function slugify(str){
	var url = str
		.toLowerCase() // change everything to lowercase
		.replace(/^\s+|\s+$/g, "") // trim leading and trailing spaces		
		.replace(/[_|\s]+/g, "-") // change all spaces and underscores to a hyphen
		.replace(/[^a-z0-9-]+/g, "") // remove all non-alphanumeric characters except the hyphen
		.replace(/[-]+/g, "-") // replace multiple instances of the hyphen with a single instance
		.replace(/^-+|-+$/g, "") // trim leading and trailing hyphens				
		;
	return url;
}

function getIEVersion() {
	var rv = -1; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.test(ua) != null)
			rv = parseFloat( RegExp.$1 );
	}
	return rv;
}


/* ---- Site Switcher ---- */

function initSiteSwitcher() {

	var siteSwitcher = $('#siteSwitcher');
	var currentValue = siteSwitcher.val();

	// remember current site in case user cancels the switch (so can change dropdown back to original value)
	siteSwitcher.data('remembered-value', currentValue);

	// handle site selector being changed
	siteSwitcher.on('change', function () {
		var selectedSiteName = $(this).children(':selected').text();
		if (confirm('Please confirm you wish to edit the ' + selectedSiteName + ' site...')) {
			// remember this value for subsequent switch and submit form
			$(this).data('remembered-value', $(this).val());
			$(this).parent('form:first').submit();
		} else {
			// switch dropdown back to previously selected site
			$(this).val($(this).data('remembered-value'));
		}
	});
}


/* ---- Show images after being selected in CKFinder ---- */

function initDisplayImageOnBrowseSelect() {

	// NOTE: The class attribute of the Ckeditorr->browseField needs to include 'image' for this feature to be added
	$('input.image').each(
		function (idx, ele) {
			var inputId = ele.id;

			// create <img> tag that will appear beneath the Browse button
			var imgTag = '<img class="preview" style="max-height: 150px"/>';

			// create remove button that will set the input value blank (with onchange causing image to be hidden)
			var removeButtonId = 'remove-' + inputId;
			var removeButton = '<img id="'+removeButtonId+'" src="/assets/admin/icon_fail.png" style="position: absolute; top: 0px; right: 0px; cursor: pointer" />';

			// append image and button elements after the browse button
			$(ele).parent().append(
				'<div class="preview"><label></label><div style="display: inline-block; position: relative; padding-right: 30px">' + imgTag + removeButton + '</div></div>'
			);

			// change type of the input that stores the image path to "hidden" (so it's not displayed)
			$(ele).prop('type', 'hidden');

			// when new image is selected then set its path in the <img> tag to display it (or hide it if blank)
			$(ele).on('change', function () {
				var src = $(this).val();
				if (src) {
					if ((/\.(gif|jpg|jpeg|png|svg)$/i).test(src)) {
						$(this).siblings('div.preview').show().find('img.preview').prop('src', src);
					} else {
						alert('Please select a valid image file (.jpg, .gif, .png, .svg)');
					}
				} else {
					$(this).siblings('div.preview').hide();
				}
			});

			// if remove button clicked then set image src to blank
			$('#'+removeButtonId).on('click', function() {
				if (confirm('Please confirm you wish to remove this image...')) {
					$(ele).val('').change();
				}
			});

			// fire change event initially to display image if currently set
			$(ele).change();
		}
	);
}


function initMobileMenu(){

	if(viewport().width < mobileBreakPoint){

		$('.nav').hide();

		$('.mobile-menu').off('click');
		$('.mobile-menu').on('click',function(e){
			$('.nav li ul').hide();
			$('.nav li:first-child ul').css({'display': 'block', 'opacity': 1, 'zIndex': 999});
			$('.nav').slideToggle(menuSlideSpeed);
			$(this).find('span').toggleClass('is-clicked');
			e.preventDefault();
			e.stopPropagation();
		});

		$('.nav > li > a').off('click');
		$('.nav > li > a').on('click',function(e){
			console.log("test");
			if($(this).parent().find('ul').length > 0){
				$(this).parent().parent().find('ul').hide();  // hide all dropdowns first
				$(this).parent().find('ul').css({'display': 'block', 'opacity': 1, 'zIndex': 999}); // display current dropdown
				e.preventDefault();  // disable main menu link
				e.stopPropagation();
			}
		});
	}
}


function viewport() {
	var e = window, a = 'inner';
	if (!('innerWidth' in window )) {
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}
