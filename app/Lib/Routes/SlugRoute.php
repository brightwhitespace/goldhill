<?php
class SlugRoute extends CakeRoute {
 
    function parse($url) {
		
        $params = parent::parse($url);
        if (empty($params)) {
            return false;
        }
        App::uses('AppModel', 'Model');
        App::uses('Page', 'Model');
		App::uses('Product', 'Model');
		App::uses('Category', 'Model');
		App::uses('Site', 'Model');

		$siteModel = new Site();
		$site = $siteModel->find('first', array(
			'conditions' => array('Site.url' => $params['site'] . '/' . $params['country']),
			'recursive' => -1
		));

		if ($site) {
			$Page = new Page();
			$count = $Page->find('count', array(
				'conditions' => array('Page.page_url' => $params['slug'], 'Page.site_id' => $site['Site']['id']),
				'recursive' => -1
			));
		}

		if(!$count){
			$Category = new Category();
			$count = $Category->find('count', array(
				'conditions' => array('Category.url' => $params['slug']),
				'recursive' => -1
			));
		}
		
		if(!$count){
			$Product = new Product();
			$count = $Product->find('count', array(
				'conditions' => array('Product.url' => $params['slug']),
				'recursive' => -1
			));
		}
		
        if ($count) {
			// pass slug back to connect function
			$params['pass']['slug'] = $params['slug'];
			$params['pass']['site'] = $params['site'];
			$params['pass']['country'] = $params['country'];
            return $params;
        }
		Debugger::dump($params);
		
        return false;
    }
 
}
