<?php
class PurchaseOrderInvoice extends AppModel {
	
	var $name = 'PurchaseOrderInvoice';
	var $displayField = 'ref';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $belongsTo = array(
		'PurchaseOrder'
	);	
	
	
	var $validate = array(
		'value' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a value'
			),
		),
		'date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a date'
			),
		),
		'ref' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter an invoice no.'
			),
		)
	);
	
	function get($id){
		$options = array();
		$options['conditions'] = array(
			'PurchaseOrderInvoice.id' => $id
		);
		
		return $this->find('first',$options);
	}
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if(isset($val['PurchaseOrderInvoice'])){			
				if($val['PurchaseOrderInvoice']['date'] != '0000-00-00'){
					$results[$key]['PurchaseOrderInvoice']['date'] = date('d-m-Y',strtotime($val['PurchaseOrderInvoice']['date']));								
				}else{
					$results[$key]['PurchaseOrderInvoice']['date'] = '';
				}
				if($val['PurchaseOrderInvoice']['date_passed'] != '0000-00-00'){
					$results[$key]['PurchaseOrderInvoice']['date_passed'] = date('d-m-Y',strtotime($val['PurchaseOrderInvoice']['date_passed']));								
				}else{
					$results[$key]['PurchaseOrderInvoice']['date_passed'] = '';
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['PurchaseOrderInvoice']['date'])){
			$this->data['PurchaseOrderInvoice']['date'] = date('Y-m-d',strtotime($this->data['PurchaseOrderInvoice']['date']));
		}
		
		if (!empty($this->data['PurchaseOrderInvoice']['date_passed'])){
			$this->data['PurchaseOrderInvoice']['date_passed'] = date('Y-m-d',strtotime($this->data['PurchaseOrderInvoice']['date_passed']));
		}
		
		return true;
	}

	
}