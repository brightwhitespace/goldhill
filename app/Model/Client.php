<?php
class Client extends AppModel {
	
	var $name = 'Client';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'Client.name ASC';

	var $hasMany = array(
		'Contact',
		'Project',
		'User',
	);

	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a company name'
			),
		)
	);

	function getClient($id){
		$options = array();
		$options['conditions'] = array(
			'Client.id' => $id
		);
		$options['contain'] = array(
			'Contact'
		);

		return $this->find('first',$options);
	}

	function getClients($findType = 'all', $contain = null)
	{
		$options = array();
		$options['order'] = array(
			'Client.name'
		);

		if ($contain) {
			$options['contain'] = $contain;
		}

		return $this->find($findType, $options);
	}


}