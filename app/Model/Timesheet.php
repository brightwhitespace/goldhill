<?php
class Timesheet extends AppModel {

	var $name = 'Timesheet';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'Timesheet.date ASC';

	var $belongsTo = array(
		'Project'
	);

	var $hasMany = array(
		'EmployeesTimesheet',
	);

	var $validate = array(
		'date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a date'
			),
		),
	);

	function get($id){
		$options = array();
		$options['conditions'] = array(
			'Timesheet.id' => $id
		);
		$options['contain'] = array(
			'Project',
			'EmployeesTimesheet',
		);

		return $this->find('first',$options);
	}

	function getAll($id, $order = 'asc'){
		$options = array();
		$options['conditions'] = array(
			'Timesheet.project_id' => $id
		);
		$options['contain'] = array(
			'Project',
			'EmployeesTimesheet',
		);
		$options['order'] = array(
			'Timesheet.date' => $order
		);

		return $this->find('all',$options);
	}

	/**
	 * @param int $employeeId
	 * @param DateTime $startDate
	 * @param DateTime $endDate
	 * @return array
	 */
	function getEmployeeHoursWorkedReportData($employeeId, $startDate, $endDate) {

		$timesheets = $this->getEmployeeTimesheetsForDateRange($employeeId, $startDate, $endDate);

		$days = array();

		foreach ($timesheets as $timesheet) {

			$dateKey = date('j M Y (D)', strtotime($timesheet['Timesheet']['date']));
			$projectKey = $timesheet['Project']['name'];

			$days[$dateKey][$projectKey][] = $timesheet['EmployeesTimesheet'];
		}

		return $days;
	}

	/**
	 * @param int $employeeId
	 * @param DateTime $startDate
	 * @param DateTime $endDate
	 * @return array
	 */
	function getEmployeeTimesheetsForDateRange($employeeId, $startDate, $endDate) {

		return $this->find('all', array(
			'fields' => 'Timesheet.id, Timesheet.date, EmployeesTimesheet.*, Project.name',
			'joins' => array(
				array('table' => 'employees_timesheets',
					'alias' => 'EmployeesTimesheet',
					'type' => 'INNER',
					'conditions' => array(
						'EmployeesTimesheet.timesheet_id = Timesheet.id',
						'EmployeesTimesheet.employee_id = ' . $employeeId,
					)
				),
				array('table' => 'projects',
					'alias' => 'Project',
					'type' => 'INNER',
					'conditions' => array(
						'Project.id = Timesheet.project_id',
					)
				)
			),
			'conditions' => array(
				'Timesheet.date >=' => $startDate->format('Y-m-d'),
				'Timesheet.date <=' => $endDate->format('Y-m-d'),
			),
			'order' => 'Timesheet.date ASC, Project.name'
		));
	}

	function getTotals($id){
        $options = array();
        $options['conditions'] = array(
            'Timesheet.project_id' => $id
        );
        $options['contain'] = array(
            'Project',
            'EmployeesTimesheet',
        );

        $timesheets = $this->find('all',$options);

        $total = 0;
        $qty = 0;

        foreach($timesheets as $timesheet){
            $total += $timesheet['Timesheet']['total'];
            $qty += count($timesheet['EmployeesTimesheet']);
        }


        return array('qty'=>$qty,'total'=>$total);
    }

	/**
	 * @param array $timesheetEntries
	 * @return array|null
	 */
	static function populateEmployeeCosts($timesheetEntries)
	{
		foreach ($timesheetEntries as $idx => $timesheetEntry) {
			$timesheetEntries[$idx] = self::populateEmployeeCost($timesheetEntry);
		}

		return $timesheetEntries;
	}

	/**
	 * @param array $timesheetEntry
	 * @return array|null
	 */
	static function populateEmployeeCost($timesheetEntry)
	{
		$model = null;
		if (isset($timesheetEntry['EmployeesTimesheet'])) {
			$model = 'EmployeesTimesheet';
			$timesheetEntry = $timesheetEntry['EmployeesTimesheet'];
		}

		$hoursWorked = 0;
		$breakHours = 0;
		$cost = 0;

		if (!empty($timesheetEntry['rate_ph']) && !empty($timesheetEntry['rate_ph']) && !empty($timesheetEntry['rate_ph'])) {
			$hoursWorked = self::getMinutesBetweenTimes($timesheetEntry['start_time'], $timesheetEntry['end_time']) / 60;
			$breakHours = !empty($timesheetEntry['break_hours']) && is_numeric($timesheetEntry['break_hours']) ? $timesheetEntry['break_hours'] : 0;
			$hoursWorked = $hoursWorked - $breakHours;
			$cost = $hoursWorked * $timesheetEntry['rate_ph'];
		}

		$timesheetEntry['hours_worked'] = $hoursWorked;
		$timesheetEntry['break_hours'] = $breakHours;
		$timesheetEntry['cost'] = $cost;

		return $model ? array($model => $timesheetEntry) : $timesheetEntry;
	}


	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($val['Timesheet'])) {
				if (!empty($val['Timesheet']['date']) && $val['Timesheet']['date'] != '0000-00-00') {
					$results[$key]['Timesheet']['date'] = date('d-m-Y', strtotime($val['Timesheet']['date']));
				} else {
					$results[$key]['Timesheet']['date'] = '';
				}
			}
		}

		return $results;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['Timesheet']['date'])) {
			$this->data['Timesheet']['date'] = date('Y-m-d',strtotime($this->data['Timesheet']['date']));
		}

		// if EmployeesTimesheet entries are present, populate their individual hours worked and cost fields,
		// and then populate the total cost in the Timesheet itself
		if (!empty($this->data['EmployeesTimesheet'])) {
			$this->data['EmployeesTimesheet'] = self::populateEmployeeCosts($this->data['EmployeesTimesheet']);
			$this->data['Timesheet']['total'] = self::sumNestedValuesInArray($this->data['EmployeesTimesheet'], 'cost', 'EmployeesTimesheet');

			// EmployeesTimesheet entries are present - if this is existing timesheet then delete its timesheet entries
			// as they will be re-saved
			if (!empty($this->data['Timesheet']['id'])) {
				$this->EmployeesTimesheet->deleteAll(array('EmployeesTimesheet.timesheet_id' => $this->data['Timesheet']['id']));
			}
		}

		return true;
	}


}