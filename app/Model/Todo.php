<?php
class Todo extends AppModel {
	
	var $name = 'Todo';
	var $displayField = 'Todo.description';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'Todo.completed_date IS NULL DESC, Todo.priority ASC, Todo.due_date IS NOT NULL DESC, Todo.due_date ASC';


	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		),
		'CreatedByUser' => array(
			'className' => 'User',
			'foreignKey' => 'created_by_user_id'
		),
		'CompletedByUser' => array(
			'className' => 'User',
			'foreignKey' => 'completed_by_user_id'
		),
		'InProgressByUser' => array(
			'className' => 'User',
			'foreignKey' => 'in_progress_by_user_id'
		),
	);

	var $validate = array(
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please provide a description',
			),
		),
	);


	const STATUS_COMPLETED = 'C';
	const STATUS_IN_PROGRESS = 'I';
	const STATUS_PENDING = 'P';

	static function getStatuses()
	{
		return array(
			'P' => 'Pending',
			'I' => 'In Progress',
			'C' => 'Completed'
		);
	}

	const PRIORITY_HIGH = 1;
	const PRIORITY_MEDIUM = 2;
	const PRIORITY_NORMAL = 3;

	static function getPriorities()
	{
		return array(
			3 => 'Normal',
			//2 => 'Medium',
			1 => 'High',
		);
	}

	function getTodo($id)
	{
		$options = array();
		$options['conditions'] = array(
			'Todo.id' => $id
		);
		$options['contain'] = array(
			'User',
			'CreatedByUser',
			'CompletedByUser',
			'InProgressByUser',
		);

		return $this->find('first', $options);
	}

	/**
	 * @param bool $userId  Restrict to this user id (pass in true to get all assigned tasks, pass in false to get all unassigned tasks, null returns all tasks)
	 * @param bool $archived  If true just return archived, if false return not archived, if null return all
	 * @return array|null
	 */
	function getTodos($userId = null, $archived = null, $completed = null)
	{
		$options = array(
			'conditions' => array(),
			'contain' => array(
				'User',
				'CreatedByUser',
				'CompletedByUser',
				'InProgressByUser',
			)
		);

		if ($userId === true) {
			$options['conditions'][] = 'Todo.user_id IS NOT NULL';
		} else if ($userId === false) {
			$options['conditions'][] = 'Todo.user_id IS NULL';
		} else if (is_numeric($userId)) {
			$options['conditions'][] = 'Todo.user_id = ' . $userId;
		}

		if ($archived === false) {
			$options['conditions'][] = 'Todo.archived = 0';
		} else if ($archived === true) {
			$options['conditions'][] = 'Todo.archived = 1';
		}

		if ($completed === true) {
			$options['conditions'][] = 'Todo.completed_date IS NOT NULL';
		} else if ($completed === false) {
			$options['conditions'][] = 'Todo.completed_date IS NULL';
		}

		return $this->find('all', $options);
	}


	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (!empty($val['Todo']['due_date'])) {
				$results[$key]['Todo']['due_date_Y-m-d'] = $val['Todo']['due_date'];
				$results[$key]['Todo']['due_date'] = date('d-m-Y',strtotime($val['Todo']['due_date']));
			}
		}
		return $results;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['Todo']['due_date'])){
			$this->data['Todo']['due_date'] = date('Y-m-d',strtotime($this->data['Todo']['due_date']));
		}

		return true;
	}
}