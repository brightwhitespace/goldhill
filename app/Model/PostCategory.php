<?php
class PostCategory extends AppModel {
	
	var $name = 'PostCategory';
	var $recursive = -1;
	var $displayField = 'name';
	var $actsAs = array('Containable');
	var $order = 'PostCategory.order_num ASC';
	
	var $validate = array(
		'name' => array('rule'=>'notBlank','message'=>'Please enter a category name'),
		'url'=> array(
				'rule1' => array(
						'rule'=>'notBlank',
						'message' => 'Please enter a category URL'
				),
				'rule2' => array(
						'rule'=>'isUnique',
						'message' => 'Please enter a unique category URL',
						'on' => 'create'
				)
		)
	);
	
	var $hasAndBelongsToMany = array('Post');

	function getDisplayedPostCategories($findType = 'all')
	{
		return $this->find($findType, array(
			'conditions' => array('PostCategory.displayed' => 1)
		));
	}

	function getPostCategoriesAsPages($activeCategoryId = null)
	{
		$cats = $this->find('all', array(
			'conditions' => array('PostCategory.displayed' => 1),
			'fields' => 'PostCategory.id, PostCategory.name, PostCategory.url'
		));

		$categoriesForPageMenu = array();
		foreach ($cats as $cat) {
			$simulatedPage = array('Page' => array(
				'id' => $cat['PostCategory']['id'],
				'page_title' => $cat['PostCategory']['name'],
				'menu_title' => $cat['PostCategory']['name'],
				'page_url' => 'news/' . $cat['PostCategory']['url'],
				'parent_id' => 0,
				'active' => $cat['PostCategory']['id'] == $activeCategoryId, // show as active in menu (i.e. selected)
			));
			$categoriesForPageMenu[] = $simulatedPage;
		}

		return $categoriesForPageMenu;
	}
}
