<?php
App::uses('Sanitize', 'Utility');


class AccessGroup extends AppModel
{
	var $name = 'AccessGroup';
	var $recursive = -1;
	var $actsAs = array('Acl');
	
	/*
	var $validate = array(
		"group_name" => VALID_NOT_EMPTY
	);
	*/
	
	var $hasMany = 'User';

	const ADMIN = 29;
	const EMPLOYEE = 33;
	const OFFICE = 34;
	const OFFICE_ADMIN = 35;
	const CLIENT = 37;


	function parentNode(){
		if (!$this->id) {
			return null;
		}

		$data = $this->read();

		if (!$data['AccessGroup']['parent_id']){
	  		return null;
		} else {
	  		return array('model' => 'AccessGroup', 'foreign_key' => $data['AccessGroup']['id']);
		}
	}
	
	function getGroupList($exclude = null, $orderBy = null){
		
		$conditions = array();
		
		if($exclude){
			$conditions = array(
				'NOT' => array(
						'AccessGroup.group_name' => $exclude
				)
			);
		}
		$this->recursive = -1;
		
		return $this->find('list',array('conditions'=>$conditions,'fields'=>'AccessGroup.group_name','order'=>$orderBy));
	}
	
	function afterSave($created) {

		// Do this if the save operation was an insertion/record creation
		// and not an update operation
  		if($created) {
  			// Ah, yes... we'll be needing the Sanitize component
  			$sanitize = new Sanitize();	

			// Get the id of the inserted record
			$id = $this->getLastInsertID();

			// Instantiate an ARO model that will be used for updating
			// the ARO
			$aro = new Aro();

			// I'm using updateAll() instead of saveField()
			// Instead of querying the table to get the id of the
			// ARO node that corresponds to the user, I just provided
			// two field conditions whose combination uniquely identifies
			// the node (Model=> User, Foreign Key=> User id).

			// I don't know why it wasn't sanitizing my input and not
			// enclosing the input in quotes. I had to do it myself
			
			$aro->create();
			$aroData['Aro']['model'] = 'AccessGroup';
			$aroData['Aro']['foreign_key'] = $id;
			$aroData['Aro']['alias'] = $this->data['AccessGroup']['group_name'];
			
			$aro->save($aroData);
			
			/*$aro->updateAll(
			array('alias'=>'\''.$sanitize->escape($this->data['AccessGroup']['group_name']).'\''),
				array('Aro.model'=>'AccessGroup', 'Aro.foreign_key'=>$id)
			);*/
			
			//debug($this->data);
		}
		return true;
	}

}

?>