<?php
class PurchaseOrder extends AppModel {
	
	var $name = 'PurchaseOrder';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $belongsTo = array(
		'Contact',
		'User',
		'Employee',
		'Project' => array(
			'dependent' => true
		),
		'Supplier',
		'Type',
		'ConfirmedByUser' => array(
			'className' => 'User',
			'foreignKey' => 'confirmed_by_user_id'
		),
	);	
	
	var $hasMany = array(
		'PurchaseOrderInvoice'
	);
	
	var $validate = array(
		'project_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select a project'
			),
		),
		'supplier_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select a supplier'
			),
		),
		'user_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select a PO contact'
			),
		),
		'employee_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select a site contact'
			),
		),
		'quantity' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a quantity'
			),
		),
		'rate' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the rate'
			),
		),
		'start_date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the start date'
			),
		),
		'end_date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the end date'
			),
		)
	);
	
	
	function getPurchaseOrder($id){
		$options = array();
		$options['conditions'] = array(
			'PurchaseOrder.id' => $id
		);
		$options['contain'] = array(
			'Contact',
			'Project',
			'Supplier',
			'PurchaseOrderInvoice',
			'Type',
			'User',
			'Employee',
			'ConfirmedByUser',
		);
		
		return $this->find('first',$options);
	}
	
	function getLatest($projectId = null,$supplierId = null){
		$options = array();
		if($projectId){
			$options['conditions'] = array(
				'PurchaseOrder.project_id' => $projectId
			);
		}
		if($supplierId){
			$options['conditions'] = array(
				'PurchaseOrder.supplier_id' => $supplierId
			);
		}
		$options['contain'] = array(
			'Contact',
			'Project',
			'Supplier',
			'PurchaseOrderInvoice',
			'Type'
		);
		$options['order'] = array('PurchaseOrder.created' => 'DESC');
		$options['limit'] = 5;
		
		return $this->find('all',$options);
	}
	
	function getHireItems($projectId = null){
		$options = array();
		$options['conditions'] = array(
			'PurchaseOrder.returned' => '0',
			'PurchaseOrder.type_id' => 1,
			//'PurchaseOrder.end_date <' => date('Y-m-d',strtotime('NOW + 2 days'))
		);
		if($projectId){
			$options['conditions']['PurchaseOrder.project_id'] = $projectId;
		}
		$options['contain'] = array(
			'Contact',
			'Project',
			'Supplier'
		);
		$options['order'] = array(
			'PurchaseOrder.end_date' => 'ASC'
		);
		$options['limit'] = 10;
		
		return $this->find('all',$options);
	}
	
	function getTotals($projectId,$type = null){
        $options = array();

        $options['conditions'] = array(
            'PurchaseOrder.project_id' => $projectId
        );

        if($type){
            $options['conditions']['Type.name'] = $type;

        }

        $options['contain'] = array('Type');

        $pos = $this->find('all',$options);

        $qty = count($pos);
        $total = 0;

        foreach($pos as $po){
            $total += $po['PurchaseOrder']['value'];
        }

        return array('qty'=>$qty,'total'=>$total);

    }
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if(isset($val['PurchaseOrder'])){			
				if($val['PurchaseOrder']['start_date'] != '0000-00-00'){
					$results[$key]['PurchaseOrder']['start_date'] = date('d-m-Y',strtotime($val['PurchaseOrder']['start_date']));								
				}else{
					$results[$key]['PurchaseOrder']['start_date'] = '';
				}
				if($val['PurchaseOrder']['end_date'] != '0000-00-00'){
					$results[$key]['PurchaseOrder']['end_date'] = date('d-m-Y',strtotime($val['PurchaseOrder']['end_date']));								
				}else{
					$results[$key]['PurchaseOrder']['end_date'] = '';
				}
				
				if(isset($val['PurchaseOrderInvoice'])){
					$invoiced = 0;
					foreach($val['PurchaseOrderInvoice'] as $invoice){
						$invoiced += $invoice['value'];
					}
					$results[$key]['PurchaseOrder']['invoiced'] = $invoiced;
				}else{
					$results[$key]['PurchaseOrder']['invoiced'] = 0;
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['PurchaseOrder']['start_date'])){
			$this->data['PurchaseOrder']['start_date'] = date('Y-m-d',strtotime($this->data['PurchaseOrder']['start_date']));
		}
		
		if (!empty($this->data['PurchaseOrder']['end_date'])){
			$this->data['PurchaseOrder']['end_date'] = date('Y-m-d',strtotime($this->data['PurchaseOrder']['end_date']));
		}
		
		return true;
	}

	
}