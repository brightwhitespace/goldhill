<?php
class CertificatesEmployee extends AppModel {
	
	var $name = 'CertificatesEmployee';
	var $recursive = -1;
	var $actsAs = array('Containable');

	var $belongsTo = array(
		'Certificate',
		'Employee',
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'created_by_user_id'
		),
	);

	var $validate = array(
		'employee_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select an employee'
			),
		),
		'certificate_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select a certificate'
			),
		),
	);


	public function beforeSave($options = array())
	{
		return parent::beforeSave($options);
	}
}