<?php
class Employee extends AppModel {
	
	var $name = 'Employee';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'Employee.name ASC';


	var $belongsTo = array(
		'EmployeePaymentMethod'
	);


	var $hasMany = array(
		'EmployeesTimesheet',
		'CertificatesEmployee',
		'WageAdjustment' => array('order' => 'WageAdjustment.date DESC'),
		'Tool',
		'User',
	);

	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter employee\'s name'
			),
		),
		'rate_ph' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter rate per hour'
			),
		),
	);

	function get($id)
	{
		$options = array();
		$options['conditions'] = array(
			'Employee.id' => $id
		);
		$options['contain'] = array(
			'EmployeesTimesheet',
			'CertificatesEmployee' => array('Certificate', 'User'),
			'WageAdjustment',
			'Tool' => array('User'),
			'EmployeePaymentMethod'
		);
		
		return $this->find('first',$options);
	}

	function getEmployees($findType = 'all', $includeArchived = false, $contain = null)
	{
		$options = array();
		$options['order'] = array(
			'Employee.name'
		);

		if (!$includeArchived) {
			$options['conditions'] = array(
				'Employee.archived = 0'
			);
		}

		if ($contain) {
			$options['contain'] = $contain;
		}

		return $this->find($findType, $options);
	}

	function getList()
	{
		$options = array();
		$options['order'] = array(
			'Employee.name'
		);
		$options['fields'] = array(
			'Employee.id','Employee.name'
		);

		return $this->find('list',$options);
	}
}