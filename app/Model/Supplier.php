<?php
class Supplier extends AppModel {
	
	var $name = 'Supplier';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'Supplier.name ASC';
	
	var $hasMany = array(
		'Contact',
		'PurchaseOrder',
		'AgencyEmployee' => array(
			'order' => 'AgencyEmployee.name ASC'
		)
	);
	
	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a company name'
			),
		),
		'tel' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a telephone number'
			),
		),
		'email' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter an email address'
			),
		),
	);

	function getSupplier($id){
		$options = array();
		$options['conditions'] = array(
			'Supplier.id' => $id
		);
		$options['contain'] = array(
			'Contact',
			'AgencyEmployee'
		);

		return $this->find('first',$options);
	}

	function getSuppliers($findType = 'all', $isAgency = null)
	{
		$options = array();

		if ($isAgency !== null) {
			$options['conditions'] = array(
				'Supplier.is_agency' => $isAgency
			);
		}

		$options['contain'] = array(
			'Contact',
			'AgencyEmployee'
		);

		return $this->find($findType, $options);
	}


}