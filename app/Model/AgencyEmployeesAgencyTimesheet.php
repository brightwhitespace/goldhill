<?php
class AgencyEmployeesAgencyTimesheet extends AppModel {
	
	var $name = 'AgencyEmployeesAgencyTimesheet';
	var $recursive = -1;
	var $actsAs = array('Containable');

	var $belongsTo = array(
		'AgencyEmployee',
		'AgencyTimesheet',
	);

	var $validate = array(
		'supplier_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select an agency'
			),
		),
		'agency_employee_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select an employee'
			),
		),
		'start_time' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a start time'
			),
		),
		'end_time' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter an end time'
			),
		),
		'rate_ph' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the rate per hour'
			),
		),
	);


	public function beforeValidate($options = array())
	{
		$this->data = $this->AgencyTimesheet->populateEmployeeCost($this->data);

		// If not admin user then hours worked must be greater than minimum value
		if (!Configure::read('userIsAdmin') && $this->data['AgencyEmployeesAgencyTimesheet']['hours_worked'] < Configure::read('minAgencyHoursWorked')) {
			$this->invalidate('end_time', 'Min ' . Configure::read('minAgencyHoursWorked') . ' hours to be worked');
			return false;
		}

		return true;
	}

	public function beforeSave($options = array())
	{
		return parent::beforeSave($options);
	}
}