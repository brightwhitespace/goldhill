<?php
class ContentBlock extends AppModel {

	var $name = 'ContentBlock';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $hasMany = array(
		'Version' => array(
			'className' => 'ContentBlock',
			'order' => 'Version.modified',
			'foreignKey' => 'parent_id',
			'conditions' => 'Version.published = 0'
		)
	);
	
	var $belongsTo = array('Page');
}
?>