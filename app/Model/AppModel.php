<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * This is a placeholder class.
 * Create the same file in app/Model/AppModel.php
 * Add your application-wide methods to the class, your models will inherit them.
 *
 * @package       Cake.Model
 */
class AppModel extends Model {


    static function sumNestedValuesInArray($array, $key, $model = null)
    {
        $total = 0;
        foreach ($array as $item) {
            $total += $model ? $item[$model][$key] : $item[$key];
        }

        return $total;
    }

    static function getListOfTimes($startTime, $endTime, $intervalInMinutes, $separator = ':')
    {
        $times = array();
        $startMinutes = self::getMinutesFromTime($startTime, $separator);
        $endMinutes = self::getMinutesFromTime($endTime, $separator);

        for ($minutes = $startMinutes; $minutes <= $endMinutes; $minutes += $intervalInMinutes) {
            $time = self::getTimeFromMinutes($minutes, $separator);
            $times[$time] = $time;
        }

        return $times;
    }

    static function getTimeFromMinutes($minutes, $separator = ':')
    {
        $hour = sprintf('%02d', floor($minutes / 60));
        $minute = sprintf('%02d', floor($minutes % 60));

        return $hour . $separator . $minute;
    }

    static function getMinutesFromTime($time, $separator = ':')
    {
        $timeParts = explode($separator, $time);

        return ($timeParts[0] * 60) + $timeParts[1];
    }

    static function getMinutesBetweenTimes($startTime, $endTime, $separator = ':')
    {
        return self::getMinutesFromTime($endTime, $separator) - self::getMinutesFromTime($startTime, $separator);
    }

    function beforeSave($options = array())
    {
        App::import('component', 'CakeSession');
        $thisUserID = CakeSession::read('Auth.User.id');

        // Add the created & modified by user id into the data so it can be saved automatically (providing the table fields exist)
        if (!$this->id && empty($this->data[$this->name][$this->primaryKey])) {
            // Insert
            $this->data[$this->name]['created_by_user_id'] = $thisUserID;
        }

        // Insert & Update
        $this->data[$this->name]['modified_by_user_id'] = $thisUserID;

        return parent::beforeSave($options);
    }
}
