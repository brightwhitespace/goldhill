<?php
class Storage extends AppModel {

	var $name = 'Storage';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $useTable = 'storage';
	
	
	
	function getNumber($type = ''){
		$options = array();
		
		//default to LTD
		if(empty($type)){
			$type = 'PO';
		}
		
		$options['conditions'] = array(
			'Storage.name' => $type.'_number'
		);
		
		$number = $this->find('first',$options);
		
		$currentNumber = intval($number['Storage']['value']);
		$currentNumber = str_pad($currentNumber,4,'0',STR_PAD_LEFT);		
		
		
		return $currentNumber;
		
		
		
	}
	
	function updateNumber($type = ''){
		$options = array();
		
		//default to LTD
		if(empty($type)){
			$type = 'PO';
		}
		
		$options['conditions'] = array(
			'Storage.name' => $type.'_number'
		);
		
		$number = $this->find('first',$options);
		
		$currentNumber = intval($number['Storage']['value']);
		$currentNumber = str_pad($currentNumber,4,'0',STR_PAD_LEFT);		
		
		$nextNumber = $currentNumber + 1;
		
		$this->id = $number['Storage']['id'];
		$this->saveField('value',$nextNumber);
		
		
	}

}
?>