<?php
class Form extends AppModel {
	var $name = 'Form';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $validate = array(
		'name' => array(			
			'rule' => array('notBlank'),
			'message' => 'Please enter a name for this form'			
		),
		'email' => array(			
			'rule' => array('notBlank'),
			'message' => 'Please enter an email for this form'			
		),
		'response' => array(			
			'rule' => array('notBlank'),
			'message' => 'Please enter a response message for this form'			
		)
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array('Page','Site');
	var $hasMany = array(
		'FormField' => array(
			'className' => 'FormField',
			'foreignKey' => 'form_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'FormField.order_num ASC',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'FormResult' => array(
			'className' => 'FormResult',
			'foreignKey' => 'form_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	function getForm($id,$getResults = false){
		
		$conditions = array(
			'Form.id' => $id
		);
		
		if($getResults){
			$contain = array(
				'FormField',
				'FormResult'
			);
		}else{
			$contain = array(
				'FormField'
			);
		}
		
		return $this->find('first',array('conditions'=>$conditions,'contain'=>$contain));
		
	}

}
?>