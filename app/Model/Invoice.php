<?php
class Invoice extends AppModel {
	
	var $name = 'Invoice';
	var $displayField = 'invoice_num';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'invoice_num desc';

	var $belongsTo = array(
		'Client',
		'Project',
		'InvoiceStatus'
	);

	var $hasMany = array(
		'InvoiceItem' => array('dependent' => true)
	);

	const STATUS_DRAFT = 1;
	const STATUS_APPROVED = 2;
	const STATUS_SENT = 3;

	public static function getStatuses() {
		return array(
			self::STATUS_DRAFT => 'Draft',
			self::STATUS_APPROVED => 'Approved',
			self::STATUS_SENT => 'Sent',
		);
	}

	public static function allowedToEdit($invoice, $userIsAdmin, $userId)
	{
		$idsOfUsersWhoCanEditSentInvoice = array(
			67, // Ian Barnett
		);

		$invoice = !empty($invoice['Invoice']) ? $invoice : array('Invoice' => $invoice);

		return $invoice['Invoice']['invoice_status_id'] == self::STATUS_DRAFT
				|| ($invoice['Invoice']['invoice_status_id'] == self::STATUS_APPROVED && $userIsAdmin)
				|| ($invoice['Invoice']['invoice_status_id'] == self::STATUS_SENT && in_array($userId, $idsOfUsersWhoCanEditSentInvoice));
	}

	var $validate = array(
		'client_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a client'
			),
		),
		'invoice_date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter an invoice date'
			),
		),
		'ref' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a reference'
			),
		),
	);
	
	function getInvoice($id){
		$options = array();
		$options['conditions'] = array(
			'Invoice.id' => $id
		);
		$options['contain'] = array(
			'Client' => 'Contact',
			'Project' => 'Contact',
			'InvoiceItem',
			'InvoiceStatus'
		);
		
		return $this->find('first',$options);
	}
	
	function getAll($client_id){
		$options = array();
		$options['conditions'] = array(
			'Invoice.client_id' => $client_id
		);
		
		$options['order'] = array(
			'Invoice.date' => 'desc'
		);
		
		return $this->find('all',$options);
	}
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if(isset($val['Invoice'])){
				if($val['Invoice']['invoice_date'] != '0000-00-00'){
					$results[$key]['Invoice']['invoice_date'] = date('d-m-Y',strtotime($val['Invoice']['invoice_date']));								
				}else{
					$results[$key]['Invoice']['invoice_date'] = '';
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Invoice']['invoice_date'])){
			$this->data['Invoice']['invoice_date'] = date('Y-m-d',strtotime($this->data['Invoice']['invoice_date']));
		}
		
		return true;
	}

	
}