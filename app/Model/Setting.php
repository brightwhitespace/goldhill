<?php
class Setting extends AppModel {
	
	var $name = 'Setting';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	
	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a name as a reference for the setting',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'value' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the value for the setting',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	

	function getSettings()
	{
		$settings = array();

		if ($site) {
			$settings = $this->find('list', array(
				'fields' => array('Setting.name', 'Setting.value')
			));
		}

		return $settings;
	}
}