<?php
class AgencyEmployee extends AppModel {
	
	var $name = 'AgencyEmployee';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'AgencyEmployee.name ASC';

	public $virtualFields = array(
		'name_pipe_supplier_id_pipe_rate_ph' => 'CONCAT(AgencyEmployee.name, "|", AgencyEmployee.supplier_id, "|", AgencyEmployee.rate_ph)'
	);

	var $belongsTo = array(
		'Supplier',
	);

	var $hasMany = array(
		'AgencyEmployeesAgencyTimesheet',
	);

	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter employee\'s name'
			),
		),
		'rate_ph' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter rate per hour'
			),
		),
	);

	function get($id) {
		$options = array();
		$options['conditions'] = array(
			'AgencyEmployee.id' => $id
		);
		$options['contain'] = array(
			'Supplier',
			'AgencyEmployeesAgencyTimesheet'
		);

		return $this->find('first', $options);
	}

	function getOrCreateEmployee($supplierId, $name, $ratePh = 10.0) {
		$options = array();
		$options['conditions'] = array(
			'AgencyEmployee.supplier_id' => $supplierId,
			'AgencyEmployee.name' => $name,
			'AgencyEmployee.archived' => 0,
		);
		$employee = $this->find('first', $options);

		if ($employee) {
			return $employee;

		} else if ($supplierId && $name) {
			$this->save(array(
				'supplier_id' => $supplierId,
				'name' => $name,
				'rate_ph' => $ratePh
			));
		}

		return !empty($this->id) ? $this->get($this->id) : null;
	}

	function getAll($supplierId = null, $includeArchived = true, $findType = 'all') {
		$options = array('conditions' => array());

		if ($supplierId) {
			$options['conditions']['AgencyEmployee.supplier_id'] = $supplierId;
		}

		if (!$includeArchived) {
			$options['conditions']['AgencyEmployee.archived'] = 0;
		}

		$options['order'] = array(
			'AgencyEmployee.name'
		);

		return $this->find($findType, $options);
	}
}