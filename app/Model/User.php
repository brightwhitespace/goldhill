<?php

class User extends AppModel {

	var $name = 'User';
	var $recursive = -1;
	var $actsAs = array('Containable');
	public $virtualFields = array(
		'name' => 'CONCAT(User.firstname, " ", User.lastname)'
	);

	//associations
	var $belongsTo = array('AccessGroup', 'Employee', 'Client');


	//validation
	var $validate = array(
		'username' => array(
			'email' => array(
				'rule' => 'email',
				'message' => 'Please enter a valid email address'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Email address has already been registered'
			)
		),
		'password_new' => array(
			'length' => array(
				'rule' => array('between',6,15),
				'message' => 'Password must be between 6 and 15 characters long.'
			),
			'identicalFieldValues' => array(
				'rule' => array('identicalFieldValues', 'password_confirm' ),
				'message' => 'Passwords do not match!'
			)
		),
		'firstname' => array(
			'rule'=>'notBlank',
			'message'=>'Please enter your first name'
		),
		'lastname' => array(
			'rule'=>'notBlank',
			'message'=>'Please enter your last name or initial'
		)
	);
	
	function getList($conditions = null){
		$options = array();

		// if no conditions array passed in, then default to just returning project managers
		if ($conditions === null) {
			$options['conditions'] = array(
				'User.project_manager' => 1
			);
		} else {
			$options['conditions'] = $conditions;
		}

		$options['order'] = array(
			'User.firstname','User.lastname'
		);
		$options['fields'] = array(
			'User.id','User.name'
		);
		
		return $this->find('list',$options);
	}

	
	function identicalFieldValues( $field=array(), $compare_field=null) {
        foreach( $field as $key => $value ) {
            $v1 = $value;
            $v2 = $this->data[$this->name][ $compare_field ];
            if($v1 !== $v2) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }
	
	
}
