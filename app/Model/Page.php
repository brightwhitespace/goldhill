<?php
class Page extends AppModel {
	
	var $name = 'Page';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'Page.order_num ASC';
	
	var $validate = array(
						  
		'page_title' => array(
				'rule'=>'notBlank',
				'message'=>'Please enter a page title'
		),
		
		'page_url'=> array(
				'rule1' => array(
						'rule'=>'notBlank',
						'message' => 'Please enter a page URL'
				),
			/* TODO - change to make sure page_url is unique for site
				'rule2' => array(
						'rule'=>'isUnique',
						'message' => 'Please enter a unique page URL',
						'on' => 'create'
				)
			*/
		)
		
	);
	
	var $hasMany = array(
		'Sub' => array(
			'className' => 'Page',
			'order' => 'Sub.order_num',
			'foreignKey' => 'parent_id',
			'conditions' => 'Sub.published = 1'
		),
		'Snippet',
		'ContentBlock' => array(
			'order' => 'ContentBlock.order_num',
			'conditions' => array('ContentBlock.published' => 1)
		),
		'Event'
	);
	
	var $hasOne = array(
		'Form',
	);
	
	var $belongsTo = array(
		'Parent' => array(
			'className' => 'Page',
			'foreignKey' => 'parent_id',
			'conditions' => 'Parent.published = 1'
		),
		'Menu' => array(
			'className' => 'Menu',
			'foreignKey' => 'menu_id'
		)
	);

	var $hasAndBelongsToMany = array(
		'Advert' => array(
			'className' => 'Advert',
			'joinTable' => 'adverts_pages',
			'foreignKey' => 'page_id',
			'associationForeignKey' => 'advert_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'order_num ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'RelatedPage' => array('className' => 'Page',
		   'joinTable' => 'pages_pages',
		   'foreignKey' => 'page_id',
		   'associationForeignKey' => 'parent_id'
		)
		
	);

	function getMenuPages($menu = 'Main', $multiLevel = true, $includePageBody = false, $includeHomePage = false, $site = null)
	{
		$this->recursive = -1;
		
		$fields = array('Page.id','Page.page_title','Page.menu_title','Page.page_url','Page.parent_id','Page.direct_url','Page.is_homepage','Page.hide_from_menu','Parent.page_url');
		if ($includePageBody) {
			$fields[] = 'Page.page_body';
		}

		$pageConditions = array('Page.published'=>1, 'Page.hide_from_menu'=>0, 'Menu.name'=>$menu);
		

		if (!$includeHomePage) {
			$pageConditions['Page.is_homepage'] = 0;
		}

		if ($multiLevel) {
			$pages = $this->find('threaded',
				array(
					'conditions'=>$pageConditions,
					'order'=>'Page.order_num ASC',
					'fields' => $fields,
					'contain'=> array('Menu','Parent')
				)
			);
		} else {
			$pageConditions['Page.parent_id'] = 0;
			$pages = $this->find('all',
				array(
					'conditions'=>$pageConditions,
					'order'=>'Page.order_num ASC',
					'fields' => $fields,
					'contain'=> array('Menu','Parent')
				)
			);
		}

		return $pages;
	}

	function getParentIdsOfActivePage($activePage)
	{
		if (!$activePage) {
			return array();
		}

		// get list of page to parent mappings for site
		$pageIdToParentIdMappings = $this->find('list', array(
			'conditions' => array('Page.site_id' => $activePage['Page']['site_id']),
			'fields' => array('Page.id', 'Page.parent_id')
		));

		// starting with active page, remember its parent id and use it to get the page above it.
		// Loop until the parent id is zero (i.e. reached top level page)
		$parentIds = array();
		$currentPageId = $activePage['Page']['id'];
		while (($parentId = (int)$pageIdToParentIdMappings[$currentPageId]) != 0) {
			$parentIds[] = $parentId;
			$currentPageId = $parentId;
		}

		// return array of ids in reverse order so top level parent is first
		return array_reverse($parentIds);
	}

	function getAdminPages($menuId = null)
	{
		$this->recursive = -1;

		$conditions = $menuId ? array('Page.menu_id'=>$menuId) : array();

	
		$pages = $this->find('threaded',
			array(
				'conditions'=>$conditions,
				'order'=>'Page.order_num ASC',
				'fields' => array('Page.id','Page.page_title','Page.parent_id','Page.published'),
				'contain'=> array('Menu')
			)
		);
		
		return $pages;
	}
	
	function getHomePage()
	{
		$conditions = array(
			'Page.is_homepage' => true,
			'Page.published' => 1
		);

		

		$contain = array(
			
			'Snippet',
			'Advert',
			'Form' => array('FormField'),
			'ContentBlock',
			'RelatedPage',
		);
		
		$page = $this->find('first',array('conditions'=>$conditions,'contain'=>$contain));

		return $page;
	}

	function getPage($slug = null, $childPages = false, $id = null, $directUrl = null)
	{
		if ($slug) {
			$conditions = array(
				'Page.page_url' => $slug,
				'Page.published' => 1
			);

			
		}

		if ($id) {
			$conditions = array(
				'Page.id' => $id,
				'Page.published' => 1
			);
		}

		if ($directUrl) {
			$conditions = array(
				'Page.direct_url' => $directUrl
			);
		}

		$contain = array(
			'Parent' => array('Parent'),
			
			'Snippet',
			'Advert',
			'Form' => array('FormField'),
			'ContentBlock',
			'RelatedPage' => array('Parent' => array('Parent')),
			'Resource'
		);

		if ($childPages) {
			$contain['Sub'] = array('Parent','ContentBlock');
		}

		$page = $this->find('first',array('conditions'=>$conditions,'contain'=>$contain));

		return $page;
	}

	function findParentPage($id){
		
		$page = $this->find('first',array('conditions'=>array('Page.id' => $id),'fields'=>array('Page.page_title','Page.parent_id','Page.id')));
		
		while(!empty($page['Page']['parent_id'])){			
			$page = $this->find('first',array('conditions'=>array('Page.id' => $page['Page']['parent_id']),'fields'=>array('Page.page_title','Page.parent_id','Page.id')));
		}
		
		return $page['Page']['id'];
	}
	
	function getParentPage($pid){
		
		$id = $this->findParentPage($pid);
		
		$conditions = array(
			'Page.id' => $id,
			'Page.published' => 1
		);
		
		$contain = array(
			'Parent' => array('Parent'),
			
			'Snippet',
			'Advert',
			'Form' => array('FormField'),
			'Sub' => array(
				'Parent',
				'Sub' => array(
					'Parent',
					'Sub'	
				)
			)		 
		);
		
		
		$page = $this->find('first',array('conditions'=>$conditions,'contain'=>$contain));

		return $page;
	}
	
	function getPageEdit($id){
		$conditions = array(
			'Page.id' => $id
		);
		
		$contain = array(
			'Parent' => array('Parent'),
			
			'Snippet',
			'Advert',
			'ContentBlock' => array('Version')	,
			'RelatedPage',
			'Resource'	 
		);
		
		$page = $this->find('first',array('conditions'=>$conditions,'contain'=>$contain));

		return $page;
	}

	function getHomePageLinks()
	{
		$this->recursive = -1;

		$pageConditions = array('Page.published'=>1,'Page.show_on_home' => 1);
		

		$pages = $this->find(
			'all',
			array(
				'conditions' => $pageConditions,
				'order' => 'Page.order_num ASC'
			)
		);

		return $pages;
	}
	
	function addHomePage($siteId){
		$data = array();
		$data['Page']['menu_id'] = 1;
		$data['Page']['site_id'] = $siteId;
		$data['Page']['page_title'] = 'Home';
		$data['Page']['page_url'] = 'home';
		$data['Page']['is_homepage'] = '1';
		$data['Page']['published'] = '1';
		$data['ContentBlock'][0]['content'] = 'Home';
		$data['ContentBlock'][0]['published'] = 1;
		
		$this->saveAll($data);
		
	}
	
	
}
