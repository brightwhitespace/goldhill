<?php
class Advert extends AppModel {
	var $name = 'Advert';
	var $displayField = 'name';
	var $recursive = -1;
	
	var $order = 'Advert.order_num ASC';
	
	
	var $validate = array(
		'name'=> array(
			'rule1' => array(
					'rule'=>'notBlank',
					'message' => 'Please enter a name for the advert'
			),
			'rule2' => array(
					'rule'=>'isUnique',
					'message' => 'Please enter a unique name for the advert',
					'on' => 'create'
			)
		)
	);

	var $belongsTo = array(
		'Site' => array(
			'className' => 'Site',
			'foreignKey' => 'site_id'
		),
	);

	var $hasAndBelongsToMany = array(
		'Page' => array(
			'className' => 'Page',
			'joinTable' => 'adverts_pages',
			'foreignKey' => 'advert_id',
			'associationForeignKey' => 'page_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	var $areas = array(
		'1' => 'Pages',
		'2' => 'Products',
		'3' => 'Product Categories',
		'4' => 'Projects',
		'5' => 'All'
	);
	
	
	// Possible priority values (ads are ordered by relative priority, order of ads with same priority is undefined)
	public static $priorityOptions = array( 1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, 10=>10, 11=>11, 12=>12, 13=>13, 14=>14, 15=>15, 16=>16, 17=>17, 18=>18, 19=>19, 20=>20 );

	// Positions on screen where ads may be displayed
	const POSITION_DEFAULT = 0;
	const POSITION_TOP = 1;
	const POSITION_BOTTOM = 2;
	const POSITION_LEFT = 3;
	const POSITION_RIGHT = 4;
	const POSITION_CUSTOM_1 = 11;
	const POSITION_CUSTOM_2 = 12;
	const POSITION_CUSTOM_3 = 13;
	const POSITION_CUSTOM_4 = 14;
	const POSITION_CUSTOM_5 = 15;
	
	/*
	 * Either returns a string corresponding to $value param,
	 * or if $value is null then returns array of all positions
	 * (http://www.dereuromark.de/2010/06/24/static-enums-or-semihardcoded-attributes/)
	 * 
	 * @param 	$value	integer constant (e.g. Advert::POSITION_TOP)
	 * @return  the name corresponding to $value OR array of all names if null
	 * @access	static
	 */
	static function positions($value = null) {
		$options = array(
			self::POSITION_DEFAULT => 'Default',
			self::POSITION_TOP => 'Top',
			self::POSITION_BOTTOM => 'Bottom',
			self::POSITION_LEFT => 'Left',
			self::POSITION_RIGHT => 'Right',
			self::POSITION_CUSTOM_1 => 'Custom1',
			self::POSITION_CUSTOM_2 => 'Custom2',
			self::POSITION_CUSTOM_3 => 'Custom3',
			self::POSITION_CUSTOM_4 => 'Custom4',
			self::POSITION_CUSTOM_5 => 'Custom5'
		);
		
		if ($value !== null) {
			if (array_key_exists($value, $options)) {
				return $options[$value];
			}
			return 'Default';
		}
		return $options;
	}
  
	/*
	 * Get adverts to be displayed for a page. Advert is only returned if:
	 *       published = true
	 *   AND start date <= today
	 *   AND (end date >= today OR end date is null)
	 *   AND (ad is associated with page OR (ad's "every page" value = true AND page does not override "every page" ads) )
	 *
	 * NOTE: If an ad's "every page" value is true then it is returned for all pages UNLESS the page's "override every page ads" value is true
	 *       (in this case only the ads specifically associated with the page are returned)
	 */
	
	function get($area = null){
		$options = array();
		
		if($area){
			$options['conditions'] = array(
				'Advert.area_id' => $area
			);
		}
		
		$options['order'] = array('Advert.order_num');
		
		return $this->find('all',$options);
		
	}
	
	function getAdverts($page, $position = null, $site = null) {
		$options = array();
		$options['fields'] = 'DISTINCT Advert.*';
		
		$conditions = array(
			'Advert.published'=>1
		);

		if ($position) {
			$conditions['Advert.position'] = $position;
		}
		if ($site) {
			$conditions['Advert.site_id'] = $site['Site']['id'];
		}

		if(!$page['Page']['override_every_page_ads']){
			// Not overriding "every page" ads so return all the "every page" ads as well as the 
			// adverts specifically added to this page as well
			// Need an outer join on adverts_pages to retrieve all combinations of adverts and pages...
			$joins = array(
						array(
						'table' => 'adverts_pages',
						'alias' => 'AdvertsPages',
						'type' => 'left outer',
						'foreignKey' => false,
						'conditions'=> array(
								'AdvertsPages.advert_id = Advert.id',
							)
						)
					);
			// ...then choose only those where advert is every page OR page id is current page
			// (the DISTINCT in the 'fields' entry above takes care of repeated adverts)
			// Note: using lower case 'or' so does not overwrite 'OR' from above
			$conditions['or'] = array(
					array('Advert.every_page'=>1),
					array('AdvertsPages.page_id'=>$page['Page']['id']),
				);
		}
		else {
			// just return adverts specifically added to this page
			// (do a normal join on adverts_pages and include only those whose page id is current page)
			$joins = array(
						array(
						'table' => 'adverts_pages',
						'alias' => 'AdvertsPages',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions'=> array(
								'AdvertsPages.advert_id = Advert.id',
								'AdvertsPages.page_id = '.$page['Page']['id']
							)
						)
					);
		}
		
		$options['joins'] = $joins;
		$options['conditions'] = $conditions;
		$options['order'] = 'Advert.order_num ASC';
		//debug($conditions);
		return $this->find('all',$options);
	}
	
	/*
	 * Get default adverts to be displayed on every page (i.e. adverts with "every_page" field set true)
	 */
	function getDefaultAdverts($site = null){
		$conditions = array('Advert.published'=>1,'Advert.every_page'=>1);

		if ($site) {
			$conditions['site_id'] = $site['Site']['id'];
		}

		return $this->find('all',array(
			'conditions'=>array('Advert.published'=>1,'Advert.every_page'=>1)
		));
	}

	function getHomePageSliderAdverts($site = null)
	{
		$this->recursive = -1;

		$advertConditions = array('Advert.published'=>1, 'Advert.position' => 'slider');
		if ($site) {
			$advertConditions['Advert.site_id'] = $site['Site']['id'];
		}

		$adverts = $this->find('all',
			array(
				'conditions' => $advertConditions,
				'order' => 'Advert.order_num ASC',
				'contain' => array('Site')
			)
		);

		return $adverts;
	}

}