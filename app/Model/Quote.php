<?php
class Quote extends AppModel {
	
	var $name = 'Quote';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'IFNULL(Quote.site_visit_date, IFNULL(Quote.quote_due, Quote.created)) ASC';


	var $belongsTo = array(
		'Client'
	);


	var $validate = array(
		'client_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select a client'
			),
		),
		'contact_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter contact name'
			),
		),
		'contact_tel' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter contact telephone'
			),
		),
	);

	function get($id)
	{
		$options = array();
		$options['conditions'] = array(
			'Quote.id' => $id
		);
		$options['contain'] = array(
			'Client',
		);
		
		return $this->find('first',$options);
	}

	function getQuotes($findType = 'all', $includeArchived = false, $contain = null)
	{
		$options = array();
		$options['order'] = array(
			'Quote.created DESC'
		);

		if (!$includeArchived) {
			$options['conditions'] = array(
				'Quote.archived = 0'
			);
		}

		if ($contain) {
			$options['contain'] = $contain;
		}

		return $this->find($findType, $options);
	}


	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($val['Quote'])) {
				if (!empty($val['Quote']['site_visit_date']) && $val['Quote']['site_visit_date'] != '0000-00-00') {
					$results[$key]['Quote']['site_visit_date'] = date('d-m-Y', strtotime($val['Quote']['site_visit_date']));
				} else {
					$results[$key]['Quote']['site_visit_date'] = '';
				}
				if (!empty($val['Quote']['quote_due']) && $val['Quote']['quote_due'] != '0000-00-00') {
					$results[$key]['Quote']['quote_due'] = date('d-m-Y', strtotime($val['Quote']['quote_due']));
				} else {
					$results[$key]['Quote']['quote_due'] = '';
				}
			}
		}

		return $results;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['Quote']['site_visit_date'])) {
			$this->data['Quote']['site_visit_date'] = date('Y-m-d',strtotime($this->data['Quote']['site_visit_date']));
		}
		if (!empty($this->data['Quote']['quote_due'])) {
			$this->data['Quote']['quote_due'] = date('Y-m-d',strtotime($this->data['Quote']['quote_due']));
		}

		return parent::beforeSave($options);
	}


}