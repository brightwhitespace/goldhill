<?php
class WageAdjustment extends AppModel {
	
	var $name = 'WageAdjustment';
	var $displayField = 'WageAdjustment.date';
	var $recursive = -1;
	var $order = 'WageAdjustment.date DESC';
	var $actsAs = array('Containable');


	var $belongsTo = array(
		'Employee',
	);

	var $validate = array(
		'employee_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the name of the employee',
			),
		),
		'date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the date of the adjustment',
			),
		),
		'value' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the value of the adjustment',
			),
		)
	);


	function get($id)
	{
		$options = array();
		$options['conditions'] = array(
			'WageAdjustment.id' => $id
		);
		$options['contain'] = array(
			'User',
			'ModifiedByUser',
		);

		return $this->find('first', $options);
	}


	function getAdjustments($employeeId = null, $findType= 'all')
	{
		$conditions = array();

		if ($employeeId) {
			$conditions['employee_id'] = $employeeId;
		}

		$settings = $this->find($findType, array(
			'conditions' => $conditions
		));

		return $settings;
	}

	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($val['WageAdjustment'])) {
				if ($val['WageAdjustment']['date'] != '0000-00-00') {
					$results[$key]['WageAdjustment']['date'] = date('d-m-Y', strtotime($val['WageAdjustment']['date']));
				} else {
					$results[$key]['WageAdjustment']['date'] = '';
				}
			}
		}

		return $results;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['WageAdjustment']['date'])){
			$this->data['WageAdjustment']['date'] = date('Y-m-d', strtotime($this->data['WageAdjustment']['date']));
		}

		return parent::beforeSave($options);
	}

}