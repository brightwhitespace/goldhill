<?php
class Contact extends AppModel {
	
	var $name = 'Contact';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	public $virtualFields = array(
		'name' => 'CONCAT(Contact.firstname, " ", Contact.lastname)'
	);
	
	var $belongsTo = array(
		'Client',
		'Supplier'
	);	
	
	var $validate = array(
		'firstname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a first name'
			),
		),
		'lastname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a last name'
			),
		),
		'email' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter an email address'
			),
		),
		'tel' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a contact number'
			),
		)
	);
	
	function getContact($id){
		$options = array();
		$options['conditions'] = array(
			'Contact.id' => $id
		);
		$options['contain'] = array(
			'Client'
		);
		
		return $this->find('first',$options);
	}

	
}