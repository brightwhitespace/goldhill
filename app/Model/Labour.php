<?php
class Labour extends AppModel {
	
	var $name = 'Labour';
	var $useTable = 'labour';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $belongsTo = array(
		'Project'
	);
	
	var $validate = array(
		'project_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a project'
			),
		),
		'date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a date'
			),
		),
		'quantity' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a quantity'
			),
		),
		'total' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the total'
			),
		)
	);
	
	function getLabour($id){
		$options = array();
		$options['conditions'] = array(
			'Labour.id' => $id
		);
		$options['contain'] = array(
			'Project'
		);
		
		return $this->find('first',$options);
	}
	
	function getAll($id){
		$options = array();
		$options['conditions'] = array(
			'Labour.project_id' => $id
		);
		$options['contain'] = array(
			'Project'
		);
		$options['order'] = array(
			'Labour.date' => 'desc'
		);
		
		return $this->find('all',$options);
	}
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if(isset($val['Labour'])){			
				if($val['Labour']['date'] != '0000-00-00'){
					$results[$key]['Labour']['date'] = date('d-m-Y',strtotime($val['Labour']['date']));								
				}else{
					$results[$key]['Labour']['date'] = '';
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Labour']['date'])){
			$this->data['Labour']['date'] = date('Y-m-d',strtotime($this->data['Labour']['date']));
		}
		
		return true;
	}

	
}