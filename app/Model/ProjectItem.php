<?php
class ProjectItem extends AppModel {
	
	var $name = 'ProjectItem';
	var $recursive = -1;
	var $actsAs = array('Containable');

	var $belongsTo = array(
		'Project'
	);

	var $validate = array(
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a description'
			),
		),
	);

	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if(isset($val['ProjectItem'])){
				if(!empty($val['ProjectItem']['date']) && $val['ProjectItem']['date'] != '0000-00-00'){
					$results[$key]['ProjectItem']['date'] = date('d-m-Y',strtotime($val['ProjectItem']['date']));
				}else{
					$results[$key]['ProjectItem']['date'] = '';
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['ProjectItem']['date'])) {
			$this->data['ProjectItem']['date'] = date('Y-m-d',strtotime($this->data['ProjectItem']['date']));
		}

		if (empty($this->data['ProjectItem']['cost'])) {
			$this->data['ProjectItem']['cost'] = null;
		}

		return true;
	}

	
}