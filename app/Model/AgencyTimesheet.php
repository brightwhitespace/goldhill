<?php
class AgencyTimesheet extends AppModel {

	var $name = 'AgencyTimesheet';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	var $order = 'AgencyTimesheet.date ASC';

	var $belongsTo = array(
		'Project'
	);

	var $hasMany = array(
		'AgencyEmployeesAgencyTimesheet',
	);

	var $validate = array(
		'date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a date'
			),
		),
	);

	function get($id){
		$options = array();
		$options['conditions'] = array(
			'AgencyTimesheet.id' => $id
		);
		$options['contain'] = array(
			'Project',
			'AgencyEmployeesAgencyTimesheet',
		);

		return $this->find('first',$options);
	}

	function getAll($id, $order = 'asc'){
		$options = array();
		$options['conditions'] = array(
			'AgencyTimesheet.project_id' => $id
		);
		$options['contain'] = array(
			'Project',
			'AgencyEmployeesAgencyTimesheet',
		);
		$options['order'] = array(
			'AgencyTimesheet.date' => $order
		);

		return $this->find('all',$options);
	}

	function getTotals($id){
        $options = array();
        $options['conditions'] = array(
            'AgencyTimesheet.project_id' => $id
        );
        $options['contain'] = array(
            'Project',
            'AgencyEmployeesAgencyTimesheet',
        );

        $timesheets = $this->find('all',$options);

        $total = 0;
        $qty = 0;

        foreach($timesheets as $timesheet){
            $total += $timesheet['AgencyTimesheet']['total'];
            $qty += count($timesheet['AgencyEmployeesAgencyTimesheet']);
        }


        return array('qty'=>$qty,'total'=>$total);
    }

	/**
	 * @param array $timesheetEntries
	 * @return array|null
	 */
	static function populateEmployeeCosts($timesheetEntries)
	{
		foreach ($timesheetEntries as $idx => $timesheetEntry) {
			$timesheetEntries[$idx] = self::populateEmployeeCost($timesheetEntry);
		}

		return $timesheetEntries;
	}

	/**
	 * @param array $timesheetEntry
	 * @return array|null
	 */
	static function populateEmployeeCost($timesheetEntry)
	{
		$model = null;
		if (isset($timesheetEntry['AgencyEmployeesAgencyTimesheet'])) {
			$model = 'AgencyEmployeesAgencyTimesheet';
			$timesheetEntry = $timesheetEntry['AgencyEmployeesAgencyTimesheet'];
		}

		$hoursWorked = 0;
		$breakHours = 0;
		$cost = 0;

		if (!empty($timesheetEntry['rate_ph']) && !empty($timesheetEntry['rate_ph']) && !empty($timesheetEntry['rate_ph'])) {
			$hoursWorked = self::getMinutesBetweenTimes($timesheetEntry['start_time'], $timesheetEntry['end_time']) / 60;
			$breakHours = !empty($timesheetEntry['break_hours']) && is_numeric($timesheetEntry['break_hours']) ? $timesheetEntry['break_hours'] : 0;
			$hoursWorked = $hoursWorked - $breakHours;
			$cost = $hoursWorked * $timesheetEntry['rate_ph'];
		}

		$timesheetEntry['hours_worked'] = $hoursWorked;
		$timesheetEntry['break_hours'] = $breakHours;
		$timesheetEntry['cost'] = $cost;

		return $model ? array($model => $timesheetEntry) : $timesheetEntry;
	}


	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($val['AgencyTimesheet'])) {
				if (!empty($val['AgencyTimesheet']['date']) && $val['AgencyTimesheet']['date'] != '0000-00-00') {
					$results[$key]['AgencyTimesheet']['date'] = date('d-m-Y', strtotime($val['AgencyTimesheet']['date']));
				} else {
					$results[$key]['AgencyTimesheet']['date'] = '';
				}
			}
		}

		return $results;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['AgencyTimesheet']['date'])) {
			$this->data['AgencyTimesheet']['date'] = date('Y-m-d',strtotime($this->data['AgencyTimesheet']['date']));
		}

		// if AgencyEmployeesAgencyTimesheet entries are present, populate the total cost in the AgencyTimesheet itself
		// (NOTE: individual hours worked and cost fields in all AgencyEmployeesAgencyTimesheet records will already
		//  have been populated by its beforeValidate() function)
		if (!empty($this->data['AgencyEmployeesAgencyTimesheet'])) {
			$this->data['AgencyTimesheet']['total'] = self::sumNestedValuesInArray($this->data['AgencyEmployeesAgencyTimesheet'], 'cost', 'AgencyEmployeesAgencyTimesheet');

			// AgencyEmployeesAgencyTimesheet entries are present - if this is existing timesheet then delete its timesheet entries
			// as they will be re-saved
			if (!empty($this->data['AgencyTimesheet']['id'])) {
				$this->AgencyEmployeesAgencyTimesheet->deleteAll(array('AgencyEmployeesAgencyTimesheet.agency_timesheet_id' => $this->data['AgencyTimesheet']['id']));
			}
		}

		return parent::beforeSave($options);
	}


}