<?php
class HistoryTool extends AppModel {
	
	var $name = 'HistoryTool';
	var $displayField = 'name';
	var $recursive = -1;
	var $order = 'HistoryTool.	id';
	var $actsAs = array('Containable');


	var $belongsTo = array(
		'Tool',
		'Employee',
		'Project',
		'Vehicle',
		'ToolStatus',
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'modified_by_user_id'
		),
	);

}