<?php
class Photo extends AppModel {

	var $name = 'Photo';
	var $recursive = -1;
	var $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Product' => array('className' => 'Product',
								'foreignKey' => 'product_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'Photo.order_num'
			),
			'Project' => array('className' => 'Project',
								'foreignKey' => 'project_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'Photo.order_num'
			),
			'Gallery' => array('className' => 'Gallery',
								'foreignKey' => 'gallery_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'Photo.order_num'
			),
			'ProjectCategory' => array('className' => 'ProjectCategory',
								'foreignKey' => 'project_category_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'Photo.order_num'
			)
	);

}
?>