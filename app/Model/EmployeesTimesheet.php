<?php
class EmployeesTimesheet extends AppModel {
	
	var $name = 'EmployeesTimesheet';
	var $recursive = -1;
	var $actsAs = array('Containable');

	var $belongsTo = array(
		'Employee',
		'Timesheet',
	);

	var $validate = array(
		'employee_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please select an employee'
			),
		),
		'start_time' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a start time'
			),
		),
		'end_time' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter an end time'
			),
		),
		'rate_ph' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the rate per hour'
			),
		)
	);


	public function beforeSave($options = array()) {

		$this->data = $this->Timesheet->populateEmployeeCost($this->data);

		return true;
	}
}