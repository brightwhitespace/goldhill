<?php
class Certificate extends AppModel {
	
	var $name = 'Certificate';
	var $displayField = 'name';
	var $recursive = -1;
	var $order = 'order_num';
	var $actsAs = array('Containable');


	var $hasMany = array(
		'CertificatesEmployee',
	);

	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the name of the certificate / course',
			),
		),
		'expiry_warning_days' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the expiry warning period',
			),
		)
	);

	

	function getCertificates($findType= 'all', $includeArchived = false)
	{
		$conditions = array();

		if (!$includeArchived) {
			$conditions['archived'] = 0;
		}

		$settings = $this->find($findType, array(
			'conditions' => $conditions
		));

		return $settings;
	}
}