<?php
class Tool extends AppModel {
	
	var $name = 'Tool';
	var $displayField = 'name';
	var $recursive = -1;
	var $order = 'serial_number';
	var $actsAs = array('Containable');


	var $belongsTo = array(
		'Employee',
		'Project',
		'Vehicle',
		'ToolStatus',
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'modified_by_user_id'
		),
	);

	var $hasMany = array(
		'HistoryTool',
	);


	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the tool name',
			),
		),
		'tool_status_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the tool location / assignment',
			),
		),
		'employee_id' => array(
			'checkStatus' => array(
				'rule' => array('conditionalOnStatus', 'tool_status_id', self::STATUS_INDIVIDUAL),
				'message' => 'Please select an employee',
			)
		),
		'vehicle_id' => array(
			'checkStatus' => array(
				'rule' => array('conditionalOnStatus', 'tool_status_id', self::STATUS_IN_VEHICLE),
				'message' => 'Please select a vehicle'
			)
		),
		'project_id' => array(
			'checkStatus' => array(
				'rule' => array('conditionalOnStatus', 'tool_status_id', self::STATUS_PROJECT),
				'message' => 'Please select a project'
			)
		)
	);

	const STATUS_UNASSIGNED = 1;
	const STATUS_INDIVIDUAL = 2;
	const STATUS_IN_VEHICLE = 3;
	const STATUS_PROJECT = 4;
	const STATUS_OFFICE = 5;
	const STATUS_REPAIR = 6;
	const STATUS_ARCHIVED = 7;
	const STATUS_OTHER = 8;

	static function getStatuses()
	{
		return array(
			self::STATUS_INDIVIDUAL => 'Individual',
			self::STATUS_IN_VEHICLE => 'In Vehicle',
			self::STATUS_PROJECT => 'Project (On Site)',
			self::STATUS_OFFICE => 'Office Unit',
			self::STATUS_REPAIR => 'Repair',
			self::STATUS_ARCHIVED => 'Archived / Scrapped',
			self::STATUS_OTHER => 'Other (see Notes)',
			self::STATUS_UNASSIGNED => 'Unassigned / Unknown',
		);
	}

	function get($id){
		$options = array();
		$options['conditions'] = array(
			'Tool.id' => $id
		);
		$options['contain'] = array(
			'Employee',
			'Project',
			'Vehicle',
			'ToolStatus',
			'User',
			'HistoryTool' => array('Employee','Project','Vehicle','ToolStatus','User')
		);

		return $this->find('first',$options);
	}

	function getTools($findType= 'all', $includeArchived = false)
	{
		$conditions = array();

		if (!$includeArchived) {
			$conditions['tool_status_id !='] = self::STATUS_ARCHIVED;
		}

		$settings = $this->find($findType, array(
			'conditions' => $conditions
		));

		return $settings;
	}

	function conditionalOnStatus($data, $statusField, $compareStatusId) {
		// $data array is passed using the form field name as the key so let's just get the field name to compare
		$value = array_values($data);
		$comparewithvalue = $value[0];

		// return okay if the dropdown has been selected OR it is not required (i.e. the selected status does not match the compare status)
		return $comparewithvalue || ($this->data[$this->name][$statusField] != $compareStatusId);
	}

	public function beforeSave($options = array())
	{
		// Make sure any old assignments are removed based on the current tool status
		if (!empty($this->data['Tool']['tool_status_id'])) {
			if ($this->data['Tool']['tool_status_id'] != Tool::STATUS_INDIVIDUAL) {
				$this->data['Tool']['employee_id'] = null;
			}
			if ($this->data['Tool']['tool_status_id'] != Tool::STATUS_PROJECT) {
				$this->data['Tool']['project_id'] = null;
			}
			if ($this->data['Tool']['tool_status_id'] != Tool::STATUS_IN_VEHICLE) {
				$this->data['Tool']['vehicle_id'] = null;
			}
		}

		return parent::beforeSave($options);
	}

	public function afterSave($created, $options = array())
	{
		$haveJustGeneratedSerialNumber = false;
		if (empty($this->data['Tool']['serial_number'])) {
			// put generated serial number in data so saves to history table
			$this->data['Tool']['serial_number'] = str_pad($this->id, 5, '0', STR_PAD_LEFT);
			$haveJustGeneratedSerialNumber = true;
		}

		if ($haveJustGeneratedSerialNumber) {
			// had to generate serial number, save all the data now and the history_tools record will be created in the afterSave that follows
			$this->save($this->data);
		} else {
			// have not had to generate serial number so create history_tools record as normal
			$historyToolData = array('HistoryTool' => $this->data['Tool']);
			$historyToolData['HistoryTool']['tool_id'] = $this->id;
			unset($historyToolData['HistoryTool']['id']);

			$this->HistoryTool->create();
			$this->HistoryTool->save($historyToolData);

		}

		parent::afterSave($created, $options);
	}
}