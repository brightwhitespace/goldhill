<?php
class Project extends AppModel {
	
	var $name = 'Project';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');

	var $virtualFields = array(
		'start_date_Ymd' => 'IF(Project.start_date = "0000-00-00", "", Project.start_date)',
		'end_date_Ymd' => 'IF(Project.end_date = "0000-00-00", "", Project.end_date)',
	);
	
	var $belongsTo = array(
		'Client',
		'Contact',
		'User',
		'ProjectType',
		'ProjectStatus',
	);

	var $hasOne = array(
		'ProjectMethodDoc'
	);

	var $hasMany = array(
		'PurchaseOrder' => array('dependent' => true),
		'Labour' => array('dependent' => true),
		'Timesheet' => array('dependent' => true),
		'AgencyTimesheet' => array('dependent' => true),
		'ProjectItem' => array('dependent' => true),
		'Invoice' => array('dependent' => true),
		'Tool',
		'AllTimesheetEntries',
	);
	
	var $validate = array(
		'client_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a client'
			),
		),
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a project name'
			),
		),
		'value' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a project value'
			),
		),
		'project_status_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a status'
			),
		),
	);

	const TYPE_STRIPOUT = 1;
	const TYPE_DAYWORKS = 2;

	const STATUS_PENDING = 1;
	const STATUS_LIVE = 2;
	const STATUS_COMPLETED = 3;
	const STATUS_HOLDING = 4;


	function getProject($id){
		$options = array();
		$options['conditions'] = array(
			'Project.id' => $id
		);
		$options['contain'] = array(
			'Contact',
			'Client',
			'PurchaseOrder',
			'Labour',
			'Timesheet',
			'AgencyTimesheet',
			'ProjectItem',
			'Invoice',
			'ProjectStatus',
			'User',
			'Tool',
			'ProjectMethodDoc',
		);
		
		return $this->find('first',$options);
	}
	
	function getLatest($clientId = null){
		$options = array();
		
		if($clientId){
			$options['conditions'] = array(
				'Project.client_id' => $clientId
			);
		}
		$options['contain'] = array(
            'Contact',
            'Client',
            'PurchaseOrder',
            'Labour',
            'Timesheet',
			'AgencyTimesheet',
            'ProjectItem',
            'Invoice',
            'ProjectStatus',
            'User',
			'Tool',
			'ProjectMethodDoc',
		);
		$options['order'] = array('Project.created' => 'DESC');
		$options['limit'] = 5;
		
		return $this->find('all',$options);
	}
	
	function getProjectsByProfit($clientId = null, $typeId = null, $statusIds = null){
		$options = array();
		$options['conditions'] = array();
		
		if($clientId){
			$options['conditions']['Project.client_id'] = $clientId;
		}
		if($typeId){
			$options['conditions']['Project.project_type_id'] = $typeId;
		}
		if($statusIds){
			$options['conditions']['Project.project_status_id IN'] = $statusIds;
		}
		$options['conditions']['Project.archived'] = 0;
		$options['contain'] = array(
			'Contact',
			'Client',
			'PurchaseOrder',
			'Labour',
			'Timesheet',
			'AgencyTimesheet',
			'ProjectItem',
			'Invoice',
			'ProjectStatus',
			'User',
			'Tool',
			'ProjectMethodDoc',
		);
		
		if($typeId == 2){
			$options['order'] = array('Project.start_date' => 'DESC');
		}else{
			$options['order'] = array('Project.created' => 'DESC');
		}
		
		$projects = $this->find('all',$options);		
		
		usort($projects, function ($a, $b) {
			if ($a['Project']['profit'] == $b['Project']['profit']) return 0;
			return ($a['Project']['profit'] < $b['Project']['profit']) ? -1 : 1;
		});
		
		return $projects;
	}

	/**
	 * @param null $clientId
	 * @param null $typeId
	 * @param null $statusIds
	 * @param string $listType
	 * @param DateTime|null $fromDate
	 * @param DateTime|null $toDate
	 * @param boolean|null $includeArchived
	 * @return array|null
	 */
	function getProjects($clientId = null, $typeId = null, $statusIds = null, $listType = 'all', $fromDate = null, $toDate = null, $includeArchived = null) {
		$options = array();

		if ($clientId) {
			$options['conditions']['Project.client_id'] = $clientId;
		}
		if ($typeId) {
			$options['conditions']['Project.project_type_id'] = $typeId;
		}
		if ($statusIds) {
			$options['conditions']['Project.project_status_id IN'] = $statusIds;
		}

		if ($fromDate) {
			$options['conditions']['Project.end_date >='] = $fromDate->format('Y-m-d');
		}
		if ($toDate) {
			$options['conditions']['Project.start_date <='] = $toDate->format('Y-m-d');
		}

		if (!$fromDate && !$toDate && !$includeArchived) {
			$options['conditions']['Project.archived'] = 0;
		}

		if ($listType != 'list') {
			$options['contain'] = array(
                'Contact',
                'Client',
                'PurchaseOrder',
                'Labour',
                'Timesheet',
				'AgencyTimesheet',
                'ProjectItem',
                'Invoice',
                'ProjectStatus',
                'User',
				'Tool',
				'ProjectMethodDoc',
			);
		}
		
		if($typeId == 2){
			$options['order'] = array('Project.start_date' => 'ASC');
		}else{
			$options['order'] = array('Project.created' => 'DESC');
		}
		
		$projects = $this->find($listType, $options);
		
		
		return $projects;
	}
	
	public function afterFind($results, $primary = false) {

		foreach ($results as $key => $val) {
			$spent = 0;

			// make sure associated PurchaseOrders etc are in the same level as the Project
			if (empty($val['PurchaseOrder']) && !empty($val['Project']['PurchaseOrder'])) {
				$val['PurchaseOrder'] = $val['Project']['PurchaseOrder'];
			}
			if (empty($val['Labour']) && !empty($val['Project']['Labour'])) {
				$val['Labour'] = $val['Project']['Labour'];
			}
			if (empty($val['Timesheet']) && !empty($val['Project']['Timesheet'])) {
				$val['Timesheet'] = $val['Project']['Timesheet'];
			}

			if(isset($val['PurchaseOrder'])){
				foreach($val['PurchaseOrder'] as $po){
					$spent += $po['value'];
				}				
			}

			if(isset($val['Labour'])){
				foreach($val['Labour'] as $labour){
					$spent += $labour['total'];
				}
			}

			if(isset($val['Timesheet'])){
				foreach($val['Timesheet'] as $timesheet){
					$spent += $timesheet['total'];
				}
			}

			if($spent > 0){
				$results[$key]['Project']['profit'] = 100 - (round((($spent/$results[$key]['Project']['value'])*100)));
				$results[$key]['Project']['cost'] = $spent;
			}else{
				$results[$key]['Project']['profit'] = '100';
				$results[$key]['Project']['cost'] = 0;
			}

			// calculate man hours left
			if (!empty($val['Project']['man_hours_allowed'])){
				$manHoursUsed = $this->AllTimesheetEntries->find('all', array(
					'fields' => 'sum(AllTimesheetEntries.hours_worked) AS total',
					'conditions'=> array('AllTimesheetEntries.project_id' => $val['Project']['id'])
				));

				$results[$key]['Project']['man_hours_used'] = (float)$manHoursUsed[0][0]['total'];
				$results[$key]['Project']['man_hours_left'] = $val['Project']['man_hours_allowed'] - $results[$key]['Project']['man_hours_used'];
			}

			if(isset($val['Project'])){
				if(isset($val['Project']['start_date']) && $val['Project']['start_date'] != '0000-00-00'){
					$results[$key]['Project']['start_date'] = date('d-m-Y',strtotime($val['Project']['start_date']));								
				}else{
					$results[$key]['Project']['start_date'] = '';
				}
				
				if(isset($val['Project']['end_date']) && $val['Project']['end_date'] != '0000-00-00'){
					$results[$key]['Project']['end_date'] = date('d-m-Y',strtotime($val['Project']['end_date']));								
				}else{
					$results[$key]['Project']['end_date'] = '';
				}
			}
			
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Project']['start_date'])){
			$this->data['Project']['start_date'] = date('Y-m-d',strtotime($this->data['Project']['start_date']));
		}
		
		if (!empty($this->data['Project']['end_date'])){
			$this->data['Project']['end_date'] = date('Y-m-d',strtotime($this->data['Project']['end_date']));
		}

		return true;
	}

	
}