<?php
class Post extends AppModel
{
	var $name = 'Post';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $validate = array(
		'title' => array('rule'=>'notBlank','message'=>'Please enter a title'),
		'url'=> array(
			'rule1' => array(
					'rule'=>'notBlank',
					'message' => 'Please enter a post URL'
			),
			'rule2' => array(
					'rule'=>'isUnique',
					'message' => 'Please enter a unique post URL',
					'on' => 'create'
			)
		)
	);
	
	var $belongsTo = array(
		'User'
	);
	
	var $hasMany = array(
		'Photo' => array(
			'order' => 'Photo.order_num',
			'foreignKey' => 'post_id',
		)
	);
	
	var $hasAndBelongsToMany = array(
		'PostCategory',
		'Site'
	);
	
	function getLatest(
		$numPosts = 5,
		$offset = 0,
		$categorySlug = null,
		$categorySlug2 = null,
		$featuredOnHomepage = null,
		$featuredOnLandingPage = null,
		$categoryId = null,
		$site = null
	) {
		$conditions = array(
			'Post.published' => 1,
		);

		if ($featuredOnHomepage) {
			$conditions['Post.featured'] = 1;
		}
		if ($featuredOnLandingPage) {
			$conditions['Post.featured_landing'] = 1;
		}

		$order = array(
			'Post.date DESC'		   
		);
		
		$contain = array(
			'PostCategory',
			'Photo'	 
		);
		
		$joins = array();
		
		if ($categorySlug) {
			$joins[0] = array( 
				'table' => 'post_categories_posts', 
				'alias' => "PostCategoriesPost", 
				'type' => 'inner',  
				'conditions'=> array(
					"PostCategoriesPost.post_id = Post.id"
				) 
			); 
			
			$joins[1] = array( 
				'table' => 'post_categories',
				'alias' => "PostCategory",
				'type' => 'inner',
				'conditions'=> "PostCategory.id = PostCategoriesPost.post_category_id AND PostCategory.url = '".$categorySlug ."'"
				//'conditions'=> "Category.id = CategoriesProduct.category_id"
			);
			if($categorySlug2){
				$joins[1]['conditions'] = "PostCategory.id = PostCategoriesPost.post_category_id AND (PostCategory.url = '".$categorySlug ."' OR PostCategory.url = '".$categorySlug2 ."')";
			}

		} else if ($categoryId) {
			$joins[] = array(
				'table' => 'post_categories_posts',
				'alias' => "PostCategoriesPost",
				'type' => 'inner',
				'conditions'=> array(
					"PostCategoriesPost.post_id = Post.id AND PostCategoriesPost.post_category_id = " . $categoryId
				)
			);
		}

		if ($site) {
			$joins[] = array(
				'table' => 'posts_sites',
				'alias' => 'PostsSites',
				'type' => 'inner',
				'conditions'=> array(
					"PostsSites.post_id = Post.id AND PostsSites.site_id = " . $site['Site']['id']
				)
			);
		}
		
		return $this->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$numPosts, 'offset' => $offset,'joins'=>$joins));
	}
}
