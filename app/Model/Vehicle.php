<?php
class Vehicle extends AppModel {
	
	var $name = 'Vehicle';
	var $displayField = 'reg_no';
	var $recursive = -1;
	var $order = 'reg_no';
	var $actsAs = array('Containable');


	var $hasMany = array(
		'Tool',
	);

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'modified_by_user_id'
		),
	);


	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the vehicle name',
			),
		),
		'reg_no' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the vehicle registration number',
			),
		),
		'make' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the vehicle make',
			),
		),
		'model' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter the vehicle model',
			),
		),
	);

	function get($id)
	{
		$options = array();
		$options['conditions'] = array(
			'Vehicle.id' => $id
		);
		$options['contain'] = array(
			'Tool',
			'User'
		);

		return $this->find('first',$options);
	}

	function getVehicles($findType= 'all', $includeArchived = false)
	{
		$conditions = array();

		if (!$includeArchived) {
			$conditions['archived'] = 0;
		}

		$settings = $this->find($findType, array(
			'conditions' => $conditions
		));

		return $settings;
	}

	public function afterFind($results, $primary = false)
	{
		foreach ($results as $key => $val) {
			if (isset($val['Vehicle']['mot_expiry_date']) && $val['Vehicle']['mot_expiry_date'] != '0000-00-00') {
				$results[$key]['Vehicle']['mot_expiry_date'] = date('d-m-Y', strtotime($val['Vehicle']['mot_expiry_date']));
			} else {
				$results[$key]['Vehicle']['mot_expiry_date'] = '';
			}

			if (isset($val['Vehicle']['mot_expiry_date']) && $val['Vehicle']['tax_expiry_date'] != '0000-00-00') {
				$results[$key]['Vehicle']['tax_expiry_date'] = date('d-m-Y', strtotime($val['Vehicle']['tax_expiry_date']));
			} else {
				$results[$key]['Vehicle']['tax_expiry_date'] = '';
			}
		}
		
		return $results;
	}

	public function beforeSave($options = array())
	{
		if (!empty($this->data['Vehicle']['mot_expiry_date'])) {
			$this->data['Vehicle']['mot_expiry_date'] = date('Y-m-d', strtotime($this->data['Vehicle']['mot_expiry_date']));
		}

		if (!empty($this->data['Vehicle']['tax_expiry_date'])) {
			$this->data['Vehicle']['tax_expiry_date'] = date('Y-m-d', strtotime($this->data['Vehicle']['tax_expiry_date']));
		}

		return parent::beforeSave($options);
	}
}