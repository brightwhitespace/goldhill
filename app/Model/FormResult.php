<?php
class FormResult extends AppModel {
	var $name = 'FormResult';
	var $displayField = 'result';
	var $recursive = -1;
	var $actsAs = array('Containable');
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Form' => array(
			'className' => 'Form',
			'foreignKey' => 'form_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>