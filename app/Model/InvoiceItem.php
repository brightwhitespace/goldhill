<?php
class InvoiceItem extends AppModel {
	
	var $name = 'InvoiceItem';
	var $recursive = -1;
	var $actsAs = array('Containable');

	var $belongsTo = array(
		'Invoice'
	);

	var $validate = array(
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a description'
			),
		),
	);

	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if(isset($val['InvoiceItem'])){
				if(!empty($val['InvoiceItem']['date']) && $val['InvoiceItem']['date'] != '0000-00-00'){
					$results[$key]['InvoiceItem']['date'] = date('d-m-Y',strtotime($val['InvoiceItem']['date']));
				}else{
					$results[$key]['InvoiceItem']['date'] = '';
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['InvoiceItem']['date'])){
			$this->data['InvoiceItem']['date'] = date('Y-m-d',strtotime($this->data['InvoiceItem']['date']));
		}

		if (empty($this->data['InvoiceItem']['cost'])) {
			$this->data['InvoiceItem']['cost'] = null;
		}

		return true;
	}

	
}