<?php
class Menu extends AppModel {
	
	var $name = 'Menu';
	var $displayField = 'name';
	var $recursive = -1;
	var $actsAs = array('Containable');
	
	var $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter a name for the menu',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array('Page');

}
?>