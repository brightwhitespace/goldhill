<?php
class FormsController extends AppController {

	var $name = 'Forms';	
	var $uses = array('Form','Page');
	var $fieldTypes = array(
		'text' => 'Small Text Box',
		'textarea' => 'Large Text Box',
		'radio' => 'Radio Button',
		'checkbox' => 'Tick Box',
		'select' => 'Select Box',
		'content' => 'Content Section'
	);
	var $validationTypes = array(
		'notBlank' => 'Not Blank',
	);
	var $components = array('Email');
	
	
	function send(){
		
		if(!empty($this->request->data)){
			//load form info:
			$form = $this->Form->getForm($this->request->data['Form']['id']);
			
			//save form response
			$data['FormResult']['form_id'] = $this->request->data['Form']['id'];
			$data['FormResult']['result'] = serialize($this->request->data);
			
			$this->Form->FormResult->save($data);
			
			//create email message
			$this->Email->to = $this->settings['admin_name'].'<'.$form['Form']['email'].'>';
			
			$this->Email->subject = $form['Form']['name'].': Form Submission';
			$this->Email->from = $this->settings['admin_name'] .'<'.$this->settings['admin_email'].'>';
			$this->Email->template = 'form_template'; 
			$this->Email->sendAs = 'text'; 
			
			$this->set('data',$this->request->data);
			$this->set('form',$form);
			
					
			
			if ($this->Email->send()) {
				$this->Session->setFlash($form['Form']['response'],'flash_success');
			}else{
				$this->Session->setFlash($this->Email->smtpError,'flash_failure');
			}
			
			//debug($this->request->data);
			
			$this->redirect($this->referer());
			
		}
		
	}

	
	function admin_index() {
		$this->layout = 'admin';
		$this->set('title_for_layout',$this->settings['site_name'] .' - Admin - Forms');
		$this->Form->recursive = 0;
		
		$this->paginate = array(
			'conditions' => array(
				'Form.site_id' => $this->currentSite['Site']['id']
			)
		);
		
		$this->set('forms', $this->paginate());
	}

	function admin_view($id = null) {
		
		$this->layout = 'admin';
		$this->set('title_for_layout',$this->settings['site_name'] .' - Admin - View Form');
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid form'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('form', $this->Form->read(null, $id));
	}

	function admin_add() {
		
		$this->layout = 'admin';
		$this->set('title_for_layout',$this->settings['site_name'] .' - Admin - Add Form');
		
		if (!empty($this->request->data)) {
			
			$this->request->data['Form']['site_id'] = $this->currentSite['Site']['id'];
			
			$this->request->data['FormField'][0]['name'] = 'Name';
			$this->request->data['FormField'][0]['label'] = 'Your Name';
			$this->request->data['FormField'][0]['type'] = 'text';
			$this->request->data['FormField'][0]['required'] = '1';
			
			$this->request->data['FormField'][1]['name'] = 'Email';
			$this->request->data['FormField'][1]['label'] = 'Your Email Address';
			$this->request->data['FormField'][1]['type'] = 'text';
			$this->request->data['FormField'][1]['required'] = '1';
			
			$this->request->data['FormField'][2]['name'] = 'Telephone';
			$this->request->data['FormField'][2]['label'] = 'Your Contact Number';
			$this->request->data['FormField'][2]['type'] = 'text';
			$this->request->data['FormField'][2]['required'] = '0';
			
			$this->request->data['FormField'][3]['name'] = 'Enquiry';
			$this->request->data['FormField'][3]['label'] = 'Your message';
			$this->request->data['FormField'][3]['type'] = 'textarea';
			$this->request->data['FormField'][3]['required'] = '1';
			
			$this->Form->create();
			if ($this->Form->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The form has been saved'),'flash_success');
				$this->redirect('/admin/forms/edit/'.$this->Form->id);
			} else {
				$this->Session->setFlash(__('The form could not be saved. Please try again.'),'flash_failure');
			}
		}
		$this->Page->recursive = 0;
		$this->set('pages', $this->Page->getAdminPages(null, $this->currentSite));
	}

	function admin_edit($id = null) {
		
		$this->layout = 'admin';
		$this->set('title_for_layout',$this->settings['site_name'] .' - Admin - Edit Form');
		
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid form'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Form->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The form has been saved'),'flash_success');
				if(isset($this->request->params['form']['save_&_close'])){
					$this->redirect('/admin/forms/');
				}
				$this->data = $this->Form->getForm($this->data['Form']['id']);
			} else {
				$this->Session->setFlash(__('The form could not be saved. Please try again.'),'flash_failure');
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Form->getForm($id);
		}
		
		$this->set('fieldTypes',$this->fieldTypes);
		$this->set('validationTypes',$this->validationTypes);
		$this->Page->recursive = 0;
		$this->set('pages', $this->Page->getAdminPages(null, $this->currentSite));
		
		
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for form'),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Form->delete($id)) {
			$this->Session->setFlash(__('Form deleted'),'flash_success');
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Form was not deleted'),'flash_failure');
		$this->redirect(array('action' => 'index'));
	}
	
	function admin_deletefield($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for field'),'flash_failure');
			$this->redirect($this->referer().'#fields');
		}
		if ($this->Form->FormField->delete($id)) {
			$this->Session->setFlash(__('Field deleted'),'flash_success');
			$this->redirect($this->referer().'#fields');
		}
		$this->Session->setFlash(__('Field was not deleted'),'flash_failure');
		$this->redirect($this->referer().'#fields');
	}
	
	function admin_export($id){
		$form = $this->Form->getForm($id,true);
		
		$csv = 'DATE SUBMITTED';
		
		foreach($form['FormField'] as $field){
			$csv .= ','.$field['name'];
		}
		
		$csv .= "\n";
		
		foreach($form['FormResult'] as $result){
			$results = unserialize($result['result']);
			
			$csv .= $result['created'];
			
			foreach($form['FormField'] as $field){
				if(isset($results['Form'][$field['name']])){
					
					$order   = array(",", "\n", "\r");
					$replace = ' ';
					$csv .= ','.str_replace($order,$replace,$results['Form'][$field['name']]);
				}else{
					$csv .= ',';
				}
			}
			$csv .= "\n";
		}
		
		$filename = $form['Form']['name'] .'-results.csv';
			
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"" . $filename . "\"" );
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		
		echo $csv;
		
		exit();
	}
	
}
?>