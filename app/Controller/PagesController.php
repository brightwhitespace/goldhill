<?php

App::import('Controller', 'Products');

class PagesController extends AppController{

	var $name = 'Pages';
	var $helpers = array('Html','Form','Js','Time','Session','Ckeditor','Blog','Combinator');
	var $uses = array('Page','User','AccessGroup','Post','PurchaseOrder','Project');  // 'Post','Advert'
	var $components = array('RequestHandler','Email','Auth','Session');
	var $templates = array(
		'display' => 'Default',
		'landing-page' => 'Landing page',
		'child-pages' => 'Child page list'
		/*'child-pages-with-images' => 'Child page list with photos',
		'events-page' => 'Event list page',
		'donate-page' => 'Donate Page',
		'donate-regularly' => 'Donate Regularly Page'*/
	);
	
	var $copyData = array();

	function home()
	{
		if ($this->currentUser['User']['access_group_id'] == AccessGroup::CLIENT) {
			$this->redirect('/clients/home');
		}
	}

	function display($slug, $siteCode = null, $countryCode = 'uk')
	{
		if ($this->RequestHandler->isAjax()) {
			$this->layout = 'ajax';
			$this->set('isAjax',true);
		} else {
			$this->set('isAjax',false);
		}

		
		if ($page) {
			//display page
			$parentPage = $this->Page->getParentPage($page['Page']['id']);

			$parentIds = $this->Page->getParentIdsOfActivePage($page);
			$this->set('parentIds', $parentIds);

			// get parent landing page
			if ($parentIds) {
				$parentLandingPage = $this->Page->findById($parentIds[0]);
			} else {
				$parentLandingPage = $page;
			}
			$this->set('parentLandingPage', $parentLandingPage);

			// get adverts
			$this->set('adverts', $this->Page->Advert->getAdverts($page));

			$page = $this->_applyFilters($page);
			$this->_setMetaData($page['Page']);
			$this->set(compact('page','parentPage'));

			// get news posts if landing page
			

			if(!empty($page['Page']['template'])){				
				$this->render($page['Page']['template']);
			}
			
		} else {
			//switch to shop mode
			//$this->displayShop();
		}
	}
	
	
	
	function contact($subject = null){	
	
		//get any homepage content
		$page = $this->Page->find('first',array('conditions'=>array('Page.page_title'=>'Contact Us','Page.published'=>1)));

		$this->_setMetaData($page['Page']);
		
		$this->set('page',$page);
		
		if(!empty($this->request->data)){
			
			//validate
			$this->Page->validate = array(
					'name'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your name'						
					),
					'position'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your position'						
					),
					'company'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your company name'						
					),
					'address1'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your address line 1'						
					),
					'postcode'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your postcode'						
					),
					'email'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your email'						
					),
					'tel'=> array(						
						'rule'=>'notBlank',
						'message' => 'Please enter your telephone'						
					)
					
			);
			
			$this->Page->set($this->request->data);
			
			if($this->Page->validates($this->request->data)){
				//send email
				
				$this->Email->to = $this->settings['admin_name'] .'<'.$this->settings['admin_email'].'>';
				//$this->Email->cc = array('barnst@gmail.com');
				$this->Email->subject = 'Enquiry from '.$this->request->data['Page']['name'];
				$this->Email->from = $this->request->data['Page']['name'] .'<'.$this->request->data['Page']['email'].'>';
				$this->Email->template = 'contact_email'; 
				$this->Email->replyTo = $this->settings['admin_name'] .'<'.$this->settings['admin_email'].'>';
				$this->Email->sendAs = 'text'; 
				
				$this->set('data',$this->request->data);
				
				
				if ($this->Email->send()) {
					$this->set('msg', empty($this->settings['contact_form_sent_message']) ? 'Your message has been sent' : $this->settings['contact_form_sent_message']);
				} else {
					$this->Session->setFlash($this->Email->smtpError);
				}
			}
		} else {
			if ($subject) {
				uses('sanitize');
				$cleaner = new Sanitize();
				$subject = $cleaner->clean($subject);
				$this->request->data['Page']['subject'] = $subject;
			}
		}
	}
	
	function sitemap()
	{
		$this->pageTitle = 'Sitemap - '.$this->site_prefs['meta_title'];
		$this->_setMetaData();
		
		$this->set('menu_pages',$this->Page->getMenuPages('Main Menu', true, false, true));
	}
	
	function search()
	{
		$this->pageTitle = 'Search Results'; // - '.$this->site_prefs['meta_title'];
		$this->_setMetaData();
		
		if (!empty($this->request->data)) {
			App::uses('Sanitize', 'Utility');
			$searchTerm = Sanitize::html($this->request->data['Page']['search_term']);
		
			//$searchTerm = $this->request->data['Page']['search_term'];
			$this->Session->write('Search.term',$searchTerm);
		} else {
			$searchTerm = $this->Session->read('Search.term');
			//$searchTerm = Sanitize::html($this->request->data['Page']['search_term']);
		}
		
		if($searchTerm){
			//search pages
			$options['conditions'] = array(
				'Page.published' => 1,
				'or' =>array(
					'Page.page_title LIKE' => "%$searchTerm%",
					'Page.page_body LIKE' => "%$searchTerm%"
				)
			);
			$options['order'] = array('Page.page_title');
			$options['fields'] = array('Page.page_title','Page.id','Page.page_body','Page.direct_url','Page.page_url','Parent.page_url');
			$options['contain'] = array('Parent','ContentBlock');
			
			$pages = $this->Page->find('all',$options);
			$this->set('pages',$pages);
			
			//search products
			$options = array();
			$options['conditions'] = array(
				'Product.published' => 1,
				'or' =>array(
					'Product.name LIKE' => "%$searchTerm%",
					'Product.description LIKE' => "%$searchTerm%"
				)
			);
			$options['order'] = array('Product.name');
			$options['contain'] = array('Category'=>array('ParentCategory'));
			
			$products = $this->Product->find('all',$options);
			$this->set('products',$products);
			
			//search posts
			$options = array();
			$options['conditions'] = array(
				'Post.published' => 1,
				'or' =>array(
					'Post.title LIKE' => "%$searchTerm%",
					'Post.body LIKE' => "%$searchTerm%"
				)
			);
			$options['order'] = array('Post.title');
			$options['contain'] = array('PostCategory');
			
			$posts = $this->Post->find('all',$options);
			$this->set('posts',$posts);
			
			$this->set('searchTerm',$searchTerm);
		}
	}
	
	function edit()
	{
		$this->layout = 'default';
		
		if(!empty($this->request->data)){
			if($this->Page->save($this->request->data)){
				$this->Session->setFlash('Changes to your page has been successfully saved');				
			}
		}
		
		$this->redirect($this->referer());
	}
	
	function admin_index()
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','Pages - View All - '.$this->settings['site_name']);
		//get list of menus
		$this->Page->Menu->recursive = -1;
		$menus = $this->Page->Menu->find('all');
		$this->set('admin_menus',$menus);
		
		if(!empty($this->request->data)){
			$this->Session->write('Search.menu_id',$this->request->data['Search']['menu_id']);
			$menuId = $this->request->data['Search']['menu_id'];
		}else{
			$menuId = $this->Session->read('Search.menu_id');
			$this->request->data['Search']['menu_id'] = $menuId;
		}
		
		$this->Page->recursive = 0;
		$this->set('pages', $this->Page->getAdminPages($menuId));
		
		if($menuId){
			$this->set('selMenu',$this->Page->Menu->find('first',array('conditions'=>array('Menu.id'=>$menuId))));
		}
		
		$this->set('menus',$this->Page->Menu->find('list',array('fields'=>array('Menu.id','Menu.name'))));
	}
	
	function admin_list()
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','Pages List - '.$this->settings['site_name']);
		$conditions = array();
		$this->set('pages',$this->Page->find('all',array(
			'conditions' => $conditions,
			'order'=>array('Page.page_title'),
		)));
	}
	
	function admin_add()
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','Pages - Add New Page - '.$this->settings['site_name']);
		$this->Page->recursive = 0;
		$conditions = array();
		$this->set('pages', $this->Page->find('threaded',array(
			'conditions' => $conditions,
			'order'=>'Page.order_num'
		)));
		
		if (!empty($this->request->data)) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash('Page added successfully');
				if(isset($this->data['save_&_close'])){					
					$this->redirect('/admin/pages');
				} else {
					$this->redirect('/admin/pages/edit/'.$this->Page->id);
				}
			}
		}
		
		$this->set('menus',$this->Page->Menu->find('list',array('fields'=>array('Menu.id','Menu.name'))));
		$this->set('accessgroups',$this->AccessGroup->find('list',array('fields'=>'AccessGroup.group_name')));
		$this->set('adverts',$this->Page->Advert->find('list',array(
			'conditions' => array(),
			'fields'=>'Advert.name'
		)));
	}

	function admin_edit($id = null)
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','Pages - Edit Page - '.$this->settings['site_name']);

		$this->Page->recursive = 0;
		$conditions = array();
		$this->set('pages', $this->Page->find('threaded',array(
			'conditions' => $conditions,
			'order'=>'Page.order_num'
		)));
		
		if (!$id && empty($this->request->data)) {
			$this->flash(__('Cannot find page to edit'), array('action'=>'index'));
		}
		
		if (!empty($this->request->data)) {
			if ($this->Page->saveAll($this->request->data)) {
				$this->Session->setFlash('Changes to your page has been successfully saved');
				if (isset($this->data['save_&_close'])) {
					$this->redirect('/admin/pages/');
				}
				$this->request->data = $this->Page->getPageEdit($this->Page->id);
			}
		} else {
			$this->request->data = $this->Page->getPageEdit($id);
			$this->setReturnUrl($this->referer());
		}

		$this->set('menus', $this->Page->Menu->find('list',array('fields'=>array('Menu.id','Menu.name'))));
		$this->set('accessgroups', $this->AccessGroup->find('list',array('fields'=>'AccessGroup.group_name')));
		$this->set('adverts', $this->Page->Advert->find('list',array(
			'conditions' => array(),
			'fields'=>'Advert.name'
		)));
		$this->set('resources',$this->Page->Resource->find('list',array('fields'=>'Resource.name')));
		$this->set('templates',$this->templates);
		$this->set('postCategories', $this->Page->PostCategory->find('list'));
	}
	
	function admin_deletecontent($id = null)
	{
		if (!$id) {
			$this->flash(__('Invalid Page'), array('action'=>'index'));
		}
		if ($this->Page->ContentBlock->delete($id)) {
			$this->Session->setFlash('Page has been successfully deleted');
			$this->redirect($this->referer());
		}
	}
	
	function admin_delete($id = null)
	{
		if (!$id) {
			$this->flash(__('Invalid Page'), array('action'=>'index'));
		}
		if ($this->Page->delete($id)) {
			$this->Session->setFlash('Page has been successfully deleted');
			$this->redirect($this->referer());
		}
	}
	
	function admin_order($menuId=null)
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','Pages - Change Order - '.$this->settings['site_name']);

		//$this->set('data',$this->Page->find('all',array('conditions'=>"Page.parent_id = $id",'order'=>'Page.order_num ASC')));

		if (!$menuId){
			$menuId = $this->Session->read('Search.menu_id');
		}
		
		$menus = $this->Page->Menu->find('all');
		$this->set('admin_menus',$menus);
		
		if (!$menuId && isset($menus[0]['Menu']['id'])) {
			$menuId = $menus[0]['Menu']['id'];
		}
		
		$this->Page->recursive = 0;
		$this->set('pages', $this->Page->getAdminPages($menuId));
	}
	
	function admin_changeorder($site = null)
	{
		$order_num = 0;
		foreach ($this->data['page'] as $id => $parent) {
			$this->request->data['Page']['order_num'] = $order_num;
			$this->request->data['Page']['parent_id'] = $parent;
			$this->request->data['Page']['id'] = $id;
			
			$this->Page->save($this->request->data,false);
			$order_num ++;
		}

		$this->layout = 'ajax';
	}
	
	function admin_admin($projectStatusGroupName = 'current')
	{
		//default admin page
		$this->layout = 'admin';
		$this->set('title_for_layout', $this->settings['site_name'].' - Admin');

		$this->_updateProjectStatuses();

		$projectStatusGroups = array(
			'current' => array(Project::STATUS_PENDING, Project::STATUS_LIVE, Project::STATUS_COMPLETED),
			'holding'=> array(Project::STATUS_HOLDING)
		);
		$projectStatusGroupName = isset($projectStatusGroups[$projectStatusGroupName]) ? $projectStatusGroupName : 'current';

		$this->set('projects', $this->Project->getProjectsByProfit(null, 1, $projectStatusGroups[$projectStatusGroupName]));
		$this->set('projectsDayWorks', $this->Project->getProjects(null, 2, $projectStatusGroups[$projectStatusGroupName]));
		$this->set('purchaseOrdersHire', $this->PurchaseOrder->getHireItems());
		$this->set('projectStatusGroupList', array_combine(array_keys($projectStatusGroups), array_keys($projectStatusGroups)));
		$this->set('projectStatusGroupName', $projectStatusGroupName);
	}
		
	function admin_preview($id)
	{
		$this->layout = 'default';

		$this->Page->recursive = 1;
		$page = $this->Page->findById($id);

		if ($page) {
			//display page
			if (!empty($page['Parent']['parent_id'])) {
				$parentPage = $this->Page->getParentPage($page['Parent']['parent_id']);
			} else {
				if (!empty($page['Page']['parent_id'])) {
					$parentPage = $this->Page->getParentPage($page['Page']['parent_id']);
				}
			}

			if (!isset($parentPage)) {
				$parentPage = $page;
			}

			$page['Page']['page_body'] = $this->_applyFilters($page['Page']['page_body']);
			$this->_setMetaData($page['Page']);
			$this->set(compact('page','parentPage'));
			$this->set('adverts_for_layout', $this->Page->Advert->getAdverts($page));

			$adverts = $this->Page->Advert->get(array('1','5'));

			$this->set('adverts',$adverts);
		}
	}
	
	function admin_files()
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','File Manager');
	}
	
	function _applyFilters($page)
	{
		$terms = $this->Glossary->getTerms();
		
		for ($i = 0; $i < count($page['ContentBlock']); $i++) {
			$page['ContentBlock'][$i] = $this->_applyGlossary($page['ContentBlock'][$i],$terms);
		}
		
		return $page;
	}
	
	function _applyGlossary($str,$terms)
	{
		$find = array();
		$definitions = array();

		foreach ($terms as $term) {
			//$find[] = $term['Glossary']['term'];
			//$definitions[] = '<span class="glossary" title="'.$term['Glossary']['definition'].'">'.$term['Glossary']['term'].'</span>';
			//search for lowercase and upper case of the terms
			
			$strFind = str_replace("/"," ",$term['Glossary']['term']);
			
			$find[] = "/(?<![a-zA-Z-])(".$strFind.")(?![a-zA-Z])(?!([^<]+)?>)/i";
			$definitions[] = '<span class="glossary" title="'.$term['Glossary']['definition'].'">'."\${1}</span>";
		}
		
		return preg_replace($find, $definitions, $str);
	}
	
	
	
	
	
}
