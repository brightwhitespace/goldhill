<?php

class SagepayComponent extends Component
{
	// NOTE: This component applies to Sagepay SERVER (rather than FORM)
	
	// Specify vendor name and server to connect to in init() function
	var $vendor_name;	// e.g. brightwhitespac
	var $connect_to;	// i.e. SIMULATOR | TEST | LIVE
	var $transaction_type;	// PAYMENT | REPEAT
	
	var $purchase_url;
	var $repeat_url;
	var $notification_url;
	
	// for PAYMENT transaction registration
	var $currency="GBP";
	var $protocol="2.23";
	var $vendor_tx_code;
	var $basket_str;	
	var $post_data;
	var $response_data;
	
	// for returning results after processing PAYMENT callback
	var $callback_data_out = array();
	
	// for REPEAT
	var $related_vps_tx_id;
	var $related_vendor_tx_code;
	var $related_security_key;
	var $related_tx_auth_no;
	
	
	var $localSimulatorStatusList = array(
				'OK' => 'OK : Authorised',
				'NOTAUTHED' => 'NOTAUTHED : Declined',
				'ABORT' => 'ABORT : Cancelled by customer or timed out',
				'REJECTED' => 'REJECTED : Failed 3D-Secure or AVS/CV2 rule-bases',
				'AUTHENTICATED' => 'AUTHENTICATED by 3D-Secure, can now be Authed',
				'REGISTERED' => 'REGISTERED to be Authorised (not 3D-Secure Authed)',
				'ERROR' => 'ERROR : Error during payment process');
		
	
	function init($vendor_name, $connect_to='SIMULATOR', $transaction_type='PAYMENT') {
		
		$this->vendor_name = $vendor_name;
		$this->connect_to = $connect_to;
		$this->transaction_type = $transaction_type;
		
		//select server to connect to
		if( $connect_to=="LIVE" ) {
			$this->purchase_url="https://live.sagepay.com/gateway/service/vspserver-register.vsp";
			
			//$this->purchase_url="https://test.sagepay.com/showpost/showpost.asp";
			 
			$this->repeat_url="https://live.sagepay.com/gateway/service/repeat.vsp";
		}
		elseif( $connect_to=="TEST" ) {
			$this->purchase_url="https://test.sagepay.com/gateway/service/vspserver-register.vsp";
			$this->repeat_url="https://test.sagepay.com/gateway/service/repeat.vsp";
		}
		else { // SIMULATOR
			$this->purchase_url="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRegisterTx";
			$this->repeat_url="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRepeatTx";
		}
	}
	
		
	function formatBasket($order, $deliveryCost=false, $vatPercent = 20) {
		
		// Format of basket is:
		//  <total no of items inc delivery>:<item1 desc>:<qty>:<net cost>:<tax>:<cost>:<qty*cost>: ... :Delivery:1:<net cost>:<tax>:<cost>:<1*cost>
		// e.g.
		//  2:Pioneer NSDV99 DVD-Surround Sound System:1:424.68:74.32:499.00:499.00:Delivery:1:5.00:1.00:6.00:6.00
		
		$this->basket_str = count($order['OrderItem']) + ($deliveryCost !== false ? 1 : 0); // number of items (inc delivery if not false)
		
		// add each OrderItem from basket
		foreach($order['OrderItem'] as $item){
			
			
			
			$this->basket_str.= ':'.str_replace(array('"','&'), array('','and'), $item['name']).':';
			$this->basket_str.= $item['quantity'].':'; // quantity
			$this->basket_str.= $item['net'].':';
			$this->basket_str.= $item['tax'].':';
			$this->basket_str.= $item['unit'].':';
			$this->basket_str.= $item['total']; // item cost * quantity
		}
		
		//add delivery if not false
		if( $deliveryCost !== false ) {
			
			$deliveryNet = (100/(100 + $vatPercent)) * $deliveryCost;
			
			$this->basket_str.= ':Delivery:';
			$this->basket_str.= '1:';
			$this->basket_str.= number_format($deliveryNet,2).':';
			$this->basket_str.= number_format($deliveryCost - $deliveryNet,2).':';
			$this->basket_str.= number_format($deliveryCost, 2).':';
			$this->basket_str.= number_format($deliveryCost, 2); // total delivery cost
		}
		
	}
	
	
	function formatPost($orderDetails, $totalCost, $desc, $currency=null){
		
		$orderId = $orderDetails['id'];		
		$this->vendor_tx_code = $this->generateTxCode($orderId);
		
		$strPost="VPSProtocol=" . $this->protocol;
		$strPost=$strPost . "&TxType=" . $this->transaction_type; //PAYMENT by default.  You can change this in the includes file
		
		$strPost=$strPost . "&Vendor=" . $this->vendor_name;
		$strPost=$strPost . "&VendorTxCode=" . $this->vendor_tx_code; 
		$strPost=$strPost . "&Amount=" . $totalCost; // Formatted to 2 decimal places with leading digit
		$strPost=$strPost . "&Currency=" . ($currency ? $currency : $this->currency);
		// Up to 100 chars of free format description
		$strPost=$strPost . "&Description=" . $desc;
		
		/* The Notification URL is the page to which Sage Pay Server calls back when a transaction completes
		** You can change this for each transaction, perhaps passing a session ID or state flag if you wish */
		$strPost=$strPost . "&NotificationURL=" . $this->notification_url;
	
		// Check if transaction_type is "PAYMENT" (standard default) or "REPEAT"
	
		if( $this->transaction_type == 'REPEAT' ) {
			// REPEAT auth code
			$strPost=$strPost . "&RelatedVPSTxId=" . $orderDetails['related_vps_tx_id'];
			$strPost=$strPost . "&RelatedVendorTxCode=" . $orderDetails['related_vendor_tx_code'];
			$strPost=$strPost . "&RelatedSecurityKey=" . $orderDetails['related_security_key'];
			$strPost=$strPost . "&RelatedTxAuthNo=" . $orderDetails['related_tx_auth_no'];
		}
		else {	
			// Standard PAYMENT
			$strPost=$strPost . "&CustomerName=" . $orderDetails['billing_firstname'].' '.$orderDetails['billing_lastname'];
			$strPost=$strPost . "&CustomerEMail=" . $orderDetails['email']; 
			
			//Billing Details
			$strPost=$strPost . "&BillingFirstnames=" . $orderDetails['billing_firstname'];
			$strPost=$strPost . "&BillingSurname=" . $orderDetails['billing_lastname'];
			$strPost=$strPost . "&BillingAddress1=" . $orderDetails['billing_address1'];
			
			if(!empty($orderDetails['address2'])){
				$strPost=$strPost . "&BillingAddress2=" . $orderDetails['billing_address2'];
			}
			if(!empty($orderDetails['address3'])){
				$strPost=$strPost . ', '.$orderDetails['billing_address3'];
			}
			if(!empty($orderDetails['city'])){
				$strPost=$strPost . "&BillingCity=" . $orderDetails['billing_city'];
			}
			//if(!empty($orderDetails['billing_county'])){
			//	$strPost=$strPost . "&BillingState=" . $orderDetails['billing_county'];
			//}
			if(!empty($orderDetails['country'])){
				$strPost=$strPost . "&BillingCountry=" . $orderDetails['billing_country'];
			}else{
				$strPost=$strPost . "&BillingCountry=GB";
			}
			$strPost=$strPost . "&BillingPostCode=" . $orderDetails['billing_postcode'];
			
			//Delivery Details
			$strPost=$strPost . "&DeliveryFirstnames=" . $orderDetails['firstname'];
			$strPost=$strPost . "&DeliverySurname=" . $orderDetails['lastname'];
			$strPost=$strPost . "&DeliveryAddress1=" . $orderDetails['address1'];
			
			if(!empty($orderDetails['address2'])){
				$strPost=$strPost . "&DeliveryAddress2=" . $orderDetails['address2'];
			}
			if(!empty($orderDetails['address3'])){
				$strPost=$strPost . ', '.$orderDetails['address3'];
			}
			if(!empty($orderDetails['city'])){
				$strPost=$strPost . "&DeliveryCity=" . $orderDetails['city'];
			}
			//if(!empty($orderDetails['county'])){
			//	$strPost=$strPost . "&DeliveryState=" . $orderDetails['county'];
			//}
			if(!empty($orderDetails['country'])){
				$strPost=$strPost . "&DeliveryCountry=" . $orderDetails['country'];
			}else{
				$strPost=$strPost . "&DeliveryCountry=GB";
			}
			$strPost=$strPost . "&DeliveryPostCode=" . $orderDetails['postcode'];
			
			// Optionally add the contact number if present
			if( isset($orderDetails['contact_number']) && $orderDetails['contact_number'] ) {
				$strPost=$strPost . "&BillingPhone=" . $orderDetails['billing_contact_number'];
				$strPost=$strPost . "&DeliveryPhone=" . $orderDetails['contact_number'];
			}
				
			// For charities registered for Gift Aid, set to 1 to display the Gift Aid check box on the payment pages
			$strPost=$strPost . "&AllowGiftAid=0";
				
			/* Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default 
			** It can be changed dynamically, per transaction, if you wish.  See the VSP Server Protocol document */
			if ($this->transaction_type!=="AUTHENTICATE")
				$strPost=$strPost . "&ApplyAVSCV2=0";
				
			/* Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default 
			** It can be changed dynamically, per transaction, if you wish.  See the VSP Server Protocol document */
			$strPost=$strPost . "&Apply3DSecure=0";
			
			// Optional setting for Profile can be used to set a simpler payment page. See protocol guide for more info. **
			$strPost=$strPost . "&Profile=NORMAL"; //NORMAL is default setting. Can also be set to LOW for the simpler payment page version.

			// ADD BASKET
			$strPost=$strPost . "&Basket=" . $this->basket_str; // As created above 
		}
		
		$this->post_data = $strPost;
		//debug($strPost);
	}
	
	
	/*************************************************************
		Send a post request with cURL
			$url = URL to send request to
			$data = POST data to send (in URL encoded Key=value pairs)
	*************************************************************/
	function requestPost(){
		// Set a one-minute timeout for this script
		set_time_limit(60);
	
		$url = $this->transaction_type == 'REPEAT' ? $this->repeat_url : $this->purchase_url;
		$data = $this->post_data;
		
		// Initialise output variable
		$output = array();
	
		// Open the cURL session
		$curlSession = curl_init();
	
		// Set the URL
		curl_setopt ($curlSession, CURLOPT_URL, $url);
		// No headers, please
		curl_setopt ($curlSession, CURLOPT_HEADER, 0);
		// It's a POST request
		curl_setopt ($curlSession, CURLOPT_POST, 1);
		// Set the fields for the POST
		curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
		// Return it direct, don't print it out
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER,1); 
		// This connection will timeout in 30 seconds
		curl_setopt($curlSession, CURLOPT_TIMEOUT,30); 
		//The next two lines must be present for the kit to work with newer version of cURL
		//You should remove them if you have any problems in earlier versions of cURL
		curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);
	
		//Send the request and store the result in an array
		
		$rawresponse = curl_exec($curlSession);
		
		
		//Store the raw response for later as it's useful to see for integration and understanding 
		$_SESSION["rawresponse"]=$rawresponse;
		//Split response into name=value pairs
		$response = split(chr(10), $rawresponse);
		// Check that a connection was made
		if (curl_error($curlSession)){
			// If it wasn't...
			$output['Status'] = "FAIL";
			$output['StatusDetail'] = curl_error($curlSession);
		}
	
		// Close the cURL session
		curl_close ($curlSession);
	
		// Tokenise the response
		for ($i=0; $i<count($response); $i++){
			// Find position of first "=" character
			$splitAt = strpos($response[$i], "=");
			// Create an associative (hash) array with key/value pairs ('trim' strips excess whitespace)
			$output[trim(substr($response[$i], 0, $splitAt))] = trim(substr($response[$i], ($splitAt+1)));
		} // END for ($i=0; $i<count($response); $i++)
	
		// Return the output in response_data
		$this->response_data = $output;
	}


	function processPaymentCallback($data, $strSecurityKey, $strVendorName, $successRedirectUrl=null, $failureRedirectUrl=null) {
		
		$strStatus = 'OK';
		$strStatusDetail = 'Test';
		
		if( $strSecurityKey ) {
			$strVendorTxCode = isset($data["VendorTxCode"]) ? $data["VendorTxCode"] : '';
			$strVPSTxId = isset($data["VPSTxId"]) ? $data["VPSTxId"] : '';

			/** Now get the VPSSignature value from the POST, and the StatusDetail in case we need it **/
			$strVPSSignature = isset($data["VPSSignature"]) ? $data["VPSSignature"] : '';
			$strStatusDetail = isset($data["StatusDetail"]) ? $data["StatusDetail"] : '';
	
			/** Retrieve the other fields, from the POST if they are present **/
			$strTxAuthNo = isset($data["TxAuthNo"]) ? $data["TxAuthNo"] : '';
			$strAVSCV2 = isset($data["AVSCV2"]) ? $data["AVSCV2"] : '';
			$strAddressResult = isset($data["AddressResult"]) ? $data["AddressResult"] : '';
			$strPostCodeResult = isset($data["PostCodeResult"]) ? $data["PostCodeResult"] : '';
			$strCV2Result = isset($data["CV2Result"]) ? $data["CV2Result"] : '';
			$strGiftAid = isset($data["GiftAid"]) ? $data["GiftAid"] : '';
			$str3DSecureStatus = isset($data["3DSecureStatus"]) ? $data["3DSecureStatus"] : '';
			$strCAVV = isset($data["CAVV"]) ? $data["CAVV"] : '';
			$strAddressStatus = isset($data["AddressStatus"]) ? $data["AddressStatus"] : '';
			$strPayerStatus = isset($data["PayerStatus"]) ? $data["PayerStatus"] : '';
			$strCardType = isset($data["CardType"]) ? $data["CardType"] : '';
			$strLast4Digits = isset($data["Last4Digits"]) ? $data["Last4Digits"] : '';
	
			/** Now we rebuild the POST message, including our security key, and use the MD5 Hash **
			** component that is included to create our own signature to compare with **
			** the contents of the VPSSignature field in the POST.  Check the Sage Pay Server protocol **
			** if you need clarification on this process **/
			$strMessage=$strVPSTxId . $strVendorTxCode . $strStatus . $strTxAuthNo . $strVendorName . $strAVSCV2 . $strSecurityKey 
						   . $strAddressResult . $strPostCodeResult . $strCV2Result . $strGiftAid . $str3DSecureStatus . $strCAVV
						   . $strAddressStatus . $strPayerStatus . $strCardType . $strLast4Digits ;
		
			$strMySignature = strtoupper(md5($strMessage));
		}
	
		/** We can now compare our MD5 Hash signature with that from Sage Pay Server **/
		if (!$strSecurityKey || $strMySignature !== $strVPSSignature)
		{
			$this->callback_data_out['dbStatus'] = 'INVALID';
			$this->callback_data_out['dbStatusDetail'] = 'Cannot match the MD5 Hash. Order might be tampered with.';
			
			$this->callback_data_out['replyStatus'] = 'INVALID';		
			$this->callback_data_out['replyRedirectURL'] = $failureRedirectUrl;
			$this->callback_data_out['replyStatusDetail'] = 'Cannot match the MD5 Hash. Order might be tampered with.';	
		}
		else {
			/** Great, the signatures DO match, so we can update the database and redirect the user appropriately **/			
			$this->callback_data_out['dbStatus'] = $strStatus;
			$this->callback_data_out['dbStatusDetail'] = $this->getDbStatusDetail($strStatus, $strStatusDetail);
			
			$this->callback_data_out['replyStatus'] = 'OK';
			$this->callback_data_out['replyRedirectURL'] = $successRedirectUrl;
			$this->callback_data_out['replyStatusDetail'] = '';
		}
		
	}
	

	function getDbStatusDetail($strStatus, $strStatusDetail='<undefined>') {
		
		if ($strStatus=="OK")
			$dbStatusDetail="AUTHORISED - The transaction was successfully authorised with the bank.";
		elseif ($strStatus=="NOTAUTHED") 
			$dbStatusDetail="DECLINED - The transaction was not authorised by the bank.";
		elseif ($strStatus=="ABORT")
			$dbStatusDetail="ABORTED - The customer clicked Cancel on the payment pages, or the transaction was timed out due to customer inactivity.";
		elseif ($strStatus=="REJECTED")
			$dbStatusDetail="REJECTED - The transaction was failed by your 3D-Secure or AVS/CV2 rule-bases.";
		elseif ($strStatus=="AUTHENTICATED")
			$dbStatusDetail="AUTHENTICATED - The transaction was successfully 3D-Secure Authenticated and can now be Authorised.";
		elseif ($strStatus=="REGISTERED")
			$dbStatusDetail="REGISTERED - The transaction could not be 3D-Secure Authenticated, but has been registered to be Authorised.";
		elseif ($strStatus=="ERROR")
			$dbStatusDetail="ERROR - There was an error during the payment process.  The error details are: " . $strStatusDetail;
		else
			$dbStatusDetail="UNKNOWN - An unknown status was returned from Sage Pay.  The Status was: " . $strStatus .  ", with StatusDetail:" . $strStatusDetail;
			
		return $dbStatusDetail;
	}
	
	
	function createRepeatOrder($previousOrder, $includeCardDetails=true) {
		$repeatOrder = array('Order'=>array());
		$repeatOrder['Order']['id'] = null;
		$repeatOrder['Order']['tx_type'] = 'REPEAT';
		$repeatOrder['Order']['user_id'] = $previousOrder['Order']['user_id'];
		$repeatOrder['Order']['cost'] = $previousOrder['Order']['cost'];
		$repeatOrder['Order']['description'] = $previousOrder['Order']['description'];
		$repeatOrder['Order']['related_vendor_tx_code'] = $previousOrder['Order']['vendor_tx_code'];
		$repeatOrder['Order']['related_vps_tx_id'] = $previousOrder['Order']['vps_tx_id'];
		$repeatOrder['Order']['related_security_key'] = $previousOrder['Order']['security_key'];
		$repeatOrder['Order']['related_tx_auth_no'] = $previousOrder['Order']['tx_auth_no'];
		if( $includeCardDetails ) {
			$repeatOrder['Order']['last_4_digits'] = $previousOrder['Order']['last_4_digits'];
			$repeatOrder['Order']['card_type'] = $previousOrder['Order']['card_type'];
		}
		
		return $repeatOrder;
	}

	function createPaymentOrder($previousOrder, $newStatus='') {
		$newOrder = $previousOrder;
		$newOrder['Order']['id'] = null;
		$newOrder['Order']['tx_type'] = 'PAYMENT';
		$newOrder['Order']['status'] = $newStatus;
		$newOrder['Order']['status_detail'] = '';
		unset($newOrder['Order']['vendor_tx_code']);
		unset($newOrder['Order']['vps_tx_id']);
		unset($newOrder['Order']['security_key']);
		unset($newOrder['Order']['tx_auth_no']);
		unset($newOrder['Order']['related_vendor_tx_code']);
		unset($newOrder['Order']['related_vps_tx_id']);
		unset($newOrder['Order']['related_security_key']);
		unset($newOrder['Order']['related_tx_auth_no']);
		unset($newOrder['Order']['post_data']);
		unset($newOrder['Order']['last_4_digits']);
		unset($newOrder['Order']['card_type']);
		unset($newOrder['Order']['created']);
		
		return $newOrder;
	}


	function generateTxCode($id){
		$random = rand(10000000,99999999); 
		return $id.'-'.$random;
	}

}

?>