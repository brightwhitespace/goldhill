<?php
class WageAdjustmentsController extends AppController {

	var $name = 'WageAdjustments';
	var $layout = 'admin';
	var $uses = array('WageAdjustment', 'Employee', 'WageAdjustmentsEmployee');


	function admin_add($employeeId)
	{
		$this->set('title_for_layout','Wage Adjustments - Add New Wage Adjustment');

		if (!empty($this->request->data)) {
			$this->WageAdjustment->create();

			$this->request->data['WageAdjustment']['employee_id'] = $employeeId;

			if ($this->WageAdjustment->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The wage adjustment has been saved'));
				$this->redirect('/admin/employees/view/' . $employeeId);
			} else {
				$this->Session->setFlash(__('The wage adjustment could not be saved. Please try again.'));
			}
		}

		$this->set('employeeId', $employeeId);
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Wage Adjustments - Edit Wage Adjustment');

		$wageAdjustment = $this->WageAdjustment->read(null, $id);

		if (!$wageAdjustment && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid wage adjustment'));
			$this->redirect($this->referer());
		}

		if (!empty($this->request->data)) {
			if ($this->WageAdjustment->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The wage adjustment has been saved'));
				$this->redirect('/admin/employees/view/' . $wageAdjustment['WageAdjustment']['employee_id']);
			} else {
				$this->Session->setFlash(__('The wage adjustment could not be saved. Please try again.'));
			}

		} else {
			$this->request->data = $wageAdjustment;
		}

		$this->set('employeeId', $wageAdjustment['WageAdjustment']['employee_id']);
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Wage Adjustment'));
		} else if ($this->WageAdjustment->delete($id)) {
			$this->Session->setFlash(__('Wage Adjustment deleted'));
		} else {
			$this->Session->setFlash(__('Wage Adjustment was not deleted'));
		}

		$this->redirect($this->referer());
	}

}
