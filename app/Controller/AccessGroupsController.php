<?php

class AccessGroupsController extends AppController
{
	var $name = 'AccessGroups';
	var $layout = 'admin';
	var $components = array("Acl");
	var $actsAs = array('Acl'=>'requester');
	
	// Assign actions in class to one of the CRUD = $this->Auth->actionMap['organize'] = 'update';
	
	/*function parentNode(){
		if (!$this->id) {
			return null;
		}
		$data = $this->read();
		if (!$data['AccessGroup']['parent_id']){
			return null;
		} else {
			return $data['AccessGroup']['parent_id'];
		}
	}*/
	
	function admin_index(){
		$this->set('options_for_layout','accessgroups');
		$this->set('id_for_layout',-1);
		$this->AccessGroup->recursive = -1;
		$this->set('accessGroups',$this->AccessGroup->find('all'));
	}
	
	function admin_add(){
		$this->set('options_for_layout','accessgroups');
		$this->set('id_for_layout',-1);
		
		if(empty($this->request->data)){
			$this->render();
		}else{				
			if($this->AccessGroup->save($this->request->data)){
				$this->Session->setFlash('Access Group has been successfully added');
				$this->redirect('/admin/access_groups');
			}	
			
		}
	}
	
	function admin_edit($id = null) {
		$this->set('options_for_layout','accessgroups');
		$this->set('id_for_layout',-1);
		if (!$id && empty($this->request->data)) {
			$this->flash(__('Invalid AccessGroup'), array('action'=>'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->AccessGroup->save($this->request->data)) {
				$this->Session->setFlash('Access Group has been successfully updated');
				$this->redirect('/admin/access_groups');
			} else {
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->AccessGroup->read(null, $id);
		}
	}
	
	function admin_delete($id){
		$this->set('options_for_layout','accessgroups');
		$this->set('id_for_layout',-1);
		if($this->AccessGroup->delete($id)){
			$this->Session->setFlash('Access Group has been successfully deleted');
			$this->redirect($this->referer());
		}else{
			$this->flash("Error Deleting Group, try again...","/admin/access_groups");
		}
	}
	
	function getRootId(){
		$aco = new Aco();
	   	$root = $aco->findByAlias('ROOT');
	  	return $root['Aco']['id']; 
	}
	
	function setPermission($group, $controller, $action, $permission){

		$aco = new Aco();
		//get root ACO
		$rootAco = $aco->findByAlias('ROOT');
		//check if ACO exists, create if not
		$controllerAco = $aco->findByAlias($controller);
		if(empty($controllerAco)){
			$aco->create();
			$aco->save( array (
					'alias' => $controller,
					'parent_id' => $rootAco['Aco']['id'],
			));
			$controllerAco = $aco->findByAlias($controller);
		}

		// Set up ACO for action if it's not present.
		$actionAco = $aco->find('first', array(
				'conditions'=>array('parent_id' => $controllerAco['Aco']['id'], 'alias' => $action)));
		if(empty($actionAco)){
			$aco->create();
			$result = $aco->save(array(
				'alias' => $action,
				'parent_id' => $controllerAco['Aco']['id'],
			));
			$actionAco = $aco->find('first', array(
				'conditions'=>array('parent_id' => $controllerAco['Aco']['id'], 'alias' => $action)));
		}

		// Set up perms now.
		//debug($group['AccessGroup']['group_name']);
		//debug($permission.' => '.$controller.'/'.$action);
		if($permission == 'allow'){
			$this->Acl->allow( $group['AccessGroup']['group_name'], $controller . '/' . $action );
		}else{
			$this->Acl->deny( $group['AccessGroup']['group_name'], $controller . '/' . $action );
		}
	}
	
	function admin_permissions($id=null){	
		$this->set('options_for_layout','accessgroups');
		$this->set('id_for_layout',-1);
		
		$group = $this->AccessGroup->read(null,$id);
		$group_name = $group['AccessGroup']['group_name'];
		
		//$this->Acl->allow( 'Anonymous', 'AccessGroups/admin_permissions' );
		
		if(!empty($this->request->data)){			
			$this->appClasses = $this->listControllers();			
			foreach($this->appClasses as $controller => $actions){
				foreach($actions as $action){
					$pageAction = $controller.'/'.$action;
					$permission = 'deny';	
					if(!empty($this->request->data[$group_name])){
						foreach($this->request->data[$group_name] as $f){
							if($f == $pageAction){
								$permission = 'allow';							
							}	
						}
					}
					//echo "$group_name, $controller, $action, $permission<br />";
					$this->setPermission($group,$controller,$action,$permission);
				}
			}
			$this->Session->setFlash('Access Group permissions have been successfully updated');
			$this->redirect('/admin/access_groups');
		}else{	
			$this->set('group',$group);	
			$this->set("i",0);
			$this->appClasses = $this->listControllers();
			$pageList = array();
			$accessList = array();
			foreach($this->appClasses as $class => $methods){
				foreach($methods as $meth){
					$page = $class."/".$meth;
					
					//$this->createACO($class,$meth);
					
					array_push($pageList,$page);
					if($this->Acl->check($group_name, $page, "read")){
						$accessList[$page] = true;
					}else{
						$accessList[$page] = false;
					}
				}
			}
			$this->set("accessList",$accessList);
			$this->set("pages",$pageList);
		}
	}
	
	function createACO($controller,$action){
		
		$aco = new Aco();
		//get root ACO
		$rootAco = $aco->findByAlias('ROOT');
		
		$controllerAco = $aco->findByAlias($controller);
		if(empty($controllerAco)){
			$aco->create();
			$aco->save( array (
					'alias' => $controller,
					'parent_id' => $rootAco['Aco']['id'],
			));
			$controllerAco = $aco->findByAlias($controller);
		}

		// Set up ACO for action it's not present.
		$actionAco = $aco->find(array('parent_id' => $controllerAco['Aco']['id'], 'alias' => $action));
		if(empty( $actionAco)){
			$aco->create();
			$aco->save(array(
				'alias' => $action,
				'parent_id' => $controllerAco['Aco']['id'],
			));
			$actionAco = $aco->find( array( 'parent_id' => $controllerAco['Aco']['id'], 'alias' => $action ) );
		}
	}
	
	
	
	function listControllers() {
		$controllerClasses = App::objects('Controller');
//debug(get_class_methods('AppController'));
        foreach($controllerClasses as $controller) {
			//debug($controller);
            if ($controller != 'App') {
                $fileName = $controller.'.php';
                $file = APP . 'Controller' . DS.$fileName;
                require_once($file);
                $className = $controller;
                $actions = get_class_methods($className);
//debug($actions);
				if(!empty($actions)){
					foreach($actions as $k => $v) {
						if ($v{0} == '_') {
							unset($actions[$k]);
						}
					}
				}
                $parentActions = get_class_methods('AppController');
                $controllers[str_replace('Controller', '', $controller)] = array_diff($actions, $parentActions);
            }
        }
		
        return $controllers;
	}
}

?>