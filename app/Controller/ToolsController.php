<?php
class ToolsController extends AppController {

	var $name = 'Tools';
	var $layout = 'admin';
	var $uses = array('Tool', 'Employee', 'ToolsEmployee', 'Project', 'Vehicle');

	function index()
	{
		$this->layout = 'default';

		// Need to find tools whose assigned employee is the same employee_id associated with the logged in user...
		$conditions = array(
			'Tool.employee_id' => $this->currentUser['Employee']['id']
		);

		$this->paginate = array('conditions' => $conditions);
		$this->Tool->recursive = 0;
		$this->set('tools', $this->paginate());
	}

	function edit($id = null)
	{
		$this->layout = 'default';

		$this->set('title_for_layout','Tools - Edit Tool');

		if (!$id || !($tool = $this->Tool->get($id)) || $tool['Tool']['employee_id'] != $this->currentUser['Employee']['id']) {
			$this->Session->setFlash(__('This tool is not assigned to you'));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			if ($this->Tool->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The tool has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The tool could not be saved. Please try again.'));
			}

		} else {
			$this->request->data = $tool;

			$this->setReturnUrl($this->referer(), true);
		}

		$this->set('returnUrl', $this->readReturnUrl());

		$this->set('employeeList', $this->Employee->getEmployees('list', false));
		$this->set('vehicleList', $this->Vehicle->getVehicles('list'));
		$this->set('projectList', $this->Project->getProjects(null, null, null, 'list'));
	}

	function admin_index()
	{
		App::uses('Tool', 'Model');

		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			if (!empty($this->data['Search']['tool_status_id'])) {
				$conditions['Tool.tool_status_id'] = $this->data['Search']['tool_status_id'];

				switch ($this->data['Search']['tool_status_id']) {
					case Tool::STATUS_INDIVIDUAL;
						if (!empty($this->data['Search']['employee_id'])) {
							$conditions['Tool.employee_id'] = $this->data['Search']['employee_id'];
						}
						break;
					case Tool::STATUS_IN_VEHICLE:
						if (!empty($this->data['Search']['vehicle_id'])) {
							$conditions['Tool.vehicle_id'] = $this->data['Search']['vehicle_id'];
						}
						break;
					case Tool::STATUS_PROJECT:
						if (!empty($this->data['Search']['project_id'])) {
							$conditions['Tool.project_id'] = $this->data['Search']['project_id'];
						}
						break;
				}
			}

			$conditions['or'] = array(
				'Tool.name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Employee.name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Project.name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Vehicle.reg_no LIKE' => '%'.$this->data['Search']['term'].'%',
			);
		}

		if (empty($this->data['Search']['tool_status_id'])) {
			// hide archived by default
			$conditions['Tool.tool_status_id !='] = Tool::STATUS_ARCHIVED;
		}

		$this->set('employeeList', $this->Employee->getEmployees('list'));
		$this->set('vehicleList', $this->Vehicle->getVehicles('list'));
		$this->set('projectList', $this->Project->getProjects(null, null, null, 'list'));

		$this->Tool->recursive = 0;

		if (empty($this->request->data['Search']['show_all_records'])) {
			$this->paginate = array('conditions' => $conditions);
			$this->set('tools', $this->paginate());
		} else {
			$this->set('tools', $this->Tool->find('all', array('conditions' => $conditions)));
		}

	}

	function admin_add()
	{
		$this->set('title_for_layout','Tools - Add New Tool');

		if (!empty($this->request->data)) {
			$this->Tool->create();

			if ($this->Tool->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The tool has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The tool could not be saved. Please try again.'));
			}
		} else {
			$this->setReturnUrl($this->referer(), true);
		}

		$this->set('returnUrl', $this->readReturnUrl());

		$this->set('employeeList', $this->Employee->getEmployees('list'));
		$this->set('vehicleList', $this->Vehicle->getVehicles('list'));
		$this->set('projectList', $this->Project->getProjects(null, null, null, 'list'));
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Tools - Edit Tool');

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid tool'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Tool->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The tool has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The tool could not be saved. Please try again.'));
			}

		} else {
			$this->request->data = $this->Tool->read(null, $id);

			$this->setReturnUrl($this->referer(), true);
		}

		$this->set('returnUrl', $this->readReturnUrl());

		$this->set('employeeList', $this->Employee->getEmployees('list', false));
		$this->set('vehicleList', $this->Vehicle->getVehicles('list'));
		$this->set('projectList', $this->Project->getProjects(null, null, null, 'list'));
	}

	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for tool'));
			$this->redirect(array('action'=>'index'));
		}

		$tool = $this->Tool->get($id);

		$this->set('tool', $tool);
		$this->set('files', glob(APP . "tmp/tool/$id/*"));
	}

	function admin_archive($id, $archive = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for tool'));
			$this->redirect($this->referer());
		}

		$this->Tool->id = $id;
		$this->Tool->saveField('archived', $archive);
		$this->Session->setFlash(__('Tool was ' . ($archive ? 'archived' : 'restored from archive')));

		$this->redirect($this->referer());
	}

}
