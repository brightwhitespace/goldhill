<?php
class AdvertsController extends AppController
{
	var $name = 'Adverts';
	var $layout = 'admin';
	var $pageTitle = 'Adverts';
	var $uses = array('Advert','Page');
	
	var $positions = array(
		'slider' => 'Slider',
		'banner' => 'Banner',
		'sidebar' => 'Sidebar',
		'footer' => 'Footer',
	);


	function admin_index()
	{
		$this->set('title_for_layout','Adverts - '.$this->settings['site_name']);
		
		$this->Advert->recursive = -1;
		$this->paginate = array(
			'order'=>'Advert.order_num ASC'
		);

		$conditions = array();
		if ($this->data) {
			$conditions['name LIKE'] = '%' . $this->data['Search']['term'] . '%';
		}
		if ($this->currentSite) {
			$conditions['Advert.site_id'] = $this->currentSite['Site']['id'];
		}
		$this->paginate = array('conditions'=>$conditions);
		
		$this->set('adverts', $this->paginate());
	}

	function admin_add()
	{
		$this->set('title_for_layout','New Advert - '.$this->settings['site_name']);
		
		if (!empty($this->request->data)) {
			$this->Advert->create();
			$this->request->data['Advert']['site_id'] = $this->currentSite['Site']['id'];
			if ($this->Advert->save($this->request->data)) {
				$this->Session->setFlash(__('The advert has been saved'));
					$this->redirect('/admin/adverts');
			} else {
				$this->Session->setFlash(__('The advert could not be saved. Please try again.'),'flash_failure');
			}
		}
		
		$this->Page->recursive = 0;
		$this->set('pages', $this->Page->getAdminPages(null, $this->currentSite));
		$this->set('positions', $this->positions);
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Edit Advert - '.$this->settings['site_name']);
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid advert'));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			$this->request->data['Advert']['site_id'] = $this->currentSite['Site']['id'];
			if ($this->Advert->save($this->request->data)) {
				$this->Session->setFlash(__('The advert has been saved','flash_success'));
				$this->redirect('/admin/adverts');
			} else {
				$this->Session->setFlash(__('The advert could not be saved. Please try again.'),'flash_failure');
			}
		}
		if (empty($this->request->data)) {
			$this->Advert->recursive = 1;
			$this->request->data = $this->Advert->read(null, $id);
		}
		
		$this->Page->recursive = 0;
		$this->set('pages', $this->Page->getAdminPages(null, $this->currentSite));
		$this->set('positions', $this->positions);
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for advert'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Advert->delete($id)) {
			$this->Session->setFlash(__('Advert deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Advert was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	function admin_order()
	{
		$this->set('title_for_layout','Change Advert Order - '.$this->settings['site_name']);
		
		$this->Advert->recursive = -1;
		$this->paginate = array(
				'order'=>'name ASC'
		);
		
		$this->set('adverts', $this->Advert->find('all',array(
			'conditions'=> $this->currentSite ? array('Advert.site_id' => $this->currentSite['Site']['id']) : array(),
			'order'=>array('Advert.order_num')
		)));
	}
	
	function admin_changeorder()
	{
		$order_num = 0;
		foreach ($this->data['advert'] as $id => $parent){
			$this->request->data['Advert']['order_num'] = $order_num;
			//$this->request->data['Advert']['parent_id'] = $parent;
			$this->request->data['Advert']['id'] = $id;
			
			$this->Advert->save($this->request->data,false);
			$order_num ++;
		}
		
		$this->layout = 'ajax';
		
		exit();
	}
}
