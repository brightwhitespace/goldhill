<?php

class UsersController extends AppController {

	var $name = 'Users';
	var $components = array('Auth','Email','Session','Cookie','RequestHandler');
	var $uses = array('User','PurchaseOrder');

	var $layout = 'admin';


	//Auth Functions
	function login()
	{
		$this->layout = 'login';
		$this->set('title_for_layout','Please Login - '.$this->settings['site_name']);
		
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$user = array();
					$user['User'] = $this->Auth->user();
				if (isset($this->data['rememberme']) && $this->data['rememberme']) {
					// Want to keep logged in so save a unique ref in user table
					
					$uniqueRef = $this->Auth->password($user['User']['id'].time());
					$user['User']['remember_me_ref'] = $uniqueRef;
					if ($this->User->save($user)) {
						// Now write cookie containing user id and unique ref
						$this->Cookie->write('remember_me_ref', $user['User']['id'].'-'.$uniqueRef, false, '2 weeks');
					}
				}
				
				$group = $this->User->AccessGroup->read(null,$user['User']['access_group_id']);
                 
                $redirectUrl = $this->Auth->redirect();				
                 
                if(strlen($redirectUrl) <= 1 && !empty($group['AccessGroup']['landing_page'])){
                    $redirectUrl = $group['AccessGroup']['landing_page'];
                }

				$this->Session->write('justLoggedIn', true);

				$this->redirect(strlen($redirectUrl) > 1 ? $redirectUrl : '/');

			} else {
				$this->Session->setFlash(__('Username or password is incorrect'), 'flash_failure_perm');
			}
		}
	}

	function logout()
	{
		$this->Session->destroy();
		$this->redirect($this->Auth->logout());
	}
	

	function admin_index($group=null)
	{
		$this->set('title_for_layout','Users - view all');
		
		$this->set('accessgroups',$this->User->AccessGroup->getGroupList('Anonymous', 'AccessGroup.group_name DESC'));
		
		$this->paginate = array('contain'=>'AccessGroup');

		// If not admin user then can only edit own details
		$conditions = !$this->userIsAdmin ? array('User.id' => $this->currentUser['User']['id']) : array();

		// Hide any 'deleted' users (i.e. where status is 'D')
		$conditions['status !='] = 'D';

        $this->set('users', $this->paginate('User', $conditions));
	}

	function admin_add()
	{
		$this->set('title_for_layout','Users - add');
		
		if (!empty($this->request->data)) {
			
			if (isset($this->data['cancel'])) {
				$this->redirect('/admin/users/');
			}
			
			if (!empty($this->request->data['User']['password_new'])) {
				$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password_new']);
			} 
				
			if ($this->User->saveAll($this->data)) {
				$this->Session->setFlash('User has been successfully added');
				$this->redirect('/admin/users/');
			} else {
				$this->Session->setFlash('Problem adding user');
				//$this->request->data['User']['password_confirm'] = null;
			}
		}

		$this->set('accessgroups',$this->User->AccessGroup->getGroupList('Anonymous', 'AccessGroup.group_name DESC'));
		$this->set('employeeList', $this->User->Employee->getEmployees('list'));
		$this->set('clientList', $this->User->Client->getClients('list'));
	}

	function admin_edit($id = null)
	{
		$this->set('options_for_layout','users');
		$this->set('id_for_layout',-1);
		
		if (!$id && empty($this->request->data)) {
			$this->flash(__('Invalid User'), array('action'=>'index'));
		}
		if (!empty($this->request->data)) {
			
			if (isset($this->data['cancel'])) {
				$this->redirect('/admin/users/');
			}
			
			if (!empty($this->request->data['User']['password_new'])) {
				$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password_new'] );
			} else {
				// new password not specified so unset in data to prevent failing validation
				unset($this->request->data['User']['password_new']);
			}
			
			// Update User
			if ($this->User->saveAll($this->request->data)) {
				$this->Session->setFlash('User has been successfully edited', 'flash_success');
				$this->redirect('/admin/users/');
			} else {
				$this->Session->setFlash('Problem updating user details', 'flash_failure');
			}
		}
		
		if (empty($this->request->data)) {
			$this->request->data = $this->User->find('first', array(
				'conditions'=>array('User.id'=>$id)
			));
		}

		$this->set('accessgroups',$this->User->AccessGroup->getGroupList('Anonymous', 'AccessGroup.group_name DESC'));
		$this->set('employeeList', $this->User->Employee->getEmployees('list'));
		$this->set('clientList', $this->User->Client->getClients('list'));
	}

	function admin_delete($id = null)
	{
		$this->set('options_for_layout','users');
		$this->set('id_for_layout',-1);

		if (!$this->userIsAdmin || $id == $this->currentUser['User']['id']) {
			$this->Session->setFlash('You cannot delete this user.');
			$this->redirect('/admin/users');
		} else if (is_numeric($id) && ($userToDelete = $this->User->findById($id))) {
			$this->User->id = $id;
			if ($this->User->saveField('status', 'D')) {
				$this->Session->setFlash('User has been successfully deleted');
				$this->redirect('/admin/users');
			}
		} else {
			$this->flash(__('Invalid User'), array('action'=>'index'));
		}
	}
}
