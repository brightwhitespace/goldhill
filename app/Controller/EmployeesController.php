<?php
class EmployeesController extends AppController {

	var $name = 'Employees';
	var $layout = 'admin';

	var $daysList = array(
		'Monday' => 'Monday',
		'Tuesday' => 'Tuesday',
		'Wednesday' => 'Wednesday',
		'Thursday' => 'Thursday',
		'Friday' => 'Friday',
		'Saturday' => 'Saturday',
		'Sunday' => 'Sunday',
	);

	function admin_index($archive = 0)
	{
		$this->processSearchData();

		$conditions = array('Employee.archived' => $archive);

		if ($this->data) {
			$conditions['name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		$this->paginate = array('conditions' => $conditions, 'contain' => 'Tool');
		$this->Employee->recursive = 0;
		$this->set('employees', $this->paginate());
		$this->set('archive', $archive);
	}

	function admin_add()
	{
		$this->set('title_for_layout','Employees - Add New Employee');
		if (!empty($this->request->data)) {
			$this->Employee->create();
			
			if ($this->Employee->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The employee has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please try again.'));
			}
		}

		$this->set('daysList', $this->daysList);
		$this->set('paymentMethodList', $this->Employee->EmployeePaymentMethod->find('list'));
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Employees - Edit Employee');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid employee'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Employee->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The employee has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Employee->findById($id);
		}

		$this->set('daysList', $this->daysList);
		$this->set('paymentMethodList', $this->Employee->EmployeePaymentMethod->find('list'));
	}

	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for employee'));
			$this->redirect(array('action'=>'index'));
		}

		$employee = $this->Employee->get($id);
		$employee['CertificatesEmployeeLatestUnique'] = Hash::combine($employee['CertificatesEmployee'], '{n}.certificate_id', '{n}');

		$this->set('employee', $employee);
		$this->set('employeeList', $this->Employee->getEmployees('list', false));
		$this->set('certificateList', $this->Certificate->find('list'));
		$this->set('reloadPageAfterCertUpdate', true);
		$this->set('files', glob(APP . "tmp/employee/$id/*"));
	}

	function admin_delete($id = null)
	{
		/*
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for employee'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Employee->delete($id)) {
			$this->Session->setFlash(__('Employee deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Employee was not deleted'));
		$this->redirect(array('action' => 'index'));
		*/
	}

	function admin_archive($id, $archive = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for sub-contractor'));
			$this->redirect($this->referer());
		}

		$this->Employee->id = $id;
		$this->Employee->saveField('archived', $archive);
		$this->Session->setFlash(__('Sub-contractor was ' . ($archive ? 'archived' : 'restored from archive')));

		$this->redirect($this->referer());
	}

}
