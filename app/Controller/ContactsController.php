<?php
class ContactsController extends AppController {

	var $name = 'Contacts';
	var $layout = 'admin';

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			$conditions['name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		$this->paginate = array('conditions' => $conditions);
		$this->Contact->recursive = 0;
		$this->set('contacts', $this->paginate());
	}

	function admin_add()
	{
		$this->set('title_for_layout','Contacts - Add New Contact');
		if (!empty($this->request->data)) {
			$this->Contact->create();
			
			if ($this->Contact->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The contact has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The contact could not be saved. Please try again.'));
			}
		}else{
			$this->setReturnUrl($this->referer());
			if(isset($this->request->params['named']['client_id'])){
				$this->request->data['Contact']['client_id'] = $this->request->params['named']['client_id'];
			}
			if(isset($this->request->params['named']['supplier_id'])){
				$this->request->data['Contact']['supplier_id'] = $this->request->params['named']['supplier_id'];
			}
		}
		
		$this->set('returnUrl',$this->readReturnUrl());
		$this->set('clients',$this->Contact->Client->find('list',array('order'=>array('Client.name'))));
		$this->set('suppliers',$this->Contact->Supplier->find('list',array('order'=>array('Supplier.name'))));
		
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Contacts - Edit Contact');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid contact'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Contact->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The contact has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The contact could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->setReturnUrl($this->referer());
			$this->request->data = $this->Contact->getContact($id);
		}
		
		$this->set('returnUrl',$this->readReturnUrl());
		$this->set('clients',$this->Contact->Client->find('list',array('order'=>array('Client.name'))));
		
	}
	

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for contact'));
			$this->redirect($this->referer());
		}
		if ($this->Contact->delete($id)) {
			$this->Session->setFlash(__('Contact deleted'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Contact was not deleted'));
		$this->redirect($this->referer());
	}
}
