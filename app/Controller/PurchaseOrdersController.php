<?php
class PurchaseOrdersController extends AppController {

	var $name = 'PurchaseOrders';
	var $layout = 'admin';
	var $uses = array('PurchaseOrder','Storage');
	
	function add()
	{
		$this->layout = 'default';
		
		$this->set('title_for_layout','Purchase Orders - Add New Purchase Order');
		if (!empty($this->request->data)) {
			$this->PurchaseOrder->set($this->request->data);
			if($this->PurchaseOrder->validates()){
				$this->Session->write('PO.new',$this->request->data);
				$this->redirect('/purchase_orders/add_step2');
			}
		} else {
			$data = $this->Session->read('PO.new');
			if ($data) {
				$this->request->data = $data;
			} else {
				$this->request->data['PurchaseOrder']['quantity'] = 1;
				$this->request->data['PurchaseOrder']['excess_tonage'] = 50;	
			}
			
			$this->request->data['PurchaseOrder']['user_id'] = $this->currentUser['User']['id'];
		}
		
		//$this->set('projects',$this->PurchaseOrder->Project->find('list',array('conditions'=>array('Project.user_id' => $this->currentUser['User']['id']),'order'=>'Project.name')));
		$this->set('projects',$this->PurchaseOrder->Project->find('list',array('conditions'=>array('Project.archived' => 0))));
		
		$this->set('suppliers',$this->PurchaseOrder->Supplier->find('list',array('order'=>'Supplier.name')));
		$this->set('types',$this->PurchaseOrder->Type->find('list',array('order'=>'Type.name')));
		$this->set('users',$this->PurchaseOrder->User->getList());
	}
	
	function add_step2()
	{
		$this->layout = 'default';
		
		$this->set('title_for_layout','Purchase Orders - Add New Purchase Order');
		if (!empty($this->request->data)) {
			
			if ($this->request->data['PurchaseOrder']['type_id'] == 2 || $this->request->data['PurchaseOrder']['type_id'] == 3) {
				unset($this->PurchaseOrder->validate['end_date']);
			}
			
			$data = $this->Session->read('PO.new');
			$newData['PurchaseOrder'] = array_merge($data['PurchaseOrder'],$this->request->data['PurchaseOrder']);
			
			$this->PurchaseOrder->set($newData);
			if ($this->PurchaseOrder->validates()) {
				$this->Session->write('PO.new',$newData);
				$this->redirect('/purchase_orders/add_preview');
			}
		} else {
			$this->request->data = $this->Session->read('PO.new');			
		}
		
		$this->set('projects',$this->PurchaseOrder->Project->find('list',array('order'=>'Project.name')));
		$this->set('suppliers',$this->PurchaseOrder->Supplier->find('list',array('order'=>'Supplier.name')));
		$this->set('types',$this->PurchaseOrder->Type->find('list',array('order'=>'Type.name')));
		$this->set('users',$this->PurchaseOrder->User->getList());
	}
	
	function add_preview()
	{
		$this->layout = 'default';
		
		$this->set('title_for_layout','Purchase Orders - Add New Purchase Order');
		if (!empty($this->request->data)) {
			
			$this->request->data = $this->Session->read('PO.new');
			
			if($this->request->data['PurchaseOrder']['type_id'] == 2){
				unset($this->PurchaseOrder->validate['end_date']);
			}
			
			$this->PurchaseOrder->create();
			$this->request->data['PurchaseOrder']['ref'] = 'PO'.$this->Storage->getNumber('PO');
			if ($this->PurchaseOrder->save($this->request->data)) {
				$this->_sendemail($this->PurchaseOrder->id);
				$this->Storage->updateNumber('PO');
				$this->Session->delete('PO.new');
				//$this->Session->setFlash(__('The purchase order has been saved'));
				$this->redirect('/purchase_orders/add_complete/'.$this->PurchaseOrder->id);
			} else {
				$this->Session->setFlash(__('The purchase order could not be saved. Please try again.'));
			}
		} else {
			$data = $this->Session->read('PO.new');
			
			$project = $this->PurchaseOrder->Project->getProject($data['PurchaseOrder']['project_id']);
			$supplier = $this->PurchaseOrder->Supplier->getSupplier($data['PurchaseOrder']['supplier_id']);
			$suppliercontact = $this->PurchaseOrder->Supplier->Contact->getContact($data['PurchaseOrder']['contact_id']);
			$type = $this->PurchaseOrder->Type->find('first',array('conditions'=>array('Type.id' => $data['PurchaseOrder']['type_id'])));
			$user = $this->PurchaseOrder->User->find('first',array('conditions'=>array('User.id'=>$data['PurchaseOrder']['user_id'])));
			
			$this->set(compact('data','project','supplier','type','user','suppliercontact'));
		}
	}
	
	function add_complete($id)
	{
		$this->layout = 'default';
		
		$po = $this->PurchaseOrder->getPurchaseOrder($id);
		
		$this->set('po',$po);
	}
	
	function index()
	{
		$this->layout = 'default';
		
		$conditions = array('PurchaseOrder.user_id' => $this->currentUser['User']['id']);
		if ($this->data) {
			$conditions['or'] = array(
				'PurchaseOrder.ref LIKE' => '%'.$this->data['Search']['term'].'%',
				'PurchaseOrder.desc LIKE' => '%'.$this->data['Search']['term'].'%',
				'Project.name LIKE' => '%'.$this->data['Search']['term'].'%',
			);
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'contain' => array(
				'PurchaseOrderInvoice',
				'Project'
			),
			'order' => array('PurchaseOrder.created'=>'desc')
		);
		$this->PurchaseOrder->recursive = 0;
		$this->set('purchase_orders', $this->paginate());
	}

	function admin_index()
	{
		$this->processSearchData();

		$this->set('searchFilterOptions', array(
			'awaiting_credit_note' => 'Awaiting Credit Notes',
			'overdue_hire_items' => 'Overdue Hire Items',
			'pos_not_completed' => 'POs Not Completed',
			'pos_tbc' => 'POs To Be Confirmed',
		));

		if (!empty($this->request->params['named']['filter'])){
			$this->request->data['Search']['filter'] = $this->request->params['named']['filter'];
			$this->request->data['Search']['term'] = !empty($this->request->data['Search']['term']) ? $this->request->data['Search']['term'] : '';
		}

		$conditions = array();
		
		if(!empty($this->request->params['named']['project_id'])){
			$conditions['PurchaseOrder.project_id'] = $this->request->params['named']['project_id'];
		}

		if(!empty($this->request->params['named']['supplier_id'])){
			$conditions['PurchaseOrder.supplier_id'] = $this->request->params['named']['supplier_id'];
		}
		
		if(!empty($this->request->params['named']['type_id'])){
			$conditions['PurchaseOrder.type_id'] = $this->request->params['named']['type_id'];
		}
		
		if(!empty($this->request->params['named']['returned'])){
			$conditions['PurchaseOrder.returned'] = $this->request->params['named']['returned'];
		}
		
		if ($this->data) {
			$conditions['or'] = array(
				'PurchaseOrder.ref LIKE' => '%'.$this->data['Search']['term'].'%',
				'PurchaseOrder.desc LIKE' => '%'.$this->data['Search']['term'].'%',
				'Project.name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Supplier.name LIKE' => '%'.$this->data['Search']['term'].'%',
				//'Client.name LIKE' => '%'.$this->data['Search']['term'].'%'
			);

			if (!empty($this->data['Search']['filter'])) {
				switch ($this->data['Search']['filter']) {
					case 'awaiting_credit_note':
						$conditions[] = 'PurchaseOrder.value < (select SUM(value) from purchase_order_invoices poi where poi.purchase_order_id = PurchaseOrder.id)';
						break;
					case 'overdue_hire_items':
						$conditions[] = 'PurchaseOrder.type_id = 1 AND PurchaseOrder.returned = 0 AND PurchaseOrder.end_date <= CURDATE()';
						break;
					case 'pos_not_completed':
						$conditions[] = 'PurchaseOrder.complete = 0';
						break;
					case 'pos_tbc':
						$conditions[] = 'PurchaseOrder.confirmed = 0';
						break;
				}
			}
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'contain' => array(
				'PurchaseOrderInvoice',
				'Project',
				'Supplier',
				'User',
				'ConfirmedByUser',
			),
			'order' => array('PurchaseOrder.created'=>'desc')
		);
		$this->PurchaseOrder->recursive = 0;
		$this->set('purchase_orders', $this->paginate());
	}

	function admin_add($projectId = null)
	{
		$this->set('title_for_layout','Purchase Orders - Add New Purchase Order');

		$projectSearchConditions = array('Project.archived' => 0);
		if ($projectId) {
			$projectSearchConditions = array('or' => array($projectSearchConditions, 'Project.id = ' . $projectId));
		}

		if (!empty($this->request->data)) {

			if($this->request->data['PurchaseOrder']['type_id'] == 2 || $this->request->data['PurchaseOrder']['type_id'] == 3){
				unset($this->PurchaseOrder->validate['end_date']);
			}
			
			$this->PurchaseOrder->create();
			$this->request->data['PurchaseOrder']['ref'] = 'PO'.$this->Storage->getNumber('PO');

			if (isset($this->request->data['save']) || isset($this->request->data['save_and_remain'])) {
				$this->request->data['PurchaseOrder']['confirmed'] = 1;
			}

			if ($this->PurchaseOrder->saveAll($this->request->data)) {
				$this->Storage->updateNumber('PO');

				if (isset($this->request->data['save']) || isset($this->request->data['save_and_remain'])) {
					$this->_sendemail($this->PurchaseOrder->id);
					$this->Session->setFlash(__('The purchase order has been saved'));
				} else {
					$this->Session->setFlash(__('The purchase order has been marked as To Be Confirmed'));
				}

				if (isset($this->request->data['save_and_remain']) || isset($this->request->data['tbc_and_remain'])) {
					$this->setReturnUrl($this->getReturnUrl(), true);
					$this->redirect('/admin/purchase_orders/edit/' . $this->PurchaseOrder->id);
				} else {
					$this->redirect('/admin/purchase_orders/view/' . $this->PurchaseOrder->id);
				}
			} else {
				$this->Session->setFlash(__('The purchase order could not be saved. Please try again.'));
			}
		} else {
			$this->request->data['PurchaseOrder']['project_id'] = $projectId;
			$this->request->data['PurchaseOrder']['quantity'] = 1;
			$this->request->data['PurchaseOrder']['excess_tonage'] = 50;
			$this->setReturnUrl($this->referer(), true);
		}

		$this->set('selectedProjectId', $projectId);
		$this->set('projects',$this->PurchaseOrder->Project->find('all',array('order'=>'Project.name','conditions'=>$projectSearchConditions)));
		$this->set('suppliers',$this->PurchaseOrder->Supplier->find('list',array('order'=>'Supplier.name')));
		$this->set('types',$this->PurchaseOrder->Type->find('list',array('order'=>'Type.name')));
		$this->set('users',$this->PurchaseOrder->User->getList());
		$this->set('employees',$this->PurchaseOrder->Employee->getList());
		$this->set('returnUrl',$this->readReturnUrl());
	}

	function admin_edit($id = null, $reconcile = 0)
	{
		$this->set('title_for_layout','Purchase Orders - Edit PurchaseOrder');

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid purchase_order'));
			$this->redirect(array('action' => 'index'));
		}

		// get current PO here as may need to check whether certain values have changed in submitted data
		$po = $this->PurchaseOrder->getPurchaseOrder($id);

		if (!empty($this->request->data)) {
			if ((!empty($this->request->data['PurchaseOrder']['type_id']) && $this->request->data['PurchaseOrder']['type_id'] == 2)
				|| (empty($this->request->data['PurchaseOrder']['type_id']) && $po['PurchaseOrder']['type_id'] == 2)) {
				unset($this->PurchaseOrder->validate['end_date']);
			}

			// if End Date has been changed then must record a reason in the Notes field
			$endDateUpdated = false;
			$failedToUpdateNotes = false;
			if ($this->request->data['PurchaseOrder']['end_date'] != $po['PurchaseOrder']['end_date']) {
				$endDateUpdated = true;
				if ($this->request->data['PurchaseOrder']['notes'] == $po['PurchaseOrder']['notes']) {
					$failedToUpdateNotes = true;
					$this->PurchaseOrder->validationErrors['notes'] = "Please record the reason for changing the End Date";
				}
			}

			if (isset($this->request->data['confirm'])) {
				$this->request->data['PurchaseOrder']['confirmed'] = 1;
				$this->request->data['PurchaseOrder']['confirmed_by_user_id'] = $this->currentUser['User']['id'];
			}

			if (!$failedToUpdateNotes && $this->PurchaseOrder->saveAll($this->request->data)) {
				if (isset($this->request->data['confirm'])) {
					$this->_sendemail($id);
					$this->Session->setFlash(__('The purchase order has been confirmed'));
				} else {
					$this->Session->setFlash(__('The purchase order has been saved'));

					// If PO end date has been updated then email Goldhill only
					if ($endDateUpdated) {
						$this->_sendemail($id, true);
					}
				}

				if (isset($this->request->data['reconcile'])) {
					$this->redirect(array('action'=>'admin_add_invoice', $id));
				} else {
					$this->redirect($this->getReturnUrl() . '#po-list');
				}
			} else {
				// Make sure full PO data is in request in case an Office user has submitted form (as most fields are disabled)
				// Also make sure that changed data overwrites existing data by making changed data the 2nd argument in array_merge
				$po['PurchaseOrder'] = array_merge($po['PurchaseOrder'], $this->request->data['PurchaseOrder']);
				$this->request->data = $po;
				$this->Session->setFlash(__('The purchase order could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $po;

			// only set return url to referring page if have not come from the reconcile / add_invoice page
			if (strpos($this->referer(), '/purchase_orders/add_invoice') === false
					&& strpos($this->referer(), '/purchase_orders/add') == false) {
				$this->setReturnUrl($this->referer(), true);
			}
			
			if($po['PurchaseOrder']['invoiced'] >= $po['PurchaseOrder']['value']){
				$this->request->data['PurchaseOrder']['complete'] = 1;
			}
		}

		$projectId = !empty($this->request->data['PurchaseOrder']['project_id']) ? $this->request->data['PurchaseOrder']['project_id'] : 0;
		$this->set('selectedProjectId', $projectId);
		$projectSearchConditions = array('or' => array('Project.archived' => 0, 'Project.id ' => $projectId));

		$this->set('po_is_confirmed', $po['PurchaseOrder']['confirmed']);

		$this->set('projects',$this->PurchaseOrder->Project->find('all',array('order'=>'Project.name','conditions'=>$projectSearchConditions)));
		$this->set('suppliers',$this->PurchaseOrder->Supplier->find('list',array('order'=>'Supplier.name')));
		$this->set('types',$this->PurchaseOrder->Type->find('list',array('order'=>'Type.name')));
		$this->set('users',$this->PurchaseOrder->User->getList());
		$this->set('employees',$this->PurchaseOrder->Employee->getList());
		$this->set('files', glob(APP . "/tmp/purchase_order/$id/*"));
		$this->set('returnUrl',$this->readReturnUrl());
	}
	
	function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for purchase order'));
			$this->redirect(array('action'=>'index'));
		}
		
		$purchase_order = $this->PurchaseOrder->getPurchaseOrder($id);
		
		$this->set('purchase_order',$purchase_order);
	}

	function admin_delete($id = null)
	{
//		if (!$id) {
//			$this->Session->setFlash(__('Invalid id for purchase order'));
//			$this->redirect($this->referer());
//		}
//		if ($this->PurchaseOrder->delete($id)) {
//			$this->Session->setFlash(__('Purchase Order deleted'));
//			$this->redirect($this->referer());
//		}
		$this->Session->setFlash(__('Purchase Order was not deleted'));
		$this->redirect($this->referer());
	}

	function admin_set_returned($id, $value)
	{
		if (!$id || !($po = $this->PurchaseOrder->findById($id))) {
			$this->Flash->failure(__('Invalid purchase_order'));
			$this->redirect($this->referer());
		}

		$this->PurchaseOrder->id = $id;
		if ($this->PurchaseOrder->saveField('returned', $value)) {
			$this->Session->setFlash(__('Purchase Order updated successfully'), 'flash_success');
		} else {
			$this->Session->setFlash(__('Purchase Order could not be updated'), 'flash_failure');
		}
		$this->redirect($this->referer() . '#po-list');
	}

	function admin_add_invoice($id)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for purchase order'));
			$this->redirect($this->referer());
		}
		
		$this->set('title_for_layout','Purchase Orders - Add Invoice');
		if (!empty($this->request->data)) {
			
			$po = $this->PurchaseOrder->getPurchaseOrder($id);
			
			if ($this->userIsAdmin == false) {
				if (($po['PurchaseOrder']['invoiced'] + $this->request->data['PurchaseOrderInvoice']['value']) > $po['PurchaseOrder']['value']) {
					$this->PurchaseOrder->PurchaseOrderInvoice->invalidate('value','The total invoices raised against the PO exceeds the PO total');
				}
			}
			
			if ($this->PurchaseOrder->PurchaseOrderInvoice->validates()) {
				$this->PurchaseOrder->PurchaseOrderInvoice->create();
				
				if ($this->PurchaseOrder->PurchaseOrderInvoice->save($this->request->data)) {
					
					$this->Session->setFlash(__('The purchase order invoice has been saved'));
					$this->redirect($this->getReturnUrl());
				} else {
					$this->Session->setFlash(__('The purchase order could not be saved. Please try again.'));
				}
			}
		} else {
			$this->request->data['PurchaseOrderInvoice']['purchase_order_id'] = $id;
			$this->setReturnUrl($this->referer(), true);
		}
	}
	
	function admin_edit_invoice($invoiceId)
	{
		if (!$invoiceId) {
			$this->Session->setFlash(__('Invalid id for purchase order'));
			$this->redirect($this->referer());
		}
		
		$this->set('title_for_layout','Purchase Orders - Edit Invoice');
		if (!empty($this->request->data)) {
			
			if ($this->PurchaseOrder->PurchaseOrderInvoice->save($this->request->data)) {			
				$this->Session->setFlash(__('The purchase order invoice has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The purchase order could not be saved. Please try again.'));
			}
		} else {
			$this->request->data = $this->PurchaseOrder->PurchaseOrderInvoice->get($invoiceId);
			$this->setReturnUrl($this->referer(), true);
		}
	}
	
	function admin_delete_invoice($invoiceId)
	{
		if (!$invoiceId) {
			$this->Session->setFlash(__('Invalid id for invoice'));
			$this->redirect($this->referer());
		}
		if ($this->PurchaseOrder->PurchaseOrderInvoice->delete($invoiceId)) {
			$this->Session->setFlash(__('Purchase Order Invoice deleted'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Purchase Order Invoice was not deleted'));
		$this->redirect($this->referer());
	}
	
	function _sendemail($id, $emailGoldhillOnly = false)
	{
		App::uses('CakeEmail', 'Network/Email');
		
		$po = $this->PurchaseOrder->getPurchaseOrder($id);
		
		$toEmail = $this->settings['admin_email'];
		
		$email = new CakeEmail();
		$email->config('smtp')
			->template('po_confirmation', 'default') //I'm assuming these were created
			->emailFormat('html')
			->to($toEmail)
			->bcc($_SERVER['SERVER_PORT'] == '8888' ? 'andrew+goldhill_po_bcc@brightwhitespace.co.uk' : 'james@goldhillcontracting.co.uk')
			->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
			->replyTo('james@goldhillcontracting.co.uk')
			->subject('Purchase Order '.$po['PurchaseOrder']['ref'].': '.$this->settings['site_name'])
			->viewVars(array(
				'po' => $po,
				'url' => 'http://'.$_SERVER['HTTP_HOST']
			)
		);

		if (!$emailGoldhillOnly) {
			if (!empty($po['Contact']['email'])) {
				$email->bcc($_SERVER['SERVER_PORT'] == '8888' ? 'andrew+goldhill_po_contact@brightwhitespace.co.uk' : $po['Contact']['email']);
			} else if (!empty($po['Supplier']['email'])) {
				$email->bcc($_SERVER['SERVER_PORT'] == '8888' ? 'andrew+goldhill_po_supplier@brightwhitespace.co.uk' : $po['Supplier']['email']);
			}
		}

		$email->send();
	}
	
}
