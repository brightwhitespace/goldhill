<?php
class VehiclesController extends AppController {

	var $name = 'Vehicles';
	var $layout = 'admin';
	var $uses = array('Vehicle', 'Employee', 'VehiclesEmployee', 'Project');

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			$conditions['or'] = array(
				'Vehicle.reg_no LIKE' => '%'.$this->data['Search']['term'].'%',
				'Vehicle.make LIKE' => '%'.$this->data['Search']['term'].'%',
				'Vehicle.model LIKE' => '%'.$this->data['Search']['term'].'%',
			);
		}

		$this->paginate = array('conditions' => $conditions, 'contain' => 'Tool');
		$this->Vehicle->recursive = 0;
		$this->set('vehicles', $this->paginate());
	}

	function admin_add()
	{
		$this->set('title_for_layout','Vehicles - Add New Vehicle');

		if (!empty($this->request->data)) {
			$this->Vehicle->create();

			if ($this->Vehicle->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The vehicle has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vehicle could not be saved. Please try again.'));
			}
		}
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Vehicles - Edit Vehicle');

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid vehicle'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Vehicle->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The vehicle has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vehicle could not be saved. Please try again.'));
			}

		} else {
			$this->request->data = $this->Vehicle->read(null, $id);
		}
	}

	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for vehicle'));
			$this->redirect(array('action'=>'index'));
		}

		$vehicle = $this->Vehicle->get($id);

		$this->set('vehicle', $vehicle);
		$this->set('files', glob(APP . "tmp/vehicle/$id/*"));
	}

	function admin_archive($id, $archive = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for vehicle'));
			$this->redirect($this->referer());
		}

		$this->Vehicle->id = $id;
		$this->Vehicle->saveField('archived', $archive);
		$this->Session->setFlash(__('Vehicle was ' . ($archive ? 'archived' : 'restored from archive')));

		$this->redirect($this->referer());
	}

}
