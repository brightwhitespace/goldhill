<?php
class PhotosController extends AppController {

	var $name = 'Photos';
	var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler', 'Upload','PImage');
	var $pageTitle = 'Photos';

	function admin_index($projectId = null,$productId = null,$galleryId = null,$projectCategoryId = null,$postId = null) {
		if($this->RequestHandler->isAjax()){
			$this->layout = 'ajax';
		}else{
			$this->layout = 'admin';
		}
		
		if($projectId){
			$options['conditions'] = array(
				'Photo.project_id' => $projectId
			);
		}
		
		if($productId){
			$options['conditions'] = array(
				'Photo.product_id' => $productId
			);
		}
		
		if($galleryId){
			$options['conditions'] = array(
				'Photo.gallery_id' => $galleryId
			);
		}
		
		if($postId){
			$options['conditions'] = array(
				'Photo.post_id' => $postId
			);
		}
		
		if($projectCategoryId){
			$options['conditions'] = array(
				'Photo.project_category_id' => $projectCategoryId
			);
		}
		
		$options['order'] = array('Photo.order_num');
		
		$photos = $this->Photo->find('all',$options);
		
		$this->set('photos',$photos);
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->flash(__('Invalid Photo', true), array('action'=>'index'));
		}
		$this->set('photo', $this->Photo->read(null, $id));
	}

	function admin_add() {
		$this->layout = 'admin';
		$this->pageTitle .= ' - add new';
		$this->set('id_for_layout',-1);
	
		if (!empty($this->data)) {			
		
			$this->Photo->create();
			if ($this->Photo->save($this->data)) {
				$this->Session->setFlash('Photo added');
				$this->redirect($this->getReturnUrl('/admin/'));
			} else {
			}			
		}else{
			$this->setReturnUrl($this->referer());
			
			if(isset($this->request->params['named']['product_id'])){
				$this->request->data['Photo']['product_id'] = $this->request->params['named']['product_id'];
			}
			if(isset($this->request->params['named']['gallery_id'])){
				$this->request->data['Photo']['gallery_id'] = $this->request->params['named']['gallery_id'];
			}
			if(isset($this->request->params['named']['project_id'])){
				$this->request->data['Photo']['project_id'] = $this->request->params['named']['project_id'];
			}
		}
	}

	function admin_edit($id = null) {
		$this->layout = 'admin';
		$this->pageTitle .= ' - edit';
		$this->set('options_for_layout','products');
		$this->set('id_for_layout',-1);
	
		if (!$id && empty($this->data)) {
			$this->flash(__('Invalid Photo', true), array('action'=>'index'));
		}
		if (!empty($this->data)) {
		
			if ($this->Photo->save($this->data)) {
				$this->Session->setFlash('Photo edited');
				$this->redirect($this->getReturnUrl('/admin/products/'));
			} else {
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Photo->read(null, $id);
			$this->setReturnUrl($this->referer());
		}
	}

	function admin_delete($id = null) {
		
		if($this->RequestHandler->isAjax()){
			$this->layout = 'ajax';
		}
		
		$photo = $this->Photo->read(null,$id);
				
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$photo['Photo']['image_thumb'])){
			unlink($_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$photo['Photo']['image_thumb']);
		}
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$photo['Photo']['image_src'])){
			unlink($_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$photo['Photo']['image_src']);
		}
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$photo['Photo']['image_enlargement'])){
			unlink($_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$photo['Photo']['image_enlargement']);
		}
		
		if ($this->Photo->delete($id)) {
			if($this->RequestHandler->isAjax()){
				echo "true";
			}else{
				$this->Session->setFlash('Photo deleted');
				$this->redirect($this->referer().'#two');
			}
		}
		
		exit();
	}
	
	function admin_setdefault($id = null,$mid=null) {
		if (!$id) {
			$this->flash(__('Invalid Photo', true), '/admin/individuals/view/'.$mid);
		}
		$photos = $this->Photo->find('all',array('conditions'=>array("parent_id = $mid")));
		foreach($photos as $photo){
			if($photo['Photo']['id'] == $id){
				$photo['Photo']['default'] = 1;
			}else{
				$photo['Photo']['default'] = 0;
			}
			$this->Photo->save($photo);
		}
		$this->Session->setFlash('Default photo changed');
		$this->redirect($this->referer());
	}
	
	function admin_upload($projectId = null,$productId = null,$galleryId = null,$projectCategoryId = null,$postId = null){		
		
		App::uses('Upload','Lib');
		
		if($this->RequestHandler->isAjax()){
			$this->layout = 'ajax';
		}
		
		if(!$this->RequestHandler->isAjax()){
			$this->request->params['form']['file'] = $this->request->params['form']['multiUpload'];
		}
		
		$adjustType = 'resize';
		
		if($projectId){
			$type = 'project';
		}
		
		if($productId){
			$type = 'product';
			$adjustType = 'resize';
		}
		
		if($galleryId){
			$type = 'gallery';
		}
		
		if($postId){
			$type = 'post';
		}
		
		if($projectCategoryId){
			$type = 'project_category';
			$adjustType = 'resizeWidthCrop';
		}
		
		if(!empty($this->request->params['form']['file'])){
		
		
			$folder = '/files/images/'.$type.'/';	
			$destination = $_SERVER['DOCUMENT_ROOT'].'/app/webroot'.$folder;
			$this->Upload->createfolder($destination);
			
			$file = $this->request->params['form']['file'];
			
			
			
			//$result = $this->Upload->upload($file, $destination, null, array('type' => 'resizecrop', 'size' => array('400', '300'), 'output' => 'jpg'));
			$result = $this->Upload->upload($file, $destination, null, null,array('jpg','jpeg','gif','png','bmp'));
			
			if (!$this->Upload->errors){
				
				$newFileName= $this->Upload->result;				
				
				$existingFilename = substr($newFileName,0,strlen($newFileName)-4);
				$ext = $this->Upload->ext($newFileName);
				//new file names
				$thumbnail = $existingFilename.'_th.'.$ext;	
				$thumbSize = split(",",$this->settings[$type.'_image_thumb_size']);
				$medium = $existingFilename.'_med.'.$ext;	
				$mediumSize = split(",",$this->settings[$type.'_image_medium_size']);
				$large = $existingFilename.'_lg.'.$ext;
				$largeSize = split(",",$this->settings[$type.'_image_large_size']);
				$max = $existingFilename.'_mx.'.$ext;
				$maxSize = split(",",$this->settings[$type.'_image_max_size']);
				
				$uploader = new Upload($destination.$newFileName);			
				//thumb size image
				//$this->PImage->resizeImage('resizeCrop',$newFileName,$destination,$thumbnail,$thumbSize[0],$thumbSize[1],80,false);
				$uploader->file_new_name_body = $existingFilename.'_th';
				$uploader->image_resize = true;
				$uploader->image_x = $thumbSize[0];
				$uploader->image_y = $thumbSize[1];
				$uploader->image_ratio_crop = true;
				$uploader->image_ratio_fill = true;
				$uploader->image_background_color = '#FFFFFF';
				$uploader->jpeg_quality = 70;
				$uploader->Process($destination);
				$thumbnail = $uploader->file_dst_name_body.'.'.$uploader->file_dst_name_ext;
				
				
				//medium size image
				//$this->PImage->resizeImage($adjustType,$newFileName,$destination,$medium,$mediumSize[0],$mediumSize[1],80,false);
				$uploader->file_new_name_body = $existingFilename.'_med';
				$uploader->image_resize = true;
				$uploader->image_x = $mediumSize[0];
				$uploader->image_y = $mediumSize[1];
				
				$uploader->image_ratio_fill = true;
				$uploader->image_background_color = '#FFFFFF';
				$uploader->jpeg_quality = 70;
				$uploader->Process($destination);
				$medium = $uploader->file_dst_name_body.'.'.$uploader->file_dst_name_ext;
				
				
				//large image settings
				//$this->PImage->resizeImage($adjustType,$newFileName,$destination,$large,$largeSize[0],$largeSize[1],80,false);
				$uploader->file_new_name_body = $existingFilename.'_lg';
				$uploader->image_resize = true;
				$uploader->image_x = $largeSize[0];
				$uploader->image_y = $largeSize[1];
				
				$uploader->image_ratio_fill = true;
				$uploader->image_background_color = '#FFFFFF';
				$uploader->jpeg_quality = 70;
				$uploader->Process($destination);
				$large = $uploader->file_dst_name_body.'.'.$uploader->file_dst_name_ext;
				//max image settings
				//$this->PImage->resizeImage($adjustType,$newFileName,$destination,$max,$maxSize[0],$maxSize[1],80,false);
				$uploader->file_new_name_body = $existingFilename.'_mx';
				$uploader->image_resize = true;
				$uploader->image_x = $maxSize[0];
				$uploader->image_y = $maxSize[1];
				$uploader->image_ratio_fill = true;
				$uploader->image_background_color = '#FFFFFF';
				$uploader->jpeg_quality = 70;
				$uploader->Process($destination);
				$max = $uploader->file_dst_name_body.'.'.$uploader->file_dst_name_ext;
				
				
				$data['Photo']['image_thumb'] = $folder.$thumbnail;
				$data['Photo']['image_src'] = $folder.$medium;
				$data['Photo']['image_enlargement'] = $folder.$large;
				$data['Photo']['original'] = $folder.$max;
				
				
				
				if($projectId){
					$data['Photo']['project_id'] = $projectId;
				}
				
				if($productId){
					$data['Photo']['product_id'] = $productId;
				}
				
				if($galleryId){
					$data['Photo']['gallery_id'] = $galleryId;
				}
				
				if($postId){
					$data['Photo']['post_id'] = $postId;
				}
				
				if($projectCategoryId){
					$data['Photo']['project_category_id'] = $projectCategoryId;
				}
				
				$this->Photo->create();
				if ($this->Photo->save($data)) {
					if(!$this->RequestHandler->isAjax()){
						$this->Session->setFlash('Photo uploaded succesfully');
						$this->redirect($this->referer());
					}
					echo $this->request->data['index'];					
				} else {
					if(!$this->RequestHandler->isAjax()){
						$this->Session->setFlash('Photo upload failed');
						$this->redirect($this->referer());
					}
					echo 0;	
				}
				
				
			}else{
				if(!$this->RequestHandler->isAjax()){
					$this->Session->setFlash('Photo upload failed');
					$this->redirect($this->referer());
				}
			}
			
		}
		
		
		exit();
	}
	
	function admin_changeorder(){
		$order_num = 0;
		foreach ($this->data['photo'] as $id => $parent){
			$this->request->data['Photo']['order_num'] = $order_num;
			//$this->request->data['Advert']['parent_id'] = $parent;
			$this->request->data['Photo']['id'] = $id;
			
			$this->Photo->save($this->request->data,false);
			$order_num ++;
		}
		
		
		$this->layout = 'ajax';
		
		exit();
	}
	

}
?>