<?php
class FilesController extends AppController {

	var $name = 'Files';
	var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler', 'Upload','PImage');
	var $uses = array('File');

	function admin_upload($objectType, $objectId){

		App::uses('Upload','Lib');

		if ($this->request->is('ajax')) {
			$this->layout = 'ajax';
		} else {
			$this->request->params['form']['file'] = $this->request->params['form']['multiUpload'];
		}

		if (!empty($this->request->params['form']['file'])) {

			$destination = $_SERVER['DOCUMENT_ROOT'] . '/app/tmp/' . $objectType . DS . $objectId . DS;
			$this->Upload->createfolder($destination);

			$file = $this->request->params['form']['file'];

			$imageTypes = array('jpg','jpeg','gif','png','bmp');
			$docTypes = array('txt', 'rtf', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'eml', 'zip');

			switch ($objectType) {
				default:
					$allowedTypes = array_merge($imageTypes, $docTypes);
			}

			$result = $this->Upload->upload($file, $destination, null, null, $allowedTypes);

			if (!$this->Upload->errors){
				//
			}

			echo $this->request->data['index'];
		}

		exit();
	}

	function admin_rename($objectType, $objectId, $filename) {

		$newFilename = preg_replace('/[^a-zA-Z0-9\._ -]/', '', $this->request->data['value']);

		$oldFilepath = $_SERVER['DOCUMENT_ROOT'] . '/app/tmp/' . $objectType . DS . $objectId . DS . urldecode($filename);
		$newFilepath = $_SERVER['DOCUMENT_ROOT'] . '/app/tmp/' . $objectType . DS . $objectId . DS . $newFilename;

		if (rename($oldFilepath, $newFilepath)) {
			echo $newFilename;
		} else {
			echo $filename;
		}

		exit();
	}

	function admin_view($objectType, $objectId, $filename) {

		// Need to import the File utility in Cake else throws error
		App::uses('File','Utility');

		$filepath = $_SERVER['DOCUMENT_ROOT'] . '/app/tmp/' . $objectType . DS . $objectId . DS . urldecode($filename);

//		var_dump($filepath);
//		exit;

		// store mime type for "eml" (Outlook) files so Safari doesn't add ".html" to it
		$this->response->type(array('eml' => 'message/rfc822'));

		$this->response->file($filepath, array('download' => true));

		return $this->response;
	}

	function admin_delete($objectType, $objectId, $filename) {

		unlink(APP . "/tmp/$objectType/$objectId/" . urldecode($filename));

		$this->redirect($this->referer());
	}

	function admin_updateAfterUpload($objectType, $objectId){

		$this->layout = 'ajax';

		$this->set('files', glob(APP . "/tmp/$objectType/$objectId/*"));
		$this->set('objectType', $objectType);
		$this->set('objectId', $objectId);
		$this->set('allowDelete', $this->userIsAdmin);
	}


}
