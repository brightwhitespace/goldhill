<?php
class TodosController extends AppController {

	var $name = 'Todos';
	var $layout = 'admin';

	function admin_index($userId = null)
	{
		$conditions = array();

		if ($userId) {
			$this->request->data['Search']['user_id'] = $userId;
		}

		if ($this->userIsAdmin) {
			// admin user will be able to optionally choose a user, so set list of users in view...
			$this->set('users', $this->User->getList(array('User.access_group_id != ' . AccessGroup::EMPLOYEE)));

			// if user has been selected then restrict to that user's tasks only, otherwise show all assigned tasks
			if ($userId || !empty($this->data['Search']['user_id'])) {
				$conditions['Todo.user_id'] = $userId ? $userId : $this->data['Search']['user_id'];
			} else {
				$conditions[] = 'Todo.user_id IS NOT NULL';
			}

		} else {
			// not admin user so restrict to current user's tasks only
			$conditions['Todo.user_id'] = $this->currentUser['User']['id'];
		}

		// handle any other search data...
		$this->processSearchData();

		// if want to clear search
		if (!empty($this->request->query['clear'])) {
			$this->redirect('/admin/todos');
		}

		if (!empty($this->data['Search']['term'])) {
			$conditions['Todo.description LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		// archived flag will always be set
		$archivedFlag = !empty($this->data['Search']['archived']) ? true : false;
		$conditions['Todo.archived'] = $archivedFlag;

		$this->paginate = array('conditions' => $conditions);
		$this->Todo->recursive = 0;

		$this->set('todos', $this->paginate());
		$this->set('unassignedTasks', $this->Todo->getTodos(false, $archivedFlag));

	}

	function admin_add()
	{
		$this->set('title_for_layout','Todos - Add New Todo');
		if (!empty($this->request->data)) {
			$this->Todo->create();

			$this->request->data['Todo']['created_by_user_id'] = $this->currentUser['User']['id'];
			$this->_setStatusRelatedData();

			if ($this->Todo->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The todo has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The todo could not be saved. Please try again.'));
			}
		}

		$this->set('users', $this->User->getList(array('User.access_group_id != ' . AccessGroup::EMPLOYEE)));
		$this->set('emptyUserOption', 'Unassigned');
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Todos - Edit Todo');

		$todo = $this->Todo->getTodo($id);

		if (!$todo) {
			$this->Session->setFlash(__('Invalid todo'));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {

			$this->_setStatusRelatedData();
			if ($this->Todo->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The todo has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The todo could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Todo->getTodo($id);
		}

		$this->set('users', $this->User->getList(array('User.access_group_id != ' . AccessGroup::EMPLOYEE)));
		$this->set('emptyUserOption', 'Unassigned');
	}

	function _setStatusRelatedData()
	{
		// initially nullify fields related to "in progress" and "completed", and then set appropriately based on the chosen status
		$this->request->data['Todo']['in_progress_by_user_id'] = null;
		$this->request->data['Todo']['completed_date'] = null;
		$this->request->data['Todo']['completed_by_user_id'] = null;
		switch ($this->request->data['Todo']['status']) {
			case Todo::STATUS_IN_PROGRESS:
				$this->request->data['Todo']['in_progress_by_user_id'] = $this->currentUser['User']['id'];
				break;
			case Todo::STATUS_COMPLETED:
				$this->request->data['Todo']['completed_date'] = date('Y-m-d H:i:s');
				$this->request->data['Todo']['in_progress_by_user_id'] = null;
				$this->request->data['Todo']['completed_by_user_id'] = $this->currentUser['User']['id'];
				break;
		}
	}

	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for todo'));
			$this->redirect(array('action'=>'index'));
		}
		
		$todo = $this->Todo->getTodo($id);
		
		$this->set('todo',$todo);
		$projects = $this->Todo->Project->getLatest($id);
		$this->set('projects',$projects);
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for todo'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Todo->delete($id)) {
			$this->Session->setFlash(__('Todo deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Todo was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	function admin_complete($id = null, $completeFlag)
	{
		$this->layout = 'ajax';

		$this->Todo->id = $id;
		if ($completeFlag) {
			$this->Todo->saveField('status', 'C');
			$this->Todo->saveField('in_progress_by_user_id', null);
			$this->Todo->saveField('completed_date', date('Y-m-d H:i:s'));
			$this->Todo->saveField('completed_by_user_id', $this->currentUser['User']['id']);
		} else {
			$this->Todo->saveField('status', 'P');
			$this->Todo->saveField('completed_date', null);
			$this->Todo->saveField('completed_by_user_id', null);
		}
	}

	function admin_archive($id = null, $archiveFlag)
	{
		$todo = $this->Todo->getTodo($id);

		if (!$todo) {
			$this->Session->setFlash(__('Invalid id for todo'));
		}

		$this->Todo->id = $id;
		$this->Todo->saveField('archived', $archiveFlag);

		if (!$this->request->is('ajax')) {
			$this->redirect(array('action' => 'index'));
		}
	}

}
