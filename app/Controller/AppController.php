<?php

class AppController extends Controller
{
	var $uses = array('AppModel','Page','Setting','User','Todo','Project','Storage','Employee','Certificate','Tool','Timesheet','Quote','WageAdjustment');
	var $helpers = array('Html','Form','User','Number','Session','Combinator','AdminCombinator','Ckeditor','Tree','Time','Pages','Brightform','App');
	var $components = array('Acl','Auth','Session','Cookie');//,'Security');
	var $settings;

	var $currentUser;
	var $loggedInUsingCookie = false;
	var $currentGroup;


	var $userIsAdmin = false;
	var $userIsOffice = false;		// true when Access Group == 'Office' OR 'Office Admin'
	var $userIsOfficeAdmin = false;	// true when Access Group == 'Office Admin'
	var $userIsEmployee = false;
	var $userIsSubcontractor = false;

	var $storageRecords = null;
	

	function beforeFilter()
	{
		$this->checkAuth();
		$this->loadGlobals();

		// Call the processes that run on every request (no need to do this for ajax calls)
		if (!$this->request->is('ajax')) {
			// get Storage records and re-index by name for easy access
			$this->storageRecords = $this->Storage->find('all');
			$this->storageRecords = Hash::combine($this->storageRecords, '{n}.Storage.name', '{n}');

			$this->checkForCertificateExpiry();

			$this->processQuoteReminders();

			$this->processProjectStatusEmail();

			$this->processDamageRepayments();

			$this->processHoursWorkedReports();
		}

		parent::beforeFilter();
	}
	
	
	// ------------- CUSTOM FUNCTIONS ----------------------- //
	
	function checkAuth()
	{
		//check debug for test and allow all access		
		//$this->Auth->allow('*');
		
		$reUrl = $this->Session->read('reURL');
		
		if (isset($this->Auth)) {
			$this->Auth->authenticate = array(
				'Form' => array('userModel' => 'User')
			);
			$this->Auth->loginAction = '/users/login';
			
			$this->Auth->autoRedirect = true;
			
			/*if(!empty($reUrl)){
				$this->Auth->loginRedirect = $reUrl;				
			}else{
				$this->Auth->loginRedirect = '/';
			}*/
			$this->Auth->logoutRedirect = '/users/login';
			$this->Auth->authError = ''; //"Please login to continue...";
			$this->Auth->authorize = 'actions';
			// What to say when the log in was incorrect.
			$this->Auth->loginError = 'No matching account found. If you have forgotten your password, click the "Forgot Password" link.'; 
		} 

		$this->currentUser = $this->Auth->user();
		
		// If user not logged in then check for a "remember me" cookie
		if (!$this->currentUser && ($rememberMeRef = $this->Cookie->read('remember_me_ref'))) {
			$rememberMeRefParts = explode('-', $rememberMeRef, 2);
			$userId = $rememberMeRefParts[0];
			$ref = $rememberMeRefParts[1];
			if (($checkUser = $this->User->find('first',
							array('conditions'=>array('id'=>$userId, 'remember_me_ref'=>$ref))))) {
				//got match, log user in using unique ref as password
				$this->Auth->fields = array('username'=>'username', 'password'=>'remember_me_ref');
				if ($this->Auth->login($checkUser)) {
					$this->currentUser = $this->Auth->user();
					$this->loggedInUsingCookie = true;
				}
			}
		}
		
		// Copy user data into currentUser['User']
		if ($this->currentUser && !isset($this->currentUser['User'])) {
			$this->currentUser['User'] = $this->currentUser;
		}
		
		$this->currentGroup = $this->User->AccessGroup->findById($this->currentUser['User']['access_group_id']);
		
		if (!empty($this->currentGroup)) {
			if ($this->Acl->check($this->currentGroup['AccessGroup']['group_name'],$this->name.'/'.$this->request->action)) {
				$this->Auth->allow($this->request->action);
			}
			else {
				$this->Auth->deny($this->request->action);
			}
		}
		else if ($this->Acl->check('Anonymous',$this->name.'/'.$this->request->action)) {
			$this->Auth->allow($this->request->action);
		}
		$this->set('currentUser', $this->currentUser);
		$this->set('currentGroup', $this->currentGroup);

		if (!empty($this->currentGroup)) {
			if ($this->currentGroup['AccessGroup']['group_name'] == 'Superadmin' || $this->currentGroup['AccessGroup']['group_name'] == 'Admin') {
				$this->userIsAdmin = true;
			} else if ($this->currentGroup['AccessGroup']['group_name'] == 'Office Admin') {
				$this->userIsOffice = true;
				$this->userIsOfficeAdmin = true;
			} else if ($this->currentGroup['AccessGroup']['group_name'] == 'Office') {
				$this->userIsOffice = true;
				$this->userIsOfficeAdmin = false;
			} else if ($this->currentGroup['AccessGroup']['group_name'] == 'Employee') {
				$this->userIsEmployee = true;
			} else if ($this->currentGroup['AccessGroup']['group_name'] == 'Sub-contractor') {
				$this->userIsSubcontractor = true;
			}
		}
		$this->set('userIsAdmin', $this->userIsAdmin);
		$this->set('userIsOffice', $this->userIsOffice);
		$this->set('userIsOfficeAdmin', $this->userIsOfficeAdmin);
		$this->set('userIsEmployee', $this->userIsEmployee);
		$this->set('userIsSubcontractor', $this->userIsSubcontractor);
	}
		
	function loadGlobals()
	{
		// need these to make sure classes are loaded
		App::uses('Invoice', 'Model');
		App::uses('Project', 'Model');

		// global variables - can be accessed from anywhere
		Configure::write('userIsAdmin', $this->userIsAdmin);
		Configure::write('minAgencyHoursWorked', $this->_getSetting('min_agency_hours_worked', 4));

		$this->settings = $this->Setting->find('list', array('fields'=>array('Setting.name', 'Setting.value')));
		$this->_populateDefaultSettings();
		$this->set('settings_for_layout',$this->settings);
		
		//get all pages
		$this->set('topmenu_for_layout',$this->Page->getMenuPages('Top Menu', false));
		
		//get main menu pages
		$this->set('mainmenu_for_layout',$this->Page->getMenuPages('Main Menu', true, false, true));
		
		//get footer menu
		$this->set('footermenu_for_layout',$this->Page->getMenuPages('Footer', false));
		
		//get snippets
		$this->snippets = $this->Page->Snippet->find('list', array('fields'=>array('Snippet.name', 'Snippet.snippet'),'conditions'=>array('Snippet.page_id'=>0)));
		$this->set('snippets_for_layout',$this->snippets);

		// get user's uncompleted todos
		$this->set('userTodos', $this->Todo->getTodos($this->currentUser['User']['id'], false, false));

		// get unassigned tools
		$this->set('unassignedTools', $this->Tool->findByToolStatusId(Tool::STATUS_UNASSIGNED));
	}

	function checkForCertificateExpiry()
	{
		$today = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
		$lastRunRecord = $this->storageRecords['last_certificate_expiry_check_date'];

		if ($lastRunRecord) {
			$lastRunDate = DateTime::createFromFormat('Y-m-d', $lastRunRecord['Storage']['value_text']);
			if ($lastRunDate < $today) {

				// get all certificates as will need to reference their names and expiry warning times (re-index list by id to help)
				$certificates = $this->Certificate->find('all');
				$certificates = Hash::combine($certificates, '{n}.Certificate.id', '{n}');

				// get all current employees and their certificate records
				$employees = $this->Employee->getEmployees('all', false, array('CertificatesEmployee'));

				// For each employee, set the key of each CertificatesEmployee record they have to be the certificate_id
				// NOTE: this process will automatically filter out older records that have the same certificate id
				foreach ($employees as $idx => $employee) {
					$employees[$idx]['CertificatesEmployee'] = Hash::combine($employee['CertificatesEmployee'], '{n}.certificate_id', '{n}');
				}

				$certsToNotify = array();
				foreach ($employees as $employee) {
					foreach ($employee['CertificatesEmployee'] as $employeeCert) {
						if ($employeeCert['end_date']) {
							$certificate = $certificates[$employeeCert['certificate_id']];
							$endDate = DateTime::createFromFormat('Y-m-d', $employeeCert['end_date']);
							$daysUntilExpiry = $today->diff($endDate)->days * (($endDate < $today) ? -1 : 1);
							$warningDays = $certificate['Certificate']['expiry_warning_days'];

							// Notify if cert has expired OR (cert expiry falls within warning days AND has not already been notified)
							if ($daysUntilExpiry < 0 || ($daysUntilExpiry <= $warningDays && !$employeeCert['notified_expiry_on'])) {
								$certsToNotify[] = array(
									'employee_name' => $employee['Employee']['name'],
									'cert_name' => $certificate['Certificate']['name'],
									'expiry_date' => $endDate->format('jS M Y'),
									'has_expired' => $daysUntilExpiry < 0,
								);

								$this->Employee->CertificatesEmployee->id = $employeeCert['id'];
								$this->Employee->CertificatesEmployee->saveField('notified_expiry_on', $today->format('Y-m-d'));
							}
						}
					}
				}

				// send list of notifications to admin
				if ($certsToNotify) {
					$this->_sendCertificateExpiryNotificationsEmail($certsToNotify);
				}

				// set last run date as today
				$lastRunRecord['Storage']['value_text'] = $today->format('Y-m-d');
				$this->Storage->save($lastRunRecord);
			}
		}
	}

	function processQuoteReminders()
	{
		$todayYmd = date('Y-m-d');
		$tomorrowYmd = date('Y-m-d', strtotime('tomorrow'));
		$lastRunRecord = $this->storageRecords['last_quote_reminders_date'];

		if ($lastRunRecord) {
			$lastRunDateYmd = $lastRunRecord['Storage']['value_text'];
			if ($lastRunDateYmd < $todayYmd) {

				// set last run date as today (do this straight away as don't want another request to kick it off again)
				$lastRunRecord['Storage']['value_text'] = $todayYmd;
				$this->Storage->save($lastRunRecord);

				$reminderConditions = array(
					'Quotes Due Back Today' => array('archived' => 0, 'quote_due' => $todayYmd),
					'Site Visits Today' => array('archived' => 0, 'site_visit_date' => $todayYmd),
					'Quotes Due Back Tomorrow' => array('archived' => 0, 'quote_due' => $tomorrowYmd),
					'Site Visits Tomorrow' => array('archived' => 0, 'site_visit_date' => $tomorrowYmd),
				);

				$resultsByType = array();
				$sendEmail = false;
				foreach ($reminderConditions as $key => $conditions) {
					$resultsByType[$key] = $this->Quote->find('all', array('conditions' => $conditions, 'contain' => 'Client'));
					$sendEmail = $sendEmail || $resultsByType[$key];
				}

				// send list of notifications to admin
				if ($sendEmail) {
					$this->_sendQuoteRemindersEmail($resultsByType);
				}
			}
		}
	}

	function processProjectStatusEmail()
	{
		$todayYmd = date('Y-m-d');
		//$tomorrowYmd = date('Y-m-d', strtotime('tomorrow'));
		$lastRunRecord = $this->storageRecords['last_project_status_email_date'];

		if ($lastRunRecord) {
			$lastRunDateYmd = $lastRunRecord['Storage']['value_text'];
			if ($lastRunDateYmd < $todayYmd) {

				// set last run date as today (do this straight away as don't want another request to kick it off again)
				$lastRunRecord['Storage']['value_text'] = $todayYmd;
				$this->Storage->save($lastRunRecord);

				// make sure project statuses have been updated
				$this->_updateProjectStatuses();

				$commonConditions = array('archived' => 0, 'project_type_id' => Project::TYPE_STRIPOUT);

				$statusConditions = array(
					'Projects Starting Today' => $commonConditions + array('start_date' => $todayYmd),
					'Projects Ending Today' => $commonConditions + array('end_date' => $todayYmd),
					'Other Live Projects' => $commonConditions + array('start_date !=' => $todayYmd, 'end_date !=' => $todayYmd, 'project_status_id' => Project::STATUS_LIVE),
				);

				$resultsByType = array();
				$sendEmail = false;
				foreach ($statusConditions as $key => $conditions) {
					$resultsByType[$key] = $this->Project->find('all', array('conditions' => $conditions, 'contain' => 'Client'));
					$sendEmail = $sendEmail || $resultsByType[$key];
				}

				// send list of notifications to admin
				if ($sendEmail) {
					$this->_sendProjectStatusEmail($resultsByType);
				}
			}
		}
	}

	function processHoursWorkedReports()
	{
		$todayDayNumber = date('N');
		$processOnDayNumber = $this->_getSetting('day_to_process_hours_worked_reports', 5);  // default to Friday

		if ($processOnDayNumber == $todayDayNumber) {
			$today = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			$lastRunRecord = $this->storageRecords['last_hours_worked_reports_date'];

			if ($lastRunRecord) {
				$lastRunDate = DateTime::createFromFormat('Y-m-d', $lastRunRecord['Storage']['value_text']);
				if ($lastRunDate < $today) {

					// set last run date as today (do this straight away as process takes several seconds so don't want another request to kick it off again)
					$lastRunRecord['Storage']['value_text'] = $today->format('Y-m-d');
					$this->Storage->save($lastRunRecord);

					$employees = $this->Employee->getEmployees();

					foreach ($employees as $idx => $employee) {
						if ($employee['Employee']['last_working_day_name'] && strpos($employee['Employee']['email'], '@') !== false) {

							// employee has "last working day" set and an email address so send them their hours worked report

							// get date range based on their "last working day of the week"
							$nameOfLastDayOfWorkingWeek = $employee['Employee']['last_working_day_name'];
							$lastDayOfPreviousWorkingWeekUnixTime = strtotime("last $nameOfLastDayOfWorkingWeek");
							$endDate = DateTime::createFromFormat('d-m-Y', date('d-m-Y', $lastDayOfPreviousWorkingWeekUnixTime));

							$startDate = clone $endDate;
							$startDate->modify('-6 days');

							// If NOT live site then make any adjustments for testing
							if (!$this->_isLiveSite()) {
                                if ($idx > 4) break; // just send a few reports for testing
								$startDate->modify('-11 weeks'); // adjust dates to get some data
								$endDate->modify('-11 weeks');
							}

							$days = $this->Timesheet->getEmployeeHoursWorkedReportData($employee['Employee']['id'], $startDate, $endDate);

							$this->_sendHoursWorkedReportEmail($employee, $days, $startDate, $endDate);
						}
					}

				}
			}
		}
	}

	function processDamageRepayments()
	{
		$todayDayNumber = date('N');
		$processOnDayNumber = $this->_getSetting('day_to_process_damage_repayments', 1);  // Always runs on a monday, applies to earnings from previous week

		if ($processOnDayNumber == $todayDayNumber) {
			$today = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			$lastRunRecord = $this->storageRecords['last_damage_repayments_run_date'];

			if ($lastRunRecord) {
				$lastRunDate = DateTime::createFromFormat('Y-m-d', $lastRunRecord['Storage']['value_text']);
				if ($lastRunDate < $today) {

					// set last run date as today (do this straight away as don't want another request to kick it off again)
 					$lastRunRecord['Storage']['value_text'] = $today->format('Y-m-d');
					$this->Storage->save($lastRunRecord);

					$yesterday = DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('yesterday')));
					$sevenDaysAgo = DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('-7 days')));

					$employees = $this->Employee->getEmployees();

					foreach ($employees as $idx => $employee) {
						if ($employee['Employee']['current_damages_to_repay'] > 0 && $employee['Employee']['current_repayment'] > 0) {

							// employee has damage to repay, create a wage adjustment for yesterday so it comes off previous week -
							// need to calculate what they earnt and take max half of it up to the lowest value of the required payment / remaining damage
							// the adjustment will need to be subtracted from the remaining damage

							$employeeTimesheets = $this->Timesheet->getEmployeeTimesheetsForDateRange($employee['Employee']['id'], $sevenDaysAgo, $yesterday);

							$totalEarnings = 0;
							foreach ($employeeTimesheets as $employeeTimesheet) {
								$totalEarnings += $employeeTimesheet['EmployeesTimesheet']['cost'];
							}
							$maxDeductionFromWages = floor($totalEarnings / 100 * $this->_getSetting('max_percent_of_wages_for_damages', 50));

							$deduction = min(array(
								$maxDeductionFromWages,
								$employee['Employee']['current_damages_to_repay'],
								$employee['Employee']['current_repayment']
							));

							// Reduce damages owed by the deduction amount
							$this->Employee->id = $employee['Employee']['id'];
							$this->Employee->saveField('current_damages_to_repay', $employee['Employee']['current_damages_to_repay'] - $deduction);

							// create corresponding wage adjustment record for deduction
							$this->WageAdjustment->create();
							$this->WageAdjustment->save(array(
								'date' => $yesterday->format('Y-m-d'),
								'employee_id' => $employee['Employee']['id'],
								'value' => -$deduction,
								'description' => 'Damage Repayment',
								'is_damage_repayment' => true,
							));
						}
					}

				}
			}
		}
	}


	function _populateDefaultSettings()
	{
		// make sure following settings have default values
		$this->settings['server_name'] = $this->_getSetting('server_name', $_SERVER['HTTP_HOST']);
		$this->settings['site_url'] = $this->_getSetting('site_url', 'http://'.$_SERVER['HTTP_HOST']);

		if ($_SERVER['SERVER_PORT'] == '8888') {
			$this->settings['server_name'] = 'goldhill:8888';
			$this->settings['site_url'] = 'http://goldhill:8888';
		}
	}
	
	function _updateProjectStatuses() {
		// Update status of each project depending on whether they are:
		//  	PENDING (current date before start date)
		//		LIVE (current date within the project start/end range)
		//		COMPLETED (current date is after project end date OR project is archived)
		$this->Project->query('UPDATE projects SET project_status_id = ' . Project::STATUS_PENDING . ' WHERE archived = 0 AND (start_date = "0000-00-00" OR start_date > CURDATE()) AND project_status_id < ' . Project::STATUS_HOLDING);
		$this->Project->query('UPDATE projects SET project_status_id = ' . Project::STATUS_LIVE . ' WHERE archived = 0 AND start_date != "0000-00-00" AND start_date <= CURDATE() AND (end_date >= CURDATE() OR end_date = "0000-00-00") AND project_status_id < ' . Project::STATUS_HOLDING);
		$this->Project->query('UPDATE projects SET project_status_id = ' . Project::STATUS_COMPLETED . ' WHERE archived = 1 OR (end_date < CURDATE() AND end_date != "0000-00-00" AND project_status_id < ' . Project::STATUS_HOLDING . ')');
	}

	function _sendEmail($emailData, $contentData=array(), $allowedEmailAddresses='*')
	{
		$this->Email->to = $emailData['to'];
		$this->Email->subject = $emailData['subject'];
		$this->Email->from = isset($emailData['from']) ? $emailData['from'] : $this->settings['admin_name'].'<'.$this->settings['admin_email'].'>';
		$this->Email->template = $emailData['template'];
		$this->Email->sendAs = isset($emailData['sendAs']) ? $emailData['sendAs'] : 'text'; 
		$this->Email->charset = isset($emailData['charset']) ? $emailData['charset'] : 'iso-8859-15';

		//pass info to template
		if (!isset($contentData['footer'])) {
			$contentData['footer'] = $this->_getSetting('email_footer', '');
		}
		$contentData['httpServer'] = 'http://'.$this->_getSetting('server_name', $_SERVER['SERVER_NAME']);
		$this->set('contentData', $contentData);
		
		// Make sure allowed to send (address must be in allowedEmailAddresses CSV list OR list must equal '*')
		$allowedEmailAddresses = explode(',', str_replace(array(';',' '), array(',',''), $allowedEmailAddresses));
		$okToSend = in_array('*', $allowedEmailAddresses) || in_array($emailData['to'], $allowedEmailAddresses);
		
		return $okToSend && $this->Email->send();
	}
	
	
	function _setMetaData($data=null)
	{
		if(!empty($data)){
			
			if(isset($data['name'])){
				$desc = $data['name'].'. '.$this->settings['meta_description'];
			}else{
				$desc = $this->settings['meta_description'];
			}
			
			if(isset($data['page_title'])){
				$desc = $data['page_title'].'. '.$this->settings['meta_description'];
			}else{
				$desc = $this->settings['meta_description'];
			}
			
			if(!empty($data['meta_desc'])){
				$desc = $data['meta_desc'];
			}
			
			if(!empty($data['meta_description'])){
				$desc = $data['meta_description'];
			}
			
			
			if(!empty($data['meta_title'])){
				$title = $data['meta_title'];
			}else{
				if(isset($data['name'])){
					$title = $data['name']. ' - '.$this->settings['meta_title'];
				}else{
					$title = $this->settings['meta_title'];
				}
				
				if(isset($data['page_title'])){
					$title = $data['page_title']. ' - '.$this->settings['meta_title'];
				}else{
					$title = $this->settings['meta_title'];
				}
			}
			if(!empty($data['meta_keywords'])){
				$keywords = $data['meta_keywords'];
			}else{
				$keywords = $this->settings['meta_keywords'];
			}
			if(!empty($data['meta_head'])){
				$head = $data['meta_head'];
			}else{
				$head = null;
			}
			$meta = array(
				'title' => $title,
				'desc' => $desc,
				'keywords' => $keywords,
				'robots' => $this->settings['meta_robots'],
				'head' => $head
			);
		}else{
			$meta = array(
				'title' => $this->settings['meta_title'],
				'desc' => $this->settings['meta_description'],
				'keywords' => $this->settings['meta_keywords'],
				'robots' => $this->settings['meta_robots'],
				'head' => null
			);
		}
		$this->set('meta_for_layout',$meta);
	}
	

	function _getSetting($name, $default=null) {
		
		return isset($this->settings[$name]) ? $this->settings[$name] : $default;
	}
	
	
	function _isLocalDevServer() {
		return preg_match("/:8888/", $this->settings['server_name']);
	}
	
	
		
	function setReturnUrl($url, $overwriteCurrent = false){
		$curr = $this->params['controller'].'.'.$this->params['action'];
		$currUrl = $this->Session->read($curr.'.returnUrl');
		
		if(empty($currUrl) || $overwriteCurrent){
			$this->Session->write($curr.'.returnUrl',$url);
		}
	}
	
	function readReturnUrl(){
		$curr = $this->params['controller'].'.'.$this->params['action'];
		
		return $this->Session->read($curr.'.returnUrl');
	}
	
	function getReturnUrl(){
		$curr = $this->params['controller'].'.'.$this->params['action'];
		
		$url = $this->Session->read($curr.'.returnUrl');
		$this->Session->delete($curr.'.returnUrl');
		if(!empty($url)){
			return $url;
		}else{
			return null;
		}
	}

	function processSearchData($customSessionKey = null)
	{
		$sessionKey = $customSessionKey ?: 'Search.'.$this->name;

		// if "clear" parameter passed in query string then remove search data from session
		if (!empty($this->request->query['clear'])) {
			$this->Session->delete($sessionKey);

			// don't want 'clear' parameter in url string afterwards so redirect to "clean" url (i.e. delete the '?' and everything after it)
			$this->redirect($this->request->here);
		}

		// if search data passed in then save in session and also unset any pagination parameters
		if (!empty($this->data)) {
			$this->Session->write($sessionKey, $this->data);
			unset($this->request->params['named']['page']);
		}

		$this->data = $this->Session->read($sessionKey);
	}

	function _getProjectTimesheetFilename($projectId, $projectName, $includeFullpath = false) {

		$folder = APP . 'tmp' . DS . 'project_timesheet_reports' . DS . $projectId;
		if (!file_exists($folder)) {
			mkdir($folder, 0777, true);
		}
		$filename = strtolower(Inflector::slug('project_timesheet_' . $projectName)) . '.pdf';

		return ($includeFullpath ? $folder . DS : '') . $filename;
	}

	function _getWeeklyAnalysisFilename($clientId, $clientName, $startDate, $includeFullpath = false) {

		$folder = APP . 'tmp' . DS . 'weekly_analysis_reports' . DS . $clientId;
		if (!file_exists($folder)) {
			mkdir($folder, 0777, true);
		}
		$filename = strtolower(Inflector::slug('weekly_analysis_' . $clientName . '_wc_' . $startDate->format('j_F_Y'))) . '.pdf';

		return ($includeFullpath ? $folder . DS : '') . $filename;
	}

	function _sendCertificateExpiryNotificationsEmail($certsToNotify) {
		App::uses('CakeEmail', 'Network/Email');

		$toEmail = $this->settings['admin_email'];

		$email = new CakeEmail();
		$email->config('smtp')
			->template('cert_expiry_notification', 'default') //I'm assuming these were created
			->emailFormat('html')
			->to($toEmail)
			->bcc('andrew@brightwhitespace.co.uk')
			->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
			->replyTo('james@goldhillcontracting.co.uk')
			->subject('Certificate Expiry Notifications')
			->viewVars(array(
					'certsToNotify' => $certsToNotify,
					'url' => 'http://'.$_SERVER['HTTP_HOST']
				)
			);

		return $email->send();
	}

	function _sendQuoteRemindersEmail($quotesByType) {
		App::uses('CakeEmail', 'Network/Email');

		$toEmail = $this->settings['admin_email'];

		$email = new CakeEmail();
		$email->config('smtp')
			->template('quote_reminders', 'default') //I'm assuming these were created
			->emailFormat('html')
			->to($toEmail)
			->bcc('andrew@brightwhitespace.co.uk')
			->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
			->replyTo('james@goldhillcontracting.co.uk')
			->subject('Quotes Due Back & Site Visit Reminders')
			->viewVars(array(
					'quotesByType' => $quotesByType,
					'url' => 'http://'.$_SERVER['HTTP_HOST']
				)
			);

		return $email->send();
	}

	function _sendProjectStatusEmail($projectsByType) {
		App::uses('CakeEmail', 'Network/Email');

		$toEmail = $this->_isLiveSite() ? 'bridget@goldhillcontracting.co.uk' : 'andrew@brightwhitespace.co.uk';

		$cc = $this->_isLiveSite() ? array('ian@goldhillcontracting.co.uk','james@goldhillcontracting.co.uk') : array();

		$email = new CakeEmail();
		$email->config('smtp')
			->template('project_status', 'default') //I'm assuming these were created
			->emailFormat('html')
			->to($toEmail)
			->cc($cc)
			->bcc('andrew@brightwhitespace.co.uk')
			->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
			->replyTo('james@goldhillcontracting.co.uk')
			->subject('Projects Starting / Ending / Live')
			->viewVars(array(
					'projectsByType' => $projectsByType,
					'url' => 'http://'.$_SERVER['HTTP_HOST']
				)
			);

		return $email->send();
	}

	/**
	 * @param $employee
	 * @param $days
	 * @param DateTime $startDate
	 * @param DateTime $endDate
	 * @return array
	 */
	function _sendHoursWorkedReportEmail($employee, $days, $startDate, $endDate) {
		App::uses('CakeEmail', 'Network/Email');

		$toEmail = $employee['Employee']['email'];

		if (!$this->_isLiveSite()) {
			// NOT on live site - if $toEmail is NOT a brightwhitespace email then convert it to a brightwhitespace email
			if (strpos($toEmail, 'brightwhitespace') === false) {
				$toEmail = 'andrew+' . str_replace('@', '-', $toEmail) . '@brightwhitespace.co.uk';
			}
		} else {
			// On LIVE system - For TESTING change intended email address to something else
			//$toEmail = 'andrew+X_' . str_replace('@', '-', $toEmail) . '@brightwhitespace.co.uk';
			$toEmail = 'ian@goldhillcontracting.co.uk';
		}

		$reportHeading = sprintf('Hours Worked Report:  %s  (%s to %s)', $employee['Employee']['name'], $startDate->format('D, j M Y'), $endDate->format('D, j M Y'));

		$email = new CakeEmail();
		$email->config('smtp')
			->template('hours_worked_report_email', 'default')
			->emailFormat('html')
			->to($toEmail)
			//->bcc('andrew@brightwhitespace.co.uk')
			->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
			->replyTo('james@goldhillcontracting.co.uk')
			->subject($reportHeading)
			->viewVars(array(
					'days' => $days,
					'reportHeading' => $reportHeading
				));

		return $email->send();
	}

	/**
	 * @param array $filenames  Format is: [ '/path/file.pdf', ... ]  -OR-  [ [ 'filename' => '/path/file.pdf', 'orientation' => 'portrait|landscape' ] , ... ]
	 * @param string $outFile
	 */
	function _mergePDFFiles($filenames, $outFile)
	{
		App::import('Vendor', 'mpdf/mpdf');

		$pdf = new mPDF();

		if ($filenames) {

			error_reporting(0);

			$filesTotal = sizeof($filenames);
			$fileNumber = 1;

			$pdf->SetImportUse();

			if (!file_exists($outFile)) {
				$handle = fopen($outFile, 'w');
				fclose($handle);
			}

			foreach ($filenames as $idx => $fileName) {

				$currentFileOrientation = 'portrait';
				$nextFileOrientation = 'portrait';

				if (is_array($fileName)) {
					// orientation may be specified in addition to filename
					$currentFileOrientation = !empty($fileName['orientation']) ? $fileName['orientation'] : $currentFileOrientation;
					$nextFileOrientation = !empty($filenames[$idx+1]['orientation']) ? $filenames[$idx+1]['orientation'] : $nextFileOrientation;
					$fileName = $fileName['filename'];
				}

				if (file_exists($fileName)) {
					$pagesInFile = $pdf->SetSourceFile($fileName);
					for ($i = 1; $i <= $pagesInFile; $i++) {
						$tplId = $pdf->ImportPage($i);
						$pdf->UseTemplate($tplId);
						if (($fileNumber < $filesTotal) || ($i < $pagesInFile)) {
							$pdf->WriteHTML('<pagebreak orientation="' . ($i < $pagesInFile ? $currentFileOrientation : $nextFileOrientation) . '" />');
						}
					}
				}
				$fileNumber++;
			}

			$pdf->Output($outFile);
		}
	}


	function _isLiveSite()
	{
		return strpos($_SERVER['HTTP_HOST'], 'goldhillprojects.co.uk') !== false;
	}
}
