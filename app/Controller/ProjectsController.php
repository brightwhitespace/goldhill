<?php
class ProjectsController extends AppController {

	var $name = 'Projects';
	var $uses = array('Project','ProjectItem','Employee','AgencyEmployee','Supplier','Storage');
	var $layout = 'admin';

	function admin_index($archive = 0)
	{
		$this->_updateProjectStatuses();

		$conditions = array('Project.archived' => $archive);

		$projectStatusGroups = array(
			'Current' => array(Project::STATUS_PENDING, Project::STATUS_LIVE, Project::STATUS_COMPLETED),
			'Holding'=> array(Project::STATUS_HOLDING)
		);

		$this->processSearchData('Search.'.$this->name.".$archive");

		if ($this->data) {
			if (!empty($this->data['Search']['project_type_id'])) {
				$conditions['Project.project_type_id'] = $this->data['Search']['project_type_id'];
			}
			// only filter by status if not showing archived projects
			if (!$archive && !empty($this->data['Search']['project_status'])) {
				$conditions['Project.project_status_id IN'] = $projectStatusGroups[$this->data['Search']['project_status']];
			}
			$conditions['or'] = array(
				'Client.name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Project.name LIKE' => '%'.$this->data['Search']['term'].'%',
			);
		}

		if(!empty($this->request->params['named']['client_id'])){
			$conditions['Project.client_id'] = $this->request->params['named']['client_id'];
			unset($conditions['Project.archived']);
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'contain' => array(
				'PurchaseOrder',
				'Client',
				'Labour',
				'ProjectType',
				'Timesheet',
				'ProjectItem',
				'ProjectStatus',
				'Invoice'
			),
			'order' => array('created' => 'desc')
		);
		$this->Project->recursive = 0;
		$this->set('projects', $this->paginate());

		$this->set('projectTypes', $this->Project->ProjectType->find('list'));
		$this->set('projectStatusGroupList', array_combine(array_keys($projectStatusGroups), array_keys($projectStatusGroups)));
		$this->set('archive', $archive);

		$this->set('returnUrl',$this->readReturnUrl());
	}


	function admin_add($clientId = null)
	{
		$this->set('title_for_layout','Projects - Add New Project');
		if (!empty($this->request->data)) {
			$this->Project->create();

			if ($this->Project->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please try again.'));
			}
		}else{
			// first time in so need to add empty list of project items (otherwise get an error)
			$this->request->data['ProjectItem'] = array();

			$this->request->data['Project']['client_id'] = $clientId;
			$this->setReturnUrl($this->referer());
		}

		$this->set('clients',$this->Project->Client->find('list',array('order'=>array('Client.name'))));
		$this->set('projectTypes',$this->Project->ProjectType->find('list'));
		$this->set('projectStatuses',$this->Project->ProjectStatus->find('list'));
		$this->set('users',$this->Project->User->getList());
		$this->set('returnUrl',$this->readReturnUrl());
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Projects - Edit Project');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid project'));
			$this->redirect($this->referer());
		}
		if (!empty($this->request->data)) {

			$this->ProjectItem->deleteAll(array('ProjectItem.project_id' => $id));

			if ($this->Project->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Project->getProject($id);

			$this->setReturnUrl($this->referer(), true);
		}

		$this->set('projectTypes',$this->Project->ProjectType->find('list'));
		$this->set('projectStatuses',$this->Project->ProjectStatus->find('list'));
		$this->set('clients',$this->Project->Client->find('list',array('order'=>array('Client.name'))));
		$this->set('users',$this->Project->User->getList());
		$this->set('returnUrl',$this->readReturnUrl());
	}

	function admin_generate_method_docs($id = null)
	{
		$this->set('title_for_layout','Projects - Generate Method Docs');

		if (!$id || !($project = $this->Project->getProject($id))) {
			$this->Session->setFlash(__('Invalid project'));
			$this->redirect($this->referer());
		}

		if (!empty($this->request->data)) {
			// make sure project id is set in ProjectMethodDoc
			$this->request->data['ProjectMethodDoc']['project_id'] = $id;

			if ($this->Project->ProjectMethodDoc->save($this->request->data)) {

				// Add required keys and values to data
				$projectAddressParts = explode("\n", $this->request->data['ProjectMethodDoc']['project_address']);
				for ($partIndex = 0; $partIndex < 6; $partIndex++) {
					$this->request->data['ProjectMethodDoc']['project_address_'.($partIndex+1)] = !empty($projectAddressParts[$partIndex]) ? $projectAddressParts[$partIndex] : '';
				}
				$this->request->data['ProjectMethodDoc']['project_address_one_line'] = str_replace(array("\r\n", "\n"), ', ', $this->request->data['ProjectMethodDoc']['project_address']);
				$this->request->data['ProjectMethodDoc']['review_date_short'] = date('d/m/y', strtotime($this->request->data['ProjectMethodDoc']['review_date']));

				// GENERATE METHOD AND RISK DOCS

				require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

				// Template processor for Methods doc
				$methodTemplateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($_SERVER['DOCUMENT_ROOT'] . '/app/View/Projects/methods_template.docx');
				$riskTemplateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($_SERVER['DOCUMENT_ROOT'] . '/app/View/Projects/risk_template.docx');

				foreach ($this->request->data['ProjectMethodDoc'] as $key => $value) {
					if (strpos($key, 'include_') === 0) {
						// this is a section identifier, keep or delete depending on user's choice
						$sectionName = strtoupper(str_replace('include_', '', $key));
						if ($value) {
							$methodTemplateProcessor->keepBlock('BULLET_' . $sectionName);
							$methodTemplateProcessor->keepBlock('SECTION_' . $sectionName);
						} else {
							$methodTemplateProcessor->deleteBlock('BULLET_' . $sectionName);
							$methodTemplateProcessor->deleteBlock('SECTION_' . $sectionName);
						}

					} else {
						$methodTemplateProcessor->setValue($key, $value);
						$riskTemplateProcessor->setValue($key, $value);
					}
				}

				// make sure folder exists before saving
				$projectFolder = $_SERVER['DOCUMENT_ROOT'] . '/app/tmp/project/' . $id;
				if (!file_exists($projectFolder)) {
					mkdir($projectFolder, 0777, true);
				}
				
				$methodTemplateProcessor->saveAs($projectFolder . '/method_statement_v1.docx');
				$riskTemplateProcessor->saveAs($projectFolder . '/risk_assessment_v1.docx');

				$this->Session->setFlash(__('The method and risk documents have been generated'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The method and risk documents could not be generated. Please try again.'));
			}

		} else {
			$this->request->data = $project;

			$this->setReturnUrl($this->referer(), true);
		}

		$employeesData = $this->Employee->getEmployees();
		$employeesData = Hash::combine($employeesData, '{n}.Employee.id', '{n}.Employee');

		$this->set('project', $project);
		$this->set('employeesList', $this->Employee->getList());
		$this->set('employeesData', $employeesData);
		$this->set('yesNoOptions', array(1 => 'Yes', 0 => 'No'));
		$this->set('defaultSiteContactId', empty($this->request->data['ProjectMethodDoc']['site_contact_name']) && !empty($this->request->data['PurchaseOrder'][0]['employee_id']) ? $this->request->data['PurchaseOrder'][0]['employee_id'] : '');
		$this->set('returnUrl', $this->readReturnUrl());
	}

	function admin_view($id = null, $action = null)
	{
		$this->_updateProjectStatuses();

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for project'));
			$this->redirect($this->referer());
		}

		$project = $this->Project->getProject($id);

		if ($action == 'notify' && !empty($project['Contact']['email'])) {
			if ($this->_sendNotificationEmail($project)) {
				$this->Project->id = $project['Project']['id'];
				$this->Project->saveField('notification_sent_time', date('Y-m-d H:i:s'));

				$this->Session->setFlash(__('The project notification has been emailed to ') . h($project['Contact']['firstname']) . ' ' . h($project['Contact']['lastname']));

			} else {
				$this->Project->id = $project['Project']['id'];
				$this->Project->saveField('notification_sent_time', null);

				$this->Session->setFlash(__('There was a problem sending the email, please try again'));
			}
			$this->redirect($this->referer());
		}

		$purchaseOrders = $this->Project->PurchaseOrder->getLatest($id);
		$labours = $this->Project->Labour->getAll($id);
		$timesheets = $this->Project->Timesheet->getAll($id);
		$agencyTimesheets = $this->Project->AgencyTimesheet->getAll($id);

		$this->set('project',$project);
		$this->set('purchaseOrders',$purchaseOrders);
		$this->set('labours',$labours);
		$this->set('timesheets',$timesheets);
		$this->set('employeesList',$this->Employee->find('list'));
		$this->set('agencyTimesheets',$agencyTimesheets);
		$this->set('agenciesList',$this->Supplier->getSuppliers('list', 1));
		$this->set('agencyEmployeesList',$this->AgencyEmployee->find('list'));
		$this->set('files', glob(APP . "tmp/project/$id/*"));
		$this->set('files_confidential', glob(APP . "tmp/project_confidential/$id/*"));
	}

	function admin_summary($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for project'));
            $this->redirect($this->referer());
        }

        $project = $this->Project->getProject($id);

        $hireTotals = $this->Project->PurchaseOrder->getTotals($id,'Hire');
        $skipTotals = $this->Project->PurchaseOrder->getTotals($id,'Skip Hire');
        $sundryTotals = $this->Project->PurchaseOrder->getTotals($id,'Sundry');
        $labourTotals = $this->Project->PurchaseOrder->getTotals($id,'Labour');
        $timesheetTotals = $this->Project->Timesheet->getTotals($id);

        $labourTotals['qty'] += $timesheetTotals['qty'];
        $labourTotals['total'] += $timesheetTotals['total'];


        $this->set('project',$project);
        $this->set('hireTotals',$hireTotals);
        $this->set('skipTotals',$skipTotals);
        $this->set('sundryTotals',$sundryTotals);
        $this->set('labourTotals',($labourTotals));
    }

	function admin_delete($id = null)
	{
		$this->Session->setFlash(__('Not deleted - should be archived in case has associated records'), 'flash_failure');
		$this->redirect(array('action'=>'index'));
		/*
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for project'));
			$this->redirect($this->referer());
		}
		if ($this->Project->delete($id)) {
			$this->Session->setFlash(__('Project deleted'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Project was not deleted'));
		$this->redirect($this->referer());
		*/
	}

	function admin_archive($id, $archive = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for project'));
			$this->redirect($this->referer());
		}

		$this->Project->id = $id;
		$this->Project->saveField('archived', $archive);
		$this->Session->setFlash(__('Project was ' . ($archive ? 'archived' : 'restored from archive')));

		$this->redirect($this->referer());
	}

	function admin_holding($id, $holding = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for project'));
			$this->redirect($this->referer());
		}

		$this->Project->id = $id;

		if ($holding) {
			$this->Project->saveField('project_status_id', Project::STATUS_HOLDING);
			$this->Session->setFlash(__('Project was moved to Holding'));
		} else {
			$this->Project->saveField('project_status_id', Project::STATUS_COMPLETED);
			$this->Session->setFlash(__('Project was moved back to Current'));
		}

		$this->redirect($this->referer());
	}

	function admin_createInvoice($id = null, $moveProjectToHolding = null)
	{
		if ($id && ($project = $this->Project->getProject($id))) {

			$flashMessage = '';

			if (empty($project['Invoice'])) {
				// no invoice yet so create new invoice and copy project items AND rechargeable POs as invoice items
				$this->Project->Invoice->create();
				$data = array(
					'Invoice' => array(
						'client_id' => $project['Client']['id'],
						'project_id' => $project['Project']['id'],
						'invoice_num' => 'DRAFT',
						'invoice_date' => date('d-m-Y'),
						'ref' => $project['Project']['name'],
					),
					'InvoiceItem' => array()
				);

				$invoiceItemIndex = 0;
				foreach ($project['ProjectItem'] as $projectItem) {
					$data['InvoiceItem'][$invoiceItemIndex]['date'] = !empty($projectItem['date']) ? $projectItem['date'] : null;
					$data['InvoiceItem'][$invoiceItemIndex]['description'] = $projectItem['description'];
					$data['InvoiceItem'][$invoiceItemIndex]['cost'] = $projectItem['cost'];
					$invoiceItemIndex++;
				}

				foreach ($project['PurchaseOrder'] as $po) {
					if ($po['recharge']) {
						$data['InvoiceItem'][$invoiceItemIndex]['date'] = !empty($po['start_date']) ? $po['start_date'] : null;
						$data['InvoiceItem'][$invoiceItemIndex]['description'] = $po['desc'];
						$data['InvoiceItem'][$invoiceItemIndex]['cost'] = $po['value'] * (100 + $this->_getSetting('recharged_po_percent_to_add', 10)) / 100;
						$invoiceItemIndex++;
					}
				}

				$data['Invoice']['total'] = AppModel::sumNestedValuesInArray($data['InvoiceItem'], 'cost');

				// set the footer main and sub text
				$data['Invoice']['gross_status_text'] = $this->_getSetting('invoice_gross_status_text_default', '');
				$data['Invoice']['footer_main_text'] = $this->_getSetting('invoice_footer_main_text_default', '');
				$data['Invoice']['footer_sub_text'] = $this->_getSetting('invoice_footer_sub_text_default', '');
				$data['Invoice']['banking_details'] = $this->_getSetting('invoice_banking_details_default', '');

				if ($this->Project->Invoice->saveAll($data)) {
					$flashMessage = __('Draft invoice has been created. ');
				} else {
					$this->Session->setFlash(__('Draft invoice could not be created. Please try again.'), 'flash_failure');
					$this->redirect($this->referer());
				}
			}

			if ($moveProjectToHolding) {
				$this->Project->id = $id;
				$this->Project->saveField('project_status_id', Project::STATUS_HOLDING);
				$flashMessage .= __('Project moved to Holding.');
			}

			$this->Session->setFlash($flashMessage);

		} else {
			$this->Session->setFlash(__('Invalid project'));
		}

		$this->redirect($this->referer());
	}

	function _sendNotificationEmail($project) {
		App::uses('CakeEmail', 'Network/Email');

		$toEmail = $this->settings['admin_email'];
		$cc = array('ian@goldhillcontracting.co.uk');

		if(!empty($project['User']['username'])){
			$cc[] = $project['User']['username'];
		}


		$email = new CakeEmail();
		$email->config('smtp')
			->template('project_notification', 'default') //I'm assuming these were created
			->emailFormat('html')
			->to($project['Contact']['email'])
			->cc($cc)
			->bcc('andrew@brightwhitespace.co.uk')
			->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
			->replyTo('james@goldhillcontracting.co.uk')
			->subject('Project Notification - '.$project['Project']['name'])
			->viewVars(array(
					'project' => $project,
					'url' => 'http://'.$_SERVER['HTTP_HOST']
				)
			);

		return $email->send();
	}
}
