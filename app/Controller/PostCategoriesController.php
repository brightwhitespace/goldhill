<?php

class PostCategoriesController extends AppController
{
	
	var $name = 'PostCategories';
	var $layout = 'admin';
	var $pageTitle = 'PostCategories';
	var $components = array('RequestHandler');
	
	
	function admin_index() {
		$this->layout = 'admin';
		$this->set('title_for_layout','Post Categories');
		
		$this->PostCategory->recursive = -1;
		
		$this->set('post_categories', $this->paginate());
	}

	function admin_add() {
		$this->layout = 'admin';
		$this->set('title_for_layout','Post Categories - add new');
		
		if (!empty($this->request->data)) {
			if ($this->PostCategory->save($this->request->data)) {
				$this->Session->setFlash('Category added successfully');
				$this->redirect('/admin/post_categories/');
			} else {
				$this->Session->setFlash('There was a problem adding the Category, please try again.');
			}
		}
	}

	function admin_edit($id = null) {
		
		$this->layout = 'admin';
		$this->set('title_for_layout','Post Categories - edit');
		
		$this->pageTitle .= ' - edit';
		if (empty($this->request->data)) {
			if (!$id) {
				$this->flash('Invalid id for PostCategory', '/admin/post_categories/');
			}
			$this->request->data = $this->PostCategory->read(null, $id);
		}
		else {
			if ($this->PostCategory->save($this->request->data)) {
				$this->Session->setFlash('Category saved successfully');
				$this->redirect('/admin/post_categories/');
			} else {
			}
		}
	}

	function admin_delete($id = null) {
		$this->layout = 'admin';
		if (!$id) {
			$this->flash('Invalid id for PostCategory', '/admin/post_categories/index');
		}
		if ($this->PostCategory->delete($id)) {
			$this->Session->setFlash('Category deleted successfully');
			$this->redirect('/admin/post_categories/');
		}
	}
	
	function admin_order($catID = null){
		$this->layout = 'admin';
		$this->pageTitle .= ' - change order';
		$this->set('options_for_layout','categories');
		$this->set('id_for_layout',-1);
		
		
		$this->set('categories',$this->PostCategory->find('all',array('order'=>'PostCategory.order_num ASC')));
	}
	
	function admin_changeorder(){
		
		$order_num = 0;
		foreach ($this->data['postcategory'] as $id => $parent){
			$this->request->data['PostCategory']['order_num'] = $order_num;
			//$this->request->data['Advert']['parent_id'] = $parent;
			$this->request->data['PostCategory']['id'] = $id;
			
			$this->PostCategory->save($this->request->data,false);
			$order_num ++;
		}
		
		
		$this->layout = 'ajax';
		
		exit();
	}
	
}

?>