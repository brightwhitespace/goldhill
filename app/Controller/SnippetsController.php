<?php
class SnippetsController extends AppController {

	var $name = 'Snippets';
	var $layout = 'admin';

	function admin_index()
	{
		$conditions = array('Snippet.site_id' => $this->currentSite['Site']['id']);
		if ($this->data) {
			$conditions['name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		$this->paginate = array('conditions' => $conditions);
		$this->Snippet->recursive = 0;
		$this->set('snippets', $this->paginate());
	}

	function admin_add()
	{
		$this->set('title_for_layout','Snippets - Add New Snippet');
		if (!empty($this->request->data)) {
			$this->Snippet->create();
			$this->request->data['Snippet']['site_id'] = $this->currentSite['Site']['id'];
			if ($this->Snippet->save($this->request->data)) {
				$this->Session->setFlash(__('The snippet has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The snippet could not be saved. Please try again.'));
			}
		}
		$this->set('pages', $this->Snippet->Page->find('threaded', array(
			'conditions' => 'Page.site_id = ' . $this->currentSite['Site']['id'],
			'order'=>'Page.order_num'
		)));
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Snippets - Edit Snippet');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid snippet'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			$this->request->data['Snippet']['site_id'] = $this->currentSite['Site']['id'];
			if ($this->Snippet->save($this->request->data)) {
				$this->Session->setFlash(__('The snippet has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The snippet could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Snippet->read(null, $id);
		}
		$this->set('pages', $this->Snippet->Page->find('threaded', array(
			'conditions' => 'Page.site_id = ' . $this->currentSite['Site']['id'],
			'order'=>'Page.order_num'
		)));
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for snippet'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Snippet->delete($id)) {
			$this->Session->setFlash(__('Snippet deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Snippet was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
