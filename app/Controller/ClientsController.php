<?php
class ClientsController extends AppController {

	var $name = 'Clients';
	var $uses = array('Invoice','Client','Project');
	var $layout = 'admin';
	var $components = array('Mpdf');


	function home()
	{
		// default client page
		$this->layout = 'default';
		$this->set('title_for_layout', $this->settings['site_name'].' - Client Area');

		if (!empty($this->currentUser['Client']['id'])) {
			$projectStatusGroups = array(
				'Pending' => array(Project::STATUS_PENDING),
				'Live' => array(Project::STATUS_LIVE),
				'Completed' => array(Project::STATUS_COMPLETED, Project::STATUS_HOLDING),
			);

			$this->processSearchData('Search.Projects');

			if ($this->data) {
				if (!empty($this->data['Search']['project_type_id'])) {
					$conditions['Project.project_type_id'] = $this->data['Search']['project_type_id'];
				}
				if (!empty($this->data['Search']['contact_id'])) {
					$conditions['Project.contact_id'] = $this->data['Search']['contact_id'];
				}
				// only filter by status if not showing archived projects
				if (!empty($this->data['Search']['project_status'])) {
					$conditions['Project.project_status_id IN'] = $projectStatusGroups[$this->data['Search']['project_status']];
				}
				$conditions['or'] = array(
					'Project.name LIKE' => '%' . $this->data['Search']['term'] . '%',
					'Project.description LIKE' => '%' . $this->data['Search']['term'] . '%',
					'Project.address1 LIKE' => '%' . $this->data['Search']['term'] . '%',
					'Project.address2 LIKE' => '%' . $this->data['Search']['term'] . '%',
					'Project.address3 LIKE' => '%' . $this->data['Search']['term'] . '%',
					'Project.city LIKE' => '%' . $this->data['Search']['term'] . '%',
					'Project.postcode LIKE' => '%' . $this->data['Search']['term'] . '%',
				);
			}

			$conditions['Project.client_id'] = $this->currentUser['User']['client_id'];

			$this->paginate = array(
				'conditions' => $conditions,
				'contain' => array(
					'ProjectType',
					'ProjectItem',
					'ProjectStatus',
					'Invoice'
				),
				'order' => array('Project.created' => 'desc')
			);
			$this->Project->recursive = 0;
			$this->set('projects', $this->paginate('Project'));

			$this->set('projectTypes', $this->Project->ProjectType->find('list'));
			$this->set('projectStatusGroupList', array_combine(array_keys($projectStatusGroups), array_keys($projectStatusGroups)));
			$this->set('contactList', $this->Project->Contact->find('list', array('conditions'=>array('client_id'=>$this->currentUser['User']['client_id']))));
		}
	}

	function invoices($projectId = null)
	{
		// default client page
		$this->layout = 'default';
		$this->set('title_for_layout', $this->settings['site_name'].' - Client Area');

		if (!empty($this->currentUser['Client']['id'])) {

			$this->processSearchData('Search.Invoices');

			$conditions = array();

			if ($this->data) {
				if (!empty($this->data['Search']['invoice_status_id'])) {
					$conditions['invoice_status_id'] = $this->data['Search']['invoice_status_id'];
				}
				if (!empty($this->data['Search']['project_id'])) {
					$conditions['project_id'] = $this->data['Search']['project_id'];
				}
				if (!empty($this->data['Search']['contact_id'])) {
					$conditions['Project.contact_id'] = $this->data['Search']['contact_id'];
				}
				if (!empty($this->data['Search']['term'])) {
					$conditions['or'] = array(
						'Invoice.invoice_num LIKE' => '%'.$this->data['Search']['term'].'%',
						'Invoice.ref LIKE' => '%'.$this->data['Search']['term'].'%',
						'Project.name LIKE' => '%'.$this->data['Search']['term'].'%',
					);
				}
			}

			$conditions['Invoice.client_id'] = $this->currentUser['User']['client_id'];
			$conditions['Invoice.invoice_status_id'] = Invoice::STATUS_SENT;

			if ($projectId && ($project = $this->Project->getProject($projectId))
					&& $project['Project']['client_id'] == $this->currentUser['User']['client_id']) {
				// add project id to search conditions
				$conditions['Invoice.project_id'] = $projectId;
				unset($conditions['Project.contact_id']);
				unset($this->request->data['Search']['contact_id']);
				$this->set('project', $project);
			}

			$this->paginate = array(
				'conditions' => $conditions,
				'order' => array(
					'Invoice.invoice_num' => 'desc'
				),
			);
			$this->Invoice->recursive = 0;
			$this->set('invoices', $this->paginate('Invoice'));
			$this->set('contactList', $this->Project->Contact->find('list', array('conditions'=>array('client_id'=>$this->currentUser['User']['client_id']))));
		}
	}

	function view_invoice($id = null)
	{
		$this->layout = 'ajax';

		$invoice = $this->Invoice->getInvoice($id);
		if (!$invoice || $invoice['Invoice']['client_id'] != $this->currentUser['Client']['id']) {
			$this->Session->setFlash(__('Invalid id for invoice'));
			$this->redirect(array('action'=>'invoices'));
		}

		// only set return url to referring page if have not come from the invoice view page
		if (strpos($this->referer(), '/clients/view_invoice') === false) {
			$this->setReturnUrl($this->referer(), true);
		}
		$this->set('returnUrl',$this->readReturnUrl());

		$this->set('invoice',$invoice);
	}

	/**
	 * @param null $id
	 */
	function generateInvoice($id = null)
	{
		$this->layout = 'pdf_letter';

		$invoice = $this->Invoice->getInvoice($id);

		if (!$invoice || $invoice['Invoice']['client_id'] != $this->currentUser['Client']['id']) {
			$this->Session->setFlash(__('Invalid id for invoice'));
			$this->redirect(array('action'=>'invoices'));
		}

		$this->set('invoice',$invoice);

		// just download the pdf with simple filename (i.e. not full path)
		$this->Mpdf->init();
		$this->Mpdf->setFilename($this->_getInvoiceFilename($invoice));
		$this->Mpdf->setOutput('D');
	}

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			$conditions['name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		$this->paginate = array('conditions' => $conditions);
		$this->Client->recursive = 0;
		$this->set('clients', $this->paginate('Client'));
	}

	function admin_add()
	{
		$this->set('title_for_layout','Clients - Add New Client');
		if (!empty($this->request->data)) {
			$this->Client->create();
			
			if ($this->Client->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The client has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The client could not be saved. Please try again.'));
			}
		}
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Clients - Edit Client');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid client'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Client->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The client has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The client could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Client->getClient($id);
		}
		
	}
	
	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for client'));
			$this->redirect(array('action'=>'index'));
		}
		
		$client = $this->Client->getClient($id);
		
		$this->set('client',$client);
		$projects = $this->Client->Project->getLatest($id);
		$this->set('projects',$projects);
	}

	function admin_delete($id = null)
	{
		$this->Session->setFlash(__('Not deleted - should be archived in case has associated records'), 'flash_failure');
		$this->redirect(array('action'=>'index'));
		/*
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for client'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Client->delete($id)) {
			$this->Session->setFlash(__('Client deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Client was not deleted'));
		$this->redirect(array('action' => 'index'));
		*/
	}
	
	function admin_contacts($clientId, $selectedContactId = null){
		$this->layout = 'ajax';

		$options = array();
		$options['conditions'] = array(
			'Contact.client_id' => $clientId
		);
		$options['order'] = array(
			'Contact.name'
		);

		$this->set('contacts',$this->Client->Contact->find('list',$options));

		// if selectedContactId passed in then use it to preselect the contact in the dropdown
		if ($selectedContactId) {
			$this->request->data['Project']['contact_id'] = $selectedContactId;
		}
	}

	function _getInvoiceFilename($invoice, $fullpath = false)
	{
		$folder = APP . 'tmp' . DS . 'invoices';
		if (!file_exists($folder)) {
			mkdir($folder, 0777, true);
		}
		$filename = strtolower(Inflector::slug('goldhill_invoice_' . $invoice['Invoice']['invoice_num'] . '_' . $invoice['Invoice']['invoice_date'] . '_' . $invoice['Client']['name']) . '.pdf');

		return ($fullpath ? $folder . DS : '') . $filename;
	}

}
