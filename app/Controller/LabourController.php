<?php
class LabourController extends AppController {

	var $name = 'Labour';
	var $layout = 'admin';

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			$conditions['Project.name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}
		
		if(!empty($this->request->params['named']['project_id'])){
			$conditions['Labour.project_id'] = $this->request->params['named']['project_id'];
		}
		
		$this->paginate = array(
			'conditions' => $conditions,
			'order' => array('Labour.created' => 'desc')
		);
		$this->Labour->recursive = 0;
		$this->set('labours', $this->paginate());
	}

	function admin_add()
	{
		$this->set('title_for_layout','Labour - Add New Labour');

		$projectSearchConditions = array('Project.archived' => 0);

		if (!empty($this->request->data)) {
			$this->Labour->create();
			
			if ($this->Labour->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The labour has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The labour could not be saved. Please try again.'));
			}
		}else{
			$this->setReturnUrl($this->referer());
			$this->request->data['Labour']['date'] = date('d-m-Y');
			$this->request->data['Labour']['rate'] = $this->settings['default_labour_rate'];
			if(!empty($this->request->params['named']['project_id'])){
				$this->request->data['Labour']['project_id'] = $this->request->params['named']['project_id'];

				// make sure passed-in project is included in selectable project list (in case it is archived)
				$projectSearchConditions = array('or' => array($projectSearchConditions, 'Project.id = ' . $this->request->params['named']['project_id']));
			}
		}

		$this->set('projects',$this->Labour->Project->find('list',array('order'=>'Project.name','conditions'=>$projectSearchConditions)));
		
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Labour - Edit Labour');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid labour'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Labour->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The labour has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The labour could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->setReturnUrl($this->referer());
			$this->request->data = $this->Labour->getLabour($id);
		}

		$projectId = !empty($this->request->data['Labour']['project_id']) ? $this->request->data['Labour']['project_id'] : 0;
		$projectSearchConditions = array('or' => array('Project.archived' => 0, 'Project.id ' => $projectId));
		$this->set('projects',$this->Labour->Project->find('list',array('order'=>'Project.name','conditions'=>$projectSearchConditions)));

		$this->set('returnUrl',$this->readReturnUrl());
		
	}
	

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for labour'));
			$this->redirect($this->referer());
		}
		if ($this->Labour->delete($id)) {
			$this->Session->setFlash(__('Labour deleted'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Labour was not deleted'));
		$this->redirect($this->referer());
	}
}
