<?php
class SettingsController extends AppController {

	var $name = 'Settings';
	var $layout = 'admin';

	
	function admin_index() {
		
		// set default page number in session as 1 if necessary
		if( !$this->Session->check('Settings.pageIndex') ) {
			$this->Session->write('Settings.pageIndex', 1);
		}
		// If page index specified in URL then remember it in session
		if( isset($this->params['named']['page']) ) {
			$this->Session->write('Settings.pageIndex', $this->params['named']['page']);
		}
		
		$this->paginate = array(
			'page'=>$this->Session->read('Settings.pageIndex'),
			'limit' => 100,
			'order' => 'Setting.id ASC'
		);

		// allow user to edit settings for current site
		$conditions = $this->currentSite ? array('Setting.site_id' => $this->currentSite['Site']['id']) : array();

		$this->Setting->recursive = 0;
		$this->set('settings', $this->paginate(null, $conditions));
	}

	function admin_add() {
		if (!empty($this->request->data)) {
			$this->Setting->create();
			$this->request->data['Setting']['site_id'] = $this->currentSite['Site']['id'];

			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please try again.'));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid setting'));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {
			$this->request->data['Setting']['site_id'] = $this->currentSite['Site']['id'];

			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Setting->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for setting'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Setting->delete($id)) {
			$this->Session->setFlash(__('Setting deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Setting was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
?>