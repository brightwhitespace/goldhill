<?php

class PostsController extends AppController
{
	var $name = 'Posts';
	var $layout = 'admin';
	var $pageTitle = 'Posts';
	var $uses = array('Post','Page','Glossary','Site');
	var $components = array('Upload','PImage','Paginator');
	
	function display($url = null)
	{
		$this->layout = 'default';

		// check if page exists with direct_url the same as the current base url
		$page = $this->Page->getPage(null, true, null, null, $this->currentBaseUrl);
		if (!$page) {
			// direct_url not matched, so try to get the top-level news/blog page using the site url and its blog_url
			$topLevelBlogUrl = '/' . $this->currentSite['Site']['url'] . '/' . $this->currentSite['Site']['blog_url'];
			$page = $this->Page->getPage(null, true, null, null, $topLevelBlogUrl);

			if (!$page) {
				// no top level blog page so just use homepage
				$page = $this->Page->getHomePage($this->currentSite);
			}
		}
		$parentPage = $this->Page->getParentPage($page['Page']['id']);

		$parentIds = $this->Page->getParentIdsOfActivePage($page);
		$this->set('parentIds', $parentIds);

		// get parent landing page for page
		if ($parentIds) {
			$parentLandingPage = $this->Page->findById($parentIds[0]);
		} else {
			$parentLandingPage = $page;
		}
		$this->set('parentLandingPage', $parentLandingPage);

		// get adverts for page
		$this->set('adverts', $this->Page->Advert->getAdverts($page));

		$this->_setMetaData($page['Page']);
		$this->set(compact('page','parentPage'));
		$this->set('bannerPosts', array());

		// initially attempt to get category using passed in url
		$category = $url ? $this->Post->PostCategory->findByUrl($url) : null;

		// if url passed in but category was not found then it must be the url for a post
		if ($url && !$category) {
			// attempt to get post using passed in url
			$this->Post->recursive = 1;
			if ($post = $this->Post->findByUrl($url)) {
				$post = $this->_applyFilters($post);

				$this->_setMetaData($post['Post']);
				$this->set('post', $post);
				$this->render("view");
			} else {
				$this->cakeError('error404', array(array('url' => '/')));
			}

		} else {
			// will be listing posts, initialise paginator options to create correct url
			$paginatorOptions = array('controller' => 'aa', 'action' => 'uk', 'news');

			// filter by category if set
			if ($category) {
				$this->set('category', $category);
				$this->set('categoryHeading', $category['PostCategory']);

				// add category url to paginator url
				$paginatorOptions[] = $category['PostCategory']['url'];

				$joins = array();
				$joins[] = array(
					'table' => 'post_categories_posts',
					'alias' => "PostCategoriesPost",
					'type' => 'inner',
					'conditions' => array(
						"PostCategoriesPost.post_id = Post.id"
					)
				);

				$joins[] = array(
					'table' => 'post_categories',
					'alias' => "PostCategory",
					'type' => 'inner',
					'conditions' => "PostCategory.id = PostCategoriesPost.post_category_id AND PostCategory.url = '" . $category['PostCategory']['url'] . "'"
				);

				$this->paginate = array(
					'order' => 'Post.date DESC',
					'limit' => 10,
					'contain' => array('PostCategory', 'Photo'),
					'joins' => $joins,
				);

			} else {
				// no url passed so will just display all posts
				$this->set('categoryHeading', 'All News');

				$this->paginate = array(
					'order'=>'Post.date DESC',
					'limit'=> 10,
					'contain' => array('PostCategory','Photo'),
				);
			}

			$posts = $this->paginate('Post', array('Post.published'=>1));
			$this->set('posts', $posts);

			$this->set('paginatorOptions', $paginatorOptions);

			$this->render("index");
		}
	}
	
	function feed(){
		$this->layout = "rss";
		$posts = $this->Post->find('all',array('order'=>'Post.date DESC','limit'=>5));
		$this->set('posts',$posts);
		
		header("Content-type: text/xml");
	}
	
	function index(){
		
		$this->layout = 'default';
		
		$this->paginate = array(
				'order'=>'date DESC',
				'limit'=>3
		);
		$page = $this->Page->find('first',array('conditions'=>array('Page.page_title'=>'news')));
		$this->_setMetaData($page['Page']);
		
        $this->set('page',$page);
		
		$this->set('posts',$this->Post->find('all',array('conditions'=>array('Post.published'=>1),'order'=>'date DESC')));
		
	}
	
	function view($id=null){
		$this->layout = 'work';
		
		$this->paginate = array(
				'order'=>'date DESC',
				'limit'=>3
		);
		
		$page = $this->Page->find('first',array('conditions'=>array('Page.page_title'=>'Blog')));
		$this->_setMetaData($page['Page']);
		$this->set('pageheader_for_layout',$page['Page']['page_image']);
		$this->set('page',$page);
        $this->set('latestPosts', $this->paginate(null,array('Post.published'=>1)));
		$this->set('post_categories', $this->Post->PostCategory->find('all',array('order'=>'PostCategory.order_num')));
		
		if($id){
			//get post
			$this->set('selPost',$this->Post->read(null,$id));
		}
	}
	
	function refresh(){
		$this->layout = 'ajax';
		$this->set('posts',$this->Post->getLatest(2,1));
	}
	
	function admin_index() {
		$this->layout = 'admin';
		$this->set('title_for_layout','Posts');
		
		$this->Post->recursive = -1;
		$this->paginate = array(
				'order'=>'date DESC'				
		);
		$this->set('posts', $this->paginate());
	}

	function admin_add() {
		$this->layout = 'admin';
		$this->set('title_for_layout','Posts - add new post');
		if (empty($this->request->data)) {
			$this->request->data['Post']['user_id'] = $this->currentUser['User']['id'];
			$this->setReturnUrl($this->referer());
			
		} else {
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash('Post added successfully');
				if (isset($this->data['save_&_close'])) {
					$this->redirect('/admin/posts');
				} else {
					$this->redirect('/admin/posts/edit/'.$this->Post->id);
				}
			}
		}

		$this->set('post_categories', $this->Post->PostCategory->find('list'));
		$this->set('sites', $this->Post->Site->getSites(null, 'list'));

		// Initially include the site currently being edited in "Sites to appear on"
		$this->request->data['Site']['Site'][] = $this->currentSite['Site']['id'];
	}

	function admin_edit($id = null)
	{
		$this->layout = 'admin';
		$this->set('title_for_layout','Posts - edit post');

		$this->pageTitle .= ' - edit setting';
		if (empty($this->request->data)) {
			if (!$id) {
				$this->flash('Invalid id for Post', '/admin/posts/index');
			}
			$this->Post->recursive = 1;
			$this->setReturnUrl($this->referer());
			$this->request->data = $this->Post->read(null, $id);
		} else {
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash('Post saved successfully');
				if (isset($this->data['save_&_close'])) {
					$this->redirect('/admin/posts');
				}
				$this->Post->recursive = 1;
				$this->request->data = $this->Post->read(null, $this->Post->id);
			}
			
		}
		$this->set('post_categories', $this->Post->PostCategory->find('list'));
		$this->set('sites', $this->Post->Site->getSites(null, 'list'));
	}
	
	function admin_preview($id = null)
	{
		$this->layout = 'default';
		
		$this->paginate = array(
				'order'=>'date DESC',
				'limit'=>3
		);
		
        $this->set('latestPosts', $this->paginate(null,array('Post.published'=>1)));
		$this->set('post_categories', $this->Post->PostCategory->find('all',array('order'=>'PostCategory.name')));
		
		if ($id) {
			//get post
			$post = $this->Post->read(null,$id);
			$this->set('post',$post);
			$this->_setMetaData($post['Post']);
		}
		
		$this->render('view');
	}

	function admin_delete($id = null)
	{
		$this->layout = 'admin';
		if (!$id) {
			$this->flash('Invalid id for Post', '/admin/posts/index');
		}
		if ($this->Post->delete($id)) {
			$this->Session->setFlash('Post deleted successfully');
			$this->redirect('/admin/posts/');
		}
	}

	function _applyFilters($post)
	{
		$terms = $this->Glossary->getTerms();
		
		$post['Post']['body'] = $this->_applyGlossary($post['Post']['body'],$terms);		
		
		return $post;
	}
	
	function _applyGlossary($str,$terms){
		
		foreach($terms as $term){
				
			//$find[] = $term['Glossary']['term'];
			//$definitions[] = '<span class="glossary" title="'.$term['Glossary']['definition'].'">'.$term['Glossary']['term'].'</span>';
			//search for lowercase and upper case of the terms
			
			$strFind = str_replace("/"," ",$term['Glossary']['term']);
			
			$find[] = "/(?<![a-zA-Z-])(".$strFind.")(?![a-zA-Z])(?!([^<]+)?>)/i";
			$definitions[] = '<span class="glossary" title="'.$term['Glossary']['definition'].'">'."\${1}</span>";
		}
		
		return preg_replace($find, $definitions, $str);
	}
	
}

?>