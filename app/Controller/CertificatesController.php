<?php
class CertificatesController extends AppController {

	var $name = 'Certificates';
	var $layout = 'admin';
	var $uses = array('Certificate', 'Employee', 'CertificatesEmployee');

	var $expiryNotificationTimeOptions = array(
		'7' => '1 week',
		'14' => '2 weeks',
		'31' => '1 month',
		'62' => '2 months',
		'92' => '3 months',
		'123' => '4 months',
		'153' => '5 months',
		'183' => '6 months',
		'214' => '7 month',
		'245' => '8 months',
		'275' => '9 months',
		'306' => '10 months',
		'336' => '11 months',
		'366' => '12 months',
	);

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			$conditions['name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		$this->paginate = array('conditions' => $conditions);
		$this->Certificate->recursive = 0;
		$this->set('certificates', $this->paginate());

		$this->set('expiryNotificationTimeOptions', $this->expiryNotificationTimeOptions);
	}

	function admin_add()
	{
		$this->set('title_for_layout','Certificates - Add New Certificate');

		if (!empty($this->request->data)) {
			$this->Certificate->create();

			if ($this->Certificate->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The certificate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The certificate could not be saved. Please try again.'));
			}
		}

		$this->set('expiryNotificationTimeOptions', $this->expiryNotificationTimeOptions);
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Certificates - Edit Certificate');

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid certificate'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Certificate->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The certificate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The certificate could not be saved. Please try again.'));
			}

		} else {
			$this->request->data = $this->Certificate->read(null, $id);
		}

		$this->set('expiryNotificationTimeOptions', $this->expiryNotificationTimeOptions);
	}

	function admin_archive($id, $archive = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for certificate'));
			$this->redirect($this->referer());
		}

		$this->Certificate->id = $id;
		$this->Certificate->saveField('archived', $archive);
		$this->Session->setFlash(__('Certificate was ' . ($archive ? 'archived' : 'restored from archive')));

		$this->redirect($this->referer());
	}

	function admin_employee_matrix()
	{
		// get current employees
		$employees = $this->Employee->getEmployees('all', false, array('CertificatesEmployee'));

		// For each employee, set the key of each CertificatesEmployee record they have to be the certificate_id
		// so can easily find the certificate rather than looping through all records each time
		// NOTE: this process will automatically filter out older records that have the same certificate id
		foreach ($employees as $idx => $employee) {
			$employees[$idx]['CertificatesEmployee'] = Hash::combine($employee['CertificatesEmployee'], '{n}.certificate_id', '{n}');
		}

		$this->set('employees', $employees);
		$this->set('certificates', $this->Certificate->find('all'));
		$this->set('employeeList', $this->Employee->getEmployees('list', false));
		$this->set('certificateList', $this->Certificate->find('list'));

		//Debugger::dump($employees);
	}

	function admin_add_employee_certificate($employeeId, $certificateId, $endDate = null, $startDate = null, $ignorePreviousRecords = 0)
	{
		$this->layout = 'ajax';

		$employee = $this->Employee->get($employeeId);
		$certificate = $this->Certificate->findById($certificateId);

		if ($employee && $certificate) {
			$data = array('CertificatesEmployee' => array(
				'employee_id' => $employeeId,
				'certificate_id' => $certificateId,
				'end_date' => $endDate ? date('Y-m-d', strtotime($endDate)) : null,
				'start_date' => $startDate ? date('Y-m-d', strtotime($startDate)) : null,
				'ignore_previous_records' => $ignorePreviousRecords,
			));

			$this->CertificatesEmployee->create();

			if ($this->CertificatesEmployee->save($data)) {
				$this->set('success', true);
				$this->set('certificate', $certificate);

				// get updated list of certificates for employee
				$employee = $this->Employee->get($employeeId);
				$employee['CertificatesEmployee'] = Hash::combine($employee['CertificatesEmployee'], '{n}.certificate_id', '{n}');

				$this->set('employee', $employee);
			}
		}
	}

	function admin_order()
	{
		$this->set('title_for_layout','Change Certificate Order - '.$this->settings['site_name']);

		$this->Certificate->recursive = -1;
		$this->paginate = array(
			'order'=>'name ASC'
		);

		$this->set('certificates', $this->Certificate->find('all',array(
			'conditions'=> $this->currentSite ? array('Certificate.site_id' => $this->currentSite['Site']['id']) : array(),
			'order'=>array('Certificate.order_num')
		)));
	}

	function admin_changeorder()
	{
		$order_num = 0;
		foreach ($this->data['certificate'] as $id => $parent){
			$this->request->data['Certificate']['order_num'] = $order_num;
			//$this->request->data['Certificate']['parent_id'] = $parent;
			$this->request->data['Certificate']['id'] = $id;

			$this->Certificate->save($this->request->data,false);
			$order_num ++;
		}

		$this->layout = 'ajax';

		exit();
	}
}
