<?php
class ReportsController extends AppController
{

	var $name = 'Reports';
	var $layout = 'admin';
	var $uses = array('AllTimesheetEntries','Employee', 'Timesheet', 'EmployeesTimesheet', 'Project', 'ProjectItem', 'PurchaseOrder', 'Labour', 'Supplier', 'Client', 'Invoice','Quote');
	var $components = array('Mpdf');

	function admin_index()
	{
	}

	function admin_quotes_due_back()
	{
		$this->processSearchData($this->name . '.QuotesDue');

		// only include uncompleted, non-archived quotes
		$conditions = array(
			'Quote.completed = 0',
		);

		$options = array(
			'conditions' => $conditions,
			'contain' => 'Client',
			'order' => array('IFNULL(Quote.quote_due, Quote.created) ASC')
		);
		$this->Quote->recursive = 0;
		$this->set('quotes', $this->Quote->find('all', $options));
	}

	function admin_quotes_with_site_visits()
	{
		$this->processSearchData($this->name . '.QuotesWithSiteVisits');

		// only include uncompleted, non-archived quotes
		$conditions = array(
			'Quote.site_visit = 1',
			'Quote.completed = 0',
		);

		$options = array(
			'conditions' => $conditions,
			'contain' => 'Client',
			'order' => array('IFNULL(Quote.site_visit_date, Quote.created) ASC')
		);
		$this->Quote->recursive = 0;
		$this->set('quotes', $this->Quote->find('all', $options));
	}

	/**
	 * @param null $output Pass 'csv' to download csv file
	 */
	function admin_agency_labourers($output = null)
	{
		$this->processSearchData($this->name . '.AgencyLabourers');

		// create date range dropdown options (weekly ranges going back to include specified earliest date)
		$earliestTimesheet = $this->Timesheet->find('first', array('order' => 'date ASC'));
		$dateRangeOptions = $this->_getWeeklyDateRangeOptions('sunday', $earliestTimesheet['Timesheet']['date']);

		$this->set('dateRangeOptions', $dateRangeOptions);

		$this->_workOutStartAndEndDatesFromSearchData($dateRangeOptions, $earliestTimesheet['Timesheet']['date']);

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		if ($endDate >= $startDate) {

			// get DatePeriod  - need to add 1 day to (clone of) end date as last day of DatePeriod does not get included!
			$endDatePlus1Day = clone $endDate;
			$endDatePlus1Day->modify('+1 day');
			$datePeriod = new DatePeriod($startDate, new \DateInterval('P1D'), $endDatePlus1Day);

			$timesheetEntries = $this->AllTimesheetEntries->find('all', array(
				'conditions' => array(
					'date >=' => $startDate->format('Y-m-d'),
					'date <=' => $endDate->format('Y-m-d'),
					'supplier_id IS NOT NULL',
				),
				'order' => 'project_name, supplier_name, employee_name, date'
			));

			/* build array of projects from flat data rows as follows: (note values in "..." are example data)
				[ "Strip Out Project 123" =>
					[ 'project_type_id' => 1,
					  'suppliers' =>
						[ "Agency/Supplier 1" =>
							[ 'employees' =>
								[ "John Smith" =>
									[ 'total_hours_worked' => 40,	// sum of all hours_worked in 'days' array
									  'total_cost' => 450,			// sum of all cost in 'days' array
									  'average_rate_ph' => 10.5,	// average of all rate_ph in 'days' array
									  'days' =>
										[ "14-10-2016" =>
											[ 'start_time' => "07.00",
											  'end_time' => "16.30"
											  'break_hours' => 1.0
											  'hours_worked' => 8.5  (end - start - break)
											  'rate_ph' => 10.5
											  'cost' => 89.25
											]
										]
									]
								]
							]
						]
					]
				]
			*/

			$projects = array();

			$employeeTotalHoursWorked = 0;
			$employeeTotalCost = 0;
			$employeeTotalDaysWorked = 0;
			$employeeTotalRatePH = 0;

			$previousProjectName = '';
			$previousSupplierName = '';
			$previousEmployeeName = '';

			// loop through each timesheet entry and build multi-level array
			foreach ($timesheetEntries as $timesheetEntry) {

				$timesheetEntry = $timesheetEntry['AllTimesheetEntries'];

				$projectName = $timesheetEntry['project_name'];
				$supplierName = $timesheetEntry['supplier_name'];
				$employeeName = $timesheetEntry['employee_name'];
				$dateDmy = $timesheetEntry['date_dmy'];

				$day = array(
					'start_time' => $timesheetEntry['start_time'],
					'end_time' => $timesheetEntry['end_time'],
					'break_hours' => $timesheetEntry['break_hours'],
					'hours_worked' => $timesheetEntry['hours_worked'],
					'rate_ph' => $timesheetEntry['rate_ph'],
				);

				// Save the day details for the current project/supplier/employee/date combination
				// NOTE: using the project name, supplier name, employee name AND date as keys into each level of the
				// array so when new project/supplier/employee/date occurs the appropriate multi-level entry will be created
				$projects[$projectName]['suppliers'][$supplierName]['employees'][$employeeName]['days'][$dateDmy] = $day;

				if (!isset($projects[$projectName]['project_type_id'])) {
					$projects[$projectName]['project_type_id'] = $timesheetEntry['project_type_id'];
				}

				if ($employeeName != $previousEmployeeName) {
					// got a new employee so write in the previous employee's running totals
					if ($previousEmployeeName) {
						$projects[$previousProjectName]['suppliers'][$previousSupplierName]['employees'][$previousEmployeeName]['total_hours_worked'] = $employeeTotalHoursWorked;
						$projects[$previousProjectName]['suppliers'][$previousSupplierName]['employees'][$previousEmployeeName]['total_cost'] = $employeeTotalCost;
						$projects[$previousProjectName]['suppliers'][$previousSupplierName]['employees'][$previousEmployeeName]['average_rate_ph'] = $employeeTotalRatePH / $employeeTotalDaysWorked;
					}

					// reset the running totals to the current values
					$employeeTotalHoursWorked = $timesheetEntry['hours_worked'] * 1;  // * 1 to get rid of 4 decimal place number
					$employeeTotalCost = $timesheetEntry['cost'] * 1;
					$employeeTotalRatePH = $timesheetEntry['rate_ph'];
					$employeeTotalDaysWorked = 1;

					// remember this new employee for next time round (need to remember project and supplier in case these change too)
					$previousProjectName = $projectName;
					$previousSupplierName = $supplierName;
					$previousEmployeeName = $employeeName;

				} else {
					// same employee as previously so add hours_worked, cost & rate_ph to running totals
					$employeeTotalHoursWorked += $timesheetEntry['hours_worked'];
					$employeeTotalCost += $timesheetEntry['cost'];
					$employeeTotalRatePH += $timesheetEntry['rate_ph'];
					$employeeTotalDaysWorked++;
				}
			}

			// When loop finishes the last employee will not have their totals inserted so do this now
			if ($previousEmployeeName) {
				$projects[$previousProjectName]['suppliers'][$previousSupplierName]['employees'][$previousEmployeeName]['total_hours_worked'] = $employeeTotalHoursWorked;
				$projects[$previousProjectName]['suppliers'][$previousSupplierName]['employees'][$previousEmployeeName]['total_cost'] = $employeeTotalCost;
				$projects[$previousProjectName]['suppliers'][$previousSupplierName]['employees'][$previousEmployeeName]['average_rate_ph'] = $employeeTotalRatePH / $employeeTotalDaysWorked;
			}

			$this->set('reportHeading', sprintf('Agency Labourers  (%s to %s)', $startDate->format('j M Y'), $endDate->format('j M Y')));

		} else {
			$projects = array();
			$datePeriod = array();
		}

		$this->set('projects', $projects);
		$this->set('days', $datePeriod);

		// Check if csv download requested...
		if ($output == 'csv') {
			$this->response->download(sprintf('agency_abourers_%s_to_%s.csv', $startDate->format('j_M_Y'), $endDate->format('j_M_Y')));
			$this->layout = 'ajax';

			$this->render('admin_agency_labourers_csv');
		}

	}

	/**
	 * @param null $output Pass 'pdf' to download PDF, 'view' to view it
	 */
	function admin_weekly_analysis($output = null)
	{
		$this->processSearchData($this->name . '.WeeklyAnalysis');

		// create date range dropdown options (weekly ranges going back to include specified earliest date)
		$earliestTimesheet = $this->Timesheet->find('first', array('order' => 'date ASC'));
		$dateRangeOptions = $this->_getWeeklyDateRangeOptions('sunday', $earliestTimesheet['Timesheet']['date']);

		$this->set('dateRangeOptions', $dateRangeOptions);

		$this->_workOutStartAndEndDatesFromSearchData($dateRangeOptions, $earliestTimesheet['Timesheet']['date']);

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		// Handle display/selection of Client (default to City Office Interiors, id = 3)
		$clients = $this->Client->find('list');
		$this->set('clients', $clients);
		if (empty($this->request->data['Search']['client_id'])) {
			$this->request->data['Search']['client_id'] = 3;
		}

		if ($endDate >= $startDate) {

			// get DatePeriod  - need to add 1 day to (clone of) end date as last day of DatePeriod does not get included!
			$endDatePlus1Day = clone $endDate;
			$endDatePlus1Day->modify('+1 day');
			$datePeriod = new DatePeriod($startDate, new \DateInterval('P1D'), $endDatePlus1Day);

			$employeeTimesheets = $this->AllTimesheetEntries->find('all', array(
				'fields' => 'employee_name, date, date_dmy, project_name, project_type_id',
				'conditions' => array(
					'date >=' => $startDate->format('Y-m-d'),
					'date <=' => $endDate->format('Y-m-d'),
					'client_id' => $this->request->data['Search']['client_id']
				),
				'order' => 'employee_name, date ASC'
			));

			$employees = array();

			foreach ($employeeTimesheets as $employeeTimesheet) {

				$employeeName = $employeeTimesheet['AllTimesheetEntries']['employee_name'];
				$date = $employeeTimesheet['AllTimesheetEntries']['date_dmy'];
				$projectType = $employeeTimesheet['AllTimesheetEntries']['project_type_id'] == 1 ? 'P' : 'D';  // 1 = Project/Stripout; 2 = Dayworks

				if (!isset($employees[$employeeName][$date])) {
					$employees[$employeeName][$date]['D'] = array();
					$employees[$employeeName][$date]['P'] = array();
				}

				$employees[$employeeName][$date][$projectType][] = $employeeTimesheet['AllTimesheetEntries']['project_name'];
			}

			$this->set('reportHeading', sprintf('%s - Weekly Labour Analysis w/c %s', $clients[$this->request->data['Search']['client_id']], $startDate->format('jS M Y')));

		} else {
			$employees = array();
			$datePeriod = array();
		}

		$this->set('employees', $employees);
		$this->set('days', $datePeriod);

		// Check if pdf download requested and output as necessary
		if ($output == 'pdf' || $output == 'view') {
			$this->layout = 'pdf_report';

			// just download the pdf with simple filename (i.e. not full path)
			$this->Mpdf->init(array(
				'format' => 'A4-L',
				'margin_top' => 34,
				'margin_bottom' => 10,
				'margin_header' => 8,
				'margin_footer' => 5
			));

			$this->Mpdf->setFilename($this->_getWeeklyAnalysisFilename($this->request->data['Search']['client_id'], $clients[$this->request->data['Search']['client_id']], $startDate));
			$this->Mpdf->setOutput($output == 'pdf' ? 'D' : 'I');

			$this->render('admin_weekly_analysis_pdf');
		}
	}

	/**
	 * Project timesheet report is a combined report of timesheet entries and project items
	 *
	 * @param string $output If PDF download then pass "pdf", if saving PDF then pass "save", if viewing PDF then pass "view" (any other string just displays on screen as normal)
	 * @param string $projectId If included then ignore date range and include all timesheet entries
	 */
	function admin_project_timesheet($output = null, $projectId = null)
	{
		if ($output != 'save') {
			$this->processSearchData($this->name . '.ProjectTimesheet');
		}

		// If projectId passed in then set date range to cover May 2016 until today
		if ($projectId && ($project = $this->Project->getProject($projectId))) {
			$this->request->data['Search']['start_date'] = date('d-m-Y', strtotime('2016-05-01'));
			$this->request->data['Search']['end_date'] = date('d-m-Y');
		}

		// create date range dropdown options (weekly ranges going back to include specified earliest date)
		$earliestTimesheet = $this->Timesheet->find('first', array('order' => 'date ASC'));
		$dateRangeOptions = $this->_getWeeklyDateRangeOptions('wednesday', $earliestTimesheet['Timesheet']['date']);

		$this->set('dateRangeOptions', $dateRangeOptions);

		$this->_workOutStartAndEndDatesFromSearchData($dateRangeOptions, $earliestTimesheet['Timesheet']['date']);

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		// Handle display/selection of Client and Project... (these lists will also be used to create report heading)
		$clients = $this->Client->find('list');
		$this->set('clients', $clients);
		if (empty($this->request->data['Search']['client_id'])) {
			// default client to "City Office Interiors" if no client chosen
			$this->request->data['Search']['client_id'] = 3;
		}
		// get projects that were live at any point during the currently selected date range
		$projects = $this->Project->getProjects($this->request->data['Search']['client_id'], null, null, 'list', $startDate, $endDate);
		$this->set('projects', $projects);

		// if selected project id is no longer in the projects list (because dates have changed) then reset it to empty
		if (!empty($this->request->data['Search']['project_id']) && !array_key_exists($this->request->data['Search']['project_id'], $projects)) {
			$this->request->data['Search']['project_id'] = '';
		}

		// if valid projectId passed in then set search data appropriately
		if (!empty($project)) {
			$this->request->data['Search']['client_id'] = $project['Client']['id'];
			$this->request->data['Search']['project_id'] = $projectId;
		}

		if ($endDate >= $startDate && !empty($this->request->data['Search']['project_id'])) {

			$timesheets = $this->AllTimesheetEntries->find('all', array(
				'conditions' => array(
					'date >=' => $startDate->format('Y-m-d'),
					'date <=' => $endDate->format('Y-m-d'),
					'project_id' => $this->request->data['Search']['project_id']
				),
				'order' => 'date ASC, employee_name'
			));

			$projectItems = $this->ProjectItem->find('all', array(
				'conditions' => array(
					'ProjectItem.date >=' => $startDate->format('Y-m-d'),
					'ProjectItem.date <=' => $endDate->format('Y-m-d'),
					'ProjectItem.project_id' => $this->request->data['Search']['project_id']
				),
				'order' => 'ProjectItem.date ASC'
			));

			$daysTemp = array();  // temporary as will need to sort it by "Y-m-d" date key and then convert date key to full "day & date" string

			// add timesheet entries (indexed by date) to array of days
			foreach ($timesheets as $timesheet) {
				$dateKey = date('Y-m-d', strtotime($timesheet['AllTimesheetEntries']['date_dmy']));
				$daysTemp[$dateKey]['employee_timesheets'][] = $timesheet['AllTimesheetEntries'];
			}

			// add project items (indexed by date) to array of days
			foreach ($projectItems as $projectItem) {
				$dateKey = date('Y-m-d', strtotime($projectItem['ProjectItem']['date']));
				$daysTemp[$dateKey]['project_items'][] = $projectItem['ProjectItem'];
			}

			// sort the days array (otherwise project items on a day with no corresponding timesheet entries will appear at end of list)
			ksort($daysTemp);
			$days = array();
			foreach ($daysTemp as $dateKey => $val) {
				$newDateKey = date('j M Y (D)', strtotime($dateKey));
				$days[$newDateKey] = $val;
			}

			// if showing all entries for a particular project then set start & end dates to cover the results
			if (!empty($project)) {
				if (!empty($daysTemp)) {
					reset($daysTemp);
					$startDate = DateTime::createFromFormat('Y-m-d', key($daysTemp));
					end($daysTemp);
					$endDate = DateTime::createFromFormat('Y-m-d', key($daysTemp));
				} else {
					// no results so try to get start/end dates from project instead (if no project start date then use project created date)
					$startDate = $project['Project']['start_date'] ? DateTime::createFromFormat('d-m-Y', $project['Project']['start_date']) : DateTime::createFromFormat('Y-m-d', substr($project['Project']['created'], 0, 10));
					$endDate = $project['Project']['end_date'] ? DateTime::createFromFormat('d-m-Y', $project['Project']['end_date']) : $endDate;
				}

				$this->request->data['Search']['start_date'] = $startDate->format('d-m-Y');
				$this->request->data['Search']['end_date'] = $endDate->format('d-m-Y');
			}

			$this->set('days', $days);

			$projectName = !empty($project) ? $project['Project']['name'] : $projects[$this->request->data['Search']['project_id']];
			if ($startDate != $endDate) {
				$this->set('reportHeading', sprintf('%s to %s - %s (%s)', $startDate->format('j M Y'), $endDate->format('j M Y'), $projectName, $clients[$this->request->data['Search']['client_id']]));
			} else {
				$this->set('reportHeading', sprintf('%s - %s (%s)', $startDate->format('j M Y'), $projectName, $clients[$this->request->data['Search']['client_id']]));
			}

			// Check if pdf download/save/view requested and output as necessary
			if (in_array($output, array('pdf', 'save', 'view'))) {
				$this->layout = 'pdf_report';

				if ($startDate != $endDate) {
					$this->set('reportHeading', sprintf('%s TIMESHEET<br><br>Site: <span style="font-weight: normal">%s  (%s to %s)</span>', strtoupper($clients[$this->request->data['Search']['client_id']]), $projectName, $startDate->format('j M Y'), $endDate->format('j M Y')));
				} else {
					$this->set('reportHeading', sprintf('%s TIMESHEET<br><br>Site: <span style="font-weight: normal">%s  (%s)</span>', strtoupper($clients[$this->request->data['Search']['client_id']]), $projectName, $startDate->format('j M Y')));
				}

				// just download the pdf with simple filename (i.e. not full path)
				$this->Mpdf->init(array(
					'format' => 'A4-L',
					'margin_top' => 34,
					'margin_bottom' => 10,
					'margin_header' => 8,
					'margin_footer' => 5
				));

				$fullFilepath = ($output == 'save');
				$outputType = $output == 'pdf' ? 'D' : ($output == 'view' ? 'I' : 'F');

				$this->Mpdf->setFilename($this->_getProjectTimesheetFilename($this->request->data['Search']['project_id'], $projectName, $fullFilepath));
				$this->Mpdf->setOutput($outputType);

				$this->render('admin_project_timesheet_pdf');
			}

		}

	}

	function admin_labour()
	{
		$this->processSearchData($this->name . '.Labour');

		// create date range dropdown options (weekly ranges going back to include specified earliest date)
		$earliestLabour = $this->Labour->find('first', array('order' => 'date ASC'));
		$dateRangeOptions = $this->_getWeeklyDateRangeOptions('wednesday', $earliestLabour['Labour']['date']);

		$this->set('dateRangeOptions', $dateRangeOptions);

		$this->_workOutStartAndEndDatesFromSearchData($dateRangeOptions, $earliestLabour['Labour']['date']);

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		if ($endDate >= $startDate) {

			$labours = $this->Labour->find('all', array(
				'fields' => 'Labour.*, Project.name',
				'joins' => array(
					array('table' => 'projects',
						'alias' => 'Project',
						'type' => 'INNER',
						'conditions' => array(
							'Project.id = Labour.project_id',
						)
					)
				),
				'conditions' => array(
					'Labour.date >=' => $startDate->format('Y-m-d'),
					'Labour.date <=' => $endDate->format('Y-m-d'),
				),
				'order' => 'Labour.date ASC, Project.name'
			));

			$days = array();

			foreach ($labours as $labour) {

				$dateKey = date('j M Y (D)', strtotime($labour['Labour']['date']));
				$projectKey = $labour['Project']['name'];

				$days[$dateKey][$projectKey][] = $labour;
			}

			$this->set('reportHeading', sprintf('Labour - %s to %s', $startDate->format('j M Y'), $endDate->format('j M Y')));
			$this->set('days', $days);
		}

	}


	function admin_wages()
	{
		$this->processSearchData($this->name . '.Wages');

		// create date range dropdown options (weekly ranges going back to include specified earliest date)
		$earliestTimesheet = $this->Timesheet->find('first', array('order' => 'date ASC'));
		$dateRangeOptions = $this->_getWeeklyDateRangeOptions('sunday', $earliestTimesheet['Timesheet']['date']);

		$this->set('dateRangeOptions', $dateRangeOptions);

		$this->_workOutStartAndEndDatesFromSearchData($dateRangeOptions, $earliestTimesheet['Timesheet']['date']);

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		if ($endDate >= $startDate) {

			// get DatePeriod  - need to add 1 day to (clone of) end date as end day does not get included
			$endDatePlus1Day = clone $endDate;
			$endDatePlus1Day->modify('+1 day');
			$datePeriod = new DatePeriod($startDate, new \DateInterval('P1D'), $endDatePlus1Day);

			$employees = $this->Employee->find('all', array(
				'fields' => 'Employee.id, Employee.name, Employee.archived, EmployeePaymentMethod.name',
				'contain' => array(
					'EmployeesTimesheet' => array(
						'fields' => array('cost','hours_worked'),
						'Timesheet' => array(
							'fields' => 'Timesheet.date',
							'conditions' => array(
								'Timesheet.date >=' => $startDate->format('Y-m-d'),
								'Timesheet.date <=' => $endDate->format('Y-m-d'),
							)
						)
					),
					'EmployeePaymentMethod'
				),
				'order' => 'EmployeePaymentMethod.id, SUBSTRING(Employee.name, LOCATE(" ", Employee.name))'
			));

			foreach ($employees as $empIdx => $employee) {

				$employeeTotalWage = 0;
				// loop through each day in the date period...
				foreach ($datePeriod as $date) {
					$empTimesheetDateToMatch = $date->format('d-m-Y');

					// look through EmployeesTimesheet for Timesheets that have corresponding day
					// and sum the "cost" fields of each EmployeesTimesheet to get total for the day
					// (NOTE: if Timesheet['date'] is not set then this timesheet does not fall in the date period,
					//  so can unset it to remove it from further consideration)
					$dayWage = 0;
					$dayHours = 0;
					foreach ($employee['EmployeesTimesheet'] as $empTsIdx => $employeeTimesheet) {
						if (!empty($employeeTimesheet['Timesheet']['date'])) {
							$dayWage += ($employeeTimesheet['Timesheet']['date'] == $empTimesheetDateToMatch) ? $employeeTimesheet['cost'] : 0;
							$dayHours += ($employeeTimesheet['Timesheet']['date'] == $empTimesheetDateToMatch) ? $employeeTimesheet['hours_worked'] : 0;
						} else {
							unset($employee['EmployeesTimesheet'][$empTsIdx]);
						}
					}
					$employees[$empIdx]['wage_by_day'][$date->format('d/m/Y')] = $dayWage;
					$employees[$empIdx]['hours_by_day'][$date->format('d/m/Y')] = $dayHours;
					$employeeTotalWage += $dayWage;
				}

				// find any wage adjustment records for the period and apply to the employee's total
				$adjustments = $this->Employee->WageAdjustment->find('all', array(
					'conditions' => array(
						'WageAdjustment.employee_id' => $employee['Employee']['id'],
						'WageAdjustment.date >=' => $startDate->format('Y-m-d'),
						'WageAdjustment.date <=' => $endDate->format('Y-m-d'),
					),
				));

				$adjustmentsTotal = 0;
				$damageRepaymentsTotal = 0;
				foreach ($adjustments as $adjustment) {
					// check if this is damage repayment or normal adjustment
					if ($adjustment['WageAdjustment']['is_damage_repayment']) {
						$damageRepaymentsTotal += $adjustment['WageAdjustment']['value'];
					} else {
						$adjustmentsTotal += $adjustment['WageAdjustment']['value'];
					}
				}
				$employees[$empIdx]['adjustments_total'] = $adjustmentsTotal;
				$employees[$empIdx]['damage_repayments_total'] = $damageRepaymentsTotal;

				$employees[$empIdx]['wage_total'] = $employeeTotalWage + $adjustmentsTotal + $damageRepaymentsTotal;
			}

			$this->set('reportHeading', sprintf('Wages - %s to %s', $startDate->format('j M Y'), $endDate->format('j M Y')));

		} else {
			$employees = array();
			$datePeriod = array();
		}

		$this->set('employees', $employees);
		$this->set('days', $datePeriod);
		$this->set('paymentMethodList', $this->Employee->EmployeePaymentMethod->find('list'));
	}


	function admin_employee_hours_worked($employeeId = null)
	{
		// if employee id passed in then make sure it is set in session search data (if this is not first time in)
		if ($employeeId && $this->data) {
			$this->request->data['Search']['employee_id'] = $employeeId;
		}

		$this->processSearchData($this->name . '.EmployeeHoursWorked');

		$sixDaysAgo = DateTime::createFromFormat('d-m-Y', date('d-m-Y', strtotime('-6 days')));

		// if this is first time in page then set last seven days by default, also check if employee id passed in
		if (!$this->data || empty($this->request->data['Search']['employee_id'])) {
			$this->request->data = array();
			$this->request->data['Search']['start_date'] = $sixDaysAgo->format('d-m-Y');
			$this->request->data['Search']['end_date'] = date('d-m-Y');
			$this->request->data['Search']['employee_id'] = $employeeId ? $employeeId : '';
		}

		// use the dates specified in the date pickers
		if (empty($this->request->data['Search']['start_date'])) {
			$earliestTimesheet = $this->Timesheet->find('first', array('order' => 'date ASC'));
			$this->request->data['Search']['start_date'] = $earliestTimesheet['Timesheet']['date'];
		}
		if (empty($this->request->data['Search']['end_date'])) {
			$this->request->data['Search']['end_date'] = date('d-m-Y');
		}

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		$employee = $this->Employee->findById($this->request->data['Search']['employee_id']);

		if ($employee && $endDate >= $startDate) {

			$days = $this->Timesheet->getEmployeeHoursWorkedReportData($this->request->data['Search']['employee_id'], $startDate, $endDate);

			$this->set('days', $days);
		}

		$this->set('employee', $employee);
		$this->set('reportHeading', sprintf('Hours Worked: %s - %s to %s', !empty($employee) ? $employee['Employee']['name'] : '', $startDate->format('j M Y'), $endDate->format('j M Y')));
		$this->set('employees', $this->Employee->find('list', array('order' => 'SUBSTRING(Employee.name, LOCATE(" ", Employee.name))')));
	}


	function admin_diary()
	{
		$this->processSearchData($this->name . '.Diary');

		if (empty($this->request->data['Search']['date'])) {
			$this->request->data = array();
			$this->request->data['Search']['date'] = date('d-m-Y', strtotime('-1 day'));
		}

		$date = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['date']);

		$timesheets = $this->AllTimesheetEntries->find('all', array(
			'conditions' => array(
				'date = "' . $date->format('Y-m-d') . '"'
			),
			'order' => array(
				'project_name, supplier_name, employee_name'
			)
		));

		$labour = $this->Labour->find('all', array(
			'conditions' => array(
				'Labour.date = "' . $date->format('Y-m-d') . '"'
			)
		));

		$projects = array();

		// go through timesheets and add details of who worked to relevant project
		foreach ($timesheets as $timesheetEntry) {
			$projects = $this->_addTimesheetEntryToDiary($projects, $timesheetEntry);
		}

		// go through labour and add details to relevant project
		foreach ($labour as $lab) {
			$projects = $this->_addLabourToDiary($projects, $lab);
		}

		$this->set('projects', $projects);
		$this->set('dateHeading', $date->format('l, jS F Y'));
	}

	function _addTimesheetEntryToDiary($projects, $timesheetEntry)
	{
		$projects = $this->_includeProjectInDiary($projects, $timesheetEntry['AllTimesheetEntries']['project_id']);

		$projects[$timesheetEntry['AllTimesheetEntries']['project_id']]['items'][] = array(
			'title' => $timesheetEntry['AllTimesheetEntries']['supplier_name'],
			'description' => sprintf('%s (%s - %s)', h($timesheetEntry['AllTimesheetEntries']['employee_name']), $timesheetEntry['AllTimesheetEntries']['start_time'], $timesheetEntry['AllTimesheetEntries']['end_time'])
		);

		return $projects;
	}

	function _addLabourToDiary($projects, $labour)
	{

		$projId = $labour['Labour']['project_id'];


		$projects = $this->_includeProjectInDiary($projects, $projId);
		$projects[$projId]['items'][] = array(
			'title' => 'Labour:',
			'description' => $labour['Labour']['quantity'] . ' x £' . $labour['Labour']['rate'] . ' = £' . $labour['Labour']['total']
		);


		return $projects;
	}

	function _includeProjectInDiary($projects, $projId)
	{

		if (!array_key_exists($projId, $projects)) {
			$project = $this->Project->findById($projId);
			$projects[$projId] = array(
				'Project' => $project['Project'],
				'items' => array()
			);
		}

		return $projects;
	}

	function admin_pos_on_hire()
	{
		$this->processSearchData($this->name . '.PosOnHire');

		// only include "Hire" type POs that have not been returned
		$conditions = array(
			'PurchaseOrder.type_id = 1',
			'PurchaseOrder.returned = 0',
		);

		if ($this->data) {
			$conditions['or'] = array(
				'PurchaseOrder.ref LIKE' => '%' . $this->data['Search']['term'] . '%',
				'PurchaseOrder.desc LIKE' => '%' . $this->data['Search']['term'] . '%',
				'Project.name LIKE' => '%' . $this->data['Search']['term'] . '%',
				'Supplier.name LIKE' => '%' . $this->data['Search']['term'] . '%',
			);

		}

		$options = array(
			'conditions' => $conditions,
			'order' => array('PurchaseOrder.start_date' => 'asc')
		);
		$this->PurchaseOrder->recursive = 0;
		$this->set('purchase_orders', $this->PurchaseOrder->find('all', $options));
	}

	function admin_supplier_spend()
	{
		$this->processSearchData($this->name . '.SupplierSpend');

		$thisMonth = DateTime::createFromFormat('d-m-Y', '01-' . date('m-Y'));

		// create monthly ranges going back to include earliest PO as date range dropdown options
		$earliestPO = $this->PurchaseOrder->find('first', array('order' => 'created ASC'));
		$dateRangeOptions = array();
		do {
			$key = $thisMonth->format('d-m-Y') . '|' . $thisMonth->format('t-m-Y');
			$value = $thisMonth->format('F Y');
			$dateRangeOptions[$key] = $value;

			$thisMonth->modify('-1 month');
		} while (strtotime($thisMonth->format('t-m-Y')) >= strtotime($earliestPO['PurchaseOrder']['created']));

		$this->set('dateRangeOptions', $dateRangeOptions);

		// if this is first time in page then choose most recent date range by default
		if (!$this->data) {
			$this->request->data = array();
			$this->request->data['Search']['date_range_selected'] = 1;
			$this->request->data['Search']['date_range'] = '01-' . date('m-Y') . '|' . date('t-m-Y');
		}

		// if date range selected then set start and end dates from its key
		if (!empty($this->request->data['Search']['date_range_selected']) && $this->request->data['Search']['date_range']) {
			$dateRangeParts = explode('|', $this->request->data['Search']['date_range']);
			$this->request->data['Search']['start_date'] = $dateRangeParts[0];
			$this->request->data['Search']['end_date'] = $dateRangeParts[1];

		} else {
			// use the dates specified in the date pickers
			if (empty($this->request->data['Search']['start_date'])) {
				$this->request->data['Search']['start_date'] = $earliestPO['PurchaseOrder']['date'];
			}
			if (empty($this->request->data['Search']['end_date'])) {
				$this->request->data['Search']['end_date'] = date('d-m-Y');
			}
		}

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		if ($endDate >= $startDate) {
			if (isset($this->request->data['prev-month'])) {
				$startDate->modify('-1 month');
				$endDate = DateTime::createFromFormat('d-m-Y', $startDate->format('t-m-Y'));
			}
			if (isset($this->request->data['next-month'])) {
				$startDate->modify('+1 month');
				$endDate = DateTime::createFromFormat('d-m-Y', $startDate->format('t-m-Y'));
			}
			$this->request->data['Search']['start_date'] = $startDate->format('d-m-Y');
			$this->request->data['Search']['end_date'] = $endDate->format('d-m-Y');

			// start and end date fields are now set as required, set as key in date range dropdown in case it matches one of them
			$this->request->data['Search']['date_range'] = $this->request->data['Search']['start_date'] . '|' . $this->request->data['Search']['end_date'];

			$suppliers = $this->Supplier->find('all', array(
				'fields' => 'Supplier.name, SUM(PurchaseOrder.value) as spend',
				'joins' => array(
					array('table' => 'purchase_orders',
						'alias' => 'PurchaseOrder',
						'type' => 'INNER',
						'conditions' => array(
							'PurchaseOrder.supplier_id = Supplier.id',
						)
					)
				),
				'group' => 'Supplier.name',
				'conditions' => array(
					'PurchaseOrder.start_date >=' => $startDate->format('Y-m-d'),
					'PurchaseOrder.start_date <=' => $endDate->format('Y-m-d'),
				),
				'order' => 'spend DESC'
			));

			$this->set('suppliers', $suppliers);
			$this->set('reportHeading', 'Supplier Spend - ' . (!empty($dateRangeOptions[$this->request->data['Search']['date_range']]) ? $dateRangeOptions[$this->request->data['Search']['date_range']] : $startDate->format('jS F Y') . ' to ' . $endDate->format('jS F Y')));
		}

	}

	function admin_monthly_sales()
	{
		$this->processSearchData($this->name . '.MonthlySales');

		$thisMonth = DateTime::createFromFormat('d-m-Y', '01-' . date('m-Y'));

		// create monthly ranges going back to include earliest invoice as date range dropdown options
		$earliestInvoice = $this->Invoice->find('first', array('order' => 'created ASC'));
		$dateRangeOptions = array();
		do {
			$key = $thisMonth->format('d-m-Y') . '|' . $thisMonth->format('t-m-Y');
			$value = $thisMonth->format('F Y');
			$dateRangeOptions[$key] = $value;

			$thisMonth->modify('-1 month');
		} while (strtotime($thisMonth->format('t-m-Y')) >= strtotime($earliestInvoice['Invoice']['created']));

		$this->set('dateRangeOptions', $dateRangeOptions);

		// if this is first time in page then choose most recent date range by default
		if (!$this->data) {
			$this->request->data = array();
			$this->request->data['Search']['date_range_selected'] = 1;
			$this->request->data['Search']['date_range'] = '01-' . date('m-Y') . '|' . date('t-m-Y');
		}

		// if date range selected then set start and end dates from its key
		if (!empty($this->request->data['Search']['date_range_selected']) && $this->request->data['Search']['date_range']) {
			$dateRangeParts = explode('|', $this->request->data['Search']['date_range']);
			$this->request->data['Search']['start_date'] = $dateRangeParts[0];
			$this->request->data['Search']['end_date'] = $dateRangeParts[1];

		} else {
			// use the dates specified in the date pickers
			if (empty($this->request->data['Search']['start_date'])) {
				$this->request->data['Search']['start_date'] = $earliestInvoice['PurchaseOrder']['date'];
			}
			if (empty($this->request->data['Search']['end_date'])) {
				$this->request->data['Search']['end_date'] = date('d-m-Y');
			}
		}

		// should have the required date range sorted out now...
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		if ($endDate >= $startDate) {
			if (isset($this->request->data['prev-month'])) {
				$startDate->modify('-1 month');
				$endDate = DateTime::createFromFormat('d-m-Y', $startDate->format('t-m-Y'));
			}
			if (isset($this->request->data['next-month'])) {
				$startDate->modify('+1 month');
				$endDate = DateTime::createFromFormat('d-m-Y', $startDate->format('t-m-Y'));
			}
			$this->request->data['Search']['start_date'] = $startDate->format('d-m-Y');
			$this->request->data['Search']['end_date'] = $endDate->format('d-m-Y');

			// start and end date fields are now set as required, set as key in date range dropdown in case it matches one of them
			$this->request->data['Search']['date_range'] = $this->request->data['Search']['start_date'] . '|' . $this->request->data['Search']['end_date'];

			$invoices = $this->Invoice->find('all', array(
				'conditions' => array(
					'Invoice.invoice_date >=' => $startDate->format('Y-m-d'),
					'Invoice.invoice_date <=' => $endDate->format('Y-m-d'),
					'Invoice.invoice_status_id' => 3,
				),
				'contain' => 'Client',
				'order' => 'Invoice.invoice_date ASC'
			));

			$this->set('invoices', $invoices);
			$this->set('reportHeading', 'Sales - ' . (!empty($dateRangeOptions[$this->request->data['Search']['date_range']]) ? $dateRangeOptions[$this->request->data['Search']['date_range']] : $startDate->format('jS F Y') . ' to ' . $endDate->format('jS F Y')));
		}

	}

	/**
	 * @param string $nameOfLastDayOfWeek e.g. "wednesday" if week runs from Thursday to Wednesday
	 * @param string $earliestDateDMY
	 * @return array
	 */
	function _getWeeklyDateRangeOptions($nameOfLastDayOfWeek, $earliestDateDMY)
	{
		// actually including the current week now so using "this" instead of "last"
		$lastDayOfPreviousWeekUnixTime = strtotime("this $nameOfLastDayOfWeek");
		$lastDayOfPreviousWeek = DateTime::createFromFormat('d-m-Y', date('d-m-Y', $lastDayOfPreviousWeekUnixTime));

		$firstDayOfPreviousWeek = clone $lastDayOfPreviousWeek;
		$firstDayOfPreviousWeek->modify('-6 days');

		$dateRangeOptions = array();
		do {
			$key = $firstDayOfPreviousWeek->format('d-m-Y') . '|' . $lastDayOfPreviousWeek->format('d-m-Y');
			$value = $firstDayOfPreviousWeek->format('j M Y') . ' - ' . $lastDayOfPreviousWeek->format('j M Y');
			$dateRangeOptions[$key] = $value;

			$firstDayOfPreviousWeek->modify('-7 days');
			$lastDayOfPreviousWeek->modify('-7 days');
		} while (strtotime($lastDayOfPreviousWeek->format('d-m-Y')) > strtotime($earliestDateDMY));

		return $dateRangeOptions;
	}

	/**
	 * @param $dateRangeOptions
	 * @param $earliestDateDMY
	 */
	function _workOutStartAndEndDatesFromSearchData($dateRangeOptions, $earliestDateDMY)
	{
		// if this is first time in page then choose most recent date range by default
		if (!$this->data) {
			$this->request->data = array();
			$this->request->data['Search']['date_range_selected'] = 1;
			$this->request->data['Search']['date_range'] = array_keys($dateRangeOptions)[0];
		}

		// if date range selected then set start and end dates from its key
		if (!empty($this->request->data['Search']['date_range_selected']) && $this->request->data['Search']['date_range']) {
			$dateRangeParts = explode('|', $this->request->data['Search']['date_range']);
			$this->request->data['Search']['start_date'] = $dateRangeParts[0];
			$this->request->data['Search']['end_date'] = $dateRangeParts[1];

		} else {
			// otherwise use the dates specified in the date pickers
			if (empty($this->request->data['Search']['start_date'])) {
				$this->request->data['Search']['start_date'] = $earliestDateDMY;
			}
			if (empty($this->request->data['Search']['end_date'])) {
				$this->request->data['Search']['end_date'] = date('d-m-Y');
			}
		}

		// should have the appropriate date range now
		$startDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['start_date']);
		$endDate = DateTime::createFromFormat('d-m-Y', $this->request->data['Search']['end_date']);

		// make adjustment to start & end date if "+ week" or "- week" button was clicked
		if (isset($this->request->data['prev-week'])) {
			$startDate->modify('-1 week');
			$endDate->modify('-1 week');
		}
		if (isset($this->request->data['next-week'])) {
			$startDate->modify('+1 week');
			$endDate->modify('+1 week');
		}
		$this->request->data['Search']['start_date'] = $startDate->format('d-m-Y');
		$this->request->data['Search']['end_date'] = $endDate->format('d-m-Y');

		// start and end date fields are now set as required - in case individual start or end date field was modified
		// then will set them as the key in the date range dropdown (if they happen to match one of the ranges in the
		// dropdown then it will be automatically selected)
		$this->request->data['Search']['date_range'] = $this->request->data['Search']['start_date'] . '|' . $this->request->data['Search']['end_date'];
	}

}
