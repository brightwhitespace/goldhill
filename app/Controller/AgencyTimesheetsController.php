<?php
class AgencyTimesheetsController extends AppController {

	var $name = 'AgencyTimesheets';
	var $layout = 'admin';
	var $uses = array('Supplier','AgencyTimesheet', 'AgencyEmployee');

	function admin_add($projectId = null)
	{
		$this->set('title_for_layout','Agency Timesheets - Add New Agency Timesheet');

		$project = $this->AgencyTimesheet->Project->findById($projectId);
		if (!$project) {
			$this->Session->setFlash(__('Invalid project given'));
			$this->redirect($this->referer());
		}

		$returnUrl = '/admin/projects/view/'.$projectId;
		$this->set('returnUrl', $returnUrl);

		if (!empty($this->request->data)) {
			$this->AgencyTimesheet->create();
			$this->request->data['AgencyTimesheet']['project_id'] = $projectId;

			// Check for new employee(s) being added
			foreach ($this->request->data['AgencyEmployeesAgencyTimesheet'] as $idx => $timesheetEntry) {
				if ($timesheetEntry['agency_employee_id'] == 'add-new-employee') {
					$employee = $this->AgencyEmployee->getOrCreateEmployee($timesheetEntry['supplier_id'], $timesheetEntry['new_employee_name'], $timesheetEntry['rate_ph']);
					if ($employee) {
						// have found/created employee so insert their id as the selected employee
						$this->request->data['AgencyEmployeesAgencyTimesheet'][$idx]['agency_employee_id'] = $employee['AgencyEmployee']['id'];
					} else {
						// could not find/create employee so default to empty and fail validation
						$this->request->data['AgencyEmployeesAgencyTimesheet'][$idx]['agency_employee_id'] = '';
					}
				}
			}

			// check date does not already exist for this project
			if ($this->AgencyTimesheet->findByProjectIdAndDate($projectId, date('Y-m-d',strtotime($this->request->data['AgencyTimesheet']['date'])))) {
				$this->AgencyTimesheet->invalidate('date', 'Agency Timesheet with this date already exists');
			} else if ($this->AgencyTimesheet->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The timesheet has been saved'));
				$this->redirect($returnUrl);
			}
			$this->Session->setFlash(__('The timesheet could not be saved. Please try again.'));
		} else {
			// first time in so need to add blank timesheet entry ready to be populated
			$this->request->data['AgencyEmployeesAgencyTimesheet'][0] = array('agency_employee_id'=>'','break_hours'=>'1');

			// also populate new timesheet date as next available day based on latest timesheet
			if ($latestAgencyTimesheet = array_pop($this->AgencyTimesheet->getAll($projectId))) {
				$this->request->data['AgencyTimesheet'] = array('date' => date('j-n-Y', 86400 + strtotime($latestAgencyTimesheet['AgencyTimesheet']['date'])));
			} else {
				$this->request->data['AgencyTimesheet'] = array('date' => date('j-n-Y'));
			};

		}

		$this->request->data['Project'] = $project['Project'];

		$timeIntervalInMinutes = $this->_getSetting('timesheet_time_interval_in_minutes', 15);
		$this->set('startTimes', AppModel::getListOfTimes('00:00', '23:59', $timeIntervalInMinutes));
		$this->set('endTimes', AppModel::getListOfTimes('00:'.$timeIntervalInMinutes, '24:00', $timeIntervalInMinutes));

		$this->set('agencies', $this->Supplier->getSuppliers('list', 1));
		$this->set('employees', $this->AgencyEmployee->getAll(null, false));   // just current employees
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Agency Timesheets - Edit Agency Timesheet');

		$timesheet = $this->AgencyTimesheet->get($id);
		if (!$timesheet) {
			$this->Session->setFlash(__('Invalid timesheet'));
			$this->redirect($this->referer());
		}
		$this->set('timesheet', $timesheet);

		$returnUrl = '/admin/projects/view/'.$timesheet['AgencyTimesheet']['project_id'];
		$this->set('returnUrl', $returnUrl);

		if (!empty($this->request->data)) {

			// Check for new employee(s) being added
			foreach ($this->request->data['AgencyEmployeesAgencyTimesheet'] as $idx => $timesheetEntry) {
				if ($timesheetEntry['agency_employee_id'] == 'add-new-employee') {
					$employee = $this->AgencyEmployee->getOrCreateEmployee($timesheetEntry['supplier_id'], $timesheetEntry['new_employee_name'], $timesheetEntry['rate_ph']);
					if ($employee) {
						// have found/created employee so insert their id as the selected employee
						$this->request->data['AgencyEmployeesAgencyTimesheet'][$idx]['agency_employee_id'] = $employee['AgencyEmployee']['id'];
					} else {
						// could not find/create employee so default to empty and fail validation
						$this->request->data['AgencyEmployeesAgencyTimesheet'][$idx]['agency_employee_id'] = '';
					}
				}
			}

			if ($this->AgencyTimesheet->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The timesheet has been saved'));
				$this->redirect($returnUrl);
			} else {
				$this->Session->setFlash(__('The timesheet could not be saved. Please try again.'));
			}
		} else {
			$this->request->data = $timesheet;
		}

		$timeIntervalInMinutes = $this->_getSetting('timesheet_time_interval_in_minutes', 15);
		$this->set('startTimes', AppModel::getListOfTimes('00:00', '23:59', $timeIntervalInMinutes));
		$this->set('endTimes', AppModel::getListOfTimes('00:'.$timeIntervalInMinutes, '24:00', $timeIntervalInMinutes));

		$this->set('agencies', $this->Supplier->getSuppliers('list', 1));
		$this->set('employees', $this->AgencyEmployee->getAll(null, false));   // just current employees
		$this->set('allEmployees', $this->AgencyEmployee->getAll(null, true)); // include archived in case selected employee has since been archived
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for timesheet'));
		} else if ($this->AgencyTimesheet->delete($id)) {
			$this->Session->setFlash(__('Agency Timesheet deleted'));
		} else {
			$this->Session->setFlash(__('Agency Timesheet was not deleted'));
		}

		$this->redirect($this->referer());
	}
	
	
}
