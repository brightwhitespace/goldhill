<?php
class QuotesController extends AppController {

	var $name = 'Quotes';
	var $layout = 'admin';


	function admin_index($archive = 0)
	{
		$this->processSearchData();

		$conditions = array('Quote.archived' => $archive);

		if ($this->data) {
			$conditions['or'] = array(
				'Client.name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.contact_name LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.contact_tel LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.address1 LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.address2 LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.address3 LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.city LIKE' => '%'.$this->data['Search']['term'].'%',
				'Quote.postcode LIKE' => '%'.$this->data['Search']['term'].'%',
			);
		}

		// if viewing archive then order by latest: due date first, then site visit date, then created date
		// (default order is earliest: site visit date, then due date, then created date)
		$order = $archive ? 'Quote.completed ASC, IFNULL(Quote.quote_due, IFNULL(Quote.site_visit_date, Quote.created)) DESC' : null;

		$this->paginate = array('conditions' => $conditions, 'contain' => 'Client', 'order' => $order);
		$this->Quote->recursive = 0;
		$this->set('quotes', $this->paginate());
		$this->set('archive', $archive);
	}

	function admin_add()
	{
		$this->set('title_for_layout','Quotes - Add New Quote');
		if (!empty($this->request->data)) {
			$this->Quote->create();
			
			if ($this->Quote->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The quote has been saved'));
				if (isset($this->request->data['save_and_remain'])) {
					$this->redirect('/admin/quotes/edit/' . $this->Quote->id);
				} else {
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('The quote could not be saved. Please try again.'));
			}
		}

		$this->set('clients', $this->Quote->Client->getClients('list'));
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Quotes - Edit Quote');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid quote'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Quote->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The quote has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quote could not be saved. Please try again.'));
			}
		} else {
			$this->request->data = $this->Quote->findById($id);
		}

		$this->set('clients', $this->Quote->Client->getClients('list'));
		$this->set('files', glob(APP . "/tmp/quote/$id/*"));
	}

	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for quote'));
			$this->redirect(array('action'=>'index'));
		}

		$quote = $this->Quote->get($id);

		$this->set('quote', $quote);
		$this->set('files', glob(APP . "tmp/quote/$id/*"));
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for quote'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Quote->delete($id)) {
			$this->Session->setFlash(__('Quote deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Quote was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	function admin_archive($id, $archive = true)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for quote'));
			$this->redirect($this->referer());
		}

		$this->Quote->id = $id;
		$this->Quote->saveField('archived', $archive);
		$this->Session->setFlash(__('Quote was ' . ($archive ? 'archived' : 'restored from archive')));

		$this->redirect($this->referer());
	}

}
