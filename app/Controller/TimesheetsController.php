<?php
class TimesheetsController extends AppController {

	var $name = 'Timesheets';
	var $layout = 'admin';
	var $uses = array('Timesheet', 'Employee');

	function admin_add($projectId = null)
	{
		$this->set('title_for_layout','Timesheets - Add New Timesheet');

		$project = $this->Timesheet->Project->findById($projectId);
		if (!$project) {
			$this->Session->setFlash(__('Invalid project given'));
			$this->redirect($this->referer());
		}

		$returnUrl = '/admin/projects/view/'.$projectId;
		$this->set('returnUrl', $returnUrl);

		if (!empty($this->request->data)) {
			$this->Timesheet->create();
			$this->request->data['Timesheet']['project_id'] = $projectId;

			// check date does not already exist for this project
			if ($this->Timesheet->findByProjectIdAndDate($projectId, date('Y-m-d',strtotime($this->request->data['Timesheet']['date'])))) {
				$this->Timesheet->invalidate('date', 'Timesheet with this date already exists');
			} else if ($this->Timesheet->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The timesheet has been saved'));
				$this->redirect($returnUrl);
			}
			$this->Session->setFlash(__('The timesheet could not be saved. Please try again.'));
		} else {
			// first time in so need to add blank timesheet entry ready to be populated
			$this->request->data['EmployeesTimesheet'][0] = array('employee_id'=>'','break_hours'=>'1');

			// also populate new timesheet date as next available day based on latest timesheet
			if ($latestTimesheet = array_pop($this->Timesheet->getAll($projectId))) {
				$this->request->data['Timesheet'] = array('date' => date('j-n-Y', 86400 + strtotime($latestTimesheet['Timesheet']['date'])));
			} else {
				$this->request->data['Timesheet'] = array('date' => date('j-n-Y'));
			};

		}

		$this->request->data['Project'] = $project['Project'];

		$timeIntervalInMinutes = $this->_getSetting('timesheet_time_interval_in_minutes', 15);
		$this->set('startTimes', AppModel::getListOfTimes('00:00', '23:59', $timeIntervalInMinutes));
		$this->set('endTimes', AppModel::getListOfTimes('00:'.$timeIntervalInMinutes, '24:00', $timeIntervalInMinutes));

		$this->set('employees', $this->Employee->getEmployees('all', false));
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Timesheets - Edit Timesheet');

		$timesheet = $this->Timesheet->get($id);
		if (!$timesheet) {
			$this->Session->setFlash(__('Invalid timesheet'));
			$this->redirect($this->referer());
		}
		$this->set('timesheet', $timesheet);

		$returnUrl = '/admin/projects/view/'.$timesheet['Timesheet']['project_id'];
		$this->set('returnUrl', $returnUrl);

		if (!empty($this->request->data)) {
			if ($this->Timesheet->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The timesheet has been saved'));
				$this->redirect($returnUrl);
			} else {
				$this->Session->setFlash(__('The timesheet could not be saved. Please try again.'));
			}
		} else {
			$this->request->data = $timesheet;
		}

		$timeIntervalInMinutes = $this->_getSetting('timesheet_time_interval_in_minutes', 15);
		$this->set('startTimes', AppModel::getListOfTimes('00:00', '23:59', $timeIntervalInMinutes));
		$this->set('endTimes', AppModel::getListOfTimes('00:'.$timeIntervalInMinutes, '24:00', $timeIntervalInMinutes));

		$this->set('employees', $this->Employee->getEmployees('all', false));
		$this->set('allEmployees', $this->Employee->getEmployees('all', true));  // allEmployees includes archived in case old employee was previously selected
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for timesheet'));
		} else if ($this->Timesheet->delete($id)) {
			$this->Session->setFlash(__('Timesheet deleted'));
		} else {
			$this->Session->setFlash(__('Timesheet was not deleted'));
		}

		$this->redirect($this->referer());
	}
	
	
}
