<?php

class InvoicesController extends AppController {

	var $name = 'Invoices';
	var $layout = 'admin';
	var $uses = array('Invoice','Storage','Project');
	var $components = array('Mpdf','Email');

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			if (!empty($this->data['Search']['invoice_status_id'])) {
				$conditions['invoice_status_id'] = $this->data['Search']['invoice_status_id'];
			}
			if (!empty($this->data['Search']['term'])) {
				$conditions['or'] = array(
					'Invoice.invoice_num LIKE' => '%'.$this->data['Search']['term'].'%',
					'Invoice.ref LIKE' => '%'.$this->data['Search']['term'].'%',
					'Project.name LIKE' => '%'.$this->data['Search']['term'].'%',
					'Client.name LIKE' => '%'.$this->data['Search']['term'].'%',
				);
			}
		}

		// if no status chosen then hide Draft invoices by default
		if (empty($this->data['Search']['invoice_status_id'])) {
			$conditions['invoice_status_id !='] = Invoice::STATUS_DRAFT;
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'contain' => array(
				'Client',
				'InvoiceStatus',
				'Project' => array(
					'PurchaseOrder',
					'Labour',
					'Timesheet',
				)
			),
			'order' => array(
				'Invoice.invoice_num' => 'desc'
			)
		);
		$this->Invoice->recursive = 0;
		$this->set('invoices', $this->paginate());
	}

	function admin_add($idToDuplicate = null)
	{
		$this->set('title_for_layout','Invoices - Add New Invoice');

		$projectSearchConditions = array('Project.archived' => 0);

		if (!empty($this->request->data)) {
			$this->Invoice->create();
			$this->request->data['Invoice']['invoice_num'] = 'DRAFT';
			$this->request->data['Invoice']['invoice_status_id'] = Invoice::STATUS_DRAFT;
			$this->request->data['Invoice']['total'] = AppModel::sumNestedValuesInArray($this->request->data['InvoiceItem'], 'cost');

			if ($this->Invoice->saveAll($this->request->data)) {
				if (!empty($this->request->data['preview'])) {
					$this->redirect(array('action' => 'view', $this->Invoice->id, 'preview'));
				} else {
					$this->Session->setFlash(__('The invoice has been saved'));
					$this->redirect($this->getReturnUrl());
				}
			} else {
				$this->Session->setFlash(__('The invoice could not be saved. Please try again.'));
			}
		} else {
			// first time into page, if duplicating an invoice then load it into data...
			if ($idToDuplicate) {
				$invoice = $this->Invoice->getInvoice($idToDuplicate);
				$this->request->data['Invoice']['project_id'] = $invoice['Invoice']['project_id'];
				$this->request->data['Invoice']['invoice_date'] = $invoice['Invoice']['invoice_date'];
				$this->request->data['Invoice']['ref'] = $invoice['Invoice']['ref'];
				$this->request->data['Invoice']['gross_status_text'] = $invoice['Invoice']['gross_status_text'];
				$this->request->data['Invoice']['footer_main_text'] = $invoice['Invoice']['footer_main_text'];
				$this->request->data['Invoice']['footer_sub_text'] = $invoice['Invoice']['footer_sub_text'];
				$this->request->data['Invoice']['banking_details'] = $invoice['Invoice']['banking_details'];
				foreach ($invoice['InvoiceItem'] as $idx => $invoiceItem) {
					$this->request->data['InvoiceItem'][$idx] = array(
						'date' => $invoiceItem['date'],
						'description' => $invoiceItem['description'],
						'cost' => $invoiceItem['cost'],
					);
				}
			} else {
				// ...otherwise need to add blank invoice item ready to be populated
				$this->request->data['InvoiceItem'][0] = array('date'=>'', 'description'=>'', 'cost'=>'');
				$this->request->data['Invoice'] = array(
					'invoice_date' => date('j-n-Y'),
					'gross_status_text' => $this->_getSetting('invoice_gross_status_text_default', ''),
					'footer_main_text' => $this->_getSetting('invoice_footer_main_text_default', ''),
					'footer_sub_text' => $this->_getSetting('invoice_footer_sub_text_default', ''),
					'banking_details' => $this->_getSetting('invoice_banking_details_default', '')
				);
			}

			$this->setReturnUrl($this->referer(), true);

			// check for project_id being passed in as named parameter - use it to preselect project in dropdown
			if (!empty($this->request->params['named']['project_id'])) {
				$this->request->data['Invoice']['project_id'] = $this->request->params['named']['project_id'];

				// make sure passed-in project is included in selectable project list (in case it is archived)
				$projectSearchConditions = array('or' => array($projectSearchConditions, 'Project.id = ' . $this->request->params['named']['project_id']));
			}
		}

		$this->set('returnUrl',$this->readReturnUrl());

		$projectId = !empty($this->request->data['Invoice']['project_id']) ? $this->request->data['Invoice']['project_id'] : 0;
		$projectSearchConditions = array('or' => array('Project.archived' => 0, 'Project.id ' => $projectId));
		$this->set('projects',$this->Invoice->Project->find('list',array('order'=>'Project.name','conditions'=>$projectSearchConditions)));

		$this->set('projectIdToClientIdMappings',json_encode($this->Invoice->Project->find('list',array('fields'=>array('Project.id', 'Project.client_id'),'conditions'=>$projectSearchConditions))));

		$this->set('clients',$this->Invoice->Client->find('list',array('order'=>array('Client.name'))));
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Invoices - Edit Invoice');

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid invoice'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			$this->request->data['Invoice']['total'] = AppModel::sumNestedValuesInArray($this->request->data['InvoiceItem'], 'cost');

			if (!empty($this->request->data['Invoice']['toggle_sent_status'])) {
				$invoice = $this->Invoice->getInvoice($id);
				$this->request->data['Invoice']['email_sent'] = $invoice['Invoice']['email_sent'] ? null : date('Y-m-d H:i:s');
			}

			if ($this->Invoice->InvoiceItem->deleteAll(array('InvoiceItem.invoice_id' => $id)) && $this->Invoice->saveAll($this->request->data)) {
				if (!empty($this->request->data['preview'])) {
					$this->redirect(array('action' => 'view', $this->request->data['Invoice']['id'], 'preview'));
				} else {
					$this->Session->setFlash(__('The invoice has been saved'));
					$this->redirect($this->getReturnUrl());
				}
			} else {
				$this->Session->setFlash(__('The invoice could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			// only set return url to referring page if have not come from the invoice view page
			if (strpos($this->referer(), '/invoices/view/') === false) {
				$this->setReturnUrl($this->referer(), true);
			} else {
				$this->setReturnUrl('/admin/invoices');
			}
			$this->request->data = $this->Invoice->getInvoice($id);
		}

		$this->set('returnUrl',$this->readReturnUrl());

		$projectId = !empty($this->request->data['Invoice']['project_id']) ? $this->request->data['Invoice']['project_id'] : 0;
		$projectSearchConditions = array('or' => array('Project.archived' => 0, 'Project.id ' => $projectId));
		$this->set('projects',$this->Invoice->Project->find('list',array('order'=>'Project.name','conditions'=>$projectSearchConditions)));

		$this->set('projectIdToClientIdMappings',json_encode($this->Invoice->Project->find('list',array('fields'=>array('Project.id', 'Project.client_id'),'conditions'=>$projectSearchConditions))));

		$this->set('clients',$this->Invoice->Client->find('list',array('order'=>array('Client.name'))));
	}

	function admin_view($id = null, $action = null, $newInvoiceStatusId = null)
	{
		$this->layout = 'ajax';

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for invoice'));
			$this->redirect(array('action'=>'index'));
		}

		$this->set('action', $action);

		// If approve flag is set then change status and set next invoice number in invoice
		if ($newInvoiceStatusId) {
			$this->_setInvoiceStatus($id, $newInvoiceStatusId);
		}

		// only set return url to referring page if have not come from the invoice view page
		if (strpos($this->referer(), '/invoices/view/') === false) {
			$this->setReturnUrl($this->referer(), true);
		}
		$this->set('returnUrl',$this->readReturnUrl());

		$invoice = $this->Invoice->getInvoice($id);

		$this->set('invoice',$invoice);
	}

	function _setInvoiceStatus($id, $newInvoiceStatusId)
	{
		$invoice = $this->Invoice->getInvoice($id);

		if ($invoice && $invoice['Invoice']['invoice_status_id'] != $newInvoiceStatusId) {

			$invoice['Invoice']['invoice_status_id'] = $newInvoiceStatusId;

			$updatingInvoiceNumber = ($newInvoiceStatusId == Invoice::STATUS_APPROVED) && ($invoice['Invoice']['invoice_num'] == 'DRAFT' || $invoice['Invoice']['invoice_num'] == '');
			if ($updatingInvoiceNumber) {
				$invoice['Invoice']['invoice_num'] = $this->Storage->getNumber('INV');
			}

			// if Approving then set the invoice date to today's date
			if ($newInvoiceStatusId == Invoice::STATUS_APPROVED) {
				$invoice['Invoice']['invoice_date'] = date('Y-m-d');
			}

			if ($this->Invoice->save($invoice) && $updatingInvoiceNumber) {
				$this->Storage->updateNumber('INV');
			}

		}

	}

	function admin_duplicate($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for invoice'));
            $this->redirect(array('action'=>'index'));
        }
        $invoice = $this->Invoice->getInvoice($id);
        $this->request->data = $invoice;
        foreach($this->request->data['InvoiceItem'] as $key => $inv){
            unset($this->request->data['InvoiceItem'][$key]['id']);
            unset($this->request->data['InvoiceItem'][$key]['invoice_id']);
        }
        unset($this->request->data['Invoice']['id']);

        $this->Invoice->create();
        $this->request->data['Invoice']['invoice_num'] = $this->Storage->getNumber('INV');

        if ($this->Invoice->saveAll($this->request->data)) {
            $this->Storage->updateNumber('INV');
            $this->redirect(array('action' => 'edit', $this->Invoice->id));

        } else {
            $this->Session->setFlash(__('The invoice could not be saved. Please try again.'));
        }

    }

	/**
	 * @param null $id
	 * @param string $action download|save
	 */
	function admin_generateInvoice($id = null, $action = 'download')
	{
		$this->layout = 'pdf_letter';

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for invoice'));
			$this->redirect(array('action'=>'index'));
		}

		$invoice = $this->Invoice->getInvoice($id);
		$this->set('invoice',$invoice);

		// check requested action...
		if ($action == 'download') {
			// just download the pdf with simple filename (i.e. not full path)
			$this->Mpdf->init();
			$this->Mpdf->setFilename($this->_getInvoiceFilename($invoice));
			$this->Mpdf->setOutput('D');

		} else if ($action == 'save') {
			// save the pdf with full filename (i.e. including path)
			$this->Mpdf->init();
			$this->Mpdf->setFilename($this->_getInvoiceFilename($invoice, true));
			$this->Mpdf->setOutput('F');

		} else if ($action) {
			$this->Invoice->id = $id;
			$this->Invoice->saveField('invoice_printed', 1);

			// view the pdf
			$this->Mpdf->init();
			$this->Mpdf->setOutput('I');

		} else {
			$this->redirect('/admin/invoices');
		}
	}

	/**
	 * @param null $id
	 */
	function admin_sendInvoice($id = null)
	{
		App::uses('CakeEmail', 'Network/Email');

		if (!$id || !($invoice = $this->Invoice->getInvoice($id))) {
			$this->Session->setFlash(__('Invalid id for invoice'));
			$this->redirect(array('action'=>'index'));
		}

		if ($invoice['Invoice']['invoice_status_id'] == Invoice::STATUS_DRAFT) {
			$this->Session->setFlash(__('Invoice must be approved first'));
			$this->redirect(array('action'=>'index'));
		}

		// get billing email for Client
		$clientBillingEmail = $_SERVER['SERVER_PORT'] == '8888' ? 'andrew+goldhill@brightwhitespace.co.uk' : $invoice['Client']['billing_email'];

		// include saved invoice file in attachments
		$attachments = array($this->_getInvoiceFilename($invoice, true));
		$fileErrorMessage = !file_exists($attachments[0]) ? __('Error: Invoice file does not exist. ') : '';

		// also attach project timesheet report if required by client AND project is type Day Works
		if ($invoice['Client']['attach_project_timesheet_with_invoice']
				&& !empty($invoice['Project']['id']) && $invoice['Project']['project_type_id'] == Project::TYPE_DAYWORKS) {
			$attachments[] = $this->_getProjectTimesheetFilename($invoice['Project']['id'], $invoice['Project']['name'], true);
			$fileErrorMessage .= !file_exists($attachments[1]) ? __('Error: Project Timesheet file does not exist.') : '';

			if (!$fileErrorMessage) {
				$filesToMerge = array(
					array('filename' => $attachments[0]),
					array('filename' => $attachments[1], 'orientation' => 'landscape'),
				);
				$mergedFilename = str_replace('.pdf', '_and_timesheet.pdf', $attachments[0]);

				$this->_mergePDFFiles($filesToMerge, $mergedFilename);

				$attachments = array($mergedFilename);
			}
		}

		if ($fileErrorMessage) {
			$this->Session->setFlash($fileErrorMessage, 'flash_failure');

		} else if (!$clientBillingEmail) {
			$this->Session->setFlash(__('This client does not have a billing email address set up'), 'flash_failure');

		} else if ($_SERVER['SERVER_PORT'] == '8888' && strpos($clientBillingEmail, 'brightwhitespace') === false) {
			// this is a dev system and it wants to send email to a non-brightwhitespace account - this is probably bad
			$this->Session->setFlash(__('Failed as wanted to send invoice to non-brightwhitespace email from dev system!'), 'flash_failure');

		} else {
			$email = new CakeEmail();
			$cc = $_SERVER['SERVER_PORT'] == '8888' ? array() : 'james@goldhillcontracting.co.uk';
			$email->config('smtp')
				->template('invoice_with_pdf', 'default') //I'm assuming these were created
				->emailFormat('html')
				->to($clientBillingEmail)
				->cc($cc)
				->bcc('andrew+goldhillbcc@brightwhitespace.co.uk')
				->from(array('purchaseorders@goldhillcontracting.com' => 'Goldhill Contracting'))
				->replyTo('james@goldhillcontracting.co.uk')
				->subject('Goldhill Invoice ' . $invoice['Invoice']['invoice_num'] . ' ref: ' . $invoice['Invoice']['ref'])
				->viewVars(array(
					'invoice' => $invoice,
					'billingName' => $invoice['Client']['billing_name'],
					'url' => $this->settings['site_url']
				))
				->attachments($attachments);

			if (!$email->send()) {
				$this->Session->setFlash(__('Error: could not send the invoice. Please check the contact for this client/project has a valid email address.'), 'flash_failure');
			} else {
				// Invoice sent so update it to show this...
				$this->Invoice->id = $id;
				$this->Invoice->saveField('invoice_status_id', Invoice::STATUS_SENT);
					$this->Invoice->saveField('email_sent', date('Y-m-d H:i:s'));
					$this->Session->setFlash(__('Invoice has been emailed to client'), 'flash_success');

				// ...and also archive the project if there is one (project status will be automatically set to COMPLETED by AppController->_updateProjectStatuses on page refresh)
				if ($invoice['Invoice']['project_id']) {
					$this->Project->id = $invoice['Invoice']['project_id'];
					$this->Project->saveField('archived', 1);
				}
			}
		}

		$this->redirect('/admin/invoices/view/'.$invoice['Invoice']['id']);
	}

	function _getInvoiceFilename($invoice, $fullpath = false)
	{
		$folder = APP . 'tmp' . DS . 'invoices';
		if (!file_exists($folder)) {
			mkdir($folder, 0777, true);
		}
		$filename = strtolower(Inflector::slug('goldhill_invoice_' . $invoice['Invoice']['invoice_num'] . '_' . $invoice['Invoice']['invoice_date'] . '_' . $invoice['Client']['name']) . '.pdf');

		return ($fullpath ? $folder . DS : '') . $filename;
	}

	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for invoice'));
		} else if ($this->Invoice->delete($id)) {
			$this->Session->setFlash(__('Invoice deleted'));
		} else {
			$this->Session->setFlash(__('Invoice was not deleted'));
		}

		$this->redirect($this->referer());
	}

	function admin_setStatus($id = null, $statusId = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for invoice'));
		}
		else if (!in_array($statusId, array_keys(Invoice::getStatuses()))) {
			$this->Session->setFlash(__('Invalid status id'));
		} else {
			$this->_setInvoiceStatus($id, $statusId);
			$this->Session->setFlash(__('Invoice status set to ') . ucwords(Invoice::getStatuses()[$statusId]));
		}

		$this->redirect($this->referer());
	}


}
