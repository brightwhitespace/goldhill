<?php
class SuppliersController extends AppController {

	var $name = 'Suppliers';
	var $layout = 'admin';
	var $uses = array('Supplier', 'AgencyEmployee');

	function admin_index()
	{
		$this->processSearchData();

		$conditions = array();
		if ($this->data) {
			$conditions['name LIKE'] = '%'.$this->data['Search']['term'].'%';
		}

		$this->paginate = array('conditions' => $conditions);
		$this->Supplier->recursive = 0;
		$this->set('suppliers', $this->paginate());
	}

	function admin_add()
	{
		$this->set('title_for_layout','Suppliers - Add New Supplier');
		if (!empty($this->request->data)) {
			$this->Supplier->create();
			
			if ($this->Supplier->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The supplier has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The supplier could not be saved. Please try again.'));
			}
		}
		
	}

	function admin_edit($id = null)
	{
		$this->set('title_for_layout','Suppliers - Edit Supplier');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid supplier'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			
			if ($this->Supplier->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The supplier has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The supplier could not be saved. Please try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Supplier->getSupplier($id);
		}
		
	}
	
	function admin_view($id = null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for supplier'));
			$this->redirect(array('action'=>'index'));
		}
		
		$supplier = $this->Supplier->getSupplier($id);
		
		$this->set('supplier',$supplier);
		
		$purchaseOrders = $this->Supplier->PurchaseOrder->getLatest(null,$id);		
		$this->set('purchaseOrders',$purchaseOrders);
		$this->set('agencyEmployees', $this->AgencyEmployee->getAll($id, false));
		$this->set('files', glob(APP . "/tmp/supplier/$id/*"));
	}

	function admin_delete($id = null)
	{
		$this->Session->setFlash(__('Not deleted - should be archived in case has associated records'), 'flash_failure');
		$this->redirect(array('action'=>'index'));
		/*
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for supplier'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Supplier->delete($id)) {
			$this->Session->setFlash(__('Supplier deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Supplier was not deleted'));
		$this->redirect(array('action' => 'index'));
		*/
	}
	
	function admin_contacts($id, $selectedContactId = 0){
		$this->layout = 'ajax';
		
		$options = array();
		$options['conditions'] = array(
			'Contact.supplier_id' => $id
		);
		$options['order'] = array(
			'Contact.name'
		);
        $options['fields'] = array(
            'Contact.id',
            'Contact.name',
            'Contact.tel'
        );

        $contacts = $this->Supplier->Contact->find('all',$options);

        $list = array();
        foreach($contacts as $contact){
            $list[$contact['Contact']['id']] = $contact['Contact']['name'];
            $list[$contact['Contact']['id']] .= !empty($contact['Contact']['tel']) ? ' - '.$contact['Contact']['tel'] : '';
        }

		$this->request->data['PurchaseOrder']['contact_id'] = $selectedContactId;
		
		$this->set('contacts',$list);
	}

	function admin_addEmployee()
	{
		$this->set('title_for_layout','Employees - Add New Agency Employee');

		if (empty($this->request->params['named']['supplier_id'])) {  // redirect if supplier id not set in URL
			$this->redirect($this->referer());
		}

		if (!empty($this->request->data)) {
			$this->request->data['AgencyEmployee']['supplier_id'] = $this->request->params['named']['supplier_id'];

			$this->AgencyEmployee->create();
			if ($this->AgencyEmployee->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The employee has been saved'));
				$this->redirect('/admin/suppliers/view/' . $this->request->params['named']['supplier_id']);
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please try again.'));
			}
		} else {
			$this->setReturnUrl($this->referer());
			$this->request->data['AgencyEmployee']['supplier_id'] = $this->request->params['named']['supplier_id'];
		}

		$this->set('returnUrl', $this->readReturnUrl());
		$this->set('defaultAgencyEmployeeRatePH', $this->_getSetting('default_agency_employee_rate_ph', '10.00'));
	}

	function admin_editEmployee($id = null)
	{
		$this->set('title_for_layout','AgencyEmployees - Edit Agency Employee');
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid employee'));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {

			if ($this->AgencyEmployee->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The employee has been saved'));
				$this->redirect($this->getReturnUrl());
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please try again.'));
			}

		} else {
			$this->setReturnUrl($this->referer());
			$this->request->data = $this->AgencyEmployee->get($id);
		}

		$this->set('returnUrl', $this->readReturnUrl());
	}


	function admin_archiveEmployee($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for employee'));
			$this->redirect($this->referer());
		}

		$this->AgencyEmployee->id = $id;
		if ($this->AgencyEmployee->saveField('archived', true)) {
			$this->Session->setFlash(__('Employee archived'));
		} else {
			$this->Session->setFlash(__('Employee was not archived'), 'flash_failure');
		}

		$this->redirect($this->referer());
	}
}
